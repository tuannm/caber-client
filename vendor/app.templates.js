angular.module("appTemplates", []).run(["$templateCache", function($templateCache) {   'use strict';

  $templateCache.put('app/partials/dashboard.html',
    "<div class=\"page-dashboard\">\r" +
    "\n" +
    "  <div class=\"app-body padding-10\" ng-class=\"{loading: loading}\">\r" +
    "\n" +
    "    <div ng-cloak ng-repeat='alert in dashAlerts.list track by $index' class=\"bg-{{alert.severity}} panel-heading\" ng-show=\"$last\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <strong ng-bind-html=\"alert.title\">&nbsp;</strong><span ng-bind-html='alert.text'></span>\r" +
    "\n" +
    "      <button class=\"toast-close-button\" ng-click=\"dashAlerts.clear(alert)\">×</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"app-content\">\r" +
    "\n" +
    "      <!--<div ng-cloak ng-view></div>-->\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </div>\r" +
    "\n" +
    "  <!-- Rows -->\r" +
    "\n" +
    "  <div class=\"mobile-row\" ng-controller=\"RowCtrl\" ng-repeat=\"(row_name, row) in dashboard.current.rows\">\r" +
    "\n" +
    "    <div style=\"padding-top:0px\" ng-if=\"!row.collapse\">\r" +
    "\n" +
    "      <!-- Panels -->\r" +
    "\n" +
    "      <div ng-repeat=\"(name, panel) in row.panels|filter:isPanel\" ng-hide=\"panel.hide\" class=\"panel nospace\" ng-model=\"row.panels\" >\r" +
    "\n" +
    "        <!-- Content Panel -->\r" +
    "\n" +
    "        <div style=\"position:relative\">\r" +
    "\n" +
    "          <mobile-panel type=\"panel.type\" ng-cloak></mobile-panel>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div class=\"clearfix\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('app/partials/footer.html',
    "<footer id=\"footer\" class=\"midnight-blue hidden-xs\">\n" +
    "    <div class=\"col-xs-5 footer-copy-right\">\n" +
    "        &copy; 2015 <a href=\"#\">Vaber</a>.<span class=\"hidden-xs\"> All rights reserved.</span>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-7\">\n" +
    "        <ul class=\"pull-right\">\n" +
    "            <li class=\"footer-li\">\n" +
    "                <img class=\"footer-img\" src=\"images/chat1.png\" alt=\"logo\" ng-click=\"openChat()\">\n" +
    "            </li>\n" +
    "            <li data-ng-controller=\"LangCtrl\" ng-init=\"init()\">\n" +
    "                <a data-ng-show=\"lang === 'Vietnam' \" href=\"javascript:;\" data-ng-click=\"setLang('English')\">\n" +
    "                    <div class=\"flag flags-american\"></div>\n" +
    "                </a>\n" +
    "                <a data-ng-show=\"lang == 'English' \" href=\"javascript:;\" data-ng-click=\"setLang('Vietnam')\">\n" +
    "                    <div class=\"flag flags-vietnam\"></div>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</footer><!--/#footer-->\n" +
    "<div class=\"ui-widget ui-chatbox chatbox-support\" ng-show=\"chatBox.isShow\" outline=\"0\">\n" +
    "    <div class=\"ui-widget-header ui-chatbox-titlebar {{ status }} ui-dialog-header\">\n" +
    "        <span ng-click=\"hideChat()\"><i title=\"{{ status }}\"></i><span data-i18n=\"support\"></span></span>\n" +
    "        <a href=\"javascript:;\" ng-click=\"closeChat()\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Close\"\n" +
    "           class=\"ui-chatbox-icon\" role=\"button\">\n" +
    "            <i class=\"fa fa-times\"></i>\n" +
    "        </a>\n" +
    "        <a href=\"javascript:;\" ng-click=\"hideChat()\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Minimize\"\n" +
    "           class=\"ui-chatbox-icon\" role=\"button\">\n" +
    "            <i class=\"fa fa-minus\"></i>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "    <div class=\"false ui-widget-content ui-chatbox-content footer-chatbox-content\" ng-hide=\"chatBox.isHidden\">\n" +
    "        <div class=\"chatboxcontent discussion\" ng-show=\"!user.user_id\">\n" +
    "            <form ng-submit=\"startChat(user_id, user_email)\" class=\"ng-pristine ng-valid  panel-body\">\n" +
    "                <input type=\"text\" ng-model=\"user_id\" data-i18n=\"i18n-placeholder\" placeholder=\"your name\" class=\"form-control input-sm footer-input-text\" />\n" +
    "                <input type=\"email\" ng-model=\"user_email\" data-i18n=\"i18n-placeholder\" placeholder=\"email address\" class=\"form-control input-sm footer-input-text\" />\n" +
    "                <button type=\"submit\" class=\"btn btn-primary btn-sm text-center footer-btn\" data-i18n=\"start chat\"></button>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "        <form ng-submit=\"sendMessage()\" class=\"ng-pristine ng-valid\" ng-show=\"user.user_id\">\n" +
    "            <div id=\"cha4\" class=\"ui-widget-content ui-chatbox-log\" data-slim-scroll realtime-scroll=\"supports\" scroll-height=\"200px\" always-visible=\"true\" scroll-start=\"bottom\">\n" +
    "                <div class=\"ui-chatbox-msg footer-chatbox-msg\" ng-repeat=\"mess in supports\" ng-show=\"chat_id\">\n" +
    "                    <b>{{ user.session_id === mess.sender.user_id ? 'Tôi' : mess.sender.username }}: </b> <span ng-bind-html=\"mess.message\"></span>\n" +
    "                </div>\n" +
    "                <div ng-hide=\"chat_id\">\n" +
    "                    Hiện tại chúng tôi đang không có ở đây, vui lòng quay trở lại sau !!!<br/>\n" +
    "                    <a href=\"javascript:;\" ng-click=\"findSupporter()\">Hoặc thử liên hệ lại</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"has-feedback ui-widget-content ui-chatbox-input\">\n" +
    "                <div class=\"input-group\">\n" +
    "                    <input type=\"text\" class=\"form-control btn-chat-send\" ng-model=\"chatBox.message\" ng-disabled=\"!chat_id\"/>\n" +
    "                    <span class=\"input-group-btn\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary btn-chat-send\" data-i18n=\"send\"></button>\n" +
    "                    </span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/addEducation.html',
    "<!DOCTYPE html>\n" +
    "<!--\n" +
    "To change this license header, choose License Headers in Project Properties.\n" +
    "To change this template file, choose Tools | Templates\n" +
    "and open the template in the editor.\n" +
    "-->\n" +
    "<div class=\"modal-fade modal-lg\" id=\"addEducation\"  ng-backspace>\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"padding-bottom60\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <modal class=\"modal-body\">\n" +
    "                <form id=\"formEducation\" novalidate=\"novalidate\" data-smart-validate-form>\n" +
    "\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"level\">Cấp bậc</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "                                <select ng-model=\"education.degree\"\n" +
    "                                        ng-options=\"degree.name for degree in degrees track by degree.id\" name=\"unit\"\n" +
    "                                        data-smart-validate-input\n" +
    "                                        data-required\n" +
    "                                        class=\"form-control \">\n" +
    "                                </select>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                            <div class=\"col-md-12 no-padding \">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"school\">Tên trường</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                                <input ng-model=\"education.schoolName\"\n" +
    "                                       type=\"text\" name=\"school\"\n" +
    "                                       data-i18n=\"i18n-placeholder\" placeholder=\"your school is\"\n" +
    "                                       class=\"form-control \" maxlength=\"100\"\n" +
    "                                       data-smart-validate-input\n" +
    "                                       data-required\n" +
    "                                       reset-field\n" +
    "                                       data-message-required=\"validate school\"\n" +
    "                                        >\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"classification\">Xếp loại</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                                <input ng-model=\"education.level\" name=\"classification\"\n" +
    "                                       type=\"text\"\n" +
    "                                       data-i18n=\"i18n-placeholder\" placeholder=\"your classification\"\n" +
    "                                       class=\"form-control\"\n" +
    "                                       data-smart-validate-input\n" +
    "                                       reset-field maxlength=\"50\"\n" +
    "                                       data-required\n" +
    "                                       data-message-required=\"validate classification\"\n" +
    "                                        >\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"specialized\">Ngành học</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                                <input ng-model=\"education.ology\" name=\"major\"\n" +
    "                                       type=\"text\"\n" +
    "                                       data-i18n=\"i18n-placeholder\" placeholder=\"your specialized\"\n" +
    "                                       class=\"form-control \"\n" +
    "                                       data-smart-validate-input\n" +
    "                                       reset-field maxlength=\"100\"\n" +
    "                                       data-required\n" +
    "                                       data-message-required=\"validate specialized\"\n" +
    "                                        >\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"start date\">Ngày bắt đầu</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                                <!--<div class=\"input-group\">-->\n" +
    "                                    <!--<input type=\"text\" class=\"form-control\" ng-model=\"education.start_date\"-->\n" +
    "                                           <!--id=\"startDate\"-->\n" +
    "                                           <!--datepicker-popup=\"{{format}}\"-->\n" +
    "                                           <!--data-smart-validate-input-->\n" +
    "                                           <!--is-open=\"openedStartDate\"-->\n" +
    "                                           <!--name=\"openedStartDate\" placeholder=\"dd/mm/yyyy\"-->\n" +
    "                                           <!--ng-click=\"openStartDate($event)\"-->\n" +
    "                                           <!--dateformat=\"Chưa nhập dữ liệu hoặc sai định dạng\"/>-->\n" +
    "                                       <!--<span class=\"input-group-btn\">-->\n" +
    "                                        <!--<button type=\"button\" class=\"btn btn-default\"-->\n" +
    "                                                <!--ng-click=\"openStartDate($event)\"><i-->\n" +
    "                                                <!--class=\"glyphicon glyphicon-calendar\"></i></button>-->\n" +
    "                                      <!--</span>-->\n" +
    "                                <!--</div>-->\n" +
    "                                <div class=\"no-padding\" name=\"openedStartDate\" ng-model=\"education.startDate\" is-open=\"openedStartDate\" dateformat smart-date></div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"end date\">Ngày kết thúc</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 form-group no-padding\">\n" +
    "                                <!--<div class=\"input-group\">-->\n" +
    "                                    <!--<input type=\"text\" class=\"form-control\" ng-model=\"education.end_date\"-->\n" +
    "                                           <!--datepicker-popup=\"{{format}}\"-->\n" +
    "                                           <!--data-smart-validate-input-->\n" +
    "                                           <!--is-open=\"openedEndDate\"-->\n" +
    "                                           <!--name=\"openedEndDate\" placeholder=\"dd/mm/yyyy\"-->\n" +
    "                                           <!--ng-click=\"openedendDate($event)\"-->\n" +
    "                                           <!--dateformat=\"Chưa nhập dữ liệu hoặc sai định dạng\"/>-->\n" +
    "                                       <!--<span class=\"input-group-btn\">-->\n" +
    "                                        <!--<button type=\"button\" class=\"btn btn-default\"-->\n" +
    "                                                <!--ng-click=\"openedendDate($event)\"><i-->\n" +
    "                                                <!--class=\"glyphicon glyphicon-calendar\"></i></button>-->\n" +
    "                                      <!--</span>-->\n" +
    "                                <!--</div>-->\n" +
    "                                <div class=\" no-padding\" ng-model=\"education.endDate\" name=\"openedEndDate\" is-open=\"openedEndDate\" dateformat smart-date></div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12\">\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                                <label for=\"title\"><span\n" +
    "                                        data-i18n=\"achievements\">Thành tích đạt được</span></label>\n" +
    "\n" +
    "                            </div>\n" +
    "                            <div class=\"col-md-12 no-padding\">\n" +
    "                                <textarea\n" +
    "                                        ng-model=\"education.achievement\"\n" +
    "                                        class=\"form-control  padding-bot-proposal\"\n" +
    "                                        data-i18n=\"i18n-placeholder\" placeholder=\"achievements\"\n" +
    "                                        data-smart-validate-input\n" +
    "                                        reset-field maxlength=\"255\"\n" +
    "                                        data-required=\"Xin hãy nhập nội dung yêu cầu.\"\n" +
    "                                        data-message=\"Xin hãy nhập nội dung yêu cầu.\"\n" +
    "                                        rows=\"3\"\n" +
    "                                        data-message-required=\"\"></textarea>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </form>\n" +
    "\n" +
    "            </modal>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12 \">\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "\n" +
    "\n" +
    "                    <a class=\"btn btn-sm btn-primary pull-right btn-gap\" data-ng-click=\"ok()\" data-i18n=\"confirm\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary pull-right btn-gap\" data-ng-click=\"cancel()\"\n" +
    "                       data-i18n=\"cancel\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/addExperience.html',
    "<!DOCTYPE html>\n" +
    "<!--\n" +
    "To change this license header, choose License Headers in Project Properties.\n" +
    "To change this template file, choose Tools | Templates\n" +
    "and open the template in the editor.\n" +
    "-->\n" +
    "<div class=\"modal-fade modal-lg\" id=\"addExperience\" ng-backspace>\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\" padding-bottom60\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" style=\"margin-top: -2px!important;\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <form id=\"formExperience\" novalidate=\"novalidate\" data-smart-validate-form>\n" +
    "\n" +
    "\n" +
    "\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                        <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                            <label for=\"title\"><span\n" +
    "                                    data-i18n=\"position\">Vị trí đảm nhiệm</span></label>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                            <input ng-model=\"experience.position\" name=\"position\"\n" +
    "                                   type=\"text\"\n" +
    "                                   data-i18n=\"i18n-placeholder\" placeholder=\"position\"\n" +
    "                                   class=\"form-control \" maxlength=\"100\"\n" +
    "                                   data-smart-validate-input\n" +
    "                                   data-required\n" +
    "                                   reset-field\n" +
    "                                   data-message-required=\"validate position\"\n" +
    "                                    >\n" +
    "\n" +
    "                        </div>\n" +
    "                            </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                        <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                            <label for=\"title\"><span\n" +
    "                                    data-i18n=\"company\">Tại công ty</span></label>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                            <input ng-model=\"experience.companyName\" name=\"company\"\n" +
    "                                   type=\"text\"\n" +
    "                                   data-i18n=\"i18n-placeholder\" placeholder=\"company\"\n" +
    "                                   class=\"form-control \" maxlength=\"100\"\n" +
    "                                   data-smart-validate-input\n" +
    "                                   data-required\n" +
    "                                   reset-field\n" +
    "                                   data-message-required=\"validate company\"\n" +
    "                                    >\n" +
    "\n" +
    "                        </div>\n" +
    "                            </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                        <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                            <label for=\"title\"><span\n" +
    "                                    data-i18n=\"start date\">Ngày bắt đầu làm việc</span></label>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                            <!--<div class=\"input-group\">-->\n" +
    "                                <!--<input type=\"text\" class=\"form-control\" ng-model=\"experience.start_date\"-->\n" +
    "                                       <!--datepicker-popup=\"{{format}}\"-->\n" +
    "                                       <!--data-smart-validate-input-->\n" +
    "                                       <!--is-open=\"openedStartDate\"-->\n" +
    "                                       <!--name=\"openedStartDate\" placeholder=\"dd/mm/yyyy\"-->\n" +
    "                                       <!--ng-click=\"openStartDate($event)\"-->\n" +
    "                                       <!--dateformat=\"Chưa nhập dữ liệu hoặc sai định dạng\"/>-->\n" +
    "                                       <!--<span class=\"input-group-btn\">-->\n" +
    "                                        <!--<button type=\"button\" class=\"btn btn-default\"-->\n" +
    "                                                <!--ng-click=\"openStartDate($event)\"><i-->\n" +
    "                                                <!--class=\"glyphicon glyphicon-calendar\"></i></button>-->\n" +
    "                                      <!--</span>-->\n" +
    "                            <!--</div>-->\n" +
    "                            <div class=\"no-padding\" name=\"openedStartDate\" ng-model=\"experience.fromDate\" is-open=\"openedStartDate\" dateformat smart-date></div>\n" +
    "                        </div>\n" +
    "                            </div>\n" +
    "                        <div class=\"col-md-6\">\n" +
    "                        <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                            <label for=\"title\"><span\n" +
    "                                    data-i18n=\"end date\">Ngày kết thúc làm việc</span></label>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12 form-group no-padding\">\n" +
    "\n" +
    "                            <!--<div class=\"input-group\">-->\n" +
    "                                <!--<input type=\"text\" class=\"form-control\" ng-model=\"experience.end_date\"-->\n" +
    "                                       <!--datepicker-popup=\"{{format}}\"-->\n" +
    "                                       <!--data-smart-validate-input-->\n" +
    "                                       <!--is-open=\"openedEndDate\"-->\n" +
    "                                       <!--name=\"openedEndDate\" placeholder=\"dd/mm/yyyy\"-->\n" +
    "                                       <!--ng-click=\"openedendDate($event)\"-->\n" +
    "                                       <!--dateformat=\"Chưa nhập dữ liệu hoặc sai định dạng\"/>-->\n" +
    "                                       <!--<span class=\"input-group-btn\">-->\n" +
    "                                        <!--<button type=\"button\" class=\"btn btn-default\"-->\n" +
    "                                                <!--ng-click=\"openedendDate($event)\"><i-->\n" +
    "                                                <!--class=\"glyphicon glyphicon-calendar\"></i></button>-->\n" +
    "                                      <!--</span>-->\n" +
    "                            <!--</div>-->\n" +
    "                            <div class=\"no-padding\" ng-model=\"experience.toDate\" name=\"openedEndDate\" is-open=\"openedEndDate\" dateformat smart-date></div>\n" +
    "                        </div>\n" +
    "                            </div>\n" +
    "                        <div class=\"col-md-12\">\n" +
    "                        <div class=\"col-md-12 no-padding\">\n" +
    "\n" +
    "                            <label for=\"title\"><span\n" +
    "                                    data-i18n=\"experience\">Kinh nghiệm làm việc</span></label>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-12 form-group no-padding\">\n" +
    "                            <textarea\n" +
    "                                    ng-model=\"experience.achievement\" name=\"achievements\"\n" +
    "                                    class=\"form-control  padding-bot-proposal\"\n" +
    "                                    data-i18n=\"i18n-placeholder\" placeholder=\"detail your experience\"\n" +
    "                                    data-smart-validate-input maxlength=\"255\"\n" +
    "                                    data-required\n" +
    "                                    rows=\"3\"\n" +
    "                                    reset-field\n" +
    "                                    data-message-required=\"validate experience\"></textarea>\n" +
    "                        </div> </div>\n" +
    "                        <div class=\"col-md-12 checkbox margin-left30px\">\n" +
    "\n" +
    "                                <label class=\"ui-checkbox\"><input  name=\"savelogin\" ng-model=\"experience.currentWork\"  name=\"stillwork\" value=\"Đang làm việc ở đây\"\n" +
    "                                                                  type=\"checkbox\"><span data-i18n=\"still work here\">Hiện tại vẫn đang làm việc tại đây</span></label>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancel()\" data-i18n=\"cancel\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"ok()\" data-i18n=\"confirm\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/choseRole.html',
    "<div class=\"modal-fade\" id=\"choseRole\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <div class=\"modal-dialog padding-bottom60\" data-backdrop=\"static\" data-keyboard=\"false\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"modal-body\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\r" +
    "\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\r" +
    "\n" +
    "                <div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\r" +
    "\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\r" +
    "\n" +
    "                    <p class=\"text-center form-group\">\r" +
    "\n" +
    "                        <span data-i18n=\"chose role message\">You will receive messages when you become customer or supplier of Vaber.vn</span>\r" +
    "\n" +
    "                    </p>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\r" +
    "\n" +
    "                <h3 class=\"text-center form-group\"><span data-i18n=\"get started now\">GET STARTED NOW!</span></h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"text-center form-group\">\r" +
    "\n" +
    "                        <button type=\"button\" class=\"col-md-12 col-xs-12 btn btn-lg  btn-custom btn-primary no-padding-right-left\"\r" +
    "\n" +
    "                                ng-click=\"customer()\"><span data-i18n=\"customer\" ></span>\r" +
    "\n" +
    "                        </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <span class=\"col-md-12 col-xs-12 hr-divider-text no-float-mobile\" data-i18n=\"or\">OR</span><br>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <button type=\"button\" class=\"col-md-12 col-xs-12 btn btn-lg btn-custom btn-primary no-padding-right-left margin-top-15px margin-top-1px\"\r" +
    "\n" +
    "                                ng-click=\"supplier()\"><span data-i18n=\"supplier\" ></span>\r" +
    "\n" +
    "                        </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/closePost.html',
    "<div class=\"modal-fade\" id=\"extendPopup\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"extendConditionsPopup\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "        <div class=\"modal-header header-popup\">\n" +
    "            <div class=\"col-md-12 nav-tabs\"><span data-i18n=\"close now\"></span></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "                <div class=\"no-padding col-xs-12\">\n" +
    "                    <span data-i18n=\"close order\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup no-padding\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmClose()\" data-i18n=\"confirm\"></a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelClose()\" data-i18n=\"cancel\"></a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/confirmProposal.html',
    "<div class=\"modal-fade\" id=\"confirmProposal\" xmlns=\"http://www.w3.org/1999/html\">\n" +
    "\n" +
    "    <div class=\"modal-dialog no-padding-bot\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "        <div class=\"modal-body \">\n" +
    "            <div>\n" +
    "                <div>\n" +
    "                    <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                            aria-hidden=\"true\">&times;</span></button>\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "                </div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"content-popup\"><span data-i18n=\"accept service order\">Chấp nhận yêu cầu:</span> <span  ng-bind=\"title\" class=\"font-1\"></span></div>\n" +
    "                <div class=\"content-popup\"><span data-i18n=\"creator\">Người tạo:</span> <span class=\"font-1\" ng-bind=\"customer\"></span></div>\n" +
    "\n" +
    "                <div class=\"content-popup\" data-i18n=\"info fee\">\n" +
    "                    Phí thanh toán cho yêu cầu này là 50,000 VNĐ\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\" data-i18n=\"confirm continue\">\n" +
    "                    Bạn có muốn tiếp tục không?\n" +
    "                </div>\n" +
    "                <!--<div class=\"content-popup\">-->\n" +
    "                    <!--<div class=\"col-sm-12\">-->\n" +
    "                        <!--<div class=\"col-sm-12\">-->\n" +
    "                            <!--<div class=\"radio\">-->\n" +
    "                                <!--<label>-->\n" +
    "                                    <!--<input type=\"radio\" ng-model=\"type\"-->\n" +
    "                                           <!--name=\"youPay\" value=\"youPay\" checked=\"checked\"></span>-->\n" +
    "                                    <!--<label class=\"control-label\">Thanh toán ngay</label>-->\n" +
    "                                <!--</label>-->\n" +
    "                            <!--</div>-->\n" +
    "                        <!--</div>-->\n" +
    "                        <!--<div class=\"form-group col-sm-12\">-->\n" +
    "                            <!--\"Khoản thanh toán sẽ được trừ vào tài khoản khả dụng của bạn\"-->\n" +
    "                        <!--</div>-->\n" +
    "                        <!---->\n" +
    "                    <!--</div>-->\n" +
    "                    <!--</div>-->\n" +
    "\n" +
    "                    <!--<div class=\"col-sm-12\">-->\n" +
    "                        <!--<div class=\"col-sm-12\">-->\n" +
    "                            <!--<div class=\"radio\">-->\n" +
    "                                <!--<label>-->\n" +
    "                                    <!--<input type=\"radio\" ng-model=\"type\" value=\"customerPay\"-->\n" +
    "                                           <!--name=\"customerPay\">-->\n" +
    "\n" +
    "                                    <!--<label class=\"control-label\">Nhờ Vaber thanh toán hộ</label>-->\n" +
    "                                <!--</label>-->\n" +
    "                            <!--</div>-->\n" +
    "                        <!--</div>-->\n" +
    "\n" +
    "                        <!--<div class=\"form-group col-sm-12\">-->\n" +
    "                            <!--\"Khi lựa chọn việc để Vaber trả phí hộ, nghĩa là bạn đã đồng ý thực hiện yêu cầu.Vaber sẽ trừ khoản phí này vào số tiền bạn nhận được từ khách hàng.\"-->\n" +
    "                        <!--</div>-->\n" +
    "                    <!--</div>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup no-padding-top\">\n" +
    "            <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <div class=\"pull-right\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancel()\" data-i18n=\"cancel\">Hủy</a>\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"accept()\" data-i18n=\"confirm\">Xác nhận</a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/cvPopup.html',
    "<div id=\"viewdetailCV\" class=\"modal-lg modal-fade\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "\n" +
    "                <button type=\"button\" ng-click=\"close()\" style=\"padding-top: 30px;\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"col-md-12\">\n" +
    "                        <div class=\"col-md-6 panel-heading border-wrapper form-group\">\n" +
    "                            <strong>\n" +
    "                                <span class=\"glyphicon glyphicon-user\"></span>\n" +
    "                                <span>Thông tin cá nhân</span>\n" +
    "                            </strong>\n" +
    "\n" +
    "                            <div class=\"panel-body padding-moblie\">\n" +
    "\n" +
    "                                <div class=\"col-md-12 form-group no-padding\">\n" +
    "                                    <label>Họ và tên:</label>&nbsp;\n" +
    "                                    <span>Nguyễn Văn A</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Ngày sinh:</label>&nbsp;\n" +
    "                                    <span>23/09/1991</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-6 form-group no-padding\">\n" +
    "                                    <label>Số cmnd:</label>&nbsp;\n" +
    "                                    <span>095 785 6541</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-6 form-group no-padding\">\n" +
    "                                    <label>Ngày cấp:</label>&nbsp;\n" +
    "                                    <span>30/3/2001</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Nơi cấp:</label>&nbsp;\n" +
    "                                    <span>Công an thành phố hà nội</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Địa chỉ:</label>&nbsp;\n" +
    "                                    <span>24 Nguyễn Thượng Hiền - Hai bà trưng - hà nội</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Hòm thư:</label>&nbsp;\n" +
    "                                    <span>tuannm2391@gmail.com</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Số điện thoại:</label>&nbsp;\n" +
    "                                    <span>01679241024</span>\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Giới tính:</label>&nbsp;\n" +
    "                                    <span>Nam</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-5 col-md-offset-1 panel-heading border-wrapper form-group\">\n" +
    "                            <strong><span class=\"glyphicon glyphicon-pushpin\"></span> <span>Giới thiệu bản thân\n" +
    "                            </span></strong>\n" +
    "\n" +
    "                            <div class=\"panel-body padding-moblie\">\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Ngôn ngữ:</label>&nbsp;\n" +
    "                                    <span>Tiếng Anh, Tiếng Nhật, Tiếng Pháp</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Sức khỏe:</label>&nbsp;\n" +
    "                                    <span>tốt</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Sở thích cá nhân:</label>&nbsp;\n" +
    "                                    <span>Đá pes, chơi thể thao, đi du lịch, nghe nhạc và đặc biệt thích ngắm gái đẹp</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Điêu điều về bản thân:</label>&nbsp;\n" +
    "                                    <span>hiền lành, trung thực</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <label>Mục tiêu công việc:</label>&nbsp;\n" +
    "                                    <span>tìm tòi và học hỏi các công nghệ mới, vươn lên một mức giỏi hơn trong 2 năm tới</span>\n" +
    "\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-12\">\n" +
    "                        <div class=\"col-md-6 panel-heading border-wrapper form-group\">\n" +
    "                            <strong>\n" +
    "                                <span class=\"glyphicon glyphicon-cog\"></span>\n" +
    "                                <span>Học vấn và năng lực</span>\n" +
    "                            </strong>\n" +
    "\n" +
    "                            <div class=\"panel-body padding-moblie\">\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <ul>\n" +
    "                                        <li>THCS Tây Sơn</li>\n" +
    "                                        <ul>\n" +
    "                                            <li>\n" +
    "                                                Từ năm 2003 - 2007\n" +
    "                                            </li>\n" +
    "                                        </ul>\n" +
    "                                    </ul>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <ul>\n" +
    "                                        <li>THPT Hai Bà Trưng</li>\n" +
    "                                        <ul>\n" +
    "                                            <li>\n" +
    "                                                Từ năm 2007- 2010\n" +
    "                                            </li>\n" +
    "                                        </ul>\n" +
    "                                    </ul>\n" +
    "\n" +
    "                                </div>\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <ul>\n" +
    "                                        <li>FPT - Polytechnic</li>\n" +
    "                                        <ul>\n" +
    "                                            <li>\n" +
    "                                                Từ năm 2010 - 2013\n" +
    "                                            </li>\n" +
    "                                        </ul>\n" +
    "                                    </ul>\n" +
    "\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-5 col-md-offset-1 panel-heading border-wrapper form-group\">\n" +
    "                            <strong><span class=\"glyphicon glyphicon-screenshot\"></span><span>Kinh nghiệm\n" +
    "                            </span></strong>\n" +
    "\n" +
    "                            <div class=\"panel-body padding-moblie\">\n" +
    "                                <div class=\"col-md-12 no-padding form-group\">\n" +
    "                                    <ul>\n" +
    "                                        <li>Công ty FPT</li>\n" +
    "                                        <ul>\n" +
    "                                            <li>\n" +
    "                                                Từ năm 2013 - 2015\n" +
    "                                            </li>\n" +
    "                                        </ul>\n" +
    "                                    </ul>\n" +
    "\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/form/deletePost.html',
    "<div class=\"modal-fade\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"extendConditionsPopup\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "        <div class=\"modal-header header-popup\">\n" +
    "            <div class=\"col-md-12 nav-tabs\"><span data-i18n=\"delete now\"></span></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "                <div class=\"no-padding col-xs-12\">\n" +
    "                    <span data-i18n=\"delete order\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup no-padding\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmDelete()\" data-i18n=\"confirm\"></a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelDelete()\" data-i18n=\"cancel\"></a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/deleteProposal.html',
    "<div class=\"modal-fade\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"deleteProposal\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "        <div class=\"modal-header header-popup\">\n" +
    "            <div class=\"col-md-12 nav-tabs\">Xóa đề nghị của bạn ngay bây giờ?</div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "                <div class=\"no-padding col-xs-12\">\n" +
    "                <span data-i18n=\"sure delete proposal\">Bạn có chắc muốn xóa đề nghị này không?</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup no-padding\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelDelete()\" data-i18n=\"cancel\">Hủy bỏ</a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmDelete()\" data-i18n=\"confirm\">Xác nhận</a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/deletecv.html',
    "<div class=\"modal-fade\" id=\"paymentPopup\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"rejectProposal\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "\n" +
    "                <div>\n" +
    "                    <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                            aria-hidden=\"true\">&times;</span></button>\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\"><span  data-i18n=\"confirm delete cv\">Bạn có chắc chắn loại bỏ CV của</span>&nbsp;<span ng-bind=\"nameOfCV\" class=\"font-1\"></span>&nbsp;<span data-i18n=\"out of list cv\">khỏi danh sách CV của bạn không?</span></div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup\">\n" +
    "            <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <div class=\"pull-right\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelDelete()\" data-i18n=\"cancel\">Hủy</a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"deleteCv()\" data-i18n=\"confirm\">Từ chối</a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/extend.html',
    "<div class=\"modal-fade\" id=\"extendPopup\" ng-init=\"init()\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"extendConditionsPopup\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "        <form novalidate=\"novalidate\" data-smart-validate-form>\n" +
    "        <div class=\"modal-header header-popup\">\n" +
    "            <div class=\"col-md-12 nav-tabs\"><span data-i18n=\"extend now\"></span></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body no-padding\">\n" +
    "            <div>\n" +
    "\n" +
    "                <div class=\"pad-top-10 col-xs-12\">\n" +
    "                    <div class=\"col-xs-4\"><span class=\"toast-title\" data-i18n=\"bid end date\"></span></div>\n" +
    "                    <div class=\"form-group col-xs-8 no-margin no-padding\">\n" +
    "                        <p class=\"input-group input-group-sm bootstrapWizard\">\n" +
    "                            <input type=\"text\" class=\"form-control\" ng-model=\"bidEndDate\" disabled/>\n" +
    "                        </p>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pad-top-10 col-xs-12\">\n" +
    "                    <div class=\"col-xs-4\"><span class=\"toast-title\" data-i18n=\"extend\"></span></div>\n" +
    "                    <div class=\"form-group col-xs-8 no-margin no-padding\">\n" +
    "                        <!--<p class=\"input-group input-group-sm\">-->\n" +
    "                            <!--<input type=\"text\" class=\"form-control\" datepicker-popup=\"{{format}}\"-->\n" +
    "                                   <!--ng-model=\"expiredDate\" is-open=\"opened\" data-smart-validate-input-->\n" +
    "                                   <!--placeholder=\"dd/mm/yyyy\" min-date=\"minDate\" data-required name=\"expiredDate\"-->\n" +
    "                                   <!--ng-click=\"open($event)\" dateformat=\"Chưa nhập dữ liệu hoặc sai định dạng\"/>-->\n" +
    "                                      <!--<span class=\"input-group-btn\">-->\n" +
    "                                        <!--<button type=\"button\" class=\"btn btn-default\" ng-click=\"open($event)\"><i-->\n" +
    "                                                <!--class=\"glyphicon glyphicon-calendar\"></i></button>-->\n" +
    "                                      <!--</span>-->\n" +
    "                        <!--</p>-->\n" +
    "                        <div class=\"no-padding\" ng-model=\"expiredDate\" is-open=\"opened\" name=\"expiredDate\" data-required minDate mindatemobile dateformat smart-date></div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pad-top-10 col-xs-12 non-display\">\n" +
    "                    <div class=\"col-xs-4\"></div>\n" +
    "                    <div class=\"col-xs-8\">\n" +
    "                        <timepicker ng-model=\"extent_time_expired\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"></timepicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup no-padding\">\n" +
    "            <div class=\"col-xs-12 form-actions\">\n" +
    "                <a class=\"btn btn-sm btn-primary\" smart-click=\"wizardCompleteCallback\" data-i18n=\"confirm\"></a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelExtend()\" data-i18n=\"cancel\"></a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/finishOrder.html',
    "<div class=\"modal-fade\" id=\"finishOrder\" xmlns=\"http://www.w3.org/1999/html\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "        <div class=\"modal-header header-popup\">\n" +
    "            <div class=\"col-md-12\">Bạn vừa thanh toán thành công giao dịch trên Vaber</div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "\n" +
    "                <div class=\"content-popup\">\n" +
    "                    <h4>Chi tiết giao dịch</h4>\n" +
    "\n" +
    "                    <div class=\"col-sm-12\">\n" +
    "\n" +
    "                        <table>\n" +
    "                            <tr>\n" +
    "                                <td>Tên yêu cầu:</td>\n" +
    "\n" +
    "                                <td class=\"pull-left\" ><span ng-bind=\"tiltle\"></span></td>\n" +
    "\n" +
    "                            <tr>\n" +
    "                                <td >Người thanh toán:</td>\n" +
    "\n" +
    "                                <td class=\"pull-left\" ng-bind=\"supplier\"></td>\n" +
    "                            </tr>\n" +
    "                            <tr>\n" +
    "                                <td>\n" +
    "                                    Phương thức thanh toán:\n" +
    "                                </td>\n" +
    "                                <td class=\"pull-left\"><span ng-bind=\"paymethod\"></span></td>\n" +
    "\n" +
    "                            </tr>\n" +
    "                            <tr>\n" +
    "                                <td>Thanh toán cho:</td>\n" +
    "\n" +
    "                                <td class=\"pull-left \">Vaber.com</td>\n" +
    "                            </tr>\n" +
    "                            <tr>\n" +
    "                                <th>Tổng giá trị hóa đơn:</th>\n" +
    "\n" +
    "                                <th colspan=\"2\" class=\"pull-right btn-sum\">\n" +
    "                                    <button class=\"btn btn-action btn-lg\" data-smart-wizard-summit><span ng-bind=\"sum\"></span></button>\n" +
    "                                </th>\n" +
    "                            </tr>\n" +
    "                        </table>\n" +
    "                        <div class=\"content-popup modal-vertical-centered pull-right\">\n" +
    "                            <a class=\"btn btn-sm btn-primary\" data-ng-click=\"accept()\">Xác nhận</a>\n" +
    "                            <a class=\"btn btn-sm btn-primary\" data-ng-click=\"viewHistory()\">Xem lịch sử giao dịch</a>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup\">\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/loadingTemplate.html',
    "<div class=\"cg-busy-default-wrapper\">\n" +
    "\n" +
    "   <div class=\"cg-busy-default-sign\">\n" +
    "\n" +
    "      <div class=\"cg-busy-default-spinner\">\n" +
    "         <div class=\"bar1\"></div>\n" +
    "         <div class=\"bar2\"></div>\n" +
    "         <div class=\"bar3\"></div>\n" +
    "         <div class=\"bar4\"></div>\n" +
    "         <div class=\"bar5\"></div>\n" +
    "         <div class=\"bar6\"></div>\n" +
    "         <div class=\"bar7\"></div>\n" +
    "         <div class=\"bar8\"></div>\n" +
    "         <div class=\"bar9\"></div>\n" +
    "         <div class=\"bar10\"></div>\n" +
    "         <div class=\"bar11\"></div>\n" +
    "         <div class=\"bar12\"></div>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"cg-busy-default-text\" data-i18n=\"{{$message}}\"></div>\n" +
    "\n" +
    "   </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/login.html',
    "<div class=\"modal-fade\" id=\"loginPopup\" role=\"dialog\" ria-labelledby=\"myModalLabel\">\n" +
    "\n" +
    "<div class=\"modal-dialog modal-dialog-mobile form-group\">\n" +
    "\n" +
    "<div class=\"modal-body center-block\">\n" +
    "\n" +
    "<button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "        aria-hidden=\"true\">&times;</span></button>\n" +
    "<img class=\"center-block logo-header\" src=\"images/logo.png\" alt=\"logo\" style=\"width: 200px;\"> <span\n" +
    "        class=\"ng-binding\"></span>\n" +
    "<div class=\"btn-group margin-bottom btn-width\">\n" +
    "\n" +
    "    <button type=\"button\" id=\"hasAccount\" class=\"btn btn-lg btn-line-primary active modal-btn-old-user\"\n" +
    "            ng-click='setSelection(\"return\", $event)' data-i18n=\"iam a returning user\">Tôi đã có tài\n" +
    "        khoản\n" +
    "    </button>\n" +
    "    <button type=\"button\" id=\"notHasAccount\" class=\"btn btn-lg btn-line-primary modal-btn-new-user\"\n" +
    "            ng-click='setSelection(\"new\", $event)' data-i18n=\"iam a new user\">Tôi là người mới\n" +
    "    </button>\n" +
    "</div>\n" +
    "<div class=\"animate-switch-container\" ng-switch on=\"mode.selection\">\n" +
    "\n" +
    "    <div class=\"animate-switch\" ng-switch-when=\"return\">\n" +
    "        <form id=\"formPopupLogin\" novalidate=\"novalidate\" data-smart-validate-form ng-enter=\"loginPopup()\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                                <span class=\"input-group-addon \"><label for=\"email\"\n" +
    "                                                                                class=\"glyphicon glyphicon-user\"\n" +
    "                                                                                rel=\"tooltip\"\n" +
    "                                                                                title=\"email\"></label></span>\n" +
    "                    <input type=\"text\" id=\"username\" name=\"username\" ng-model=\"account.username\"\n" +
    "                           class=\"form-control\"\n" +
    "                           reset-field\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"username or Email\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           data-message-required=\"validate input login name\"\n" +
    "                            >\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label for=\"password\"\n" +
    "                                                                            class=\"glyphicon glyphicon-lock\"\n" +
    "                                                                            rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "                    <input type=\"password\" name=\"password\" class=\"form-control \"\n" +
    "                           ng-model=\"account.password\" reset-field\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"password\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           data-message-required=\"validate input password\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"checkbox no-padding\">\n" +
    "                <!--<label class=\"remember-me\" ng-model=\"rememberMe\">-->\n" +
    "                <!--<span data-i18n=\"remember me\"></span><input id=\"savelogin\" type=\"checkbox\"-->\n" +
    "                <!--name=\"savelogin\">-->\n" +
    "                <!--</label>-->\n" +
    "                <label class=\"ui-checkbox\" ng-model=\"rememberMe\"><input id=\"savelogin\" class=\"no-margin\"\n" +
    "                                                                        name=\"savelogin\"\n" +
    "                                                                        type=\"checkbox\"><span\n" +
    "                        data-i18n=\"remember me\"></span></label>\n" +
    "            </div>\n" +
    "            <button class=\"btn btn-lg btn-block btn-custom btn-primary\" ng-click=\"loginPopup()\" data-i18n=\"login\">Login\n" +
    "            </button>\n" +
    "            <a href=\"\" class=\"forgot-password-toggle\" id=\"forgot-password-toggle\" ng-click=\"toggle()\"\n" +
    "               data-i18n=\"forgot password\">Forgot\n" +
    "                password?</a>\n" +
    "        </form>\n" +
    "        <form novalidate=\"novalidate\" data-smart-validate-form>\n" +
    "            <div ng-hide=\"forgotPass\" class=\"margin-bottom text-center form-group\">\n" +
    "\n" +
    "                <span data-i18n=\"enter your email\"><b>Nhập email của bạn</b></span>\n" +
    "\n" +
    "                <div class=\"input-group\">\n" +
    "\n" +
    "                            <span class=\"input-group-addon \"><label for=\"email_forgotPass\"\n" +
    "                                                                            class=\"glyphicon glyphicon-envelope\"\n" +
    "                                                                            rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "                    <input type=\"text\" name=\"email_forgotPass\" id=\"email_forgotPass\"\n" +
    "                           ng-model=\"account.email_forgotPass\"\n" +
    "                           reset-field\n" +
    "                           class=\"form-control \"\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"enter your email\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           data-email\n" +
    "                           data-message-required=\"validate input email\"\n" +
    "                           data-message-email=\"validate input email form\">\n" +
    "\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"form-step has-button text-center\">\n" +
    "                    <div class=\"form-step has-button text-center\">\n" +
    "                        <button class=\"btn btn-lg  btn-custom btn-lock btn-signup btn-primary\"\n" +
    "                                ng-click=\"getPassword()\" data-i18n=\"retrieve your password\">Lấy lại mật khẩu\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </form>\n" +
    "\n" +
    "        <!--<div class=\"hr-divider\"><span class=\"hr-divider-text\"></span></div>-->\n" +
    "        <!--<p><span data-i18n=\"don’t have an account\">Don't have an account?</span>-->\n" +
    "        <!--<a href=\"/#/signup\" data-section-toggle=\"signup\">-->\n" +
    "        <!--<span data-i18n=\"sign up now\"></span>-->\n" +
    "        <!--</a>-->\n" +
    "        <!--</p>-->\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"animate-switch\" ng-switch-when=\"new\">\n" +
    "        <form id=\"signupPopup\" novalidate=\"novalidate\" data-smart-validate-form>\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label for=\"email\"\n" +
    "                                                                            class=\"glyphicon glyphicon-envelope\"\n" +
    "                                                                            rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "                    <input type=\"text\" name=\"email\" id=\"email\" ng-model=\"newAccount.email\"\n" +
    "                           class=\"form-control \"\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"email address\" reset-field\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           data-message-required=\"validate input email\"\n" +
    "                           data-email\n" +
    "                           data-message-email=\"validate input email form\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label\n" +
    "                                    class=\"glyphicon glyphicon-user\"\n" +
    "                                    rel=\"tooltip\"\n" +
    "                                    title=\"username\"></label></span>\n" +
    "                    <input type=\"text\" name=\"username\" ng-model=\"newAccount.username\"\n" +
    "                           class=\"form-control \" reset-field\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"username\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           data-message-required=\"validate input login name\"\n" +
    "                            >\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label\n" +
    "                                    class=\"glyphicon glyphicon-phone\"\n" +
    "                                    rel=\"tooltip\"\n" +
    "                                    title=\"phoneNumber\"></label></span>\n" +
    "                    <input type=\"text\" name=\"phoneNumber\" ng-model=\"newAccount.phoneNumber\"\n" +
    "                           class=\"form-control \" reset-field\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"phone Number\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           numbers-only\n" +
    "                           data-message-required =\"validate phone number\"\n" +
    "                            >\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label\n" +
    "                                    class=\"glyphicon glyphicon-lock\"\n" +
    "                                    rel=\"tooltip\"\n" +
    "                                    title=\"password\"></label></span>\n" +
    "                    <input type=\"password\" class=\"form-control password\" ng-model=\"newAccount.password\"\n" +
    "                           data-i18n=\"i18n-placeholder\" placeholder=\"password\" reset-field\n" +
    "                           name=\"password\" id=\"password\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           passwordcheck=\"validate password\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <div class=\"input-group\">\n" +
    "                            <span class=\"input-group-addon \"><label class=\"glyphicon glyphicon-lock\"\n" +
    "                                                                            rel=\"tooltip\"\n" +
    "                                                                            title=\"retypepassword\"></label></span>\n" +
    "                    <input type=\"password\" class=\"form-control \"\n" +
    "                           ng-model=\"newAccount.retypepassword\" reset-field\n" +
    "                           Data-i18n=\"i18n-placeholder\" data-i18n=\"i18n-placeholder\"\n" +
    "                           placeholder=\"confirm password\" name=\"retypepassword\"\n" +
    "                           data-smart-validate-input\n" +
    "                           data-required\n" +
    "                           retypepassword=\"validate retype password\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\" btn-radio btn-custom-group\">\n" +
    "\n" +
    "                <label class=\"btn btn-plain active btn-checkbox font-color-black\">\n" +
    "                    <label class=\"ui-checkbox\"><input name=\"customer\" id=\"customer\" ng-model=\"role.customer\" ng-disabled='isClickCategory'\n" +
    "                                                      type=\"checkbox\"><span\n" +
    "                            data-i18n=\"customer\"></span></label>\n" +
    "                </label><label class=\"btn btn-plain btn-checkbox font-color-black\">\n" +
    "                <label class=\"ui-checkbox\"><input name=\"supplier\" id=\"supplier\"\n" +
    "                                                  ng-model=\"role.supplier\" ng-disabled='isClickCategory'\n" +
    "                                                  type=\"checkbox\"><span\n" +
    "                        data-i18n=\"supplier\"></span></label>\n" +
    "            </label>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"form-step has-button text-center\">\n" +
    "\n" +
    "\n" +
    "                <button class=\"btn btn-lg  btn-custom btn-lock btn-signup btn-primary\"\n" +
    "                        ng-click=\"signupPopupClick()\" data-i18n=\"sign up now\">Sign Up Now\n" +
    "                </button>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"hr-divider\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <p class=\"form-group text-center\"><span\n" +
    "                    data-i18n=\"by registering you confirm that you accept the\">Bằng việc đăng kí, bạn xác nhận đã đồng ý với</span></br>\n" +
    "                <a href=\"#/policy\"  target=\"_blank\"\n" +
    "                   data-section-toggle=\"signup\">\n" +
    "                    <span data-i18n=\"term and conditions\">Điều khoản và điều kiện</span> </a>\n" +
    "            </p>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/notifyAddonService.html',
    "<div id=\"notifyAddonService\" class=\"modal-fade\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <!--<div class=\"modal-dialog\">-->\n" +
    "                        <img class=\"center-block form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\">\n" +
    "                <!--</div>-->\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div>\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <span data-i18n=\"choose addon service1\"></span> <b>{{typeAddonService}}</b> <span data-i18n=\"choose addon service2\"></span>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <table datatable-basic no-hightlight table-options=\"totalPaymentAddonService\"\n" +
    "                               class=\"table table-striped table-bordered table-hover serviceTable\"\n" +
    "                               width=\"100%\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"kind\"></span></th>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"price\"></span></th>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"quantity\"></span></th>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"total\"></span></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"col-xs-12 pad-bot-10 no-padding\">\n" +
    "                        <div class=\"pull-left\">\n" +
    "                            <span><b data-i18n=\"total payment\"></b></span>\n" +
    "                        </div>\n" +
    "                        <div class=\"pull-right\">\n" +
    "                            <span><b>{{totalPayment}} VNĐ</b></span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <span data-i18n=\"confirm continue\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelReject()\" data-i18n=\"cancel\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmReject()\" data-i18n=\"confirm\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/form/payment.html',
    "<div class=\"modal-fade\" id=\"paymentPopup\" ng-init=\"init()\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                        <img class=\"center-block form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div>\n" +
    "                    <div>\n" +
    "                        <span data-i18n=\"payment message1\"></span>{{countSelection}}  <span data-i18n=\"payment message2\"></span>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <span data-i18n=\"payment message3\"></span> <b>{{vaberFee}} VNĐ</b>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <span data-i18n=\"confirm continue\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmAwardOrder()\" data-i18n=\"confirm\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelAwardOrder()\" data-i18n=\"cancel\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/post.html',
    "<form novalidate=\"novalidate\" >\n" +
    "<div class=\"col-sm-12\" >\n" +
    "  <div class=\"form-bootstrapWizard clearfix\">\n" +
    "    <ul class=\"bootstrapWizard\">\n" +
    "      <li data-smart-wizard-tab=\"1\" >\n" +
    "        <a href=\"#\" > <span class=\"step\">1</span> <span class=\"title\">Basic information</span>\n" +
    "        </a>\n" +
    "      </li>\n" +
    "      <li data-smart-wizard-tab=\"2\" >\n" +
    "        <a href=\"#\" > <span class=\"step\">2</span> <span class=\"title\">Billing information</span> </a>\n" +
    "      </li>\n" +
    "      <li data-smart-wizard-tab=\"3\">\n" +
    "        <a href=\"#\" > <span class=\"step\">3</span> <span class=\"title\">Domain Setup</span> </a>\n" +
    "      </li>\n" +
    "      <li data-smart-wizard-tab=\"4\">\n" +
    "        <a href=\"#\" > <span class=\"step\">4</span> <span class=\"title\">Save Form</span> </a>\n" +
    "      </li>\n" +
    "    </ul>\n" +
    "  </div>\n" +
    "  <div class=\"tab-content\">\n" +
    "    <div class=\"tab-pane\" data-smart-wizard-pane=\"1\" >\n" +
    "      <br>\n" +
    "\n" +
    "      <div class=\"row\">\n" +
    "\n" +
    "        <div class=\"col-sm-12\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-envelope fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" placeholder=\"email@address.com\" type=\"text\" name=\"email\"\n" +
    "                     data-smart-validate-input\n" +
    "                     data-required\n" +
    "                     data-email\n" +
    "                     data-message-required=\"We need your email address to contact you\"\n" +
    "                     data-message-email=\"Your email address must be in the format of name@domain.com\">\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-sm-6\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-user fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" placeholder=\"First Name\" type=\"text\" name=\"fname\"\n" +
    "                     data-smart-validate-input\n" +
    "                     data-required\n" +
    "                     data-message=\"Please specify your First name\">\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-6\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-user fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" placeholder=\"Last Name\" type=\"text\" name=\"lname\"\n" +
    "                     data-smart-validate-input data-required\n" +
    "                     data-message=\"Please specify your Last name\">\n" +
    "\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"tab-pane\" data-smart-wizard-pane=\"2\"  >\n" +
    "      <br>\n" +
    "\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-sm-4\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-flag fa-lg fa-fw\"></i></span>\n" +
    "              <select name=\"country\"  data-smart-validate-input data-required  class=\"form-control input-lg\">\n" +
    "                <option value=\"\" selected=\"selected\">Select Country</option>\n" +
    "                <option value=\"United States\">United States</option>\n" +
    "                <option value=\"United Kingdom\">United Kingdom</option>\n" +
    "                <option value=\"Afghanistan\">Afghanistan</option>\n" +
    "                <option value=\"Albania\">Albania</option>\n" +
    "                <option value=\"Algeria\">Algeria</option>\n" +
    "                <option value=\"American Samoa\">American Samoa</option>\n" +
    "                <option value=\"Andorra\">Andorra</option>\n" +
    "                <option value=\"Angola\">Angola</option>\n" +
    "                <option value=\"Anguilla\">Anguilla</option>\n" +
    "                <option value=\"Antarctica\">Antarctica</option>\n" +
    "                <option value=\"Antigua and Barbuda\">Antigua and Barbuda</option>\n" +
    "                <option value=\"Argentina\">Argentina</option>\n" +
    "                <option value=\"Armenia\">Armenia</option>\n" +
    "                <option value=\"Aruba\">Aruba</option>\n" +
    "                <option value=\"Australia\">Australia</option>\n" +
    "                <option value=\"Austria\">Austria</option>\n" +
    "                <option value=\"Azerbaijan\">Azerbaijan</option>\n" +
    "                <option value=\"Bahamas\">Bahamas</option>\n" +
    "                <option value=\"Bahrain\">Bahrain</option>\n" +
    "                <option value=\"Bangladesh\">Bangladesh</option>\n" +
    "                <option value=\"Barbados\">Barbados</option>\n" +
    "                <option value=\"Belarus\">Belarus</option>\n" +
    "                <option value=\"Belgium\">Belgium</option>\n" +
    "                <option value=\"Belize\">Belize</option>\n" +
    "                <option value=\"Benin\">Benin</option>\n" +
    "                <option value=\"Bermuda\">Bermuda</option>\n" +
    "                <option value=\"Bhutan\">Bhutan</option>\n" +
    "                <option value=\"Bolivia\">Bolivia</option>\n" +
    "                <option value=\"Bosnia and Herzegovina\">Bosnia and Herzegovina</option>\n" +
    "                <option value=\"Botswana\">Botswana</option>\n" +
    "                <option value=\"Bouvet Island\">Bouvet Island</option>\n" +
    "                <option value=\"Brazil\">Brazil</option>\n" +
    "                <option value=\"British Indian Ocean Territory\">British Indian Ocean Territory</option>\n" +
    "                <option value=\"Brunei Darussalam\">Brunei Darussalam</option>\n" +
    "                <option value=\"Bulgaria\">Bulgaria</option>\n" +
    "                <option value=\"Burkina Faso\">Burkina Faso</option>\n" +
    "                <option value=\"Burundi\">Burundi</option>\n" +
    "                <option value=\"Cambodia\">Cambodia</option>\n" +
    "                <option value=\"Cameroon\">Cameroon</option>\n" +
    "                <option value=\"Canada\">Canada</option>\n" +
    "                <option value=\"Cape Verde\">Cape Verde</option>\n" +
    "                <option value=\"Cayman Islands\">Cayman Islands</option>\n" +
    "                <option value=\"Central African Republic\">Central African Republic</option>\n" +
    "                <option value=\"Chad\">Chad</option>\n" +
    "                <option value=\"Chile\">Chile</option>\n" +
    "                <option value=\"China\">China</option>\n" +
    "                <option value=\"Christmas Island\">Christmas Island</option>\n" +
    "                <option value=\"Cocos (Keeling) Islands\">Cocos (Keeling) Islands</option>\n" +
    "                <option value=\"Colombia\">Colombia</option>\n" +
    "                <option value=\"Comoros\">Comoros</option>\n" +
    "                <option value=\"Congo\">Congo</option>\n" +
    "                <option value=\"Congo, The Democratic Republic of The\">Congo, The Democratic Republic of The</option>\n" +
    "                <option value=\"Cook Islands\">Cook Islands</option>\n" +
    "                <option value=\"Costa Rica\">Costa Rica</option>\n" +
    "                <option value=\"Cote D'ivoire\">Cote D'ivoire</option>\n" +
    "                <option value=\"Croatia\">Croatia</option>\n" +
    "                <option value=\"Cuba\">Cuba</option>\n" +
    "                <option value=\"Cyprus\">Cyprus</option>\n" +
    "                <option value=\"Czech Republic\">Czech Republic</option>\n" +
    "                <option value=\"Denmark\">Denmark</option>\n" +
    "                <option value=\"Djibouti\">Djibouti</option>\n" +
    "                <option value=\"Dominica\">Dominica</option>\n" +
    "                <option value=\"Dominican Republic\">Dominican Republic</option>\n" +
    "                <option value=\"Ecuador\">Ecuador</option>\n" +
    "                <option value=\"Egypt\">Egypt</option>\n" +
    "                <option value=\"El Salvador\">El Salvador</option>\n" +
    "                <option value=\"Equatorial Guinea\">Equatorial Guinea</option>\n" +
    "                <option value=\"Eritrea\">Eritrea</option>\n" +
    "                <option value=\"Estonia\">Estonia</option>\n" +
    "                <option value=\"Ethiopia\">Ethiopia</option>\n" +
    "                <option value=\"Falkland Islands (Malvinas)\">Falkland Islands (Malvinas)</option>\n" +
    "                <option value=\"Faroe Islands\">Faroe Islands</option>\n" +
    "                <option value=\"Fiji\">Fiji</option>\n" +
    "                <option value=\"Finland\">Finland</option>\n" +
    "                <option value=\"France\">France</option>\n" +
    "                <option value=\"French Guiana\">French Guiana</option>\n" +
    "                <option value=\"French Polynesia\">French Polynesia</option>\n" +
    "                <option value=\"French Southern Territories\">French Southern Territories</option>\n" +
    "                <option value=\"Gabon\">Gabon</option>\n" +
    "                <option value=\"Gambia\">Gambia</option>\n" +
    "                <option value=\"Georgia\">Georgia</option>\n" +
    "                <option value=\"Germany\">Germany</option>\n" +
    "                <option value=\"Ghana\">Ghana</option>\n" +
    "                <option value=\"Gibraltar\">Gibraltar</option>\n" +
    "                <option value=\"Greece\">Greece</option>\n" +
    "                <option value=\"Greenland\">Greenland</option>\n" +
    "                <option value=\"Grenada\">Grenada</option>\n" +
    "                <option value=\"Guadeloupe\">Guadeloupe</option>\n" +
    "                <option value=\"Guam\">Guam</option>\n" +
    "                <option value=\"Guatemala\">Guatemala</option>\n" +
    "                <option value=\"Guinea\">Guinea</option>\n" +
    "                <option value=\"Guinea-bissau\">Guinea-bissau</option>\n" +
    "                <option value=\"Guyana\">Guyana</option>\n" +
    "                <option value=\"Haiti\">Haiti</option>\n" +
    "                <option value=\"Heard Island and Mcdonald Islands\">Heard Island and Mcdonald Islands</option>\n" +
    "                <option value=\"Holy See (Vatican City State)\">Holy See (Vatican City State)</option>\n" +
    "                <option value=\"Honduras\">Honduras</option>\n" +
    "                <option value=\"Hong Kong\">Hong Kong</option>\n" +
    "                <option value=\"Hungary\">Hungary</option>\n" +
    "                <option value=\"Iceland\">Iceland</option>\n" +
    "                <option value=\"India\">India</option>\n" +
    "                <option value=\"Indonesia\">Indonesia</option>\n" +
    "                <option value=\"Iran, Islamic Republic of\">Iran, Islamic Republic of</option>\n" +
    "                <option value=\"Iraq\">Iraq</option>\n" +
    "                <option value=\"Ireland\">Ireland</option>\n" +
    "                <option value=\"Israel\">Israel</option>\n" +
    "                <option value=\"Italy\">Italy</option>\n" +
    "                <option value=\"Jamaica\">Jamaica</option>\n" +
    "                <option value=\"Japan\">Japan</option>\n" +
    "                <option value=\"Jordan\">Jordan</option>\n" +
    "                <option value=\"Kazakhstan\">Kazakhstan</option>\n" +
    "                <option value=\"Kenya\">Kenya</option>\n" +
    "                <option value=\"Kiribati\">Kiribati</option>\n" +
    "                <option value=\"Korea, Democratic People's Republic of\">Korea, Democratic People's Republic of</option>\n" +
    "                <option value=\"Korea, Republic of\">Korea, Republic of</option>\n" +
    "                <option value=\"Kuwait\">Kuwait</option>\n" +
    "                <option value=\"Kyrgyzstan\">Kyrgyzstan</option>\n" +
    "                <option value=\"Lao People's Democratic Republic\">Lao People's Democratic Republic</option>\n" +
    "                <option value=\"Latvia\">Latvia</option>\n" +
    "                <option value=\"Lebanon\">Lebanon</option>\n" +
    "                <option value=\"Lesotho\">Lesotho</option>\n" +
    "                <option value=\"Liberia\">Liberia</option>\n" +
    "                <option value=\"Libyan Arab Jamahiriya\">Libyan Arab Jamahiriya</option>\n" +
    "                <option value=\"Liechtenstein\">Liechtenstein</option>\n" +
    "                <option value=\"Lithuania\">Lithuania</option>\n" +
    "                <option value=\"Luxembourg\">Luxembourg</option>\n" +
    "                <option value=\"Macao\">Macao</option>\n" +
    "                <option value=\"Macedonia, The Former Yugoslav Republic of\">Macedonia, The Former Yugoslav Republic of</option>\n" +
    "                <option value=\"Madagascar\">Madagascar</option>\n" +
    "                <option value=\"Malawi\">Malawi</option>\n" +
    "                <option value=\"Malaysia\">Malaysia</option>\n" +
    "                <option value=\"Maldives\">Maldives</option>\n" +
    "                <option value=\"Mali\">Mali</option>\n" +
    "                <option value=\"Malta\">Malta</option>\n" +
    "                <option value=\"Marshall Islands\">Marshall Islands</option>\n" +
    "                <option value=\"Martinique\">Martinique</option>\n" +
    "                <option value=\"Mauritania\">Mauritania</option>\n" +
    "                <option value=\"Mauritius\">Mauritius</option>\n" +
    "                <option value=\"Mayotte\">Mayotte</option>\n" +
    "                <option value=\"Mexico\">Mexico</option>\n" +
    "                <option value=\"Micronesia, Federated States of\">Micronesia, Federated States of</option>\n" +
    "                <option value=\"Moldova, Republic of\">Moldova, Republic of</option>\n" +
    "                <option value=\"Monaco\">Monaco</option>\n" +
    "                <option value=\"Mongolia\">Mongolia</option>\n" +
    "                <option value=\"Montserrat\">Montserrat</option>\n" +
    "                <option value=\"Morocco\">Morocco</option>\n" +
    "                <option value=\"Mozambique\">Mozambique</option>\n" +
    "                <option value=\"Myanmar\">Myanmar</option>\n" +
    "                <option value=\"Namibia\">Namibia</option>\n" +
    "                <option value=\"Nauru\">Nauru</option>\n" +
    "                <option value=\"Nepal\">Nepal</option>\n" +
    "                <option value=\"Netherlands\">Netherlands</option>\n" +
    "                <option value=\"Netherlands Antilles\">Netherlands Antilles</option>\n" +
    "                <option value=\"New Caledonia\">New Caledonia</option>\n" +
    "                <option value=\"New Zealand\">New Zealand</option>\n" +
    "                <option value=\"Nicaragua\">Nicaragua</option>\n" +
    "                <option value=\"Niger\">Niger</option>\n" +
    "                <option value=\"Nigeria\">Nigeria</option>\n" +
    "                <option value=\"Niue\">Niue</option>\n" +
    "                <option value=\"Norfolk Island\">Norfolk Island</option>\n" +
    "                <option value=\"Northern Mariana Islands\">Northern Mariana Islands</option>\n" +
    "                <option value=\"Norway\">Norway</option>\n" +
    "                <option value=\"Oman\">Oman</option>\n" +
    "                <option value=\"Pakistan\">Pakistan</option>\n" +
    "                <option value=\"Palau\">Palau</option>\n" +
    "                <option value=\"Palestinian Territory, Occupied\">Palestinian Territory, Occupied</option>\n" +
    "                <option value=\"Panama\">Panama</option>\n" +
    "                <option value=\"Papua New Guinea\">Papua New Guinea</option>\n" +
    "                <option value=\"Paraguay\">Paraguay</option>\n" +
    "                <option value=\"Peru\">Peru</option>\n" +
    "                <option value=\"Philippines\">Philippines</option>\n" +
    "                <option value=\"Pitcairn\">Pitcairn</option>\n" +
    "                <option value=\"Poland\">Poland</option>\n" +
    "                <option value=\"Portugal\">Portugal</option>\n" +
    "                <option value=\"Puerto Rico\">Puerto Rico</option>\n" +
    "                <option value=\"Qatar\">Qatar</option>\n" +
    "                <option value=\"Reunion\">Reunion</option>\n" +
    "                <option value=\"Romania\">Romania</option>\n" +
    "                <option value=\"Russian Federation\">Russian Federation</option>\n" +
    "                <option value=\"Rwanda\">Rwanda</option>\n" +
    "                <option value=\"Saint Helena\">Saint Helena</option>\n" +
    "                <option value=\"Saint Kitts and Nevis\">Saint Kitts and Nevis</option>\n" +
    "                <option value=\"Saint Lucia\">Saint Lucia</option>\n" +
    "                <option value=\"Saint Pierre and Miquelon\">Saint Pierre and Miquelon</option>\n" +
    "                <option value=\"Saint Vincent and The Grenadines\">Saint Vincent and The Grenadines</option>\n" +
    "                <option value=\"Samoa\">Samoa</option>\n" +
    "                <option value=\"San Marino\">San Marino</option>\n" +
    "                <option value=\"Sao Tome and Principe\">Sao Tome and Principe</option>\n" +
    "                <option value=\"Saudi Arabia\">Saudi Arabia</option>\n" +
    "                <option value=\"Senegal\">Senegal</option>\n" +
    "                <option value=\"Serbia and Montenegro\">Serbia and Montenegro</option>\n" +
    "                <option value=\"Seychelles\">Seychelles</option>\n" +
    "                <option value=\"Sierra Leone\">Sierra Leone</option>\n" +
    "                <option value=\"Singapore\">Singapore</option>\n" +
    "                <option value=\"Slovakia\">Slovakia</option>\n" +
    "                <option value=\"Slovenia\">Slovenia</option>\n" +
    "                <option value=\"Solomon Islands\">Solomon Islands</option>\n" +
    "                <option value=\"Somalia\">Somalia</option>\n" +
    "                <option value=\"South Africa\">South Africa</option>\n" +
    "                <option value=\"South Georgia and The South Sandwich Islands\">South Georgia and The South Sandwich Islands</option>\n" +
    "                <option value=\"Spain\">Spain</option>\n" +
    "                <option value=\"Sri Lanka\">Sri Lanka</option>\n" +
    "                <option value=\"Sudan\">Sudan</option>\n" +
    "                <option value=\"Suriname\">Suriname</option>\n" +
    "                <option value=\"Svalbard and Jan Mayen\">Svalbard and Jan Mayen</option>\n" +
    "                <option value=\"Swaziland\">Swaziland</option>\n" +
    "                <option value=\"Sweden\">Sweden</option>\n" +
    "                <option value=\"Switzerland\">Switzerland</option>\n" +
    "                <option value=\"Syrian Arab Republic\">Syrian Arab Republic</option>\n" +
    "                <option value=\"Taiwan, Province of China\">Taiwan, Province of China</option>\n" +
    "                <option value=\"Tajikistan\">Tajikistan</option>\n" +
    "                <option value=\"Tanzania, United Republic of\">Tanzania, United Republic of</option>\n" +
    "                <option value=\"Thailand\">Thailand</option>\n" +
    "                <option value=\"Timor-leste\">Timor-leste</option>\n" +
    "                <option value=\"Togo\">Togo</option>\n" +
    "                <option value=\"Tokelau\">Tokelau</option>\n" +
    "                <option value=\"Tonga\">Tonga</option>\n" +
    "                <option value=\"Trinidad and Tobago\">Trinidad and Tobago</option>\n" +
    "                <option value=\"Tunisia\">Tunisia</option>\n" +
    "                <option value=\"Turkey\">Turkey</option>\n" +
    "                <option value=\"Turkmenistan\">Turkmenistan</option>\n" +
    "                <option value=\"Turks and Caicos Islands\">Turks and Caicos Islands</option>\n" +
    "                <option value=\"Tuvalu\">Tuvalu</option>\n" +
    "                <option value=\"Uganda\">Uganda</option>\n" +
    "                <option value=\"Ukraine\">Ukraine</option>\n" +
    "                <option value=\"United Arab Emirates\">United Arab Emirates</option>\n" +
    "                <option value=\"United Kingdom\">United Kingdom</option>\n" +
    "                <option value=\"United States\">United States</option>\n" +
    "                <option value=\"United States Minor Outlying Islands\">United States Minor Outlying Islands</option>\n" +
    "                <option value=\"Uruguay\">Uruguay</option>\n" +
    "                <option value=\"Uzbekistan\">Uzbekistan</option>\n" +
    "                <option value=\"Vanuatu\">Vanuatu</option>\n" +
    "                <option value=\"Venezuela\">Venezuela</option>\n" +
    "                <option value=\"Viet Nam\">Viet Nam</option>\n" +
    "                <option value=\"Virgin Islands, British\">Virgin Islands, British</option>\n" +
    "                <option value=\"Virgin Islands, U.S.\">Virgin Islands, U.S.</option>\n" +
    "                <option value=\"Wallis and Futuna\">Wallis and Futuna</option>\n" +
    "                <option value=\"Western Sahara\">Western Sahara</option>\n" +
    "                <option value=\"Yemen\">Yemen</option>\n" +
    "                <option value=\"Zambia\">Zambia</option>\n" +
    "                <option value=\"Zimbabwe\">Zimbabwe</option>\n" +
    "              </select>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-map-marker fa-lg fa-fw\"></i></span>\n" +
    "              <select class=\"form-control input-lg\"  data-smart-validate-input data-required name=\"city\">\n" +
    "                <option value=\"\" selected=\"selected\">Select City</option>\n" +
    "                <option>Amsterdam</option>\n" +
    "                <option>Atlanta</option>\n" +
    "                <option>Baltimore</option>\n" +
    "                <option>Boston</option>\n" +
    "                <option>Buenos Aires</option>\n" +
    "                <option>Calgary</option>\n" +
    "                <option>Chicago</option>\n" +
    "                <option>Denver</option>\n" +
    "                <option>Dubai</option>\n" +
    "                <option>Frankfurt</option>\n" +
    "                <option>Hong Kong</option>\n" +
    "                <option>Honolulu</option>\n" +
    "                <option>Houston</option>\n" +
    "                <option>Kuala Lumpur</option>\n" +
    "                <option>London</option>\n" +
    "                <option>Los Angeles</option>\n" +
    "                <option>Melbourne</option>\n" +
    "                <option>Mexico City</option>\n" +
    "                <option>Miami</option>\n" +
    "                <option>Minneapolis</option>\n" +
    "              </select>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-4\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-envelope-o fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" placeholder=\"Postal Code\" type=\"text\" name=\"postal\"  data-smart-validate-input data-required data-minlength=\"4\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div class=\"row\">\n" +
    "        <div class=\"col-sm-6\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-phone fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" data-smart-masked-input=\"+99 (999) 999-9999\" data-mask-placeholder=\"X\"\n" +
    "                     placeholder=\"+1\" type=\"text\" name=\"wphone\"  data-smart-validate-input data-required data-minlength=\"10\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-6\">\n" +
    "          <div class=\"form-group\">\n" +
    "            <div class=\"input-group\">\n" +
    "              <span class=\"input-group-addon\"><i class=\"fa fa-mobile fa-lg fa-fw\"></i></span>\n" +
    "              <input class=\"form-control input-lg\" data-smart-masked-input=\"+99 (999) 999-9999\" data-mask-placeholder=\"X\"\n" +
    "                     placeholder=\"+01\" type=\"text\" name=\"hphone\"  data-smart-validate-input data-required data-minlength=\"10\">\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"tab-pane\" data-smart-wizard-pane=\"3\" >\n" +
    "      <br>\n" +
    "\n" +
    "      <div class=\"alert alert-info fade in\">\n" +
    "        <button class=\"close\" data-dismiss=\"alert\">\n" +
    "          ×\n" +
    "        </button>\n" +
    "        <i class=\"fa-fw fa fa-info\"></i>\n" +
    "        <strong>Info!</strong> Place an info message box if you wish.\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label>This is a label</label>\n" +
    "        <input class=\"form-control input-lg\" placeholder=\"Another input box here...\" type=\"text\" name=\"etc\" id=\"etc\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    <div class=\"tab-pane\" data-smart-wizard-pane=\"4\" >\n" +
    "      <br>\n" +
    "\n" +
    "      <h1 class=\"text-center text-success\"><strong><i class=\"fa fa-check fa-lg\"></i> Complete</strong></h1>\n" +
    "      <h4 class=\"text-center\">Click next to finish</h4>\n" +
    "      <br>\n" +
    "      <br>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <div class=\"form-actions\">\n" +
    "    <div class=\"row\">\n" +
    "      <div class=\"col-sm-12\">\n" +
    "        <ul class=\"pager wizard no-margin hidden-lg hidden-md\">\n" +
    "          <li class=\"previous\" data-smart-wizard-prev>\n" +
    "            <a href=\"#\" class=\"btn btn-lg btn-default\"> Previous </a>\n" +
    "          </li>\n" +
    "          <li class=\"next\" data-smart-wizard-next>\n" +
    "            <a href=\"#\" class=\"btn btn-lg txt-color-darken\"> Next </a>\n" +
    "          </li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "</form>"
  );


  $templateCache.put('app/partials/form/ratingUser.html',
    "<div class=\"modal-fade\" id=\"ratingUser\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\"  data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "\n" +
    "                <div>\n" +
    "                    <button type=\"button\" ng-click=\"close()\" class=\"close\" style=\"padding-top: 35px\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                            aria-hidden=\"true\">&times;</span></button>\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"center-rating\">\n" +
    "\n" +
    "                <rating class=\"ui-rating size-h3 ui-rating-warning \"\n" +
    "                        ng-model=\"rate.start\"\n" +
    "                        max=\"5\" ng-click=\"getSetStatus()\"\n" +
    "                        on-hover=\"hoveringOver(value)\"\n" +
    "                        on-leave=\"overStar = null\"\n" +
    "                        state-on=\"'fa fa-star'\"\n" +
    "                        state-off=\"'fa fa-star-o'\"></rating>\n" +
    "\n" +
    "                </div>\n" +
    "                <div  class=\"text-center col-md-12 col-xs-12\">\n" +
    "                    <label data-i18n=\"very bad\" class=\"non-display very-bad\"></label>\n" +
    "                    <label data-i18n=\"bad\" class=\"non-display bad\"></label>\n" +
    "                    <label data-i18n=\"normal\" class=\"non-display normal\"></label>\n" +
    "                    <label data-i18n=\"good\" class=\"non-display good\"></label>\n" +
    "                    <label data-i18n=\"very good\" class=\"non-display very-good\"></label>\n" +
    "                </div>\n" +
    "\n" +
    "\n" +
    "                <div class=\"col-md-12\">\n" +
    "\n" +
    "                    <label for=\"title\"><span data-i18n=\"review\"\n" +
    "                            >Nhận xét</span></label>\n" +
    "\n" +
    "                </div>\n" +
    "                <div class=\"col-md-12\">\n" +
    "                    <textarea\n" +
    "                            ng-model=\"rate.comment\"\n" +
    "                            class=\"form-control input-lg padding-bot-proposal\"\n" +
    "                            data-i18n=\"i18n-placeholder\"\n" +
    "                            placeholder=\"comment rate\" maxlength=\"255\"\n" +
    "                            rows=\"5\" ></textarea>\n" +
    "                </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup\">\n" +
    "            <div class=\"col-xs-12 hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <div class=\" pull-right\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancel()\" data-i18n=\"cancel\">Hủy</a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"save()\" data-i18n=\"confirm\">Đồng ý</a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/rejectProposal.html',
    "<div class=\"modal-fade\" id=\"paymentPopup\">\n" +
    "\n" +
    "    <div class=\"modal-dialog\" id=\"rejectProposal\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div>\n" +
    "\n" +
    "                <div>\n" +
    "                    <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                            aria-hidden=\"true\">&times;</span></button>\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\" data-i18n=\"info reject so\">Bạn có chắc chắn loại bỏ đề nghị này không?</div>\n" +
    "                <div class=\"content-popup\" data-i18n=\"when reject so\">\n" +
    "                    Việc từ chối yêu cầu này hệ thống của chúng tôi sẽ giảm uy tín của bạn.\n" +
    "                    Bạn có chắc chắn muốn từ chối đề nghị này.\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-footer footer-popup\">\n" +
    "            <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <div class=\"pull-right\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancel()\" data-i18n=\"cancel\">Hủy</a>\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"reject()\" data-i18n=\"confirm\">Từ chối</a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/rejectSupplier.html',
    "<div id=\"rejectSupplier\" class=\"modal-fade\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" style=\"padding-top: 30px;\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"no-padding col-xs-12\">\n" +
    "                    <span>Bạn không muốn nhà cung cấp này làm việc cho yêu cầu cho bạn?</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmReject()\" data-i18n=\"confirm\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelReject()\" data-i18n=\"cancel\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/form/showListCV.html',
    "<div id=\"showListCV\" class=\"modal-fade modal-lg\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" style=\"padding-top: 30px;\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <table datatable-basic on-selection=\"viewDetailCV\" table-options=\"listCV\" show-hidden\n" +
    "                       class=\"datatable table table-striped table-bordered table-hover\" mouseover-apply=\"Desktop\" >\n" +
    "                    <thead>\n" +
    "                    <tr>\n" +
    "                        <th class=\"text-center verticalMiddle\"><span data-i18n=\"first name\"></span></th>\n" +
    "                        <th class=\"text-center verticalMiddle\"><span data-i18n=\"gender\"></span></th>\n" +
    "                        <th class=\"text-center verticalMiddle\"><span data-i18n=\"experience year\"></span></th>\n" +
    "                        <th class=\"text-center verticalMiddle\"><span data-i18n=\"skills\"></span></th>\n" +
    "                        <th data-hide=\"phone,tablet,desktop\"><span data-i18n=\"yob\"></span></th>\n" +
    "                        <th data-hide=\"phone,tablet,desktop\"><span data-i18n=\"join time\"></span></th>\n" +
    "                        <th data-hide=\"phone,tablet,desktop\"><span data-i18n=\"note\"></span></th>\n" +
    "                    </tr>\n" +
    "                    </thead>\n" +
    "                </table>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/form/signupFaceBook.html',
    "<!DOCTYPE html>\n" +
    "<!--\n" +
    "To change this license header, choose License Headers in Project Properties.\n" +
    "To change this template file, choose Tools | Templates\n" +
    "and open the template in the editor.\n" +
    "-->\n" +
    "<div class=\"modal-fade\" id =\"signupFacebook\">\n" +
    "    <div class=\"modal-content  modal-dialogSignup-mobile modal-dialogsignup\">\n" +
    "        <div class=\"modal-dialog\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "\n" +
    "                <h3 class=\"modal-title\">\"Chào mừng đến với Vaber.com\" </h3>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body \">\n" +
    "\n" +
    "\n" +
    "                    <div class=\" margin-row-mobile margin-row\">\n" +
    "                        <h3 class=\"text-center\">\n" +
    "\n" +
    "                        </h3>\n" +
    "                        <div class=\"width-mobile1 col-md-6\">\n" +
    "\n" +
    "                            <div class=\"input-group form-step\">\n" +
    "                                <span class=\"input-group-addon input-lg\"><label for=\"email\" class=\"glyphicon glyphicon-envelope\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "                                <input type=\"text\" name=\"email\" ng-model= \"account.email\" class=\"form-control input-lg \" disabled>\n" +
    "                            </div>\n" +
    "\n" +
    "\n" +
    "                            <div class=\"input-group form-step\">\n" +
    "                                <span class=\"input-group-addon input-lg\"><label for=\"username\" class=\"glyphicon glyphicon-user\" rel=\"tooltip\" title=\"username\"></label></span>\n" +
    "                                <input type=\"text\" name=\"username\" ng-model=\"account.name\" class=\"form-control input-lg\">\n" +
    "                            </div>\n" +
    "                            <br>\n" +
    "                            <div class=\"btn-group btn-radio\">\n" +
    "\n" +
    "                                <button type=\"button\" class=\"btn btn-lg btn-checkbox\" >\n" +
    "                                    <label class=\"ui-checkbox\"><input name=\"customer\" ng-model=\"account.customer\" value=\"Employer\" type=\"checkbox\" ><span>Customer</span></label>\n" +
    "                                </button>\n" +
    "                                <button type=\"button\" class=\"btn btn-lg btn-checkbox\">\n" +
    "                                    <label class=\"ui-checkbox\"><input name=\"supplier\" id=\"looking_for_work\" ng-model=\"account.supplier\" value=\"Worker\" type=\"checkbox\" ><span>Supplier</span></label>\n" +
    "                                </button>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"width-mobile2 col-md-6 margin-auto\">\n" +
    "\n" +
    "                            <img src=\"//graph.facebook.com/{{account.id}}/picture?type=large\" alt=\"logo\" style=\"width: 150px; height: 200px;\">\n" +
    "\n" +
    "                        </div>\n" +
    "\n" +
    "                        <div class=\"col-md-12 form-step\">\n" +
    "\n" +
    "                            <div class=\"has-button btn-custom-mobile\">\n" +
    "                                <button class=\"btn btn-lg  btn-custom btn-lock btn-signup btn-action\" ng-click=\"signupPopupClick()\">Sign Up Now</button>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('app/partials/form/upgradeOrder.html',
    "<div id=\"upgradeOrder\" class=\"modal-fade\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" style=\"padding-top: 30px;\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <div class=\"modal-dialog\">\n" +
    "                    <div>\n" +
    "                        <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                             style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div>\n" +
    "                    <div class=\"content-popup\">\n" +
    "                       Chọn dịch vụ nâng cấp\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                        <table datatable-basic id=\"addonService\" no-hightlight table-options=\"serviceTable\" show-hidden\n" +
    "                               class=\"table table-striped table-bordered table-hover serviceTable\" on-selection=\"validateAddon\"\n" +
    "                               width=\"100%\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"selected\"></span></th>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"kind\"></span></th>\n" +
    "                                <th data-hide=\"phone,tablet,desktop\"><span data-i18n=\"description sevice\"></span></th>\n" +
    "                                <th class=\"text-center\"><span data-i18n=\"price\"></span></th>\n" +
    "                                <th data-hide=\"phone,tablet,desktop\"><span data-i18n=\"quantity\"></span></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"col-xs-12 pad-bot-10\">\n" +
    "                        <div class=\"pull-left\">\n" +
    "                            <span><b>Tổng số tiền:</b></span>\n" +
    "                        </div>\n" +
    "                        <div class=\"pull-right\">\n" +
    "                            <span><b>{{totalPayment}} VNĐ</b></span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"confirmPayment()\" data-i18n=\"confirm\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancelPayment()\" data-i18n=\"cancel\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/form/viewContactDetail.html',
    "<div class=\"modal-fade\" id=\"viewContactDetail\" xmlns=\"http://www.w3.org/1999/html\">\n" +
    "\n" +
    "    <div class=\"modal-dialog no-padding-bot\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "        <div class=\"modal-body \">\n" +
    "            <div>\n" +
    "                <div>\n" +
    "                    <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span\n" +
    "                            aria-hidden=\"true\">&times;</span></button>\n" +
    "                    <img class=\"center-block logo-header form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                         style=\"width: 50%;\"> <span class=\"ng-binding\"></span>\n" +
    "\n" +
    "                    <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\">\n" +
    "                    <span data-i18n=\"complete trasaction\">Bạn đã hoàn thành giao dịch: </span>&nbsp;<span ng-bind =\"serviceOrderTitle\" class=\"font-1\"> Âm nhạc 123</span>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\">\n" +
    "                   <span data-i18n=\"cost complete\"> Giá bạn đã đặt cho giao dịch: </span>&nbsp;<span ng-bind =\"expectedCost\" class=\"font-1\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\" >\n" +
    "                   <span data-i18n=\"please confirm\"> Bạn vui lòng chờ liên lạc từ khách hàng hoặc trực tiếp liên hệ với khách hàng theo thông tin bên dưới</span>\n" +
    "                </div>\n" +
    "                <div class=\"content-popup\"><span  data-i18n=\"customer\"></span>: <span class=\"font-1\">{{aliasName}}</span></div>\n" +
    "                <div class=\"content-popup\"><span data-i18n=\"phone number\"></span>: <span class=\"font-1\" ng-bind=\"phoneNo\">013256898</span></div>\n" +
    "                <div class=\"content-popup\"><span data-i18n=\"email\"></span>: <span class=\"font-1\" ng-bind=\"email\">onggiavuitinh@gmail.com</span></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer footer-popup no-padding-top\">\n" +
    "            <div class=\"hr-divider center-block form-group\"><span class=\"hr-divider-text\"></span></div>\n" +
    "            <div class=\"pull-right\">\n" +
    "\n" +
    "                <a class=\"btn btn-sm btn-primary\" data-ng-click=\"cancel()\" data-i18n=\"close\">Đóng</a>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/form/warningPayment.html',
    "<div class=\"modal-fade\" id=\"paymentPopup\" ng-init=\"init()\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <button type=\"button\" ng-click=\"close()\" class=\"close\" data-dismiss=\"modal\"\n" +
    "                        aria-label=\"Close\"><span\n" +
    "                        aria-hidden=\"true\">&times;</span></button>\n" +
    "                <img class=\"center-block form-group\" src=\"images/logo.png\" alt=\"logo\"\n" +
    "                     style=\"width: 50%;\">\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div>\n" +
    "                    <div>\n" +
    "                       <span data-i18n=\"warning payment1\"></span>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"content-popup\">\n" +
    "                       <span data-i18n=\"warning payment2\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer footer-popup\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"paymentLater()\" data-i18n=\"payment later\"></a>\n" +
    "                    <a class=\"btn btn-sm btn-primary\" data-ng-click=\"paymentNow()\" data-i18n=\"payment\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/partials/header.html',
    "<header class=\"clearfix\" ng-controller='HeaderCtrl' ng-init=\"init()\">\n" +
    "\t<!-- Logo -->\n" +
    "\t<div class=\"logo\">\n" +
    "\t\t<a href=\"#/default\"> <img src=\"images/logo.png\" alt=\"logo\"> <span>{{main.brand}}</span> </a>\n" +
    "\t</div>\n" +
    "\n" +
    "\t<!-- needs to be put after logo to make it working-->\n" +
    "\t<div class=\"menu-button none-border\" toggle-off-canvas>\n" +
    "\t\t<span class=\"icon-bar\"></span>\n" +
    "\t\t<span class=\"icon-bar\"></span>\n" +
    "\t\t<span class=\"icon-bar\"></span>\n" +
    "\t</div>\n" +
    "\n" +
    "\t<div class=\"top-nav\">\n" +
    "        <ul class=\"nav-left list-unstyled \" roles=\"all\">\n" +
    "\t\t\t<li>\n" +
    "\t\t\t\t<a toggle-off-canvas ><i class=\"fa fa-bars\"></i></a>\n" +
    "\t\t\t</li>\n" +
    "\t\t</ul>\n" +
    "\t\t<ul class=\"nav-left pull-right list-unstyled padding-7\" roles>\n" +
    "\t\t\t<li>\n" +
    "\t\t\t\t<button class=\"header-login-bt btn\" id=\"login-normal\" role=\"button\" data-toggle=\"modal\"  ng-click=\"loginClick()\" data-i18n=\"login\">\n" +
    "\t\t\t\t\tLogin\n" +
    "\t\t\t\t</button>\n" +
    "\t\t\t</li>\n" +
    "\t\t\t<li>\n" +
    "\t\t\t\t<button class=\"header-signup-bt btn\" id=\"sign-up\" role=\"button\" data-toggle=\"modal\" ng-click=\"signupClick()\" data-i18n=\"sign up\">Sign Up</button>\n" +
    "\t\t\t</li>\n" +
    "\t\t\t<!--<li>-->\n" +
    "\t\t\t\t<!--<button id=\"header-post-project\" class=\"btn btn-primary btn-small \" ng-click=\"makeOrderClick()\">Tạo yêu cầu</button>-->\n" +
    "\t\t\t<!--</li>-->\n" +
    "\t\t</ul>\n" +
    "\n" +
    "\t\t<ul class=\"nav-left pull-right list-unstyled\" roles = \"all\" >\n" +
    "\t\t\t<li class=\"dropdown\">\n" +
    "\t\t\t\t<a href=\"javascript:;\" class=\"dropdown-toggle \" data-toggle=\"dropdown\">\n" +
    "                    <i class=\"fa fa-comment-o\"></i> <span class=\"badge badge-info\" ng-show=\"chat_notifications.length > 0\">{{ chat_notifications.length }}</span> </a>\n" +
    "\t\t\t\t<div class=\"dropdown-menu pull-right with-arrow\">\n" +
    "\t\t\t\t\t<div class=\"panel-heading\">\n" +
    "\t\t\t\t\t\tBạn có {{ chat_notifications.length }} tin nhắn mới.\n" +
    "\t\t\t\t\t</div>\n" +
    "\t\t\t\t\t<ul class=\"list-group\" data-slim-scroll scroll-height=\"200px\" scroll-width=\"300px\">\n" +
    "\t\t\t\t\t\t<li class=\"list-group-item\" ng-repeat=\"notification in chat_notifications\" ng-click=\"openChat();\">\n" +
    "\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"media\"> <span class=\"pull-left media-icon\"> <span class=\"square-icon sm bg-info\"><i class=\"fa fa-comment-o\"></i></span> </span>\n" +
    "\t\t\t\t\t\t\t<div class=\"media-body\">\n" +
    "\t\t\t\t\t\t\t\t<span class=\"block\">{{ notification.title }}</span>\n" +
    "\t\t\t\t\t\t\t\t<span class=\"text-muted\">{{ notification.time | date: 'HH:mm dd/MM/yyyy' }}</span>\n" +
    "\t\t\t\t\t\t\t</div> </a>\n" +
    "\t\t\t\t\t\t</li>\n" +
    "\t\t\t\t\t</ul>\n" +
    "\t\t\t\t\t<div class=\"panel-footer\">\n" +
    "\t\t\t\t\t\t<a href=\"javascript:;\" ng-click=\"openChat();\">Xem tất cả tin nhắn.</a>\n" +
    "\t\t\t\t\t</div>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</li>\n" +
    "\t\t\t<li class=\"dropdown no-border\">\n" +
    "\t\t\t\t<a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"fa fa-bell-o nav-icon\"></i> <span class=\"badge badge-info\" ng-show=\"notifications.news.true > 0\">{{ notifications.news.true }}</span> </a>\n" +
    "\t\t\t\t<div class=\"dropdown-menu pull-right with-arrow\">\n" +
    "                        <div class=\"panel-heading\">Bạn có {{ notifications.news.true }} thông báo mới.</div>\n" +
    "\t\t\t\t\t<ul class=\"list-group\" data-slim-scroll scroll-height=\"300px\" scroll-width=\"300px\">\n" +
    "\t\t\t\t\t\t<li class=\"list-group-item\" ng-repeat=\"notification in notifications.messages track by $index\">\n" +
    "\t\t\t\t\t\t\t<a data-ng-click=\"displayNotification(notification)\" class=\"media\">\n" +
    "                                <span class=\"pull-left media-icon\">\n" +
    "                                    <span class=\"square-icon sm bg-success\">\n" +
    "                                        <i class=\"fa fa-bell-o\"></i>\n" +
    "                                    </span>\n" +
    "                                </span>\n" +
    "                                <div class=\"media-body\">\n" +
    "                                    <span class=\"block\" ng-class=\"{'text-bold': notification.is_read === false}\" >{{ notification.title }}</span>\n" +
    "                                    <span class=\"text-muted block\">{{ notification.time | date: 'HH:mm dd/MM/yyyy' }}</span>\n" +
    "                                </div>\n" +
    "                            </a>\n" +
    "\t\t\t\t\t\t</li>\n" +
    "                    </ul>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t</li>\n" +
    "\t\t\t<li class=\"dropdown text-normal nav-profile\">\n" +
    "                <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <img id=\"avatar\"\n" +
    "                                                                                            ng-src=\"{{myImage}}\" alt=\"\"\n" +
    "                                                                                            class=\"img-circle img30_30\"><span class=\"fa fa-caret-down\"></span></a>\n" +
    "\t\t\t\t<ul class=\"dropdown-menu dropdown-dark pull-right with-arrow\">\n" +
    "\t\t\t\t\t<li>\n" +
    "                        <a href=\"#/updateuser\"> <i class=\"fa fa-user\"></i> <span data-i18n=\"My Profile\"></span> </a>\n" +
    "\t\t\t\t\t</li>\n" +
    "                    <li id=\"btn-switchRole\">\n" +
    "                        <a href=\"javascript:;\" ng-click=\"switchRole();\">\n" +
    "                            <i class=\"fa fa-exchange\"></i>\n" +
    "                            <span data-i18n=\"switch user\"></span>\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "\t\t\t\t\t<li>\n" +
    "\t\t\t\t\t\t<a ng-click=\"logoutClick()\"> <i class=\"fa fa-sign-out\"></i> <span data-i18n=\"Log Out\"></span> </a>\n" +
    "\t\t\t\t\t</li>\n" +
    "\t\t\t\t</ul>\n" +
    "\t\t\t</li>\n" +
    "\t\t</ul>\n" +
    "\t</div>\n" +
    "\n" +
    "</header>\n" +
    "<script type=\"text/ng-template\" id=\"loginPopup.html\">\n" +
    "\t<div class=\"modal-fade\" id =\"loginPopup\">\n" +
    "\t\t<div class=\"modal-dialog\" ng-controller='loginPopup' id =\"loginPopup\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "\t\t\t<div class=\"modal-header\">\n" +
    "\n" +
    "\t\t\t\t<h3 class=\"modal-title\">Login Cber.com!</h3>\n" +
    "\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"modal-body\">\n" +
    "\n" +
    "\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-block btn-facebook btn-custom\" ng-click=\"authenticate('facebook')\">\n" +
    "\t\t\t\t\t<span class=\"icon-facebook\"></span>\n" +
    "\t\t\t\t\t<span>Login with Facebook</span>\n" +
    "\t\t\t\t</button>\n" +
    "\n" +
    "\t\t\t\t<div class=\"hr-divider\"><span class=\"hr-divider-text\">OR</span></div>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"email\" class=\"glyphicon glyphicon-user\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"text\" ng-model=\"username\" class=\"form-control input-lg\" placeholder=\"Username or Email\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"password\" class=\"glyphicon glyphicon-lock\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"password\" class=\"form-control input-lg\" ng-model=\"password\" placeholder=\"Password\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t<div class=\"checkbox\">\n" +
    "\t\t\t\t\t<label class=\"remember-me\">\n" +
    "\t\t\t\t\t\tRemember me<input id=\"savelogin\" type=\"checkbox\" name=\"savelogin\">\n" +
    "\t\t\t\t\t</label>\n" +
    "\t\t\t\t</div>\n" +
    "\t\t\t\t<button class=\"btn btn-primary btn-lg btn-block btn-custom\" ng-click=\"showProjects()\">Login</button>\n" +
    "\t\t\t\t<div class=\"form-step has-icon><a href=\"#\" class=\"forgot-password-toggle\" id=\"forgot-password-toggle\">Forgot password?</a></div>\n" +
    "\t\t</div >\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\t</div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"signupPopup.html\">\n" +
    "\t<div class=\"modal-fade\" id =\"signupPopup\">\n" +
    "\t\t<div class=\"modal-dialog\" ng-controller='signupPopup' id =\"signupPopup\" data-backdrop=\"static\" data-keyboard=\"false\">\n" +
    "\n" +
    "\n" +
    "\t\t\t<div class=\"modal-header\">\n" +
    "\t\t\t\t<h3 class=\"modal-title\">Sign Up Free Today!</h3>\n" +
    "\n" +
    "\t\t\t</div>\n" +
    "\n" +
    "\t\t\t<div class=\"modal-body\" ng-controller='signupPopup' id=\"loginPopup\">\n" +
    "\t\t\t\t<button type=\"button\" class=\"btn btn-lg btn-block btn-facebook\" ng-click=\"authenticate('facebook')\">\n" +
    "\t\t\t\t\t<span class=\"icon-facebook\"></span>\n" +
    "\t\t\t\t\t<span>Login with Facebook</span>\n" +
    "\t\t\t\t</button>\n" +
    "\n" +
    "\t\t\t\t<div class=\"hr-divider\"><span class=\"hr-divider-text\">OR</span></div>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"email\" class=\"glyphicon glyphicon-envelope\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"text\" ng-model=\"username\" class=\"form-control input-lg\" placeholder=\"Email\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"email\" class=\"glyphicon glyphicon-user\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"text\" ng-model=\"username\" class=\"form-control input-lg\" placeholder=\"Username\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"password\" class=\"glyphicon glyphicon-lock\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"password\" class=\"form-control input-lg\" ng-model=\"password\" placeholder=\"Password\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"input-group\">\n" +
    "\t\t\t\t\t<span class=\"input-group-addon input-lg\"><label for=\"password\" class=\"glyphicon glyphicon-lock\" rel=\"tooltip\" title=\"email\"></label></span>\n" +
    "\t\t\t\t\t<input type=\"password\" class=\"form-control input-lg\" ng-model=\"password\" placeholder=\"Re-Type Password\">\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\n" +
    "\t\t\t\t<div class=\"form-step has-label text-center\">\n" +
    "\t\t\t\t\t<strong>I am </strong>\n" +
    "\t\t\t\t\t<label class=\"btn btn-plain active\">\n" +
    "\t\t\t\t\t\t<span class=\"radio\">\n" +
    "\t\t\t\t\t\t<input type=\"radio\" name=\"looking_for\" id=\"looking_to_hire\" value=\"Employer\" data-role=\"none\">Customer</span>\n" +
    "\t\t\t\t\t</label><label class=\"btn btn-plain\">\n" +
    "\t\t\t\t\t\t<span class=\"radio\">\n" +
    "\t\t\t\t\t\t<input type=\"radio\" name=\"looking_for\" id=\"looking_for_work\" value=\"Worker\" data-role=\"none\">Supplier</span>\n" +
    "\t\t\t\t</label>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"form-step has-button text-center\">\n" +
    "\t\t\t\t\t<button class=\"btn btn-success btn-lg .btn-lock .btn-signup\" ng-click=\"signupPopupClick()\">Sign Up Now</button>\n" +
    "\t\t\t\t</div>\n" +
    "\n" +
    "\t\t\t\t<div class=\"hr-divider\"><span class=\"hr-divider-text\"></span></div>\n" +
    "\t\t\t\t<p class= \"form-group text-center\">Bằng việc đăng kí, bạn xác nhận đã đồng ý với</br> <a href=\"#\" data-section-toggle=\"signup\">Điều khoản và điều kiện </a>và <a href=\"#\" target=\"_blank\"> Chính sách quyền riêng tư </a>\n" +
    "\t\t\t\t</p>\n" +
    "\t\t\t</div>\n" +
    "\t\t</div>\n" +
    "\n" +
    "\t</div>\n" +
    "\n" +
    "</script>\n" +
    "<!--<script type=\"text/javascript\">\n" +
    "      $(function(){\n" +
    "    $(\".dropdown\").hover(            \n" +
    "            function() {\n" +
    "                $('.dropdown-menu', this).stop( true, true ).fadeIn(\"fast\");\n" +
    "                $(this).toggleClass('open');\n" +
    "                $('b', this).toggleClass(\"caret caret-up\");                \n" +
    "            },\n" +
    "            function() {\n" +
    "                $('.dropdown-menu', this).stop( true, true ).fadeOut(\"fast\");\n" +
    "                $(this).toggleClass('open');\n" +
    "                $('b', this).toggleClass(\"caret caret-up\");                \n" +
    "            });\n" +
    "    });\n" +
    "    \n" +
    "</script>-->"
  );


  $templateCache.put('app/partials/nav.html',
    "<div id=\"nav-wrapper-vertical\" data-ng-controller=\"nav\" ng-init=\"init();\">\n" +
    "    <div class=\"col-xs-12 hidden-md hidden-lg hidden-sm border-bot-grey\" roles=\"all\">\n" +
    "        <span id=\"iconDown\" class=\"fa fa-caret-down non-display\"></span>\n" +
    "        <div class=\"col-xs-4\">\n" +
    "            <ul class=\"ul-pad-mar\">\n" +
    "                <li class=\"dropdown text-normal nav-profile\">\n" +
    "                    <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <img id=\"avatarNav\"\n" +
    "                                                                                                ng-src=\"{{myImage}}\"\n" +
    "                                                                                                alt=\"\"\n" +
    "                                                                                                class=\"img-circle img40_40\">\n" +
    "                        <span class=\"hidden-xs\"></span> </a>\n" +
    "                    <ul id=\"btn-switchRoleMobile\" class=\"dropdown-menu with-arrow\">\n" +
    "                        <li>\n" +
    "                            <a href=\"javascript:;\" ng-click=\"switchRole();\">\n" +
    "                                <i class=\"fa fa-exchange\"></i>\n" +
    "                                <span data-i18n=\"switch user\"></span>\n" +
    "                            </a>\n" +
    "                        </li>\n" +
    "                        <!--<li>-->\n" +
    "                        <!--<a href=\"javascript:;\"> <i class=\"fa fa-sign-out\"></i> <span-->\n" +
    "                        <!--data-i18n=\"Cập nhật hồ sơ\"></span> </a>-->\n" +
    "                        <!--</li>-->\n" +
    "                    </ul>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-8\">\n" +
    "            <div class=\"label-username\"><a href=\"#/updateuser\" title=\"{{username}}\"><span>{{username|truncate}}</span></a></div>\n" +
    "            <div class=\"label-logout\"><a href=\"javascript:;\" ng-click=\"logoutClicks();\"><span\n" +
    "                    data-i18n=\"log out\"></span></a></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-xs-12 hidden-lg hidden-md hidden-sm no-padding height-50\" roles>\n" +
    "        <div class=\"logo border-bottom\">\n" +
    "            <a href=\"#/default\"> <img src=\"images/logo.png\" alt=\"logo\"></a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"top-nav col-xs-12 no-padding hidden-lg hidden-md hidden-sm border-bot-grey\">\n" +
    "        <ul class=\"nav-left list-unstyled mar-left-5\">\n" +
    "            <li class=\"dropdown\" roles=\"all\">\n" +
    "                <a href=\"javascript:;\" title=\"Tin nhắn\" class=\"dropdown-toggle \" data-toggle=\"dropdown\"> <i class=\"fa fa-comment-o\"></i>\n" +
    "                    <span class=\"badge badge-info\" roles=\"all\" ng-show=\"chat_notifications.length > 0\">{{ chat_notifications.length }}</span> </a>\n" +
    "\n" +
    "                <div class=\"dropdown-menu pull-left with-arrow\">\n" +
    "                    <div class=\"panel-heading \">\n" +
    "                        Bạn có {{ chat_notifications.length }} tin nhắn mới.\n" +
    "                    </div>\n" +
    "                    <ul class=\"list-group\" data-slim-scroll scroll-height=\"200px\" scroll-width=\"300px\">\n" +
    "                        <li class=\"list-group-item\" ng-repeat=\"notification in chat_notifications\" ng-click=\"openChat();\">\n" +
    "                            <a href=\"javascript:;\" class=\"media\">\n" +
    "                                <span class=\"pull-left media-icon\"> <span class=\"square-icon sm bg-info\"><i class=\"fa fa-comment-o\"></i></span> </span>\n" +
    "                                <div class=\"media-body\">\n" +
    "                                    <span class=\"block\">{{notification.title}}</span>\n" +
    "                                    <span class=\"text-muted\">{{notification.time | date: 'HH:mm dd/MM/yyyy'}}</span>\n" +
    "                                </div>\n" +
    "                            </a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                    <div class=\"panel-footer\">\n" +
    "                        <a href=\"javascript:;\" ng-click=\"openChat();\">Xem tất cả tin nhắn.</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "            <li class=\"dropdown no-border\" roles=\"all\">\n" +
    "                <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"fa fa-bell-o nav-icon\"></i> <span class=\"badge badge-info\" ng-show=\"notifications.news.true > 0\">{{ notifications.news.true }}</span> </a>\n" +
    "                <div class=\"dropdown-menu pull-left with-arrow\">\n" +
    "                    <div class=\"panel-heading\">Bạn có {{ notifications.news.true }} thông báo.</div>\n" +
    "                    <ul class=\"list-group\" data-slim-scroll scroll-height=\"200px\" scroll-width=\"200px\">\n" +
    "                        <li class=\"list-group-item\" ng-repeat=\"notification in notifications.messages track by $index\">\n" +
    "                            <a data-ng-click=\"displayNotification(notification)\" class=\"media\">\n" +
    "                                <span class=\"pull-left media-icon\">\n" +
    "                                    <span class=\"square-icon sm bg-success\">\n" +
    "                                        <i class=\"fa fa-bell-o\"></i>\n" +
    "                                    </span>\n" +
    "                                </span>\n" +
    "                                <div class=\"media-body\">\n" +
    "                                    <span class=\"block\" ng-class=\"{'text-bold': notification.is_read === false}\" >{{ notification.title }}</span>\n" +
    "                                    <span class=\"text-muted block\">{{ notification.time | date: 'HH:mm dd/MM/yyyy' }}</span>\n" +
    "                                </div>\n" +
    "                            </a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <img class=\"img40_30 pad-left-15 chat-design  mar-left-20\" src=\"images/chat1.png\" title=\"Liên hệ hỗ trợ\" alt=\"logo\" ng-click=\"openChat()\">\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <div class=\"col-xs-12 no-padding\" title=\"Chuyển đổi ngôn ngữ\">\n" +
    "                    <a data-ng-show=\"lang === 'Vietnam' \" href=\"javascript:;\" data-ng-click=\"setLang('English')\">\n" +
    "                        <div class=\"flag flags-american margin-l-25 mar-left-80 flag-design\"></div>\n" +
    "                    </a>\n" +
    "                    <a data-ng-show=\"lang == 'English' \" href=\"javascript:;\" data-ng-click=\"setLang('Vietnam')\">\n" +
    "                        <div class=\"flag flags-vietnam margin-l-25 mar-left-80 flag-design\"></div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "    <!--<div class=\"col-xs-12 hidden-lg border-bot-grey\">-->\n" +
    "        <!--<div class=\"col-xs-6 text-center border-r\">-->\n" +
    "            <!--<img class=\"img30_30\" src=\"images/chat1.png\" alt=\"logo\" ng-click=\"openChat()\">-->\n" +
    "\n" +
    "        <!--</div>-->\n" +
    "        <!--<div class=\"col-xs-6\">-->\n" +
    "            <!--<a data-ng-show=\"lang === 'Vietnam' \" href=\"javascript:;\" data-ng-click=\"setLang('English')\">-->\n" +
    "                <!--<div class=\"flag flags-vietnam margin-l-25\"></div>-->\n" +
    "            <!--</a>-->\n" +
    "            <!--<a data-ng-show=\"lang == 'English' \" href=\"javascript:;\" data-ng-click=\"setLang('Vietnam')\">-->\n" +
    "                <!--<div class=\"flag flags-american margin-l-25\"></div>-->\n" +
    "            <!--</a>-->\n" +
    "        <!--</div>-->\n" +
    "    <!--</div>-->\n" +
    "\n" +
    "    <div class=\"col-xs-12 no-padding\">\n" +
    "\n" +
    "        <ul id=\"nav\"\n" +
    "            data-slim-scroll\n" +
    "            data-highlight-active>\n" +
    "\n" +
    "            <li ng-repeat=\"root in menu.data\" class=\"{{root.active}} border-bot-grey\" role=\"parent\"\n" +
    "                roles=\"{{root.roles}}\">\n" +
    "                <a data-ng-click=\"redirect(root.url)\" class=\"subnav-link cursor-pointer\"><i class=\"{{root.icon}}\"></i><span\n" +
    "                        data-i18n=\"{{root.name}}\"></span></a>\n" +
    "                <ul class=\"sub-menu\">\n" +
    "                    <li ng-repeat=\"children in root.children\" ng-if=\"root.children.length > 0\"\n" +
    "                        class=\"{{children.active}} border-bot-grey-grey\" role=\"children\"\n" +
    "                        roles=\"{{children.roles}}\">\n" +
    "                        <a data-ng-click=\"redirect(children.url)\" class=\"sub-subnav-link cursor-pointer\"><i class=\"{{children.icon}}\"></i>&nbsp;&nbsp;<span\n" +
    "                                data-i18n=\"{{children.name}}\"></span></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div class=\"ui-widget ui-chatbox chatbox-support\" ng-show=\"chatBox.isShow\" outline=\"0\">\n" +
    "        <div class=\"ui-widget-header ui-chatbox-titlebar {{ online }} ui-dialog-header\">\n" +
    "            <span><i title=\"{{ online }}\"></i><span data-i18n=\"support\"></span></span>\n" +
    "            <a ng-href=\"javascript:;\" ng-click=\"closeChat()\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Close\"\n" +
    "               class=\"ui-chatbox-icon\" role=\"button\">\n" +
    "                <i class=\"fa fa-times\"></i>\n" +
    "            </a>\n" +
    "            <a ng-href=\"javascript:;\" ng-click=\"hideChat()\" rel=\"tooltip\" data-placement=\"top\" data-original-title=\"Minimize\"\n" +
    "               class=\"ui-chatbox-icon\" role=\"button\">\n" +
    "                <i class=\"fa fa-minus\"></i>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div class=\"false ui-widget-content ui-chatbox-content footer-chatbox-content\" ng-hide=\"chatBox.isHidden\">\n" +
    "            <div class=\"chatboxcontent discussion\" ng-show=\"!user.user_id\">\n" +
    "                <form ng-submit=\"startChat(user_id, user_email)\" class=\"ng-pristine ng-valid  panel-body\">\n" +
    "                    <input type=\"text\" ng-model=\"user_id\" data-i18n=\"i18n-placeholder\" placeholder=\"your name\" class=\"form-control input-sm footer-input-text\" />\n" +
    "                    <input type=\"email\" ng-model=\"user_email\" data-i18n=\"i18n-placeholder\" placeholder=\"email address\" class=\"form-control input-sm footer-input-text\" />\n" +
    "                    <button type=\"submit\" class=\"btn btn-primary btn-sm text-center footer-btn\" data-i18n=\"start chat\"></button>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "            <form ng-submit=\"sendMessage()\" class=\"ng-pristine ng-valid\" ng-show=\"user.user_id\">\n" +
    "                <div id=\"cha4\" class=\"ui-widget-content ui-chatbox-log custom-scroll\">\n" +
    "                    <div class=\"ui-chatbox-msg footer-chatbox-msg\" ng-repeat=\"mess in messages\" ng-show=\"chat_id\">\n" +
    "                        <b>{{ user.session_id === mess.sender.user_id ? 'Tôi' : mess.sender.username }}: </b> <span ng-bind-html=\"mess.message\"></span>\n" +
    "                    </div>\n" +
    "                    <div ng-hide=\"chat_id\">\n" +
    "                        Hiện tại chúng tôi đang không có ở đây, vui lòng quay trở lại sau !!!\n" +
    "                        <a href=\"javascript:;\" ng-click=\"findSupporter()\">Hoặc thử liên hệ lại</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"has-feedback ui-widget-content ui-chatbox-input\">\n" +
    "                    <div class=\"input-group\">\n" +
    "                        <input type=\"text\" class=\"form-control btn-chat-send\" ng-model=\"chatBox.message\" ng-disabled=\"!chat_id\"/>\n" +
    "                    <span class=\"input-group-btn\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary btn-chat-send\" data-i18n=\"send\"></button>\n" +
    "                    </span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n"
  );


  $templateCache.put('app/partials/projects.html',
    "<div class=\"page-dashboard\">\r" +
    "\n" +
    "  <div class=\"app-body\" ng-class=\"{loading: loading}\">\r" +
    "\n" +
    "    <div ng-cloak ng-repeat='alert in dashAlerts.list track by $index' class=\"alert-{{alert.severity}} dashboard-notice\" ng-show=\"$last\">\r" +
    "\n" +
    "      <button type=\"button\" class=\"close\" ng-click=\"dashAlerts.clear(alert)\" style=\"padding-right:50px\">\r" +
    "\n" +
    "        &times;\r" +
    "\n" +
    "      </button>\r" +
    "\n" +
    "      <strong ng-bind-html=\"alert.title\">&nbsp;</strong><span ng-bind-html='alert.text'></span>\r" +
    "\n" +
    "      <div style=\"padding-right:10px\" class='pull-right small'> {{$index + 1}} alert(s)</div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div ng-show=\"loading\" class=\"app-content-loading\">\r" +
    "\n" +
    "      <i class=\"fa fa-spinner fa-spin loading-spinner\"></i>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"app-content\">\r" +
    "\n" +
    "      <!--<div ng-cloak ng-view></div>-->\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </div>\r" +
    "\n" +
    "  <!-- Rows -->\r" +
    "\n" +
    "  <div class=\"mobile-row\" ng-controller=\"RowCtrl\" ng-repeat=\"(row_name, row) in dashboard.current.rows\">\r" +
    "\n" +
    "    <div style=\"padding-top:0px\" ng-if=\"!row.collapse\">\r" +
    "\n" +
    "      <!-- Panels -->\r" +
    "\n" +
    "      <div ng-repeat=\"(name, panel) in row.panels|filter:isPanel\" ng-hide=\"panel.hide\" class=\"panel nospace\" ng-model=\"row.panels\" >\r" +
    "\n" +
    "        <!-- Content Panel -->\r" +
    "\n" +
    "        <div style=\"position:relative\">\r" +
    "\n" +
    "          <mobile-panel type=\"panel.type\" ng-cloak></mobile-panel>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div class=\"clearfix\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );
 }]);