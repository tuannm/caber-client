( function() {'use strict';
		/**
		 *Strict Mode is a new feature in ECMAScript 5 that allows you to
		 * place a program, or a function, in a "strict" operating context.
		 * This strict context prevents certain actions from being taken
		 * and throws more exceptions.
		 *
		 * Strict mode helps out in a couple ways:

		 It catches some common coding bloopers, throwing exceptions.
		 It prevents, or throws errors, when relatively "unsafe" actions are taken
		 (such as gaining access to the global object).
		 It disables features that are confusing or poorly thought out.
		 */
		var _uri, hasModule = ( typeof module !== 'undefined' && module.exports);
		/*jslint regexp: true, white: true, maxerr: 50, indent: 2 */
		function parseURI(url) {
			var m = String(url).replace(/^\s+|\s+$/g, '').match(/^([^:\/?#]+:)?(\/\/(?:[^:@]*(?::[^:@]*)?@)?(([^:\/?#]*)(?::(\d*))?))?([^?#]*)(\?[^#]*)?(#[\s\S]*)?/);
			// authority = '//' + user + ':' + pass '@' + hostname + ':' port
			return ( m ? {
				href : m[0] || '',
				protocol : m[1] || '',
				authority : m[2] || '',
				host : m[3] || '',
				hostname : m[4] || '',
				port : m[5] || '',
				pathname : m[6] || '',
				search : m[7] || '',
				hash : m[8] || ''
			} : null);
		}

		function removeDotSegments(input) {
			var output = [];
			input.replace(/^(\.\.?(\/|$))+/, '').replace(/\/(\.(\/|$))+/g, '/').replace(/\/\.\.$/, '/../').replace(/\/?[^\/]*/g, function(p) {
				if (p === '/..') {
					output.pop();
				} else {
					output.push(p);
				}
			});
			return output.join('').replace(/^\//, input.charAt(0) === '/' ? '/' : '');
		}

		function extend(a, b) {
			for (var i in b) {
				if (b.hasOwnProperty(i)) {
					a[i] = b[i];
				}
			}
			return a;
		}

		_uri = extend(this, {
			get : function(base, href) {// RFC 3986
				href = parseURI(href || '');
				base = parseURI(base || '');
				return !href || !base ? null : (href.protocol || base.protocol) + (href.protocol || href.authority ? href.authority : base.authority) + removeDotSegments(href.protocol || href.authority || href.pathname.charAt(0) === '/' ? href.pathname : (href.pathname ? ((base.authority && !base.pathname ? '/' : '') + base.pathname.slice(0, base.pathname.lastIndexOf('/') + 1) + href.pathname) : base.pathname)) + (href.protocol || href.authority || href.pathname ? href.search : (href.search || base.search)) + href.hash;
			},
			object : function(query) {
				var qryStringArr = encodedURL.split('&');
				var jsonobj = {}, paramvalue = '';
				for ( i = 0; i < qryStringArr.length; i++) {
					paramvalue = qryStringArr[i].split('=');
					jsonobj[paramvalue[0]] = paramvalue[1];
				}
				return jsonobj;
			},
			encode : function(object) {
				return jQuery.param(object);
			},	
			add: function(key, value, url) {
				if (!url)
					url = window.location.href;
				var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");
		
				if (re.test(url)) {
					if ( typeof value !== 'undefined' && value !== null)
						return url.replace(re, '$1' + key + "=" + value + '$2$3');
					else {
						var hash = url.split('#');
						url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
						if ( typeof hash[1] !== 'undefined' && hash[1] !== null)
							url += '#' + hash[1];
						return url;
					}
				} else {
					if ( typeof value !== 'undefined' && value !== null) {
						var separator = url.indexOf('?') !== -1 ? '&' : '?', hash = url.split('#');
						url = hash[0] + separator + key + '=' + value;
						if ( typeof hash[1] !== 'undefined' && hash[1] !== null)
							url += '#' + hash[1];
						return url;
					} else
						return url;
				}
			}
		});

		// CommonJS module is defined
		if (hasModule) {
			module.exports = _uri;
		}

		/*global ender:false */
		if ( typeof ender === 'undefined') {
			// here, `this` means `window` in the browser, or `global` on the server
			// add `moment` as a global object via a string identifier,
			// for Closure Compiler "advanced" mode
			this['_uri'] = _uri;
		}
		/*global define:false */
		if ( typeof define === "function" && define.amd) {
			define("uriparser", [], function() {
				return _uri;
			});
		}
	}.call(this));
