/**
 * Satellizer 0.9.4
 * (c) 2015 Sahat Yalkabov
 * License: MIT
 */
(function (window, angular) {
    "use strict";

    angular.module("auth", [])
        .constant("AUTH_EVENTS", {
            loginSuccess: "auth-login-success",
            loginFailed: "auth-login-failed",
            logoutSuccess: "auth-logout-success",
            sessionTimeout: "auth-session-timeout",
            notAuthenticated: "auth-not-authenticated",
            notAuthorized: "auth-not-authorized",
            serviceUnavailable: "service-unavailable"
        })
        .constant("AUTH_CONFIG", {
            defaultUrl: "/default",
            errorUrl: "/error",
            successUrl: "/success"
        })
        .constant("USER_ROLES", {
            all: "*",
            admin: "ROLE_ADMIN",
            guest: "ROLE_GUEST",
            customer: "ROLE_CUSTOMER",
            supplier: "ROLE_SUPPLIER",
            supporter: "ROLE_ADMIN",
            broker: "ROLE_SUPPLIER&userType=B"


        })
        .factory("AuthService", function ($http, Session, base64) {
            var authService = {};

            authService.authenticate = function (credentials) {
                Session.create(credentials.session_id, credentials.user_id, credentials.user_role, credentials.multi, credentials.accessToken, credentials.data);
                localStorage.setItem("credentials", base64.encode(JSON.stringify(credentials)));
            };

            authService.logout = function () {
                Session.destroy();
            };

            authService.isAuthenticated = function () {
                return !!Session.userId;
            };

            authService.isAuthorized = function (authorizedRoles) {
                if (!angular.isArray(authorizedRoles)) {
                    authorizedRoles = [authorizedRoles];
                }
                return (authService.isAuthenticated() &&
                authorizedRoles.indexOf(Session.userRole) !== -1);
            };

            return authService;
        })
        .service("Session", function () {
            this.userRole = "guest";
            this.create = function (sessionId, userId, userRole, multi, accessToken, data) {
                this.id = sessionId;
                this.userId = userId;
                this.userRole = userRole;
                this.data = data;
                this.multi = multi;
                this.accessToken = accessToken;
            };
            this.changeRole = function() {
                this.userRole = this.userRole == "customer" ? "supplier" : "customer";
            };
            this.destroy = function () {
                this.id = null;
                this.userId = null;
                this.data = null;
                this.userRole = "guest";
                this.multi = null;
                this.accessToken = null;
                localStorage.removeItem("credentials");
            };
        })
        .run(function ($rootScope, USER_ROLES, AUTH_EVENTS, AUTH_CONFIG,  AuthService, Session, $location, base64 ) {
            $rootScope.$on('$stateChangeStart', function (event, next) {
                if (!AuthService.isAuthenticated() && localStorage.getItem('credentials')) {
                    AuthService.authenticate(JSON.parse(base64.decode(localStorage.getItem('credentials'))));
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                }
                var authorizedRoles = "all";
                if (next && next.authorizedRoles) {
                    authorizedRoles = next.authorizedRoles;
                }
                if (authorizedRoles !== "all" && !AuthService.isAuthorized(authorizedRoles) && authorizedRoles !== "guest") {
                    event.preventDefault();
                    if (AuthService.isAuthenticated()) {
                        // user is not allowed
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                    } else {
                        // user is not logged in
                        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                    }
                } else if (AuthService.isAuthenticated() && authorizedRoles === "guest") {
                    event.preventDefault();
                    $location.path(AUTH_CONFIG.defaultUrl);
                }
            });
        }).config(function ($httpProvider) {
            $httpProvider.interceptors.push([
                "$injector",
                function ($injector) {
                    return $injector.get("AuthInterceptor");
                }
            ]);
        })
        .factory("AuthInterceptor", function ($rootScope, $q, AUTH_EVENTS) {
            return {
                responseError: function (response) {
                    $rootScope.$broadcast({
                        401: AUTH_EVENTS.notAuthenticated,
                        403: AUTH_EVENTS.notAuthorized,
                        419: AUTH_EVENTS.sessionTimeout,
                        440: AUTH_EVENTS.sessionTimeout,
                        502: AUTH_EVENTS.serviceUnavailable,
                        503: AUTH_EVENTS.serviceUnavailable,
                        0: AUTH_EVENTS.serviceUnavailable
                    }[response.status], response);
                    return $q.reject(response);
                },
                response: function (response) {
                    if(response.data && !response.data.result){
                        if(response.data.error && response.data.error.code){
                            $rootScope.$broadcast({
                                401: AUTH_EVENTS.notAuthenticated,
                                403: AUTH_EVENTS.notAuthorized,
                                419: AUTH_EVENTS.sessionTimeout,
                                498: AUTH_EVENTS.sessionTimeout,
                                440: AUTH_EVENTS.sessionTimeout,
                                502: AUTH_EVENTS.serviceUnavailable,
                                503: AUTH_EVENTS.serviceUnavailable
                            }[response.data.error.code], response.data.error);
                            return $q.reject(response);
                        }
                    }
                    return response;
                }
            };
        })
        .directive("roles", function ($compile, AuthService, AUTH_EVENTS) {
            return {
                restrict: "A",
                link: function (scope, element, attributes) {
                    var checkElement = function () {
                        if (attributes["roles"].length <= 0) {
                            if (AuthService.isAuthenticated()) {
                                $(element).css("display", "none");
                            } else {
                                $(element).css("display", "block");
                            }
                        } else {
                            var value = attributes["roles"].split(",");
                            if (AuthService.isAuthorized(value) || (AuthService.isAuthenticated() && attributes["roles"] === "all")) {
                                $(element).css("display", "block");
                            } else {
                                $(element).css("display", "none");
                            }
                        }
                    };
                    scope.$on(AUTH_EVENTS.loginSuccess, function () {
                        checkElement();
                    });
                    scope.$on(AUTH_EVENTS.logoutSuccess, function () {
                        checkElement();
                    });

                    checkElement();
                }
            };

        });

})(window, window.angular);
