<div class="col-xs-12 no-padding padding-bot-10 margin-top-5 padding-l-r-4">
    <a href="tel:+84901701701" class="btn btn-primary pad10 col-xs-12 hidden-md hidden-lg hidden-sm"><span
    class="glyphicon glyphicon-earphone"></span>&nbsp;<span>Gọi hỗ trợ khách hàng</span></a>
</div>
<div class="clearfix hidden-md hidden-lg hidden-sm col-xs-12"></div>
    <div class="col-xs-12 no-padding padding-bot-5 padding-l-r-4">
        <a id="toggerInfo" href="javascript:void(0);" data-ng-click="toggerInfo()"
        class="btn btn-primary pad10 col-xs-12 hidden-md hidden-lg hidden-sm"><span class="fa fa-eye"></span>&nbsp;<span>Xem thông tin hỗ trợ</span></a>
    </div>

    <div class="col-sm-8">
        <h4 class="hidden-xs">Xem thông tin hỗ trợ</h4>

        <div id="helpInfo" class="hidden-xs">
            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để đăng ký tài khoản
                Vaber?</a>

                <div class="col-md-12 non-display margin-top-5 question-help">
                    <p>Nếu bạn không có tài khoản Vaber, bạn có thể đăng ký tài khoản bằng một vài bước</p>

                    <p>1. Truy cập Vaber.vn</p>

                    <p>2. Nhấn vào Đăng ký ở đầu trang</p>

                    <div class="col-md-12 col-xs-12 padding-bot-10">
                        <img src="images/I.png" class="img-thumbnail">
                        </div>

                        <p>3. Điền địa chỉ email, Tên đăng nhập, Số điện thoại, Mật khẩu và chọn loại tài khoản bạn muốn
                        đăng ký.</p>

                        <p>4. Nhấn vào Tạo tài khoản</p>

                    </div>
                </div>


                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                    <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để đổi mật khẩu của tôi?</a>

                    <div class="col-md-12 non-display margin-top-5 question-help">
                        <p>Nếu bạn đã đăng nhập vào tài khoản Vaber, bạn có thể đổi mật khẩu của mình bằng cách</p>

                        <p>1. Ở menu, nhấp vào Cập nhật hồ sơ</p>

                        <p>2. Chọn tab Thiết lập tài khoản</p>

                        <div class="col-md-12 col-xs-12 padding-bot-10">
                            <img src="images/II.png" class="img-thumbnail">
                            </div>

                            <p>3. Nhập mật khẩu hiện tại và mật khẩu mới của bạn</p>

                            <p>4. Bấm vào Lưu</p>

                            <p>Nếu quên mật khẩu, bạn có thể đặt lại mật khẩu</p>


                        </div>
                    </div>

                    <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                        <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để đặt lại mật khẩu?</a>

                        <div class="col-md-12 non-display margin-top-5 question-help">
                            <p>Nếu bạn không thể đăng nhập vào Vaber và cần đặt lại mật khẩu của mình</p>

                            <p>1. Truy cập trang đăng nhập vào Vaber.vn</p>

                            <p>2. Bấm vào Quên mật khẩu ở gần nút Đăng nhập</p>

                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                <img src="images/III.png" class="img-thumbnail">
                                </div>

                                <p>3. Nhập địa chỉ email, sau đó bấm Lấy lại mật khẩu</p>

                                <p>4. Hệ thống sẽ gửi đường dẫn để đặt lại mật khẩu vào địa chỉ email bạn vừa nhập</p>

                                <p>5. Đăng nhập vào email, bấm vào đường dẫn trong email do Vaber gửi đến và đặt lại mật khẩu mới.</p>


                            </div>
                        </div>

                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                            <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để thêm loại tài khoản?</a>

                            <div class="col-md-12 non-display margin-top-5 question-help">
                                <p>Nếu tài khoản của bạn chỉ có 1 quyền Khách hàng/Nhà cung cấp. Bạn muốn tài khoản của bạn có cả 2 quyền Khách
                                hàng và Nhà cung cấp?</p>

                                <p>1. Đăng nhập vào Vaber</p>

                                <p>2. Ở menu, chọn Cập nhật hồ sơ</p>

                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                    <img src="images/IV.png" class="img-thumbnail">
                                    </div>

                                    <p>3. Ở tab Thiết lập tài khoản, chọn Loại tài khoản muốn thêm</p>

                                    <p>4. Bấm Lưu</p>

                                </div>
                            </div>

                            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để đăng yêu cầu?</a>

                                <div class="col-md-12 non-display margin-top-5 question-help">
                                    <p>1. Đăng nhập vào Vaber với loại tài khoản Customer</p>

                                    <p>2. Ở menu, chọn Đăng yêu cầu</p>

                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                        <img src="images/V.png" class="img-thumbnail">
                                        </div>

                                        <p>3. Nhập các thông tin về yêu cầu của bạn</p>

                                        <p>4. Bấm nút Đăng ngay để đăng yêu cầu</p>

                                        <p>5. Nếu bạn muốn yêu cầu được chi tiết hơn, dễ dàng hấp hẫn các nhà cung cấp đặt giá, bấm nút Nâng cao để điền
                                        thêm thông tin.</p>

                                    </div>
                                </div>

                                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                    <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để cập nhật kỹ năng?</a>

                                    <div class="col-md-12 non-display margin-top-5 question-help">
                                        <p>1. Đăng nhập vào Vaber với loại tài khoản Nhà cung cấp</p>

                                        <p>2. Nếu lần đầu tiên đăng nhập, bạn có thể liệt kê kỹ năng ở màn hình hiển thị đầu tiên. Hoặc lựa chọn Cập
                                        nhật kỹ năng ở menu bên tay trái</p>


                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                            <img src="images/VI_.png" class="img-thumbnail">
                                            </div>

                                            <p>3. Chọn lĩnh vực ngành nghề của bạn: </p>

                                            <div class="col-md-12 col-xs-12">
                                                <p>- Việc nhanh </p>

                                                <p>- Nhân sự</p>

                                                <p>- Giúp việc</p>

                                                <p>- Gia sư</p>

                                                <p>- Yêu cầu khác</p>
                                            </div>


                                            <p>4. Chọn kỹ năng phù hợp với bạn. </p>

                                            <p>5. Bấm nút Hoàn thành</p>

                                        </div>
                                    </div>

                                    <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để cập nhật thông tin cá nhân?</a>

                                        <div class="col-md-12 non-display margin-top-5 question-help">
                                            <p>1. Đăng nhập vào Vaber.vn</p>

                                            <p>2. Ở menu, chọn Cập nhật hồ sơ</p>

                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                <img src="images/VII.png" class="img-thumbnail">
                                                </div>

                                                <p>3. Ở tab Thông tin tài khoản, nhập thông tin cá nhân của bạn</p>

                                                <p>4. Bấm nút Lưu</p>
                                            </div>
                                        </div>

                                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                            <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào để đưa đề xuất cho 1 yêu cầu?</a>

                                            <div class="col-md-12 non-display margin-top-5 question-help">
                                                <p>1. Đăng nhập vào Vaber.vn với loại tài khoản Nhà cung cấp</p>

                                                <p>2. Ở menu, chọn Quản lý đề xuất</p>

                                                <p>3. Ở tab Danh sách yêu cầu, chọn Yêu cầu ở trạng thái Đang mở để đặt giá.</p>

                                                <p>4. Bấm nút Đặt giá</p>


                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                    <img src="images/quan ly de xuat.png" class="img-thumbnail">
                                                    </div>

                                                    <p>5. Hiển thị màn hình chi tiết yêu cầu, nhập thông tin đặt giá:</p>

                                                    <div class="col-md-12 col-xs-12">
                                                        <p>- Tiêu đề đề xuất</p>

                                                        <p>- Mô tả chi tiết đề xuất </p>

                                                        <p>- Giá đề xuất</p>

                                                        <p>- Đính kèm hồ sơ (CV)</p>

                                                    </div>

                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                        <img src="images/detailOrder.png" class="img-thumbnail">
                                                        </div>

                                                        <p>6. Bấm nút Đặt giá</p>
                                                    </div>
                                                </div>

                                                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                    <a class="action-show-detail color-primary cursor-point">&#9679; Làm cách nào đề trao yêu cầu cho nhà cung cấp?</a>

                                                    <div class="col-md-12 non-display margin-top-5 question-help">
                                                        <p>1. Đăng nhập vào Vaber.vn với loại tài khoản Khách hàng</p>

                                                        <p>2. Ở menu, chọn Quản lý yêu cầu</p>

                                                        <p>3. Ở tab Danh sách yêu cầu, chọn Yêu cầu ở trạng thái Đang đặt giá</p>


                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                            <img src="images/detailpost.png" class="img-thumbnail">
                                                            </div>

                                                            <p>4. Bấm nút Xem chi tiết</p>

                                                            <p>5. Tại màn hình Chi tiết yêu cầu, hiển thị danh sách đặt giá</p>

                                                            <p>6. Chọn 1 hoặc nhiều đề xuất đề trao yêu cầu</p>

                                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                <img src="images/viewdetailpost.png" class="img-thumbnail">
                                                                </div>

                                                                <p>7. Bấm nút Trao yêu cầu</p>

                                                                <p>8. Bạn sẽ được yêu cầu thanh toán 1 khoản phí Vaber . Đối với 1 order, bạn có thể trao cho nhiều nhà cung cấp
                                                                chỉ với 1 lần thanh toán phí Vaber.</p>

                                                                <p>9. Sau khi thanh toán thành công, yêu cầu của bạn đã được trao và đang chờ nhà cung cấp chấp nhận yêu cầu
                                                                đó.</p>

                                                            </div>
                                                        </div>

                                                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                            <a class="action-show-detail color-primary cursor-point">&#9679; Làm thế nào để thanh toán phí Vaber?</a>

                                                            <div class="col-md-12 non-display margin-top-5 question-help">
                                                                <p>1. Khi bạn trao yêu cầu hay chấp nhận yêu cầu, bạn sẽ được chuyển đến màn hình Nganluong.vn</p>

                                                                <p>2. Chọn hình thức bạn muốn thanh toán</p>

                                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                    <img src="images/payment1.png" class="img-thumbnail">
                                                                    </div>

                                                                    <p>3. Nhập thông tin người thanh toán</p>

                                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                        <img src="images/payment2.png" class="img-thumbnail">
                                                                        </div>

                                                                        <p>4. Bấm nút Tiếp tục</p>

                                                                        <p>5. Nhập thông tin tài khoản, rồi bấm nút Thanh toán</p>

                                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                            <img src="images/payment3.png" class="img-thumbnail">
                                                                            </div>

                                                                            <p>6. Bấm OK để tiếp tục xác nhận thông tin thanh toán</p>

                                                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                <img src="images/payment4.png" class="img-thumbnail">
                                                                                </div>

                                                                                <p>7. Sau khi thanh toán thành công, bạn hãy chờ đợi đến khi tự động chuyển về Vaber.vn</p>

                                                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                    <img src="images/payment5.png" class="img-thumbnail">
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                                                <a class="action-show-detail color-primary cursor-point">&#9679; Các thông báo lỗi khi thanh toán?</a>

                                                                                <div class="col-md-12 non-display margin-top-5 question-help">
                                                                                    <p>Một số lỗi thường gặp trong quá trình thanh toán</p>

                                                                                    <p>1. Bạn chọn phương thức thanh toán online bằng thẻ ATM, thẻ ngân hàng nội địa. Sau khi thanh toán thành công,
                                                                                    vui lòng không đóng trình duyệt chờ đợi cho đến khi màn hình tự động chuyển về Vaber.vn</p>

                                                                                    <p>2. Giao dịch bị từ chối bởi ngân hàng phát hành thẻ -> Việc thanh toán của bạn chưa thành công, hãy thử lại
                                                                                    sau ít phút</p>

                                                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                        <img src="images/error1.png" class="img-thumbnail">
                                                                                        </div>

                                                                                        <p>3. Truy cập bị từ chối</p>

                                                                                        <p>Trong trường hợp tài khoản của bạn đã bị trừ tiền, nhưng lại hiển thị thông báo lỗi “Truy cập bị từ chối”,
                                                                                        hãy
                                                                                        gọi điện hoặc gửi email đến bộ phận Chăm sóc khách hàng của Vaber để được hỗ trợ</p>

                                                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                            <img src="images/error2.png" class="img-thumbnail">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-sm-4 text-center hidden-xs ">
                                                                                <img class="img150_150" src="images/telephone_support1.jpg">
                                                                                    <h5 class="color-danger">Đường dây nóng: 0901701701</h5>
                                                                                    <h5 class="color-danger">Email: support@vaber.vn</h5>
                                                                                </div>

                                                                                <script>
                                                                                $(document).ready(function() {
                                                                                    $('.action-show-detail').click(function () {
                                                                                        $('.question-help').slideUp();
                                                                                        if($(this).parent().find(".question-help").is(":hidden")){
                                                                                            $(this).parent().find(".question-help").slideDown();
                                                                                        } else{
                                                                                            $(this).parent().find(".question-help").slideUp();
                                                                                        }



                                                                                    });

                                                                                    $("#toggerInfo").click(function(){
                                                                                    if($("#helpInfo").hasClass("hidden-xs")){
                                                                                    $("#helpInfo").removeClass("hidden-xs");
                                                                                    } else{
                                                                                    $("#helpInfo").addClass("hidden-xs");
                                                                                    }
                                                                                    })
                                                                                    });

                                                                                </script>