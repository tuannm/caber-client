
<p >
<strong>1. </strong><strong>Quy đ</strong><strong>ị</strong><strong>nh chung:</strong></p>
<p >
1.1.Việc bạn sử dụng trang web v&agrave; c&aacute;c dịch vụ, phần mềm v&agrave; c&aacute;c sản phẩm của Vaber.vn (gọi chung l&agrave; &quot;Dịch vụ&quot; sau đ&acirc;y) l&agrave; t&ugrave;y thuộc v&agrave;o c&aacute;c điều khoản v&agrave; điều kiện trong t&agrave;i liệu n&agrave;y cũng như c&aacute;c ch&iacute;nh s&aacute;ch bảo mật, c&aacute;c văn bản liệt k&ecirc; ch&iacute;nh s&aacute;ch v&agrave; bất kỳ quy tắc kh&aacute;c v&agrave; ch&iacute;nh s&aacute;ch kh&aacute;c của Vaber.vn. T&agrave;i liệu n&agrave;y v&agrave; c&aacute;c quy định, ∙∙ch&iacute;nh s&aacute;ch của Vaber.vn được xem l&agrave; &quot;điều khoản&quot;. Bằng c&aacute;ch truy cập c&aacute;c trang web hoặc sử dụng dịch vụ, bạn đồng &yacute; chấp nhận v&agrave; bị r&agrave;ng buộc bởi c&aacute;c điều khoản. Xin vui l&ograve;ng kh&ocirc;ng sử dụng dịch vụ hoặc trang web nếu bạn kh&ocirc;ng chấp nhận tất cả c&aacute;c điều khoản.</p>
<p >
1.2.Bạn kh&ocirc;ng thể sử dụng Dịch vụ v&agrave; kh&ocirc;ng chấp nhận c&aacute;c điều khoản nếu (a) bạn kh&ocirc;ng đủ tuổi ph&aacute;p l&yacute; để tạo th&agrave;nh một hợp đồng r&agrave;ng buộc với Vaber.vn, hoặc (b) bạn kh&ocirc;ng được ph&eacute;p nhận bất kỳ dịch vụ theo ph&aacute;p luật của Việt Nam hoặc c&aacute;c quốc gia / khu vực kh&aacute;c bao gồm c&aacute;c quốc gia / v&ugrave;ng m&agrave; bạn đang cư tr&uacute; hoặc nơi bạn đang sử dụng c&aacute;c dịch vụ.</p>
<p >
1.3.Vaber c&oacute; thể sửa đổi bất k&igrave; điều khoản bất cứ l&uacute;c n&agrave;o bằng c&aacute;ch đăng c&aacute;c thay đổi c&oacute; li&ecirc;n quan v&agrave; điều khoản tại trang web, bạn đồng &yacute; rằng c&aacute;c điều khoản n&agrave;y sẽ được &aacute;p dụng cho bạn.</p>
<p >
1.4.Bạn c&oacute; k&yacute; kết một thỏa thuận ri&ecirc;ng biệt (k&yacute; kết trực tuyến hoặc k&yacute; kết bản cứng), với Vaber.vn hoặc li&ecirc;n kết của ch&uacute;ng t&ocirc;i đối với bất kỳ dịch vụ (Sau đ&acirc;y gọi l&agrave; &quot;Hiệp định bổ sung&quot;), nếu c&oacute; bất kỳ xung đột hay m&acirc;u thuẫn giữa c&aacute;c Điều khoản v&agrave; Hiệp định bổ sung, Hiệp định bổ sung sẽ được ưu ti&ecirc;n hơn c&aacute;c điều khoản trong mối quan hệ dịch vụ với c&aacute;c b&ecirc;n li&ecirc;n quan.</p>
<p >
1.5.C&aacute;c điều khoản kh&ocirc;ng thể nếu kh&ocirc;ng được sửa đổi ngoại trừ bằng văn bản của một vi&ecirc;n chức c&oacute; thẩm quyền của Vaber.vn.</p>
<p >
<strong>2. </strong><strong>D</strong><strong>ị</strong><strong>ch v</strong><strong>ụ</strong><strong> c</strong><strong>ủ</strong><strong>a Vaber:</strong></p>
<p >
2.1.Th&ocirc;ng qua website Vaber.vn, Vaber cung cấp c&aacute;c dịch vụ của m&igrave;nh, bao gồm:</p>
<p >
a) Đối với t&agrave;i khoản đăng y&ecirc;u cầu (Sau đ&acirc;y gọi l&agrave; t&agrave;i khoản kh&aacute;ch h&agrave;ng):</p>
<p>
&nbsp;&nbsp;&nbsp- Vaber cung cấp c&aacute;c dịch vụ miễn ph&iacute; sau đ&acirc;y:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Quyền đăng y&ecirc;u cầu/ hủy y&ecirc;u cầu miễn ph&iacute;.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Quyền quản l&yacute; y&ecirc;u cầu đ&atilde; đăng</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Tiếp nhận, ph&ecirc; duyệt, từ chối c&aacute;c đề xuất.</p>
<p>
&nbsp;&nbsp;&nbsp- Vaber cung cấp mất ph&iacute; c&aacute;c dịch vụ sau đ&acirc;y:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ C&aacute;c dịch vụ n&acirc;ng cao bao gồm: B&ocirc;i đậm đỏ y&ecirc;u cầu, Gửi email theo số lượng t&agrave;i khoản kh&aacute;ch h&agrave;ng y&ecirc;u cầu, Gửi SMS theo số lượng t&agrave;i khoản y&ecirc;u cầu. Ph&iacute; dịch vụ được c&ocirc;ng bố trực tiếp tại t&agrave;i khoản kh&aacute;ch h&agrave;ng.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Ph&iacute; giao dịch: L&agrave; khoản ph&iacute; m&agrave; kh&aacute;ch h&agrave;ng trả cho vaber để biết th&ocirc;ng tin người đề xuất. Ph&iacute; giao dịch được chi trả một lần duy nhất khi kh&aacute;ch h&agrave;ng chọn đề xuất đầu ti&ecirc;n, Vaber kh&ocirc;ng thu ph&iacute; giao dịch cho c&aacute;c đề xuất được chấp nhận tiếp theo.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ C&aacute;c dịch vụ kh&aacute;c được c&ocirc;ng bố trực tiếp tại website Vaber.vn v&agrave; c&aacute;c thỏa thuận trực tiếp giữa Vaber v&agrave; kh&aacute;ch h&agrave;ng.</p>
<p >
b) C&aacute;c dịch vụ của người đăng đề xuất:</p>
<p>
&nbsp;&nbsp;&nbsp- Vaber cung cấp miễn ph&iacute; c&aacute;c dịch vụ sau đ&acirc;y:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ T&igrave;m kiếm c&aacute;c y&ecirc;u cầu tại Vaber.vn</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Đăng c&aacute;c đề xuất của m&igrave;nh cho kh&aacute;ch h&agrave;ng</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Quản l&yacute; c&aacute;c đề xuất</p>
<p>
&nbsp;&nbsp;&nbsp- Vaber cung cấp mất ph&iacute; c&aacute;c dịch vụ sau đ&acirc;y:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ Ph&iacute; giao dịch: L&agrave; khoản ph&iacute; m&agrave; người đăng đề xuất trả cho Vaber để biết th&ocirc;ng tin của kh&aacute;ch h&agrave;ng . Khoản ph&iacute; n&agrave;y được trả khi kh&aacute;ch h&agrave;ng đồng &yacute; đề xuất của người đề xuất. Khoản ph&iacute; được trả một lần.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ C&aacute;c dịch vụ kh&aacute;c được c&ocirc;ng bố trực tiếp tại website Vaber.vn v&agrave; c&aacute;c thỏa thuận trực tiếp giữa Vaber v&agrave; người đăng đề xuất.</p>
<p>
2.2.Vaber cung cấp dịch vụ của m&igrave;nh th&ocirc;ng qua website Vaber.vn . Bạn cần phải đăng k&iacute; th&agrave;nh vi&ecirc;n tại Vaber.vn để sử dụng c&aacute;c dịch vụ của Vaber. Bằng việc đăng k&iacute; sử dụng t&agrave;i khoản tại Vaber, bạn đồng &yacute; sử dụng c&aacute;c dịch vụ của Vaber v&agrave; c&aacute;c điều khoản quy định k&egrave;m.</p>
<p>
2.3.Vaber.vn c&oacute; thể khởi động, thay đổi, n&acirc;ng cấp, &aacute;p đặt điều kiện, đ&igrave;nh chỉ, hoặc ngăn chặn bất cứ Dịch Vụ (hoặc bất kỳ t&iacute;nh năng n&agrave;o trong dịch vụ) m&agrave; kh&ocirc;ng cần th&ocirc;ng b&aacute;o trước, ngoại trừ trong trường hợp dịch vụ m&agrave; Vaber thu ph&iacute;, thay đổi n&agrave;y sẽ kh&ocirc;ng đ&aacute;ng kể ảnh hưởng xấu đến th&agrave;nh vi&ecirc;n trả tiền trong việc sử dụng dịch vụ đ&oacute;.</p>
<p>
2.4.Vaber.vn c&oacute; quyền chấm dứt, đ&igrave;nh chỉ hoặc ngăn chặn bất cứ Dịch vụ n&agrave;o trong trường hợp ph&aacute;t hiện c&aacute;c h&agrave;nh vi gian dối, cung cấp c&aacute;c th&ocirc;ng tin sai lệch, g&acirc;y hại đến c&aacute;c b&ecirc;n li&ecirc;n quan hay vi phạm ph&aacute;p luật.</p>
<p>
2.5.Trong c&aacute;c trường bất khả kh&aacute;ng (thi&ecirc;n tai, l&yacute; do kh&aacute;ch quan,..), Vaber.vn c&oacute; thể gi&aacute;n đoạn việc cung cấp dịch vụ cho c&aacute;c th&agrave;nh vi&ecirc;n. Tuy nhi&ecirc;n, bạn đồng &yacute; rằng Vaber.vn lu&ocirc;n cố gắng khắc phục c&aacute;c sự cố n&agrave;y một c&aacute;ch nhanh nhất c&oacute; thể, việc gi&aacute;n đoạn cung cấp dịch vụ trong trường hợp bất khả kh&aacute;ng nằm ngo&agrave;i &yacute; muốn của Vaber.</p>
<p>
<strong>3. </strong><strong>Đăng th&ocirc;ng tin t</strong><strong>ạ</strong><strong>i Vaber:</strong></p>
<p>
3.1.C&aacute;c th&ocirc;ng tin về y&ecirc;u cầu v&agrave; đề xuất tại Vaber phải r&otilde; r&agrave;ng, theo đ&uacute;ng c&aacute;c trường th&ocirc;ng tin m&agrave; Vaber y&ecirc;u cầu. Bạn kh&ocirc;ng n&ecirc;n đăng lặp lại c&aacute;c y&ecirc;u cầu của m&igrave;nh để tr&aacute;nh c&aacute;c hiểu lầm v&agrave; sơ s&oacute;t kh&ocirc;ng đ&aacute;ng c&oacute;.</p>
<p>
3.2.Bạn cam kết rằng c&aacute;c th&ocirc;ng tin bạn đăng tại Vaber l&agrave; đ&uacute;ng sự thật, kh&ocirc;ng vi phạm ph&aacute;p luật, kh&ocirc;ng đăng c&aacute;c nội dung k&iacute;ch động, nội dung khi&ecirc;u d&acirc;m, nội dung t&ocirc;n gi&aacute;o, chia rẽ sắc tộc,.., l&agrave; hợp ph&aacute;p v&agrave; hợp lệ với c&aacute;c điều khoản quy định tại Vaber</p>
<p>
3.3.Hệ thống tạo ra c&aacute;c email được gửi đến cho bạn để tạo điều kiện th&ocirc;ng b&aacute;o trạng th&aacute;i của Y&ecirc;u cầu v&agrave; cập nhật c&aacute;c đề xuất của th&agrave;nh vi&ecirc;n theo dịch vụ của Vaber.vn. Nếu bạn kh&ocirc;ng muốn nhận những email n&agrave;y, bạn c&oacute; thể đi trực tiếp v&agrave;o t&agrave;i khoản của bạn v&agrave; hủy dịch vụ bằng c&aacute;ch đ&oacute;ng y&ecirc;u cầu trước đ&oacute; của bạn.</p>
<p>
3.4.Bạn cam kết rằng bạn sẽ kh&ocirc;ng c&ocirc;ng bố bất kỳ th&ocirc;ng tin hoặc thực hiện bất kỳ h&agrave;nh động n&agrave;o vi phạm quyền của bất kỳ b&ecirc;n thứ ba, bao gồm nhưng kh&ocirc;ng giới hạn bất kỳ quyền sở hữu tr&iacute; tuệ, quyền c&ocirc;ng khai, quyền lợi của nh&acirc;n c&aacute;ch, quyền ri&ecirc;ng tư, v&agrave; c&aacute;c quyền kh&aacute;c của b&ecirc;n thứ ba kh&ocirc;ng x&aacute;c định cụ thể tại khoản n&agrave;y.</p>
<p>
3.5.Trong trường hợp bạn đ&atilde; vi phạm bất kỳ điều khoản đ&atilde; n&ecirc;u ở tr&ecirc;n, Vaber.vn c&oacute; quyền thực hiện bất kỳ h&agrave;nh động thực thi ph&aacute;p l&yacute; cần thiết. V&iacute; dụ về c&aacute;c h&agrave;nh động thực thi đ&oacute; bao gồm nhưng kh&ocirc;ng giới hạn để x&oacute;a c&aacute;c thong tin đ&atilde; được đăng của bạn, hạn chế c&aacute;c quyền để gửi c&aacute;c y&ecirc;u cầu của bạn, đưa ra cảnh b&aacute;o cho bạn, hoặc chấm dứt thỏa thuận th&agrave;nh vi&ecirc;n của bạn với Vaber.vn,..</p>
<p>
3.6.C&aacute;c th&ocirc;ng tin, t&agrave;i liệu sau bị cấm đăng tải tại Vaber:</p>
<p >
1) Th&ocirc;ng tin khuyến kh&iacute;ch c&aacute;c hoạt động bất hợp ph&aacute;p.</p>
<p >
2) C&aacute;c hạng mục l&agrave; ph&acirc;n biệt chủng tộc, t&ocirc;n gi&aacute;o v&agrave; x&uacute;c phạm sắc tốc, hoặc th&uacute;c đẩy sự hận th&ugrave;, bạo lực, ph&acirc;n biệt chủng tộc hay t&ocirc;n gi&aacute;o;</p>
<p >
3) Qu&agrave; tặng, xổ số, v&eacute; số hay c&aacute;c cuộc thi.</p>
<p >
4) Cổ phiếu, tr&aacute;i phiếu, l&atilde;i suất đầu tư v&agrave; chứng kho&aacute;n kh&aacute;c; v&agrave;</p>
<p >
5) T&agrave;i liệu khi&ecirc;u d&acirc;m / vật phẩm t&igrave;nh dục.</p>
<p >
6) C&aacute;c th&ocirc;ng tin vi phạm ph&aacute;p luật kh&aacute;c.</p>
<p>
3.7.Bạn x&aacute;c nhận v&agrave; đồng &yacute; rằng Vaber.vn thể cung cấp th&ocirc;ng tin li&ecirc;n lạc của bạn, bao gồm nhưng kh&ocirc;ng giới hạn t&ecirc;n li&ecirc;n lạc, địa chỉ email, điện thoại, điện thoại di động v&agrave; số fax của bạn, để người đề xu&aacute;t li&ecirc;n lạc với bạn cho những cơ hội kinh doanh trong tương lai.</p>
<p>
3.8.Bạn x&aacute;c nhận v&agrave; đồng &yacute; rằng Vaber.vn c&oacute; thể sử dụng th&ocirc;ng tin th&agrave;nh vi&ecirc;n của bạn, bao gồm nhưng kh&ocirc;ng giới hạn đến th&ocirc;ng tin c&aacute; nh&acirc;n của c&ocirc;ng ty, h&igrave;nh ảnh c&aacute; nh&acirc;n, lịch sử giao dịch, th&ocirc;ng tin li&ecirc;n lạc c&aacute; nh&acirc;n, v&agrave; vv, cho việc th&uacute;c đẩy thị trường v&agrave; c&aacute;c dịch vụ tại Vaber.vn</p>
<p>
<strong>4. </strong><strong>Ngh</strong><strong>ĩ</strong><strong>a v</strong><strong>ụ</strong><strong> c</strong><strong>ủ</strong><strong>a th&agrave;nh vi&ecirc;n:</strong></p>
<p>
4.1. Th&agrave;nh vi&ecirc;n:</p>
<p>
a) Người d&ugrave;ng phải được đăng k&yacute; tr&ecirc;n trang web truy cập hoặc sử dụng một số dịch vụ (một t&agrave;i khoản đăng k&yacute; cũng được gọi l&agrave; một &quot;th&agrave;nh vi&ecirc;n&quot; dưới đ&acirc;y). Trừ trường hợp được sự chấp thuận của Vaber.vn, mỗi th&agrave;nh vi&ecirc;n chỉ được đăng k&iacute; một t&agrave;i khoản tại Vaber.vn. Vaber.vn c&oacute; ​​thể hủy bỏ hoặc chấm dứt t&agrave;i khoản th&agrave;nh vi&ecirc;n của một t&agrave;i khoản nếu Vaber.vn c&oacute; ​​l&yacute; do để nghi ngờ rằng t&agrave;i khoản đ&atilde; đăng k&yacute; hoặc kiểm so&aacute;t đồng thời hai hoặc nhiều t&agrave;i khoản th&agrave;nh vi&ecirc;n hoặc vi phạm c&aacute;c quy định tại Vaber.vn. Hơn nữa, Vaber.vn c&oacute; ​​thể từ chối đơn của người sử dụng để đăng k&yacute; v&igrave; bất kỳ l&yacute; do n&agrave;o</p>
<p>
b) Sau khi đăng k&yacute; tr&ecirc;n trang web, mỗi th&agrave;nh vi&ecirc;n được Vaber.vn cấp một t&agrave;i khoản v&agrave; mật khẩu. Một t&agrave;i khoản c&oacute; thể c&oacute; một t&agrave;i khoản email dựa tr&ecirc;n web với kh&ocirc;ng gian lưu trữ giới hạn cho c&aacute;c th&agrave;nh vi&ecirc;n để gửi hoặc nhận email.</p>
<p>
c) Mỗi t&agrave;i khoản v&agrave; mật khẩu được cấp bởi Vaber.vn l&agrave; duy nhất. Mỗi th&agrave;nh vi&ecirc;n phải chịu tr&aacute;ch nhiệm về việc bảo mật v&agrave; an ninh của t&agrave;i khoản v&agrave; mật khẩu của bạn v&agrave; cho tất cả c&aacute;c hoạt động diễn ra trong t&agrave;i khoản của bạn. Th&agrave;nh vi&ecirc;n kh&ocirc;ng được chia sẻ, chuyển nhượng, hoặc cho ph&eacute;p việc sử dụng c&aacute;c t&agrave;i khoản th&agrave;nh vi&ecirc;n, ID hoặc mật khẩu của bạn bởi một người kh&aacute;c b&ecirc;n ngo&agrave;i của tổ chức kinh doanh ri&ecirc;ng của th&agrave;nh vi&ecirc;n. Th&agrave;nh vi&ecirc;n đồng &yacute; th&ocirc;ng b&aacute;o ngay lập tức nếu Vaber.vn biết được bất kỳ sử dụng tr&aacute;i ph&eacute;p mật khẩu hoặc t&agrave;i khoản của bạn hoặc bất kỳ vi phạm bảo mật của t&agrave;i khoản của bạn.</p>
<p>
d) Th&agrave;nh vi&ecirc;n đồng &yacute; rằng tất cả c&aacute;c hoạt động diễn ra trong t&agrave;i khoản của bạn (bao gồm nhưng kh&ocirc;ng giới hạn, đăng bất kỳ c&ocirc;ng ty hay sản phẩm th&ocirc;ng tin, nhấp chấp nhận bất kỳ thỏa thuận kh&aacute;c hoặc quy tắc, hoặc đăng k&yacute; để thực hiện bất kỳ thanh to&aacute;n cho bất kỳ dịch vụ, gửi email bằng c&aacute;ch sử dụng t&agrave;i khoản email hoặc gửi SMS) sẽ được coi l&agrave; đ&atilde; được ủy quyền bởi c&aacute;c th&agrave;nh vi&ecirc;n.</p>
<p>
e) Th&agrave;nh vi&ecirc;n thừa nhận rằng việc chia sẻ t&agrave;i khoản của bạn với người kh&aacute;c, hoặc cho ph&eacute;p nhiều người sử dụng b&ecirc;n ngo&agrave;i tổ chức kinh doanh của bạn để sử dụng t&agrave;i khoản của bạn (gọi chung l&agrave; &quot;đa dụng&quot;), c&oacute; thể g&acirc;y ra thiệt hại kh&ocirc;ng thể khắc phục với Vaber.vn hoặc người sử dụng c&aacute;c trang web. Th&agrave;nh vi&ecirc;n chịu tr&aacute;ch nhiệm bồi thường cho Vaber.vn, c&aacute;c chi nh&aacute;nh của ch&uacute;ng t&ocirc;i, gi&aacute;m đốc, nh&acirc;n vi&ecirc;n, đại l&yacute; v&agrave; đại diện đối với bất kỳ tổn thất hoặc thiệt hại (bao gồm nhưng kh&ocirc;ng giới hạn để mất lợi nhuận) phải chịu đựng như một kết quả của việc sử dụng nhiều t&agrave;i khoản của bạn. Th&agrave;nh vi&ecirc;n cũng đồng &yacute; rằng trong trường hợp việc sử dụng nhiều t&agrave;i khoản của bạn hoặc duy tr&igrave; t&iacute;nh bảo mật của t&agrave;i khoản của bạn, Vaber.vn sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm cho bất kỳ tổn thất hoặc thiệt hại ph&aacute;t sinh từ sự vi phạm đ&oacute; v&agrave; c&oacute; quyền đ&igrave;nh chỉ hoặc chấm dứt t&agrave;i khoản của th&agrave;nh vi&ecirc;n v&agrave; kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm với th&agrave;nh vi&ecirc;n.</p>
<p>
4.2. Tr&aacute;ch nhiệm của th&agrave;nh vi&ecirc;n</p>
<p >
a) Mỗi th&agrave;nh vi&ecirc;n đại diện, đảm bảo v&agrave; đồng &yacute; rằng: Bạn c&oacute; khả năng v&agrave; thẩm quyền để chấp nhận c&aacute;c điều khoản, cấp giấy ph&eacute;p v&agrave; ủy quyền v&agrave; thực hiện c&aacute;c nghĩa vụ dưới đ&acirc;y;</p>
<p >
b) Th&agrave;nh vi&ecirc;n sẽ được y&ecirc;u cầu cung cấp th&ocirc;ng tin hoặc t&agrave;i liệu về tổ chức, doanh nghiệp của bạn hoặc th&ocirc;ng tin về lĩnh vực m&agrave; bạn sẽ đăng th&ocirc;ng tin tại Vaber.vn hoặc c&aacute;c th&ocirc;ng tin li&ecirc;n hệ như l&agrave; một phần của qu&aacute; tr&igrave;nh đăng k&yacute; tr&ecirc;n trang web hoặc sử dụng bất kỳ dịch vụ hoặc c&aacute;c t&agrave;i khoản th&agrave;nh vi&ecirc;n. Mỗi th&agrave;nh vi&ecirc;n đại diện, đảm bảo v&agrave; đồng &yacute; rằng:</p>
<p>
&nbsp;&nbsp;&nbsp- Th&ocirc;ng tin đ&oacute; v&agrave; c&aacute;c t&agrave;i liệu liệu được nộp trong qu&aacute; tr&igrave;nh đăng k&yacute; hoặc sau đ&oacute; trong suốt sự tiếp tục của việc sử dụng c&aacute;c trang web hoặc dịch vụ l&agrave; đ&uacute;ng, ch&iacute;nh x&aacute;c v&agrave; đầy đủ.</p>
<p>
&nbsp;&nbsp;&nbsp- Bạn sẽ duy tr&igrave; v&agrave; kịp thời sửa đổi tất cả c&aacute;c th&ocirc;ng tin v&agrave; t&agrave;i liệu để giữ cho n&oacute; đ&uacute;ng, ch&iacute;nh x&aacute;c v&agrave; đầy đủ.</p>
<p >
c) Sau khi trở th&agrave;nh một th&agrave;nh vi&ecirc;n, bạn bằng l&ograve;ng đưa c&aacute;c th&ocirc;ng tin li&ecirc;n hệ về bạn trong cơ sở dữ liệu v&agrave; ủy quyền cho Vaber.vn chia sẻ c&aacute;c th&ocirc;ng tin li&ecirc;n lạc với c&aacute;c người d&ugrave;ng kh&aacute;c hoặc sử dụng th&ocirc;ng tin c&aacute; nh&acirc;n của bạn theo quy định của Ch&iacute;nh s&aacute;ch Bảo mật.</p>
<p >
d) Mỗi th&agrave;nh vi&ecirc;n đại diện, đảm bảo v&agrave; đồng &yacute; rằng (1) bạn phải chịu tr&aacute;ch nhiệm cho việc thu thập tất cả c&aacute;c giấy ph&eacute;p cần thiết của b&ecirc;n thứ ba v&agrave; c&aacute;c điều khoản li&ecirc;n quan đến nội dung bất kỳ Người sử dụng m&agrave; bạn gửi, đăng hoặc hiển thị; (2) bất kỳ nội dung người d&ugrave;ng m&agrave; bạn gửi, đăng hoặc hiển thị kh&ocirc;ng vi phạm hoặc vi phạm bất kỳ bản quyền, bằng s&aacute;ng chế, nh&atilde;n hiệu h&agrave;ng ho&aacute;, t&ecirc;n thương mại, b&iacute; mật thương mại hoặc bất kỳ c&aacute; nh&acirc;n hoặc quyền sở hữu kh&aacute;c của bất kỳ b&ecirc;n thứ ba (&quot;Quyền của B&ecirc;n Thứ Ba&quot;) ;</p>
<p >
e) Mỗi th&agrave;nh vi&ecirc;n tiếp tục đại diện, đảm bảo v&agrave; đồng &yacute; rằng c&aacute;c nội dung người d&ugrave;ng m&agrave; bạn gửi, đăng hoặc hiển thị c&oacute; tr&aacute;ch nhiệm:</p>
<p >
&nbsp;&nbsp;&nbsp- L&agrave; đ&uacute;ng, ch&iacute;nh x&aacute;c, đầy đủ v&agrave; hợp ph&aacute;p;</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng sai s&oacute;t, sai lệch hoặc lừa đảo;</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng chứa th&ocirc;ng tin l&agrave; phỉ b&aacute;ng, b&ocirc;i nhọ, đe dọa, quấy rối, khi&ecirc;u d&acirc;m, bị phản đối, phản cảm, khi&ecirc;u d&acirc;m hoặc c&oacute; hại cho trẻ vị th&agrave;nh ni&ecirc;n;</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng chứa th&ocirc;ng tin l&agrave; ph&acirc;n biệt đối xử hoặc th&uacute;c đẩy ph&acirc;n biệt đối xử dựa tr&ecirc;n chủng tộc, giới t&iacute;nh, t&ocirc;n gi&aacute;o, quốc tịch, khuyết tật, định hướng giới t&iacute;nh hay tuổi t&aacute;c;</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng vi phạm c&aacute;c sản phẩm Liệt k&ecirc; Ch&iacute;nh s&aacute;ch, Điều khoản kh&aacute;c hoặc &aacute;p dụng bất kỳ Hiệp định kh&aacute;c</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng vi phạm bất kỳ luật lệ v&agrave; quy định (bao gồm nhưng kh&ocirc;ng giới hạn những người quản l&yacute; xuất khẩu, bảo vệ người ti&ecirc;u d&ugrave;ng, cạnh tranh kh&ocirc;ng l&agrave;nh mạnh, hoặc quảng c&aacute;o sai lệch c&oacute;) hoặc th&uacute;c đẩy c&aacute;c hoạt động đ&oacute; c&oacute; thể vi phạm luật ph&aacute;p v&agrave; c&aacute;c quy định hiện h&agrave;nh;</p>
<p >
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng chứa bất kỳ li&ecirc;n kết trực tiếp hay gi&aacute;n tiếp đến bất kỳ trang web kh&aacute;c bao gồm bất kỳ nội dung n&agrave;o vi phạm c&aacute;c điều khoản.</p>
<p >
f) Mỗi th&agrave;nh vi&ecirc;n tiếp tục đại diện, đảm bảo v&agrave; đồng &yacute; rằng bạn c&oacute; tr&aacute;ch nhiệm:</p>
<p>
&nbsp;&nbsp;&nbsp- Thực hiện c&aacute;c hoạt động của bạn tr&ecirc;n c&aacute;c trang web ph&ugrave; hợp với luật ph&aacute;p v&agrave; c&aacute;c quy định hiện h&agrave;nh;</p>
<p>
&nbsp;&nbsp;&nbsp- Tiến h&agrave;nh c&aacute;c giao dịch của bạn với người d&ugrave;ng kh&aacute;c của c&aacute;c trang web trong sự t&iacute;n nhiệm;</p>
<p>
&nbsp;&nbsp;&nbsp- Thực hiện c&aacute;c hoạt động của m&igrave;nh ph&ugrave; hợp với c&aacute;c Điều khoản v&agrave; &aacute;p dụng bất kỳ thỏa thuận bổ sung;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng sử dụng dịch vụ hoặc trang web để lừa gạt người hoặc tổ chức (bao gồm nhưng kh&ocirc;ng giới hạn b&aacute;n c&aacute;c mặt h&agrave;ng bị đ&aacute;nh cắp, sử dụng thẻ t&iacute;n dụng / thẻ ghi nợ bị đ&aacute;nh cắp);</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng mạo danh bất kỳ người n&agrave;o hoặc tổ chức, xuy&ecirc;n tạc ch&iacute;nh m&igrave;nh hoặc quan hệ của bạn với bất kỳ người n&agrave;o hoặc tổ chức;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng tham gia v&agrave;o spam hoặc lừa đảo;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng tham gia v&agrave;o bất kỳ hoạt động bất hợp ph&aacute;p kh&aacute;c (bao gồm nhưng kh&ocirc;ng giới hạn những người đ&oacute; sẽ cấu th&agrave;nh tội h&igrave;nh sự, l&agrave;m ph&aacute;t sinh tr&aacute;ch nhiệm d&acirc;n sự, vv) hoặc khuyến kh&iacute;ch hoặc tiếp tay cho c&aacute;c hoạt động tr&aacute;i ph&aacute;p luật;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng li&ecirc;n quan đến c&aacute;c nỗ lực để sao ch&eacute;p, t&aacute;i sản xuất, khai th&aacute;c hay chiếm đoạt nhiều thư mục độc quyền, cơ sở dữ liệu v&agrave; danh s&aacute;ch của Vaber.vn;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng li&ecirc;n quan đến bất kỳ loại virus m&aacute;y t&iacute;nh hoặc c&aacute;c thiết bị ph&aacute; hoại kh&aacute;c v&agrave; m&atilde; số đ&oacute; c&oacute; t&aacute;c dụng l&agrave;m hư hỏng, g&acirc;y trở ngại, ngăn chặn hoặc chiếm đoạt bất kỳ phần mềm hay phần cứng hệ thống, dữ liệu hoặc th&ocirc;ng tin c&aacute; nh&acirc;n;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng li&ecirc;n quan đến bất kỳ chương tr&igrave;nh để l&agrave;m suy yếu sự to&agrave;n vẹn của dữ liệu, hệ thống hoặc mạng được sử dụng bởi Vaber.vn v&agrave; / hoặc bất kỳ người d&ugrave;ng của c&aacute;c trang web hoặc truy cập tr&aacute;i ph&eacute;p v&agrave;o dữ liệu, hệ thống, mạng lưới;</p>
<p>
&nbsp;&nbsp;&nbsp- Kh&ocirc;ng tham gia v&agrave;o bất kỳ hoạt động m&agrave; nếu kh&ocirc;ng sẽ tạo ra bất kỳ tr&aacute;ch nhiệm ph&aacute;p l&yacute; đối với Vaber.vn.</p>
<p>
&nbsp;&nbsp;&nbsp- Th&agrave;nh vi&ecirc;n c&oacute; thể kh&ocirc;ng sử dụng dịch vụ v&agrave; t&agrave;i khoản th&agrave;nh vi&ecirc;n để tham gia v&agrave;o c&aacute;c hoạt động tr&ugrave;ng nhau hoặc tương tự với thị trường kinh doanh thương mại điện tử của Vaber.vn.</p>
<p >
g) Nếu th&agrave;nh vi&ecirc;n cung cấp một trọng t&agrave;i kinh doanh, th&agrave;nh vi&ecirc;n đại diện, đảm bảo v&agrave; đồng &yacute; rằng bạn đ&atilde; c&oacute; được tất cả đồng &yacute; cần thiết, ph&ecirc; duyệt v&agrave; miễn trừ của c&aacute;c đối t&aacute;c kinh doanh v&agrave; cộng sự (a) h&agrave;nh động như trọng t&agrave;i kinh doanh của bạn; (b) đăng b&agrave;i v&agrave; c&ocirc;ng bố chi tiết li&ecirc;n lạc v&agrave; th&ocirc;ng tin của họ, thư giới thiệu v&agrave; b&igrave;nh luận tr&ecirc;n danh nghĩa của họ; v&agrave; (c) m&agrave; c&aacute;c b&ecirc;n thứ ba c&oacute; thể li&ecirc;n hệ với trọng t&agrave;i kinh doanh như vậy để hỗ trợ khiếu nại hoặc b&aacute;o c&aacute;o thực hiện về bạn. Hơn nữa, bạn bảo đảm v&agrave; đồng &yacute; rằng tất cả c&aacute;c thư giới thiệu v&agrave; nhận x&eacute;t l&agrave; đ&uacute;ng sự thật v&agrave; ch&iacute;nh x&aacute;c v&agrave; c&aacute;c b&ecirc;n thứ ba c&oacute; thể li&ecirc;n hệ với c&aacute;c trọng t&agrave;i kinh doanh m&agrave; kh&ocirc;ng cần phải c&oacute; được sự đồng &yacute; của bạn.</p>
<p >
h) Th&agrave;nh vi&ecirc;n đồng &yacute; để cung cấp tất cả c&aacute;c th&ocirc;ng tin cần thiết, t&agrave;i liệu ph&ecirc; duyệt, v&agrave; l&agrave;m cho tất cả c&aacute;c hỗ trợ hợp l&yacute; v&agrave; hợp t&aacute;c cần thiết cho việc cung cấp c&aacute;c dịch vụ củaVaber.vn, đ&aacute;nh gi&aacute; xem liệu th&agrave;nh vi&ecirc;n đ&atilde; vi phạm c&aacute;c điều khoản v&agrave; / hoặc xử l&yacute; bất kỳ khiếu nại chống lại c&aacute;c th&agrave;nh vi&ecirc;n. Nếu thất bại của th&agrave;nh vi&ecirc;n để thực hiện điều đ&oacute; dẫn đến sự chậm trễ trong, đ&igrave;nh chỉ hoặc chấm dứt, việc cung cấp c&aacute;c dịch vụ bất kỳ, Vaber.vn sẽ kh&ocirc;ng c&oacute; nghĩa vụ để k&eacute;o d&agrave;i thời gian phục vụ li&ecirc;n quan cũng như kh&ocirc;ng chịu tr&aacute;ch nhiệm về bất kỳ tổn thất hoặc thiệt hại ph&aacute;t sinh từ việc chậm trễ như vậy, đ&igrave;nh chỉ hoặc chấm dứt.</p>
<p >
i) Th&agrave;nh vi&ecirc;n x&aacute;c nhận v&agrave; đồng &yacute; rằng Vaber.vn kh&ocirc;ng được y&ecirc;u cầu để chủ động gi&aacute;m s&aacute;t cũng như kh&ocirc;ng phải thực hiện bất kỳ kiểm so&aacute;t bi&ecirc;n tập n&agrave;o trong c&aacute;c nội dung của bất kỳ tin nhắn hoặc c&aacute;c t&agrave;i liệu hoặc th&ocirc;ng tin được tạo ra, thu được hoặc truy cập th&ocirc;ng qua c&aacute;c dịch vụ hoặc trang web. Vaber.vn kh&ocirc;ng x&aacute;c nhận, x&aacute;c minh hoặc x&aacute;c nhận c&aacute;c nội dung của bất kỳ &yacute; kiến hoặc vật liệu kh&aacute;c hoặc th&ocirc;ng tin được thực hiện bởi bất kỳ th&agrave;nh vi&ecirc;n. Mỗi th&agrave;nh vi&ecirc;n tự chịu tr&aacute;ch nhiệm về nội dung của th&ocirc;ng tin li&ecirc;n lạc của họ v&agrave; c&oacute; thể được tổ chức một c&aacute;ch hợp ph&aacute;p chịu tr&aacute;ch nhiệm hoặc chịu tr&aacute;ch nhiệm về nội dung c&aacute;c &yacute; kiến của họ hoặc c&aacute;c t&agrave;i liệu kh&aacute;c hoặc th&ocirc;ng tin</p>
<p >
j) Th&agrave;nh vi&ecirc;n x&aacute;c nhận v&agrave; đồng &yacute; rằng mỗi th&agrave;nh vi&ecirc;n tự chịu tr&aacute;ch nhiệm tu&acirc;n theo ph&aacute;p luật v&agrave; c&aacute;c quy định &aacute;p dụng tại c&aacute;c nước tương ứng để đảm bảo rằng tất cả việc sử dụng c&aacute;c trang web v&agrave; c&aacute;c dịch vụ c&oacute; ph&ugrave; hợp với nhau.</p>
<p>
</p>
<p>
<strong>5. </strong><strong>Vi ph</strong><strong>ạ</strong><strong>m c</strong><strong>ủ</strong><strong>a th&agrave;nh vi&ecirc;n</strong></p>
<p>
5.1 Vaber.vn c&oacute; ​​quyền loại bỏ, sửa đổi hoặc loại bỏ bất kỳ nội dung người sử dụng gửi đến, đăng hoặc hiển thị tr&ecirc;n c&aacute;c trang web m&agrave; ch&uacute;ng t&ocirc;i c&oacute; l&yacute; do hợp l&yacute; để tin l&agrave; tr&aacute;i ph&aacute;p luật, vi phạm c&aacute;c điều khoản, c&oacute; thể khiến Vaber.vn hoặc c&aacute;c chi nh&aacute;nh của ch&uacute;ng t&ocirc;i chịu tr&aacute;ch nhiệm, hoặc l&agrave; nếu thấy kh&ocirc;ng ph&ugrave; hợp theo quan điểm ​​của Vaber.vn</p>
<p>
5.2 Nếu bất kỳ th&agrave;nh vi&ecirc;n vi phạm bất kỳ điều khoản hoặc nếu Vaber.vn c&oacute; cơ sở hợp l&yacute; để tin rằng th&agrave;nh vi&ecirc;n vi phạm bất kỳ điều khoản n&agrave;o, Vaber.vn c&oacute; quyền &aacute;p đặt một h&igrave;nh phạt đối với c&aacute;c th&agrave;nh vi&ecirc;n, hoặc đ&igrave;nh chỉ hoặc chấm dứt t&agrave;i khoản của th&agrave;nh vi&ecirc;n hoặc thu&ecirc; bao của bất kỳ dịch vụ m&agrave; kh&ocirc;ng c&oacute; bất kỳ tr&aacute;ch nhiệm ph&aacute;p l&yacute; với c&aacute;c th&agrave;nh vi&ecirc;n. Vaber.vn cũng c&oacute; quyền hạn chế, từ chối hoặc ngừng cung c&aacute;p bất kỳ dịch vụ n&agrave;o kh&aacute;c c&oacute; thể được cung cấp bởi Vaber.vn hiện tại hoặc tương lai. C&aacute;c h&igrave;nh phạt m&agrave; Vaber.vn c&oacute; thể &aacute;p dụng bao gồm, cảnh b&aacute;o, loại bỏ bất kỳ danh s&aacute;ch dịch vụ sản phẩm hoặc sử dụng nội dung kh&aacute;c m&agrave; c&aacute;c th&agrave;nh vi&ecirc;n đ&atilde; gửi, đăng hoặc hiển thị, &aacute;p đặt c&aacute;c hạn chế về số lượng danh s&aacute;ch dịch vụ, sản phẩm m&agrave; c&aacute;c th&agrave;nh vi&ecirc;n c&oacute; thể gửi hoặc m&agrave;n h&igrave;nh hiển thị, hoặc &aacute;p đặt c&aacute;c hạn chế về việc sử dụng bất kỳ t&iacute;nh năng hoặc chức năng của bất kỳ dịch vụ của th&agrave;nh vi&ecirc;n đối với khoảng thời gian như Vaber.vn x&eacute;t thấy th&iacute;ch hợp t&ugrave;y theo quyết định của ch&uacute;ng t&ocirc;i.</p>
<p>
5.3 Kh&ocirc;ng giới hạn tổng qu&aacute;t của c&aacute;c quy định của Điều khoản n&agrave;y, một th&agrave;nh vi&ecirc;n sẽ được coi l&agrave; vi phạm c&aacute;c điều khoản trong c&aacute;c trường hợp sau đ&acirc;y:</p>
<p >
a) khi c&oacute; khiếu nại hoặc khiếu nại của bất kỳ b&ecirc;n thứ ba, Vaber.vn c&oacute; ​​cơ sở hợp l&yacute; để tin rằng th&agrave;nh vi&ecirc;n đ&oacute; c&oacute; cố &yacute; hay về mặt vật chất kh&ocirc;ng thực hiện hợp đồng với b&ecirc;n thứ ba đ&oacute; bao gồm nhưng kh&ocirc;ng giới hạn, nơi c&aacute;c th&agrave;nh vi&ecirc;n đ&atilde; kh&ocirc;ng cung cấp bất kỳ dịch vụ theo y&ecirc;u cầu của b&ecirc;n thứ ba sau khi nhận được gi&aacute; mua, hoặc nơi c&aacute;c mục th&agrave;nh vi&ecirc;n đ&atilde; chuyển về vật chất kh&ocirc;ng đ&aacute;p ứng được c&aacute;c điều khoản v&agrave; m&ocirc; tả được n&ecirc;u trong hợp đồng của bạn với b&ecirc;n thứ ba đ&oacute;,</p>
<p >
b) Vaber.vn c&oacute; ​​căn cứ hợp l&yacute; để nghi ngờ rằng th&agrave;nh vi&ecirc;n đ&oacute; đ&atilde; sử dụng một thẻ t&iacute;n dụng bị đ&aacute;nh cắp hoặc th&ocirc;ng tin sai sự thật hoặc g&acirc;y nhầm lẫn kh&aacute;c trong bất kỳ giao dịch với một b&ecirc;n truy cập,</p>
<p >
c) Vaber.vn c&oacute; ​​căn cứ hợp l&yacute; để nghi ngờ rằng th&ocirc;ng tin được cung cấp bởi c&aacute;c th&agrave;nh vi&ecirc;n kh&ocirc;ng phải l&agrave; hiện tại hoặc ho&agrave;n chỉnh hoặc l&agrave; kh&ocirc;ng đ&uacute;ng sự thật, kh&ocirc;ng ch&iacute;nh x&aacute;c, hoặc g&acirc;y hiểu lầm, hoặc</p>
<p >
d) Vaber.vn tin rằng h&agrave;nh động của th&agrave;nh vi&ecirc;n c&oacute; thể g&acirc;y tổn thất t&agrave;i ch&iacute;nh hoặc tr&aacute;ch nhiệm ph&aacute;p l&yacute; với Vaber.vn hoặc bất kỳ người sử dụng kh&aacute;c.</p>
<p>
5.4 Vaber.vn c&oacute; quyền hợp t&aacute;c với c&aacute;c cơ quan ch&iacute;nh phủ, c&aacute;c nh&agrave; điều tra tư nh&acirc;n v&agrave; / hoặc c&aacute;c b&ecirc;n thứ ba trong việc điều tra của bất kỳ nghi ngờ h&agrave;nh vi phạm tội hoặc d&acirc;n sự. Hơn nữa, Vaber.vn c&oacute; thể tiết lộ danh t&iacute;nh v&agrave; th&ocirc;ng tin li&ecirc;n lạc của th&agrave;nh vi&ecirc;n, nếu được y&ecirc;u cầu bởi một cơ quan ch&iacute;nh phủ hoặc cơ quan thực thi ph&aacute;p luật, một b&ecirc;n thứ ba bị hại, hoặc l&agrave; kết quả của một tr&aacute;t hầu t&ograve;a hoặc h&agrave;nh động hợp ph&aacute;p kh&aacute;c. Vaber.vn sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm về những thiệt hại ph&aacute;t sinh từ hoặc kết quả c&ocirc;ng bố th&ocirc;ng tin như vậy, v&agrave; th&agrave;nh vi&ecirc;n đồng &yacute; kh&ocirc;ng mang lại bất kỳ h&agrave;nh động hoặc đơn kiện chống lại Vaber.vn cho tiết lộ như vậy.</p>
<p>
5.5 Nếu một Th&agrave;nh vi&ecirc;n c&oacute; h&agrave;nh vi vi phạm c&aacute;c Điều khoản, Vaber.vn cũng c&oacute; quyền để xuất bản c&aacute;c hồ sơ về vi phạm đ&oacute; tr&ecirc;n trang web. Nếu vi phạm đ&oacute; li&ecirc;n quan đến hoặc được hợp l&yacute; li&ecirc;n quan đến hoạt động bị nghi ngờ kh&ocirc;ng trung thực hoặc gian lận, Vaber.vn cũng bảo lưu quyền tiết lộ c&aacute;c hồ sơ về vi phạm đ&oacute; cho c&aacute;c chi nh&aacute;nh của ch&uacute;ng t&ocirc;i, c&aacute;c chi nh&aacute;nh n&agrave;y c&oacute; thể &aacute;p đặt c&aacute;c hạn chế tr&ecirc;n, đ&igrave;nh chỉ hoặc chấm dứt sử dụng to&agrave;n bộ hoặc một phần của c&aacute;c dịch vụ được cung cấp bởi c&aacute;c chi nh&aacute;nh đ&oacute; cho c&aacute;c th&agrave;nh vi&ecirc;n của th&agrave;nh vi&ecirc;n, c&oacute; những h&agrave;nh động khắc phục hậu quả kh&aacute;c, v&agrave; c&ocirc;ng bố c&aacute;c hồ sơ về h&agrave;nh vi vi phạm c&aacute;c Điều khoản tr&ecirc;n trang web do hoặc được kiểm so&aacute;t bởi c&aacute;c th&agrave;nh vi&ecirc;n như c&aacute;c chi nh&aacute;nh.</p>
<p>
5.6 Vaber.vn c&oacute; thể, bất cứ l&uacute;c n&agrave;o v&agrave; theo l&yacute; của ch&uacute;ng t&ocirc;i, &aacute;p đặt những hạn chế tr&ecirc;n, đ&igrave;nh chỉ hoặc chấm dứt sử dụng bất kỳ dịch vụ hoặc những trang web m&agrave; kh&ocirc;ng cần phải chịu tr&aacute;ch nhiệm c&aacute;c th&agrave;nh vi&ecirc;n nếu Vaber.vn đ&atilde; nhận được th&ocirc;ng b&aacute;o rằng c&aacute;c th&agrave;nh vi&ecirc;n c&oacute; vi phạm bất kỳ thoả thuận hoặc cam kết với bất kỳ chi nh&aacute;nh n&agrave;o v&agrave; vi phạm đ&oacute; li&ecirc;n quan đến hoặc được hợp l&yacute; li&ecirc;n quan đến hoạt động bị nghi ngờ kh&ocirc;ng trung thực hoặc gian lận. Vaber.vn c&oacute; quyền c&ocirc;ng bố c&aacute;c hồ sơ về vi phạm đ&oacute; tr&ecirc;n trang web m&agrave; kh&ocirc;ng cần x&aacute;c nhận từ c&aacute;c th&agrave;nh vi&ecirc;n .</p>
<p>
5.7 Mỗi th&agrave;nh vi&ecirc;n đồng &yacute; bồi thường Vaber.vn, ch&uacute;ng t&ocirc;i v&agrave; c&aacute;c chi nh&aacute;nh, gi&aacute;m đốc, nh&acirc;n vi&ecirc;n, đại l&yacute; v&agrave; đại diện v&agrave; để giữ ch&uacute;ng v&ocirc; hại, từ bất kỳ v&agrave; tất cả c&aacute;c thiệt hại, tổn thất, khiếu nại v&agrave; nợ phải trả (bao gồm cả chi ph&iacute; ph&aacute;p l&yacute; tr&ecirc;n cơ sở bồi thường đầy đủ) c&oacute; thể ph&aacute;t sinh từ việc bạn gửi, đăng hoặc hiển thị nội dung bất kỳ, từ việc bạn sử dụng c&aacute;c trang web hoặc dịch vụ, hoặc từ việc bạn vi phạm c&aacute;c điều khoản.</p>
<p>
5.8 Mỗi th&agrave;nh vi&ecirc;n đồng &yacute; th&ecirc;m rằng Vaber.vn kh&ocirc;ng chịu tr&aacute;ch nhiệm, v&agrave; sẽ kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm với bạn hoặc bất cứ ai kh&aacute;c cho bất kỳ nội dung t&agrave;i liệu n&agrave;o kh&aacute;c được truyền qua c&aacute;c trang web, bao gồm gian lận, sai sự thật, g&acirc;y hiểu lầm, c&aacute;c t&agrave;i liệu kh&ocirc;ng ch&iacute;nh x&aacute;c, phỉ b&aacute;ng, x&uacute;c phạm hoặc bất hợp ph&aacute;p v&agrave; nguy cơ thiệt hại từ chất liệu như dựa ho&agrave;n to&agrave;n với mỗi th&agrave;nh vi&ecirc;n. Vaber.vn c&oacute; quyền, để nhận sự bảo vệ độc quyền v&agrave; kiểm so&aacute;t mọi vấn đề li&ecirc;n qua đến bồi thường của c&aacute;c th&agrave;nh vi&ecirc;n, trong đ&oacute; Th&agrave;nh vi&ecirc;n phải hợp t&aacute;c với Vaber.vn trong việc bồi thường c&aacute;c thiệt hại.</p>
<p>
<strong>6 </strong><strong>Giao d</strong><strong>ị</strong><strong>ch gi</strong><strong>ữ</strong><strong>a c&aacute;c th&agrave;nh vi&ecirc;n:</strong></p>
<p>
6.1 Th&ocirc;ng qua c&aacute;c trang web, Vaber.vn cung cấp nền tảng dựa tr&ecirc;n web điện tử để trao đổi th&ocirc;ng tin giữa c&aacute;c th&agrave;nh vi&ecirc;n tại c&aacute;c lĩnh vực m&agrave; Vaber cung cấp. Vaber.vn cung cấp nền tảng giao dịch dựa tr&ecirc;n web điện tử cho th&agrave;nh vi&ecirc;n bao gồm kh&ocirc;ng giới hạn việc: chấp nhận, kết luận, quản l&yacute; c&aacute;c y&ecirc;u cầu, đề xuất với c&aacute;c điều khoản của Vaber.vn. Tuy nhi&ecirc;n, đối với bất kỳ dịch vụ n&agrave;os, Vaber.vn kh&ocirc;ng đại diện hoặc l&agrave; người đưa y&ecirc;u cầu hoặc người đề xuất, thực hiện dịch vụ trong c&aacute;c giao dịch cụ thể. Vaber.vn kh&ocirc;ng kiểm so&aacute;t v&agrave; kh&ocirc;ng chịu tr&aacute;ch nhiệm về chất lượng, an to&agrave;n, hợp ph&aacute;p v&agrave; t&iacute;nh sẵn s&agrave;ng của sản phẩm hoặc dịch vụ tr&ecirc;n trang web hoặc c&aacute;c khả năng của người đề xuất, cung cấp dịch vụ để thực hiện c&aacute;c y&ecirc;u cầu tr&ecirc;n trang web.</p>
<p>
6.2 Người d&ugrave;ng nhận thức rằng c&oacute; thể c&oacute; rủi ro đối ph&oacute; với những người hoạt động giả tạo. Vaber.vn sử dụng một số kỹ thuật để x&aacute;c minh t&iacute;nh ch&iacute;nh x&aacute;c của th&ocirc;ng tin nhất định khi th&agrave;nh vi&ecirc;n cung cấp th&ocirc;ng tin cho Vaber.vn trong qu&aacute; tr&igrave;nh đăng k&iacute;. Tuy nhi&ecirc;n, việc x&aacute;c minh người sử dụng tr&ecirc;n Internet l&agrave; kh&oacute; khăn, Vaber.vn kh&ocirc;ng thể v&agrave; kh&ocirc;ng c&oacute; mục đ&iacute;ch x&aacute;c nhận danh t&iacute;nh từng người d&ugrave;ng (bao gồm, nhưng kh&ocirc;ng giới hạn, thanh to&aacute;n vi&ecirc;n). Ch&uacute;ng t&ocirc;i khuyến kh&iacute;ch bạn sử dụng phương tiện kh&aacute;c nhau, c&aacute;ch thức kh&aacute;c nhau, để đ&aacute;nh gi&aacute; với người m&agrave; bạn đang giao dịch.</p>
<p>
6.3 Mỗi th&agrave;nh vi&ecirc;n thừa nhận rằng lu&ocirc;n tồn tại những rủi ro trong việc giao dịch th&ocirc;ng qua trang web, v&agrave; rằng n&oacute; được giả định ho&agrave;n to&agrave;n những rủi ro về tr&aacute;ch nhiệm hay thiệt hại của bất cứ loại n&agrave;o trong kết nối với c&aacute;c hoạt động tiếp theo n&agrave;o li&ecirc;n quan với c&aacute;c sản phẩm hoặc dịch vụ m&agrave; l&agrave; đối tượng của c&aacute;c giao dịch bằng c&aacute;ch sử dụng trang web. Rủi ro như vậy sẽ bao gồm, nhưng kh&ocirc;ng giới hạn, b&aacute;o c&aacute;o sai c&aacute;c sản phẩm v&agrave; dịch vụ, c&aacute;c chương tr&igrave;nh gian lận, kh&ocirc;ng đạt chất lượng, kh&ocirc;ng đ&aacute;p ứng th&ocirc;ng số kỹ thuật, sản phẩm bị lỗi hoặc nguy hiểm, c&aacute;c sản phẩm bất hợp ph&aacute;p, chậm trễ hoặc mặc định trong giao h&agrave;ng hoặc thanh to&aacute;n, t&iacute;nh to&aacute;n sai chi ph&iacute; , vi phạm về bảo h&agrave;nh, vi phạm hợp đồng v&agrave; tai nạn. C&aacute;c rủi ro đ&oacute; cũng bao gồm những rủi ro m&agrave; việc sản xuất, nhập khẩu, xuất khẩu, ph&acirc;n phối, cung cấp, hiển thị, mua, b&aacute;n v&agrave; / hoặc sử dụng c&aacute;c sản phẩm hoặc dịch vụ được cung cấp hoặc hiển thị tr&ecirc;n c&aacute;c trang web c&oacute; thể vi phạm hoặc c&oacute; thể được khẳng định l&agrave; vi phạm quyền của b&ecirc;n thứ ba, v&agrave; rủi ro m&agrave; t&agrave;i khoản c&oacute; thể phải trả chi ph&iacute; bảo vệ hoặc c&aacute;c chi ph&iacute; kh&aacute;c c&oacute; li&ecirc;n quan đến sự khẳng định của b&ecirc;n thứ ba của B&ecirc;n Bản quyền Thứ ba, hoặc kết nối với bất kỳ khiếu nại bởi bất kỳ b&ecirc;n n&agrave;o m&agrave; họ c&oacute; quyền được bảo vệ hoặc bồi thường li&ecirc;n quan đến những khẳng định về quyền, y&ecirc;u cầu hoặc khiếu nại của c&aacute;c b&ecirc;n tranh chấp của b&ecirc;n Bản quyền thứ ba. C&aacute;c rủi ro đ&oacute; cũng bao gồm những rủi ro m&agrave; người ti&ecirc;u d&ugrave;ng, người mua kh&aacute;c, người d&ugrave;ng cuối c&ugrave;ng của sản phẩm hoặc những người kh&aacute;c khẳng định đ&atilde; bị thương hoặc tổn hại li&ecirc;n quan đến sản phẩm, dịch vụ ban đầu thu được bởi người sử dụng của c&aacute;c trang web như l&agrave; kết quả của c&aacute;c giao dịch mua b&aacute;n trong kết nối với việc sử dụng c&aacute;c trang web c&oacute; thể bị tổn hại v&agrave; / hoặc khẳng định khiếu nại ph&aacute;t sinh từ việc sử dụng c&aacute;c sản phẩm như vậy. Tất cả những rủi ro n&oacute;i tr&ecirc;n được gọi chung l&agrave; &quot;rủi ro giao dịch&quot;. Mỗi người d&ugrave;ng đồng &yacute; rằng Vaber.vn sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm hoặc chịu tr&aacute;ch nhiệm cho bất kỳ thiệt hại, khiếu nại, tr&aacute;ch nhiệm, chi ph&iacute;, thiệt hại, phiền h&agrave;, gi&aacute;n đoạn kinh doanh hoặc chi ph&iacute; của bất kỳ loại c&oacute; thể ph&aacute;t sinh một kết quả hoặc kết nối với bất kỳ rủi ro giao dịch.</p>
<p>
6.4 Người d&ugrave;ng ho&agrave;n to&agrave;n chịu tr&aacute;ch nhiệm cho tất cả c&aacute;c điều khoản v&agrave; điều kiện của c&aacute;c giao dịch thực hiện, th&ocirc;ng qua hoặc l&agrave; kết quả của việc sử dụng của c&aacute;c trang web hoặc dịch vụ, bao gồm, nhưng kh&ocirc;ng giới hạn, c&aacute;c điều khoản li&ecirc;n quan đến thanh to&aacute;n, lợi nhuận, bảo h&agrave;nh, vận chuyển, bảo hiểm, ph&iacute;, thuế , ti&ecirc;u đề, giấy ph&eacute;p, phạt tiền, giấy ph&eacute;p, xử l&yacute;, vận chuyển v&agrave; lưu trữ.</p>
<p>
6.5 Người d&ugrave;ng đồng &yacute; để cung cấp tất cả c&aacute;c th&ocirc;ng tin v&agrave; c&aacute;c t&agrave;i liệu theo y&ecirc;u cầu hợp l&yacute; của Vaber.vn trong việc thực hiện kết nối với c&aacute;c giao dịch của bạn, th&ocirc;ng qua hoặc l&agrave; kết quả của việc sử dụng của c&aacute;c trang web hoặc dịch vụ. Vaber.vn c&oacute; ​​quyền đ&igrave;nh chỉ hoặc chấm dứt t&agrave;i khoản của bất kỳ người d&ugrave;ng nếu người d&ugrave;ng kh&ocirc;ng cung cấp c&aacute;c th&ocirc;ng tin v&agrave; t&agrave;i liệu cần thiết.</p>
<p>
6.6 Trong c&aacute;c sự kiện m&agrave; th&agrave;nh vi&ecirc;n c&oacute; tranh chấp với bất kỳ b&ecirc;n tham gia giao dịch, t&agrave;i khoản th&agrave;nh vi&ecirc;n c&oacute; thể bị đ&oacute;ng v&agrave; th&agrave;nh vi&ecirc;n cần bồi thường cho Vaber.vn (v&agrave; c&aacute;c đại l&yacute;, chi nh&aacute;nh, gi&aacute;m đốc, c&aacute;n bộ, vi&ecirc;n chức) mọi khiếu nại, y&ecirc;u cầu, h&agrave;nh động, thủ tục, chi ph&iacute; , chi ph&iacute; v&agrave; thiệt hại (bao gồm kh&ocirc;ng giới hạn bất kỳ thiệt hại thực tế, đặc biệt, ngẫu nhi&ecirc;n hoặc do hậu quả) ph&aacute;t sinh từ hoặc li&ecirc;n quan đến giao dịch đ&oacute;.</p>
<p>
<strong>7 </strong><strong>Gi</strong><strong>ớ</strong><strong>i h</strong><strong>ạ</strong><strong>n tr&aacute;ch nhi</strong><strong>ệ</strong><strong>m</strong></p>
<p>
7.1 MỞ RỘNG TỐI ĐA DƯỚI SỰ CHO PH&Eacute;P CỦA LUẬT, C&Aacute;C DỊCH VỤ CUNG CẤP BỞI Vaber.vn &quot;NHƯ L&Agrave;&quot;, &quot;NHƯ SẴN C&Oacute;&quot; V&Agrave; &quot;VỚI TẤT CẢ C&Aacute;C LỖI&quot;, V&Agrave; Vaber.vn DO Đ&Oacute; TỪ TỪ CHỐI TR&Aacute;CH NHIỆM BẤT KỲ V&Agrave; MỌI BẢO ĐẢM, R&Otilde; R&Agrave;NG HAY NGỤ &Yacute;, BAO GỒM NHƯNG KH&Ocirc;NG GIỚI HẠN BẤT KỲ BẢO H&Agrave;NH ĐIỀU KIỆN, CHẤT LƯỢNG, ĐỘ BỀN, THỰC HIỆN, TIN CẬY, MUA B&Aacute;N, HOẶC CHO MỘT MỤC Đ&Iacute;CH CỤ THỂ. TẤT CẢ NHƯ BẢO ĐẢM, ĐẠI DIỆN, ĐIỀU KIỆN V&Agrave; CAM KẾT ĐƯỢC LOẠI TRỪ.</p>
<p>
7.2 MỞ RỘNG TỐI ĐA DƯỚI SỰ CHO PH&Eacute;P CỦA LUẬT, Vaber.vn KH&Ocirc;NG ĐẠI DIỆN HOẶC BẢO ĐẢM VỀ C&Aacute;C GI&Aacute; TRỊ, CH&Iacute;NH X&Aacute;C, T&Iacute;NH CH&Iacute;NH X&Aacute;C, TIN CẬY, CHẤT LƯỢNG, ỔN ĐỊNH, HO&Agrave;N THIỆN HAY LƯU H&Agrave;NH CỦA BẤT CỨ TH&Ocirc;NG TIN CUNG CẤP TR&Ecirc;N HOẶC QUA NHỮNG; Vaber.vn KH&Ocirc;NG ĐẠI DIỆN HOẶC ĐẢM BẢO RẰNG SẢN XUẤT, NHẬP KHẨU, XUẤT KHẨU, PH&Acirc;N PHỐI, CUNG CẤP, M&Agrave;N H&Igrave;NH HIỂN THỊ, MUA B&Aacute;N V&Agrave; / HOẶC SỬ DỤNG SẢN PHẨM DỊCH VỤ HAY TRƯNG B&Agrave;Y TR&Ecirc;N C&Aacute;C TRANG KH&Ocirc;NG VI PHẠM BẤT KỲ QUYỀN CỦA B&Ecirc;N THỨ BA HOẶC C&Aacute;C DỊCH VỤ DO C&Aacute;C B&Ecirc;N CUNG CẤP; Vaber.vn KH&Ocirc;NG ĐẠI DIỆN HOẶC BẢO ĐẢM N&Agrave;O LI&Ecirc;N QUAN ĐẾN BẤT CỨ SẢN PHẨM HOẶC DỊCH VỤ CUNG CẤP HOẶC HIỂN THỊ TR&Ecirc;N WEBSITES.</p>
<p>
7.3 Bất kỳ t&agrave;i liệu tải về hoặc c&oacute; được th&ocirc;ng qua c&aacute;c trang web được thực hiện theo quyết định v&agrave; rủi ro của mỗi t&agrave;i khoản v&agrave; mỗi t&agrave;i khoản l&agrave; tr&aacute;ch nhiệm về bất kỳ thiệt hại cho hệ thống m&aacute;y t&iacute;nh của Vaber.vn hoặc mất dữ liệu c&oacute; thể l&agrave; kết quả từ việc tải bất kỳ t&agrave;i liệu n&agrave;y. Kh&ocirc;ng c&oacute; lời khuy&ecirc;n hay th&ocirc;ng tin, d&ugrave; bằng lời n&oacute;i hoặc bằng văn bản, thu được bằng bất kỳ t&agrave;i khoản từ Alibaba.com hoặc th&ocirc;ng qua hoặc từ c&aacute;c trang web sẽ tạo ra bất kỳ bảo h&agrave;nh kh&ocirc;ng quy định r&otilde; r&agrave;ng trong t&agrave;i liệu n&agrave;y.</p>
<p>
7.4 C&aacute;c trang web c&oacute; thể cung cấp tới c&aacute;c dịch vụ t&agrave;i hay sản phẩm được cung cấp bởi b&ecirc;n thứ ba độc lập, kh&ocirc;ng c&oacute; bảo h&agrave;nh hoặc đại diện được thực hiện li&ecirc;n quan đến dịch vụ hoặc sản phẩm đ&oacute;. Vaber.vn kh&ocirc;ng chịu tr&aacute;ch nhiệm cho bất kỳ dịch vụ hoặc sản phẩm đ&oacute;.</p>
<p>
7.5 Mỗi người d&ugrave;ng đ&acirc;y cũng đồng &yacute; bồi thường cho Vaber.vn v&agrave; c&aacute;c chi nh&aacute;nh của ch&uacute;ng t&ocirc;i, c&aacute;c gi&aacute;m đốc, c&aacute;n bộ, nh&acirc;n vi&ecirc;n v&ocirc; hại, khỏi bất kỳ v&agrave; tất cả c&aacute;c tổn thất, thiệt hại, khiếu nại, nợ phải trả (bao gồm cả chi ph&iacute; ph&aacute;p l&yacute; tr&ecirc;n cơ sở bồi thường đầy đủ) c&oacute; thể ph&aacute;t sinh trực tiếp hoặc gi&aacute;n tiếp, như l&agrave; kết quả của bất kỳ tuy&ecirc;n bố khẳng định của c&aacute;c b&ecirc;n nguy&ecirc;n c&aacute;o Thứ ba hoặc c&aacute;c b&ecirc;n thứ ba kh&aacute;c c&oacute; li&ecirc;n quan đến c&aacute;c sản phẩm được cung cấp hoặc hiển thị tr&ecirc;n c&aacute;c trang web. Mỗi người d&ugrave;ng đồng &yacute; th&ecirc;m rằng Vaber.vn kh&ocirc;ng chịu tr&aacute;ch nhiệm v&agrave; kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm đối với bạn, đối với bất kỳ t&agrave;i liệu được đăng bởi những người kh&aacute;c, bao gồm cả phỉ b&aacute;ng, x&uacute;c phạm hoặc t&agrave;i liệu bất hợp ph&aacute;p v&agrave; c&oacute; nguy cơ thiệt hại từ chất liệu như dựa ho&agrave;n to&agrave;n với mỗi người d&ugrave;ng. Vaber.vn c&oacute; quyền, để nhận sự bảo vệ độc quyền v&agrave; kiểm so&aacute;t tất cả mọi vấn đề kh&aacute;c đưa ra do bạn bồi thường, trong đ&oacute; bao gồm việc bạn sẽ hợp t&aacute;c với Vaber.vn trong việc bồi thường cho c&aacute;c tổn thất.</p>
<p>
7.6 Vaber.vn sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm cho bất kỳ thiệt hại đặc biệt, trực tiếp, gi&aacute;n tiếp, trừng phạt, ngẫu nhi&ecirc;n hoặc do hậu quả hoặc bất kỳ thiệt hại n&agrave;o (bao gồm nhưng kh&ocirc;ng giới hạn thiệt hại về lợi nhuận hay tiết kiệm, gi&aacute;n đoạn kinh doanh, mất th&ocirc;ng tin), cho d&ugrave; trong hợp đồng , sơ suất, sai lầm c&aacute; nh&acirc;n, vốn chủ sở hữu hoặc hoặc bất kỳ thiệt hại kh&aacute;c do những việc sau đ&acirc;y.</p>
<p >
a) Việc sử dụng hoặc kh&ocirc;ng thể sử dụng c&aacute;c trang web hoặc dịch vụ;</p>
<p >
b) Bất kỳ khiếm khuyết trong h&agrave;ng h&oacute;a, h&agrave;ng mẫu, dữ liệu, th&ocirc;ng tin hoặc dịch vụ mua v&agrave;o hoặc lấy từ một người d&ugrave;ng hoặc bất kỳ b&ecirc;n thứ ba n&agrave;o kh&aacute;c th&ocirc;ng qua c&aacute;c trang web;</p>
<p >
c) Vi phạm c&aacute;c Bản quyền của b&ecirc;n thứ ba hoặc c&aacute;c khiếu nại hay y&ecirc;u cầu n&agrave;o đ&oacute; sản xuất, nhập khẩu, xuất khẩu, ph&acirc;n phối, cung cấp, hiển thị, mua, b&aacute;n của người sử dụng v&agrave; / hoặc sử dụng c&aacute;c sản phẩm hoặc dịch vụ được cung cấp hoặc hiển thị tr&ecirc;n c&aacute;c trang web c&oacute; thể vi phạm hoặc c&oacute; thể được khẳng định vi phạm Bản quyền của b&ecirc;n thứ ba; hoặc khiếu nại bởi bất kỳ b&ecirc;n n&agrave;o m&agrave; họ c&oacute; quyền được bảo vệ hoặc bồi thường li&ecirc;n quan đến những khẳng định về quyền, y&ecirc;u cầu hoặc khiếu nại của c&aacute;c b&ecirc;n tranh chấp của b&ecirc;n thứ ba Bản quyền;</p>
<p >
d) Truy cập tr&aacute;i ph&eacute;p của c&aacute;c b&ecirc;n thứ ba dữ liệu hoặc th&ocirc;ng tin c&aacute; nh&acirc;n của bất kỳ người d&ugrave;ng;</p>
<p >
e) Tuy&ecirc;n bố hay h&agrave;nh của bất kỳ người d&ugrave;ng của c&aacute;c trang web; hoặc;</p>
<p >
f) Bất kỳ vấn đề li&ecirc;n quan đến dịch vụ ph&aacute;t sinh tuy nhi&ecirc;n, kể cả sơ suất.</p>
<p>
7.7 Kh&ocirc;ng phụ thuộc v&agrave;o bất cứ c&aacute;c quy định n&ecirc;u tr&ecirc;n, tr&aacute;ch nhiệm tổng hợp của Vaber.vn v&agrave; c&aacute;c đại diện của ch&uacute;ng t&ocirc;i với mỗi người d&ugrave;ng cho tất cả c&aacute;c khiếu nại ph&aacute;t sinh từ việc sử dụng c&aacute;c trang web hoặc dịch vụ trong bất kỳ năm dương lịch được giới hạn ở số lớn hơn của (a) số tiền lệ ph&iacute; khi người d&ugrave;ng đ&atilde; trả cho Vaber.vn hoặc c&aacute;c chi nh&aacute;nh của ch&uacute;ng t&ocirc;i trong năm dương lịch v&agrave; (b) số lượng tối đa cho ph&eacute;p của ph&aacute;p luật hiện h&agrave;nh. C&aacute;c c&acirc;u trước kh&ocirc;ng được ngăn chặn c&aacute;c y&ecirc;u cầu của người d&ugrave;ng để chứng minh thiệt hại thực tế. Mọi khiếu nại ph&aacute;t sinh từ việc sử dụng c&aacute;c trang web hoặc dịch vụ phải được đệ tr&igrave;nh trong v&ograve;ng một (1) năm kể từ ng&agrave;y nguy&ecirc;n nh&acirc;n của h&agrave;nh động ph&aacute;t sinh hoặc thời hạn l&acirc;u hơn theo quy định dưới bất kỳ luật &aacute;p dụng quản l&yacute; khoản n&agrave;y sử dụng.</p>
<p>
7.8 Những hạn chế v&agrave; loại trừ tr&aacute;ch nhiệm ph&aacute;p l&yacute; theo c&aacute;c Điều khoản n&agrave;y sẽ &aacute;p dụng tới mức tối đa cho ph&eacute;p của ph&aacute;p luật v&agrave; được &aacute;p dụng ngay cả khi Vaber.vn đ&atilde; th&ocirc;ng b&aacute;o hoặc kh&ocirc;ng cho th&agrave;nh vi&ecirc;n khi c&aacute;c thiệt hại ph&aacute;t sinh</p>
<p>
<strong>8 </strong><strong>B</strong><strong>ấ</strong><strong>t kh</strong><strong>ả</strong><strong> kh&aacute;ng</strong></p>
<p>
Trong mọi trường hợp Vaber.vn kh&ocirc;ng chịu tr&aacute;ch nhiệm về bất kỳ sự chậm trễ hay thất bại hay sự gi&aacute;n đoạn của c&aacute;c nội dung v&agrave; dịch vụ cung cấp th&ocirc;ng qua c&aacute;c trang kết quả trực tiếp hoặc gi&aacute;n tiếp từ h&agrave;nh vi của thi&ecirc;n nhi&ecirc;n, lực lượng hoặc g&acirc;y ra ngo&agrave;i tầm kiểm so&aacute;t hợp l&yacute; của ch&uacute;ng t&ocirc;i, bao gồm nhưng kh&ocirc;ng giới hạn, sự cố Internet, m&aacute;y t&iacute;nh, viễn th&ocirc;ng hoặc bất kỳ thiết bị thất bại kh&aacute;c, mất điện điện, đ&igrave;nh c&ocirc;ng, tranh chấp lao động, bạo loạn, nổi dậy, rối loạn d&acirc;n sự, t&igrave;nh trạng thiếu lao động, vật liệu, hỏa hoạn, lũ lụt, b&atilde;o, ch&aacute;y nổ, thi&ecirc;n tai, chiến tranh, h&agrave;nh động của ch&iacute;nh phủ, c&aacute;c đơn đặt h&agrave;ng của t&ograve;a &aacute;n trong nước hoặc nước ngo&agrave;i hoặc c&aacute;c t&ograve;a &aacute;n hoặc kh&ocirc;ng thực hiện của c&aacute;c b&ecirc;n thứ ba.</p>
<p>
<strong>9 </strong><strong>Quy</strong><strong>ề</strong><strong>n s</strong><strong>ở</strong><strong> h</strong><strong>ữ</strong><strong>u tr&iacute; tu</strong><strong>ệ</strong></p>
<p>
9.1 Vaber.vn l&agrave; chủ sở hữu duy nhất của người được cấp ph&eacute;p hợp ph&aacute;p của tất cả c&aacute;c quyền v&agrave; lợi &iacute;ch trong c&aacute;c trang web v&agrave; nội dung trang. C&aacute;c trang web v&agrave; nội dung trang web thể hiện những b&iacute; mật thương mại v&agrave; c&aacute;c quyền sở hữu tr&iacute; tuệ kh&aacute;c được bảo vệ bản quyền tr&ecirc;n to&agrave;n thế giới v&agrave; c&aacute;c luật kh&aacute;c. Tất cả c&aacute;c quyền ti&ecirc;u đề, quyền sở hữu v&agrave; sở hữu tr&iacute; tuệ trong c&aacute;c trang web v&agrave; nội dung trang web sẽ được duy tr&igrave; với Vaber.vn, c&aacute;c chi nh&aacute;nh hoặc người cấp ph&eacute;p nội dung trang của ch&uacute;ng t&ocirc;i, như trường hợp c&oacute; thể được.</p>
<p>
9.2 Vaber.vn v&agrave; c&aacute;c biểu tượng v&agrave; logo li&ecirc;n quan l&agrave; thương hiệu đ&atilde; đăng k&yacute; hoặc nh&atilde;n hiệu hoặc nh&atilde;n hiệu dịch vụ, tại c&aacute;c nước kh&aacute;c nhau v&agrave; được bảo vệ theo luật bản quyền, thương hiệu v&agrave; c&aacute;c luật về quyền sở hữu. Việc sao ch&eacute;p tr&aacute;i ph&eacute;p, sửa đổi, sử dụng hoặc c&ocirc;ng bố của c&aacute;c nh&atilde;n hiệu n&agrave;y đều bị nghi&ecirc;m cấm.</p>
<p>
9.3 Vaber.vn c&oacute; thể c&oacute; b&ecirc;n thứ ba độc lập tham gia v&agrave;o việc cung cấp c&aacute;c dịch vụ (v&iacute; dụ, c&aacute;c nh&agrave; cung cấp chứng thực v&agrave; kiểm tra dịch vụ). Bạn kh&ocirc;ng thể sử dụng bất kỳ thương hiệu, nh&atilde;n hiệu dịch vụ hoặc logo của b&ecirc;n thứ ba độc lập đ&oacute; m&agrave; kh&ocirc;ng cần sự chấp thuận trước bằng văn bản của c&aacute;c b&ecirc;n li&ecirc;n quan.</p>
<p>
</p>
<p>
</p>