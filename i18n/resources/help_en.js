<div class="col-xs-12 no-padding padding-bot-10 margin-top-5 padding-l-r-4">
    <a href="tel:+84901701701" class="btn btn-primary pad10 col-xs-12 hidden-md hidden-lg hidden-sm"><span
    class="glyphicon glyphicon-earphone"></span>&nbsp;<span>Call Customer Support</span></a>
</div>
<div class="clearfix hidden-md hidden-lg hidden-sm col-xs-12"></div>
    <div class="col-xs-12 no-padding padding-bot-5 padding-l-r-4">
        <a id="toggerInfo" href="javascript:void(0);" data-ng-click="toggerInfo()"
        class="btn btn-primary pad10 col-xs-12 hidden-md hidden-lg hidden-sm"><span class="fa fa-eye"></span>&nbsp;<span>View help info</span></a>
    </div>

    <div class="col-sm-8">
        <h4 class="hidden-xs">View help info</h4>

        <div id="helpInfo" class="hidden-xs">
            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                <a class="action-show-detail color-primary cursor-point">&#9679; How to register for a Vaber account?</a>

                <div class="col-md-12 non-display margin-top-5 question-help">
                    <p>If you do not have a Vaber account, you can register your account with a few steps</p>

                    <p>1. Access Vaber.vn</p>

                    <p>2. Click on Register at the top</p>

                    <div class="col-md-12 col-xs-12 padding-bot-10">
                        <img src="images/I.png" class="img-thumbnail">
                        </div>

                        <p>3. Enter you email address, login name, phone number, password and choose the type of account you want to register.</p>

                        <p>4. Click Create Account</p>

                    </div>
                </div>


                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                    <a class="action-show-detail color-primary cursor-point">&#9679; How can I change my password?</a>

                    <div class="col-md-12 non-display margin-top-5 question-help">
                        <p>If you are logged into the Vaber account, you can change your password by</p>

                        <p>1. On the menu, click Update Profile</p>

                        <p>2. Select Account Setup tab</p>

                        <div class="col-md-12 col-xs-12 padding-bot-10">
                            <img src="images/II.png" class="img-thumbnail">
                            </div>

                            <p>3. Enter your current password and your new password</p>

                            <p>4. Click Save</p>

                            <p>If you forget your password, you can reset the password</p>


                        </div>
                    </div>

                    <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                        <a class="action-show-detail color-primary cursor-point">&#9679; How to reset your password?</a>

                        <div class="col-md-12 non-display margin-top-5 question-help">
                            <p>If you can not log in Vaber and need to reset your password</p>

                            <p>1. Visit logged in Vaber.vn</p>

                            <p>2. Click “Forgot your password” near the “Login” button</p>

                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                <img src="images/III.png" class="img-thumbnail">
                                </div>

                                <p>3. Enter your email address, then click Retrieve password</p>

                                <p>4. The system will send a link to reset your password to the email address you have entered</p>

                                <p>5. Log in to the email, click on the link in the email sent by Vaber and reset your password..</p>


                            </div>
                        </div>

                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                            <a class="action-show-detail color-primary cursor-point">&#9679; How to add more type of account?</a>

                            <div class="col-md-12 non-display margin-top-5 question-help">
                                <p>If your account is only one authority Customer/ Supplier and you want your account including 2 rights Customers and suppliers?</p>

                                <p>1. Log in Vaber</p>

                                <p>2.	On menu, select “Update profile”</p>

                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                    <img src="images/IV.png" class="img-thumbnail">
                                    </div>

                                    <p>3. In the Account Setup tab, select the account type to add</p>

                                    <p>4. Click Save</p>

                                </div>
                            </div>

                            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                <a class="action-show-detail color-primary cursor-point">&#9679; How do I posted requirement?</a>

                                <div class="col-md-12 non-display margin-top-5 question-help">
                                    <p>1.	Log in Vaber with Customer account</p>

                                    <p>2.	On menu, select Post requirement.</p>

                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                        <img src="images/V.png" class="img-thumbnail">
                                        </div>

                                        <p>3. Enter information about your requirement</p>

                                        <p>4. Press Register now to post a requirement </p>

                                        <p>5. If you want your requirement in more detail, easily to attractive supplier give the price, click Advantage to add more information.</p>

                                    </div>
                                </div>

                                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                    <a class="action-show-detail color-primary cursor-point">&#9679; How to update skills?</a>

                                    <div class="col-md-12 non-display margin-top-5 question-help">
                                        <p>1. Log in Vaber with Supplier account</p>

                                        <p>2. If the first login, you can list skills on display first. Or choose Update skill in the menu on the left</p>


                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                            <img src="images/VI_.png" class="img-thumbnail">
                                            </div>

                                            <p>3. Select your industry sector: </p>

                                            <div class="col-md-12 col-xs-12">
                                                <p>- Quick job</p>

                                                <p>- Human resources</p>

                                                <p>- Helper</p>

                                                <p>- Tutor</p>

                                                <p>- Another requirement</p>
                                            </div>


                                            <p>4. Select the appropriate skills with you.  </p>

                                            <p>5. Click Finish </p>

                                        </div>
                                    </div>

                                    <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                        <a class="action-show-detail color-primary cursor-point">&#9679; How to update personal information?</a>

                                        <div class="col-md-12 non-display margin-top-5 question-help">
                                            <p>1. Log in Vaber.vn</p>

                                            <p>2. In the menu, choose Update Profile</p>

                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                <img src="images/VII.png" class="img-thumbnail">
                                                </div>

                                                <p>3. On the Account Information tab, enter your personal information</p>

                                                <p>4. Click Save</p>
                                            </div>
                                        </div>

                                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                            <a class="action-show-detail color-primary cursor-point">&#9679; How to take the proposal for one requirement</a>

                                            <div class="col-md-12 non-display margin-top-5 question-help">
                                                <p>1.	Log in Vaber.vn with supplier account</p>

                                                <p>2.	In the menu, choose Manage proposed</p>

                                                <p>3.	At the requirement tab list, select Request state is open for bidding.</p>

                                                <p>4.	Click Pricing</p>


                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                    <img src="images/quan ly de xuat.png" class="img-thumbnail">
                                                    </div>

                                                    <p>5.	Displays detailed requirement, import pricing information:</p>

                                                    <div class="col-md-12 col-xs-12">
                                                        <p>-	Subject proposed</p>

                                                        <p>-	Detailed description of the proposed  </p>

                                                        <p>-	Price proposals</p>

                                                        <p>-	Attach file (CV)</p>

                                                    </div>

                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                        <img src="images/detailOrder.png" class="img-thumbnail">
                                                        </div>

                                                        <p>6.	Click Bidding</p>
                                                    </div>
                                                </div>

                                                <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                    <a class="action-show-detail color-primary cursor-point">&#9679; How do you give requirement to supplier?</a>

                                                    <div class="col-md-12 non-display margin-top-5 question-help">
                                                        <p>1.	Log in Vaber.vn with customer account</p>

                                                        <p>2.	In the menu, choose Manage requirement</p>

                                                        <p>3.	At the requirement tab list, select Request state is bidding</p>


                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                            <img src="images/detailpost.png" class="img-thumbnail">
                                                            </div>

                                                            <p>4.	Click View Details</p>

                                                            <p>5.	At the requirement details screen, displays a list of bidding</p>

                                                            <p>6. Select one or more proposals to exchange requirement</p>

                                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                <img src="images/viewdetailpost.png" class="img-thumbnail">
                                                                </div>

                                                                <p>7. Click exchange requirement</p>

                                                                <p>8. You will be required to pay fees for Vaber. For first order, you can give many vendors only 1 time payment for Vaber.</p>

                                                                <p>9. After successful payment, your requirement has been handed and pending for vendors accept that request.</p>

                                                            </div>
                                                        </div>

                                                        <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                            <a class="action-show-detail color-primary cursor-point">&#9679; How to pay for Vaber</a>

                                                            <div class="col-md-12 non-display margin-top-5 question-help">
                                                                <p>1. When you exchange requirement or accepts requirement, the screen Nganluong.vn</p>

                                                                <p>2.	Select the form you want to pay</p>

                                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                    <img src="images/payment1.png" class="img-thumbnail">
                                                                    </div>

                                                                    <p>3.	Enter your billing information</p>

                                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                        <img src="images/payment2.png" class="img-thumbnail">
                                                                        </div>

                                                                        <p>4.	Click Continue</p>

                                                                        <p>5.	Enter your account information, and then click a payment button</p>

                                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                            <img src="images/payment3.png" class="img-thumbnail">
                                                                            </div>

                                                                            <p>6.	Click OK to confirm billing information</p>

                                                                            <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                <img src="images/payment4.png" class="img-thumbnail">
                                                                                </div>

                                                                                <p>7.	After successful payment, you wait until the automatic switch on Vaber.vn</p>

                                                                                <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                    <img src="images/payment5.png" class="img-thumbnail">
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                            <div class="wrapper col-md-12 col-sm-12 col-xs-12 no-padding">
                                                                                <a class="action-show-detail color-primary cursor-point">&#9679; The error message when payment?</a>

                                                                                <div class="col-md-12 non-display margin-top-5 question-help">
                                                                                    <p>Some common mistakes in the billing process</p>

                                                                                    <p>1	You choose the payment method online ATM card, domestic bank-card. After successful payment, please do not close the browser and wait until the display automatically switches on Vaber.vn</p>

                                                                                    <p>2.	Transaction rejected by the bank issuing the card -> The payment of you not succeed, try again in a few minutes</p>

                                                                                    <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                        <img src="images/error1.png" class="img-thumbnail">
                                                                                        </div>

                                                                                        <p>3. Access denied</p>

                                                                                        <p>In case your account is debited, but display the error message "Access is denied", please call or send an email to Vaber’s Customer Care department for support.</p>

                                                                                        <div class="col-md-12 col-xs-12 padding-bot-10">
                                                                                            <img src="images/error2.png" class="img-thumbnail">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-sm-4 text-center hidden-xs ">
                                                                                <img class="img150_150" src="images/telephone_support1.jpg">
                                                                                    <h5 class="color-danger">Hot line: 0901701701</h5>
                                                                                    <h5 class="color-danger">Email: support@vaber.vn</h5>
                                                                                </div>
                                                                                <script>
                                                                                $(document).ready(function() {
                                                                                    $('.action-show-detail').click(function () {
                                                                                        $('.question-help').slideUp();
                                                                                        if($(this).parent().find(".question-help").is(":hidden")){
                                                                                            $(this).parent().find(".question-help").slideDown();
                                                                                        } else{
                                                                                            $(this).parent().find(".question-help").slideUp();
                                                                                        }



                                                                                    });

                                                                                    $("#toggerInfo").click(function(){
                                                                                    if($("#helpInfo").hasClass("hidden-xs")){
                                                                                    $("#helpInfo").removeClass("hidden-xs");
                                                                                    } else{
                                                                                    $("#helpInfo").addClass("hidden-xs");
                                                                                    }
                                                                                    })
                                                                                    });

                                                                                </script>