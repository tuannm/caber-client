
<p >
<strong>1.&nbsp;General Regulation</strong></p>
<p >
1.1	&nbsp;Your use of the website and the services, software and Vaber.vn products (collectively, the "Services" hereafter) is subject to the terms and conditions contained in this document as well as security policies, the policy document lists and any other rules and policies of Vaber.vn. This document and the regulations and policies of Vaber.vn is considered the "Terms". By visiting the site or using the services, you agree to accept and be bound by these terms. Please do not use the service or site if you do not accept all the terms.</p>
<p >
1.2	&nbsp;You may not use the Services and may not accept the Terms if (a) you are not of legal age to form a binding contract with Vaber.vn, or (b) you are not allowed to receive any services under the laws of Vietnam or other countries / regions include the country / region where you reside or where you are using the service.</p>
<p >
1.3	&nbsp;Vaber may modify any terms at any time by posting the changes involved and the terms at the website, you agree that these terms shall apply to you.</p>
<p >
1.4	&nbsp;You have signed a separate agreement (signed online or hard copy signed), with Vaber.vn or our link to any service (hereinafter referred to as the "Supplementary Agreement" ), if there is any conflict or contradiction between these Terms and Supplementary Agreement, Supplementary Agreement shall take precedence over the terms of service relationships with stakeholders.</p>
<p >
1.5	&nbsp;These terms may not otherwise be modified except in writing by an authorized officer of Vaber.vn.</p>
<p >
<strong>2.	&nbsp;Vaber’s services</strong></p>
<p >
2.1	 &nbsp;Via Vaber.vn website, Vaber provide its services, including:</p>
<p >
a)	&nbsp;For an account sign requests (hereinafter referred to as client account):</p>
<p>
&nbsp;&nbsp;&nbsp-	&nbsp;Vaber provide the following free services:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Free the right to sign/ cancellation request.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Administered request posted</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Receive and approve, reject the proposal.</p>
<p>
&nbsp;&nbsp;&nbsp-	&nbsp;Vaber provide the following take charge services here:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;The enhanced services include: red bold request, Email by number of customer account request, send SMS according to the number of accounts required. Service charge is published directly in the client account.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Transaction Fee: A fee that customers pay for information vaber proponent. Transaction fees are paid only once when customers choose the first proposal, Vaber not charge transaction fees for accepted proposals next.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Other services directly published at website Vaber.vn and direct agreements between Vaber and customers.</p>
<p >
b)&nbsp;The services of the poster suggested:</p>
<p>
&nbsp;&nbsp;&nbsp-&nbsp;Vaber provide the following free services</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Searching requests at Vaber.vn</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Creating your recommendations to clients</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;Managing the proposals</p>
<p>
&nbsp;&nbsp;&nbsp-&nbsp;Vaber provide the following take charge services here:</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;	Transaction fees: A fee that subscribers pay for Vaber proposed for information from customers. This fee is paid when the customer agrees to proposal of the proposer. Fee is paid once.</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp+ &nbsp;	Other services directly published at website Vaber.vn and direct agreements between Vaber and poster proposals.</p>
<p>
2.2	&nbsp;Vaber provides its services through Vaber.vn website. You must register in Vaber.vn to use the services of Vaber. By using accounts registered in Vaber, you agree to use the services of Vaber and the provisions attached.</p>
<p>
2.3	&nbsp;Vaber.vn can startup, change, upgrade, impose conditions, suspend, or prevent any Services (or any features within the Services) without prior notice, Foreign except in cases where Vaber chargeable service, this change will not significantly adversely affect the member pays in using such services.</p>
<p>
2.4	&nbsp;Vaber.vn may terminate, suspend or stop any services in case of detection of fraudulent acts, providing false information, causing harm to the parties concerned or offense law.</p>
<p>
2.5	&nbsp;In cases of force majeure (natural disasters, objective reasons, ..), Vaber.vn may interrupt the supply of services to members. However, you agree that Vaber.vn always try to fix this problem as quickly as possible, to provide service interruption in the event of force majeure beyond wishes of Vaber.</p>
<p>
<strong>3.&nbsp;Create information in Vaber</strong></p>
<p>
3.1	&nbsp;The information on the required and recommended in Vaber must be clear, according to the information that Vaber request. You should not post repeated your request to avoid misunderstandings and unnecessary omissions.</p>
<p>
3.2	&nbsp;You agree that the information you post in Vaber is true, does not violate the law, does not publish content that promotes, pornography, religious contents, ethnically divided, .., is right legal and valid with the terms specified in Vaber</p>
<p>
3.3	&nbsp;Systems create the emails sent to you to facilitate notification of Inquiry status and update the recommendations of members of Vaber.vn by services. If you do not wish to receive these emails, you can go directly into your account and cancel the service by closing your previous requests.</p>
<p>
3.4	&nbsp;You agree that you will not publish any information or make any act that violates the rights of any third party, including but not limited to any intellectual property rights, rights of publicity, rights interests of personality, privacy, and other rights of third parties are not specified in this paragraph.</p>
<p>
3.5	&nbsp;In case you have violated any of the terms stated above, Vaber.vn be entitled to exercise any legal enforcement as needed. Examples of enforcement actions including but not limited to erase the information was posted by you, restrict the right to send your request, issued a warning to you, or to terminate the agreement your membership with Vaber.vn,...</p>
<p>
3.6	&nbsp;The information and documents which prohibited at Vaber:</p>
<p >
1) &nbsp;	Information encourage illegal activities..</p>
<p >
2) &nbsp;	Items are racist, religious and racial insult, or promote hatred, violence, racism or religion;</p>
<p >
3) &nbsp;	Gifts, lottery, lottery or contest.</p>
<p >
4) &nbsp;	Stocks, bonds, interest rate and other securities investments; </p>
<p >
5) &nbsp;Pornographic materials / items sexuality.</p>
<p >
6) &nbsp;	The information violating other laws.</p>
<p>
3.7	&nbsp;You acknowledge and agree that Vaber.vn may provide your contact information, including but not limited to contact name, email address, telephone, mobile phone and your fax number, to proponents contact you for business opportunities in the future.</p>
<p>
3.8	&nbsp;You acknowledge and agree that Vaber.vn can use your membership information, including but not limited to personal information of the company, personal photographs, historical transaction information personal contacts, and so on, for the promotion and the services market at Vaber.vn</p>
<p>
<strong>4.	&nbsp;Obligations of Users</strong></p>
<p>
4.1	&nbsp;User</p>
<p>
a) &nbsp;	Users must be registered on the site to access or use certain services (a registered account is also called a "member" below). Except with the consent of Vaber.vn, each member may only register for an account at Vaber.vn. Vaber.vn may cancel or terminate your account if Vaber.vn account of a reason to suspect that the account was registered or simultaneously control two or more Member Account or violate the provisions of Vaber.vn. Moreover, Vaber.vn may refuse the user to register for any reason</p>
<p>
b)&nbsp;After registering on the site, each member is Vaber.vn level account and password. An account can have a web-based email account with limited storage space for users to send or receive email.</p>
<p>
c)&nbsp;	Each account and password provided by Vaber.vn is unique. Each member shall be responsible for maintaining the confidentiality and security of your account and your password and for all activities that occur under your account. Members may not share, transfer, or permit the use of your account, your ID or password by someone outside of the organization's own business members. Members agree to notify immediately if Vaber.vn aware of any unauthorized use of your password or your account or any other breach of security of your account.</p>
<p>
d)&nbsp;	Members agreed that all activities that occur under your account (including but not limited to, posting of any company or product information, click accept any other agreements or rules, or post Register to make any payments for any service, email accounts using email or SMS) will be deemed to have been authorized by the members.</p>
<p>
e)&nbsp;Members recognize that sharing your account with others, or to allow many users outside of your business organization to use your account (collectively, the "multi-purpose"), may causing irreparable damage to Vaber.vn or users of the site. Member responsible for Vaber.vn compensation, our affiliates, directors, employees, agents and representatives for any loss or damage (including but not limited to loss profits) suffered as a result of the use of your multiple accounts. Members also agreed that in many cases the use of your account or maintain the security of your account, Vaber.vn will not be liable for any loss or damage arising from such violations and may suspend or terminate the accounts of Users and no responsibility for membership.</p>
<p>
4.2. &nbsp;User’s responsibility</p>
<p >
a)&nbsp;	Each member representative, guarantee and agree that: You have the ability and authority to accept these terms, permits and authorizations and carry out the following duties</p>
<p >
b)&nbsp;	Members will be asked to provide information or documents on the organization, your business or information about the area in which you will post information at Vaber.vn or contact information as a part of the registration process on the site or use any of the services or the membership account. Each member representation, guarantee and agree that:</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	Information and documents which are filed data during registration or thereafter during the continuation of the use of the Site or Services is true, correct and complete.</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	You will maintain and promptly amend all information and documents to keep it true, accurate and complete.</p>
<p >
c)&nbsp;	After become a member, you consent to give the contact information about you in the database and authorize Vaber.vn share contact information with other users or to use your personal information in accordance with the provisions of Privacy Policy.</p>
<p >
d)&nbsp;	Each member representation, guarantee and agrees that (1) you must be responsible for obtaining all necessary permits third party and the terms related to the content of any users that you submit, post or display; (2) any User Content which you submit, post or display does not violate or infringe any copyright, patent, trademark, trade name, trade secret or any other personal or other proprietary rights of any third party ("third Party rights")</p>
<p >
e)&nbsp;	Each member continues to represent, warrant and agree that User Content you submit, post or display with responsibility:</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;It is true, accurate, complete and legitimate;</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;No errors, misleading or deceptive</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;Does not contain information is defamatory, libelous, threatening, harassing, obscene, objectionable, offensive, pornographic or harmful to minors;</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;Does not contain information that discriminate or promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;No violations of the Listing Policies, Terms or apply any other Agreement</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;Do not violate any laws and regulations (including but not limited to the export manager, consumer protection, unfair competition, false advertising or any) or promoting activities which may violate the law and the regulations currently in force;</p>
<p >
&nbsp;&nbsp;&nbsp- &nbsp;Does not contain any links directly or indirectly to any other web page includes any content that violates the terms.</p>
<p >
f) &nbsp;	Each member continues to represent, warrant and agree that you are responsible:</p>
<p>
&nbsp;&nbsp;&nbsp-&nbsp;Perform your activities on the site matching the laws and regulations in force;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	Carry out your transactions with other users of the site in the trust;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Perform their activities in accordance with the Terms and apply any additional agreements;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Do not use the service or site to defraud people or organizations (including, without limitation, selling stolen goods, using your credit / debit card is stolen);</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Do not impersonate any person or entity, misrepresent yourself or your affiliation with any person or entity;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Do not engage in spam or phishing;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Do not engage in any other illegal activity (including but not limited to those that would constitute a criminal offense, give rise to civil liability, etc.) or encourage or abet activities unlawful;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Unrelated to the attempt to copy, reproduce, exploit or expropriate more proprietary directories, databases and listings of Vaber.vn;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	Do not involve any computer viruses or other destructive devices and codes that have the effect of damaging, hindering, preventing or usurp any software or hardware system, data or personal information;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	Unrelated to any program to undermine the integrity of data, systems or networks used by Vaber.vn and / or any user of the Site or unauthorized access to the data, systems and networks;</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;	Do not engage in any activity that would otherwise create any liability against Vaber.vn.</p>
<p>
&nbsp;&nbsp;&nbsp- &nbsp;Members may not use the service account and members to participate in activities that are identical or similar to the business market of Vaber.vn ecommerce.</p>
<p >
g) &nbsp;If a member provides a business referees, members represent, warrant and agree that you have obtained all necessary consents, approval and exemption from business partners and colleagues (a) to act as your business arbitration; (b) post and publish contact details and their information, letters of recommendation and comment on their behalf; and (c) that third parties may contact such business referees to support claims or statements made about you. Furthermore, you warrant and agree that all recommendations and comments are true and accurate and third parties can contact the business referees without having to obtain your consent.</p>
<p >
h)&nbsp;	Members agree to provide all necessary information and documents for approval, and make all reasonable assistance and cooperation necessary for the provision of services cuaVaber.vn, assess whether a member has violated the terms and / or handle any complaints against members. If the members fail to do so led to delays in or suspension or termination of, the provision of any service, Vaber.vn will not be obliged to extend the service life Related nor liable for any loss or damage arising from such delay, suspension or termination.</p>
<p >
i) &nbsp;	Member acknowledges and agrees that Vaber.vn not be required to actively monitor and do not need to make any editorial control of the content of any messages or materials or information information is created, collected or accessed through the service or site. Vaber.vn does not certify, verify or confirm the content of any comments or other material or information made by any member. Each member is solely responsible for the content of their communication and may be held legally liable or responsible for the content of their comments or other documents or information.</p>
<p >
j) &nbsp;	Members acknowledge and agree that each member should be responsible to comply with the laws and regulations applicable at the respective countries to ensure that all use of the site and the services matches together.</p>
<p>
</p>
<p>
<strong>5.&nbsp;	Violations of Users</strong></p>
<p>
5.1&nbsp;	Vaber.vn have the right to remove, edit or remove any content that users send, post or display on the site that we have reason to believe are reasonable unlawful, in violation the terms, may cause Vaber.vn or our affiliates are responsible, or if found inappropriate in view of Vaber.vn</p>
<p>
5.2	&nbsp;If any member violates any of the terms or if Vaber.vn have reasonable grounds to believe that a member violated any provision, Vaber.vn have the right to impose a penalty on members, or suspend or terminate the accounts of members or subscribers of any service without any liability to the members. Vaber.vn has the right to restrict, refuse or cease to provide any other service that can be provided by current or future Vaber.vn. The penalties that can be applied Vaber.vn include warnings, removing any service product list or use other content that users submit, post or display, impose limitations on the number of lists of services, products that members can send or display, or impose restrictions on the use of any features or functionality of any service member for some time as it deems appropriate Vaber.vn discretion of us.</p>
<p>
5.3	&nbsp;Without limiting the generality of the provisions of this Article, a member shall be deemed to violate the terms in the the following cases</p>
<p >
a) &nbsp;Any complaints or claims of any third party, Vaber.vn have reasonable grounds to believe that member has intentionally or physically not perform contracts with third parties which include but not limited to, where members did not provide any services requested by third parties after receiving the purchase price, or where the member has moved to position the material does not meet the terms and description outlined in your contract with the third party,</p>
<p >
b)&nbsp;Vaber.vn has reasonable grounds  to suspect that such member has used a stolen credit card information, or false or misleading in any transactions with a counter party </p>
<p >
c) &nbsp;Vaber.vn have reasonable grounds to suspect that the information supplied by the members but not in the present or not complete or is untrue, inaccurate, or misleading, or....</p>
<p >
d)&nbsp;Vaber.vn believes that the actions of a member may cause financial loss or legal liability for Vaber.vn or any other user.</p>
<p>
5.4 &nbsp;	Vaber.vn have the right to cooperate with government agencies, private investigators and / or third parties in the investigation of any suspected criminal or civil. Moreover, Vaber.vn may disclose the identity and contact information of members, if requested by a government agency or law enforcement agency, an injured third party, or is the result of a subpoena or other legal action. Vaber.vn will not be liable for damages arising out of or resulting publish such information, and members agree not to bring any action or claim against Vaber.vn for such disclosure.</p>
<p>
5.5 &nbsp;	If a Member violations of these Terms, Vaber.vn has the right to publish the records of such violations on the site. If such violation involves or is reasonably related to the activities suspected dishonesty or fraud, Vaber.vn also reserves the right to disclose the records of such violations to their affiliates I, the branch may impose restrictions on, suspend or terminate use of the whole or part of the services offered by the branch members there for members to commit Remedial action other, and announced the record of violations of the terms on the website by or controlled by the members as the branch.</p>
<p>
5.6 &nbsp;	Vaber.vn may, at any time and under our management, imposing restrictions on, suspend or terminate the use of any services or Web sites without having to be responsible for member if Vaber.vn has received notification that the member has violated any agreements or commitments to any branch and violations related to or reasonably related activities were suspect dishonesty or fraud. Vaber.vn have the right to publish the records of such violations on the site without confirmation from the members.</p>
<p>
5.7 &nbsp;	Each member agrees to indemnify Vaber.vn, our affiliates, directors, employees, agents and representatives and to hold them harmless, from any and all losses or injuries losses, claims and liabilities (including legal costs on a full indemnity basis) which may arise from you submit, post or display any content from your use of the website or services, or from your breach of these terms.</p>
<p>
5.8 &nbsp;Each member agree that Vaber.vn not responsible for, and will not be liable to you or anyone else for any other document content is passed through the site, including space fraud, false, misleading, inaccurate documents, defamatory, offensive or illegal and the risk of damages from such material based totally with each member. Vaber.vn have the right, to get the exclusive defense and control of any matter relating to the compensation of the members, including the Member shall cooperate with Vaber.vn in compensation for losses harm.</p>
<p>
<strong>6.&nbsp;	Transactions between members</strong></p>
<p>
6.1 &nbsp;	Through the website, Vaber.vn provide web-based platform for the exchange of electronic information between the members at the areas where Vaber offer. Vaber.vn provide trading platform for web-based electronic members including without limitation: acceptance, conclusion, management requirements and propose to the terms of Vaber.vn . However, for any service, Vaber.vn not represent either the person who give the request or proposal, the provision of services in specific transactions. Vaber.vn does not control and does not have responsible for the quality, safety, legality and availability of products or services on the site or the ability of the proponent, provides services to real the requirements on site.</p>
<p>
6.2 &nbsp;	Users recognize that there may be risks dealing with the fake operation. Vaber.vn use several techniques to verify the accuracy of certain information when a member provides information for Vaber.vn during registration. However, the verification of the user on the Internet is difficult, and no Vaber.vn can not confirm the identity purposes of each user (including, but not limited to, payment of staff). We encourage you to use different media, different ways, to evaluate with whom you are dealing.</p>
<p>
6.3 &nbsp;	Each member recognizes that there exists the risk of the transaction through the site, and that it is assumed completely the risks of liability or damages of any kind in connection with activities Next relating to products or services that are the subject of the transaction using the site. Such risks include, but are not limited to, false reporting products and services, the fraud scheme, unqualified, did not meet specifications, defective product or dangerous, illegal products, delay or default in delivery or payment, cost miscalculations, breach of warranty, breach of contract and accidents. These risks also include the risks that the manufacture, import, export, distribution, offer, display, purchase, sale and / or use of the products or services offered or displayed on the site may violate or may be asserted that violate the rights of third parties, and the risk that the accounts may have to pay protection costs or other costs related to the confirmation of The third party's rights Tuesday, or connected with any claim by any party that they are entitled to protection or compensation related to the assertion of rights, requests or complaints of the parties Copyright Dispute party Tuesday. These risks also include the risks that consumers, other purchasers, end-users of products or others claiming to have been injured or damage relating to products and services initially obtained by the users of the site as a result of the transactions in connection with the use of the site may be compromised and / or assert claims arising from the use of such products dress. All those risks which are collectively referred to as "risk trade". Each user agrees that Vaber.vn will not be responsible or liable for any damages, claims, liabilities, costs, damages, nuisances, business interruption or cost of any Any kind that may arise a result of or connected with any transaction risks.</p>
<p>
6.4 &nbsp;	Users are solely responsible for all the terms and conditions of the transactions conducted through or as a result of the use of the Site or Services, including, but not limited to, Terms relating to pay, profits, warranties, shipping, insurance, fees, taxes, title, license, fines, permits, handling, transportation and storage.</p>
<p>
6.5 &nbsp;	Users agree to provide all information and documents at the request of Vaber.vn reasonable in connection with the performance of your transactions, through or as a result of the use of the site or service. Vaber.vn may suspend or terminate the accounts of any users if the user does not provide the information and documents required.</p>
<p>
6.6 &nbsp;	In the event that a member has a dispute with any party to the transaction, your account may be closed and members should compensate Vaber.vn (and its agents, affiliates, directors , officers and employees) from all claims, demands, actions, procedures, costs, expenses and damages (including without limitation any actual damage, special, incidental or consequential damages ) arising from or related to such transactions.</p>
<p>
<strong>7.&nbsp;	Limitation of Liability</strong></p>
<p>
7.1 &nbsp;	MAXIMUM EXTENT PERMITTED BY LAW UNDER PERMISSION, THE SERVICES PROVIDED BY VABER.VN "AS IS," "AS AVAILABLE" AND "WITH ALL FAULTS", AND THEREFORE VABER.VN FROM DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY WARRANTY CONDITION, QUALITY, DURABILITY, PERFORMANCE, RELIABILITY, MERCHANTABILITY OR FITNESS FOR A TABLE PARTICULAR PURPOSE. ALL SUCH WARRANTIES, REPRESENTATIONS, CONDITIONS AND COMMITMENT TO BE EXCLUDED.</p>
<p>
7.2 &nbsp;	MAXIMUM EXTENT PERMITTED BY LAW UNDER PERMISSION, VABER.VN NO REPRESENTATIONS OR WARRANTIES ABOUT THE VALUE, PRECISION, ACCURACY, RELIABILITY, QUALITY, STABILITY, COMPLETENESS OR DISTRIBUTION OF ANY INFORMATION PROVIDED ON OR THROUGH THE; VABER.VN NO REPRESENTATIONS OR WARRANTIES THAT PRODUCTION, IMPORTS, EXPORTS, DISTRIBUTORS, SUPPLIERS, DISPLAY, PURCHASE, SALE AND / OR USE OF THE PRODUCTS OR SERVICES ON THE PAGE NOT DISPLAYED VIOLATION OF ANY RIGHTS OF ANY THIRD PARTY OR SERVICES PROVIDED BY THE PARTIES; VABER.VN NO REPRESENTATIONS OR WARRANTIES RELATING TO ANY PRODUCT OR SERVICE PROVIDED OR WEBSITES IN SHOW.</p>
<p>
7.3 &nbsp;	Any material downloaded or obtained through the site is done by decision and risk of each account and each account is responsible for any damage to your computer system Vaber.com. VN or data loss may result from the download of any documents. No advice or information, whether oral or written, obtained by any account from Vaber.vn or through or from the site shall create any warranty not expressly stated in this document.</p>
<p>
7.4 &nbsp;	The site can provide to financial services or products provided by independent third parties, no warranty or representation is made regarding the services or products. Vaber.vn not responsible for any services or products.</p>
<p>
7.5 &nbsp;	Each user also agrees to compensate Vaber.vn and our affiliates, directors, officers and employees harmless, from any and all losses, damages, claims , liabilities (including legal costs on a full indemnity basis) which may arise directly or indirectly, as a result of any claims asserted by the plaintiffs or defendants Tuesday other third parties relating to products offered or displayed on web pages. Each user agrees, adding that Vaber.vn not responsible and have no liability to you, for any material posted by others, including defamatory, offensive or document estate legal and risk of damages from such material based entirely with each user. Vaber.vn have the right, to get the exclusive defense and control of all other problems make indemnification by you, in which involves you will cooperate with Vaber.vn in compensation for losses.</p>
<p>
7.6 &nbsp;	Vaber.vn will not be liable for any special damages, direct, indirect, punitive, incidental or consequential damages or any damages whatsoever (including without limitation loss of profit profits or savings, business interruption, loss of information), whether in contract, negligence, tort, equity or any other damages or by any of the following:</p>
<p>
7.7&nbsp;	Do not depend on any of the above regulations, general liability of Vaber.vn and our representatives for each user for all claims arising from the use of the site or service service in any calendar year is limited to the greater of (a) the amount of the user fee paid Vaber.vn or our affiliates in the calendar year, and (b) the number The maximum allowed by current law. The preceding sentence shall not preclude the requirements of users in order to prove actual damages. All claims arising from the use of the site or service must be filed within one (1) year from the date the cause of action arises or a longer period as prescribed under any applicable law This user account management.</p>
<p>
7.8 &nbsp;	Limitations and exclusions of liability under these Terms shall apply to the maximum extent permitted by applicable law and even Vaber.vn announced or not members when the losses incurred</p>
<p>
<strong>8.	&nbsp;Force Majeure</strong></p>
<p>
 In all cases Vaber.vn not responsible for any delay or failure or disruption of the content and services provided through the results pages directly or indirectly from acts of nature, forces, or causes beyond the reasonable control of us, including, without limitation, Internet failures, computer, telecommunications or any other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor, materials, fires, floods, storms, explosions, natural disasters, war, acts of government, the single by court order in the country or abroad or the court or not done by third parties.</p>
<p>
<strong>9.&nbsp;	Intellectual Property Rights</strong></p>
<p>
9.1 &nbsp;	Vaber.vn the sole owners of legitimate license holders of all rights and interests in the sites and content. The websites and web content represents commercial secrets and other intellectual property rights protection worldwide copyright and other laws. All rights reserved title, ownership rights and intellectual property in the web and web content will be maintained with Vaber.vn, affiliates or licensors our content, as case may be.</p>
<p>
9.2 &nbsp;Vaber.vn and icons and logos are registered trademarks or trademarks or service marks, in different countries and is protected by copyright, trademark and other rights laws own. Unauthorized copying, modification, use or publication of these marks is prohibited.</p>
<p>
9.3 &nbsp;	Vaber.vn can have independent third parties engaged in the provision of services (for example, the authentication provider and check service). You may not use any trademark, service marks or logos of such independent third parties without the prior consent in writing by the parties concerned.</p>
<p>
</p>
<p>
</p>