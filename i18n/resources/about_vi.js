    <p> <b>Vaber</b> một nền tảng tìm nguồn cho mọi nhu cầu [quy mô ] toàn cầu, cho phép mọi người có thể dễ dàng tìm ra giải pháp tốt nhất cho mọi nhu cầu của mình. Bằng cách kết nối một cách hiệu quả các giá trị thực hiện với các nhu cầu phù hợp nhất, vaber giúp khuyến khích mọi giá trị được thể hiện và trở nên có giá trị và ý nghĩa nhất.</p>


    <p class = "col-md-12 no-padding">
        <span class="col-md-3 no-padding">Tên công ty: </span>
        <span class = "col-md-9 col-xs-12 no-padding"> Công ty cổ phần giải pháp và dịch vụ công nghệ toàn cầu Vaber. </span>
    </p>

    <p class="col-md-12 no-padding">
        <span class="col-md-3 no-padding">Mã số thuế: </span>
        <span class="col-md-9 col-xs-12 no-padding">0106869382.</span>
    </p>
    <p class="col-md-12 no-padding">
        <span class="col-md-3 no-padding">Địa chỉ văn phòng: </span>
        <span
        class="col-md-9 col-xs-12 no-padding">Tầng 6, tòa nhà 84 Duy tân, Cầu Giấy, Hà Nội, Việt Nam.</span>
    </p>
    <p class="col-md-12 no-padding">
        <span class="col-md-3 no-padding">Địa chỉ kinh doanh : </span>
        <span
        class="col-md-9 col-xs-12 no-padding form-group">Số 19 phố Nam Hồng, Khương Đình, Thanh Xuân, Hà Nội, Việt Nam.</span>
    </p>