    define(['lodash'], function (_) {
        "use strict";
        return function Settings(options) {
            /**
             * To add a setting, you MUST define a default. Also,
             * THESE ARE ONLY DEFAULTS.
             * They are overridden by config.js in the root directory
             * @type {Object}
             */
            var defaults = {
                default_route: '/default',
                panel_names: []
            };

            return _.extend(defaults, options);
        };
    });
