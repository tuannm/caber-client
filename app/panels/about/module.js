/**
 * Created by Admin on 5/4/2015.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.about', []);
        app.useModule(module);

        module.controller('about', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', '$window',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, $window) {
                $scope.panelMeta = {
                    status:"Stable",
                    description:"A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
                };
                // Set and populate defaults

                var _d = {
                    panels:[]
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                };
                $scope.facebookClick = function () {
                    $window.open('https://www.facebook.com/VaberVn-1713243908909670');
                };
                $scope.twitterClick = function () {
                    $window.open('https://twitter.com/Vabervn');
                };
                $scope.googleClick = function () {
                    $window.open('https://plus.google.com/103970402430943763177');
                };
                $scope.youtubeClick = function () {
                    $window.open('https://www.youtube.com/channel/UC2ICLefhpjiUJndWpFRVDGg');
                };
                $scope.instagramClick = function () {
                    $window.open('https://instagram.com/vabervn/');
                };
                $scope.showOverview = function(){
                    if($("#overview").hasClass("hidden-xs")){
                        $("#overview").removeClass("hidden-xs");
                        $("#contact").addClass("hidden-xs");
                    } else{
                        $("#overview").addClass("hidden-xs");
                    }

                }

                $scope.showContact = function(){
                    if($("#contact").hasClass("hidden-xs")){
                        $("#contact").removeClass("hidden-xs");
                        $("#overview").addClass("hidden-xs");
                    } else{
                        $("#contact").addClass("hidden-xs");
                    }

                }
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);
    });