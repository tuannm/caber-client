/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
		'angular',
		'app',
		'lodash',
		'require',
		'css!./module.css'
	],
	function (angular, app, _, require) {
		'use strict';

		var module = angular.module('bidproject.panels.smartphone.showcase', []);
		app.useModule(module);

		module.controller('sm-showcase', ['$scope', '$rootScope', '$location', 'db', 'store', function ($scope, $rootScope, $location, db, store) {
			$scope.panelMeta = {
				status: "Stable",
				description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
			};

			// Set and populate defaults
			var _d = {
				/** @scratch /panels/text/5
				 * === Parameters
				 *
				 * mode:: `html', `markdown' or `text'
				 */
				mode: "markdown", // 'html','markdown','text'
				/** @scratch /panels/text/5
				 * content:: The content of your panel, written in the mark up specified in +mode+
				 */
				content: "",
				style: {}
			};
			_.defaults($scope.panel, _d);

			$scope.init = function () {
				$scope.ready = true;
				loadCategories()
			};

			$scope.categoryClick = function (id) {
				if ($scope.categories) {
					store.set('post');
					$scope.post = {
						category: $scope.categories[id]
					}
					store.set("post", $scope.post);
					$location.path('/post/');
					if (!$scope.$$phase) {
						$scope.$apply();
					}
				}
			};

			var loadCategories = function () {
				db.get("work.json").then(function (result) {
					if (result && result.data && result.data.length > 0) {
						$scope.categories = result.data;
					}
				});
			};

		}]);
	});
