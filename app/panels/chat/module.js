/**
 * Created by ngocnh on 4/13/15.
 */
define([
        'angular',
        'app',
        'css!./module.css'
    ],
    function (angular, app) {
        var module = angular.module('bidproject.panels.chat', []);
        app.useModule(module);

        module.controller('chat', ['$scope', '$rootScope', 'socketServices',
            function ($scope, $rootScope, socketServices) {
                socketServices.chatService();
                $scope.message = '';
                $scope.userLength = Object.keys($rootScope.userWaiting).length;

                $scope.openChat = function (user) {
                    if ($rootScope.chat_id != user.chat_id) {
                        $scope.receiver = user;
                        $rootScope.chat_id = user.chat_id;
                        $rootScope.user.chat[user.chat_id].unread = 0;
                    }
                };

                $scope.sendMessage = function (message) {
                    if (message != '') {
                        socketServices.sendMessage(message);
                        $scope.message = '';
                    }
                };

                $scope.messageTo = function (user_id) {
                    $scope.message += $scope.message == '' ? user_id : ' ' + user_id;
                };

                $scope.changeStatus = function (status) {
                    socketServices.changeStatus(status);
                }
            }
        ]);
        module.directive('chatUsers', function(){
            return {
                restrict: 'E',
                replace: true,
                templateUrl: 'app/panels/chat/chat-users.html',
                scope: true,
                link: function(scope){
                    scope.open = false;
                    scope.openToggle = function(){
                        scope.open = !scope.open;
                    };

                    scope.chatUserFilter = '';
                }
            }
        });
    }
);