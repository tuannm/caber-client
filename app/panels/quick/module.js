/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.quick', []);
        app.useModule(module);

        module.controller('quick', ['$scope', '$rootScope', '$stateParams', 'db', 'AuthService', 'AUTH_EVENTS', 'windowsize', '$location', 'Session', 'api', 'logger', 'store', '$element', '$compile', '$modal', '$timeout','navigations',
            function ($scope, $rootScope, $stateParams, db, AuthService, AUTH_EVENTS, windowsize, $location, Session, api, logger, store, $element, $compile, $modal, $timeout,navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                $scope.slides = [];
                $scope.categories = [];
                $scope.skills = [];
                $scope.currency = [];
                $scope.post = store.get('post') ? store.get('post'):{};
                $scope.selected = {};
                var username = "";
                var date = new Date();
                //Maps config
                $scope.toggleBounce = function() {
                    if (this.getAnimation() != null) {
                        this.setAnimation(null);
                    } else {
                        this.setAnimation(google.maps.Animation.BOUNCE);
                    }
                }
                $scope.post.dateStart = new Date();
                $scope.minDate = new Date();

                $scope.types = "['address']";
                $scope.placeChanged = function() {
                    $scope.place = this.getPlace();
                }
                $scope.$watch('post.more.budget', function(newVal, oldVal) {
                    if(newVal != null)
                    {
                        if (newVal.toString().length > 12) {
                            $scope.post.more.budget = oldVal;
                        }
                    }
                });
                $scope.address1 = "";
                $scope.account = {};
                $scope.account.selection = "personal";
                $scope.selection = [];
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];
                $scope.selectedSkills =[];
                $scope.init = function () {
                    $scope.ready = true;
                    $scope.selected = store.get('selected');
                    //if(angular.isUndefined($scope.post.more)){
                    //    $scope.post.more = {};
                    //}
                    if(jQuery.isEmptyObject($scope.selected)){
                        $location.path("post");
                    } else {
                        if (angular.isUndefined($scope.post.more)) {
                            $scope.post.more = {};
                        }
                        getService();
                        $scope.postEnabled = false;
                        if ($scope.post.skill != undefined) {
                            if($scope.post.isTemplate){
                                var skills = []
                                for (var i = 0; i < $scope.post.skill.length; i++) {
                                    skills.push($scope.post.more.category.skills[$scope.post.skill[i]]);
                                }
                                $scope.selectedSkills = skills;
                            } else{
                                $scope.selectedSkills = $scope.post.skill;
                                $("select").val($scope.selectedSkills).trigger("change");
                            }

                        }
                        username = Session.userId;
                        $scope.dateTo = new Date();
                        $scope.hstep = 1;
                        $scope.mstep = 1;
                        languageDeviceType();
                    }
                };
                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    username = Session.userId;
                });

                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };

                function checkInputvalue(value1,value2, callback)
                {
                    var valueReplace1 = value1.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g,"o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g,"i");
                    var valueReplace2 = value2.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g,"o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g,"i");
                    db.get("validatePost.json").then(function (result) {
                        if (result.data.length > 0) {
                            var check = result.data;
                            var found = false;
                            var validateNumber1 = valueReplace1.replace(/\s+/g, '').match(/\d+/g);
                            var validateNumber2 = valueReplace2.replace(/\s+/g, '').match(/\d+/g);
                            if(!found) {
                                if(validateNumber1 != null){
                                    for (var i = 0; i < validateNumber1.length; i++) {
                                        if (validateNumber1[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                if(validateNumber2 != null){
                                    for (var i = 0; i < validateNumber2.length; i++) {
                                        if(validateNumber2[i].length > 7){
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                for (var j = 0; j < check.length; j++) {
                                    if (valueReplace1.search(check[j].name) != -1 || valueReplace2.search(check[j].name) != -1) {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if(found){
                                callback(true);
                                return true;
                            } else{
                                callback(false);
                                return false;
                            }
                        }
                    });
                }
                $scope.removeHasError = function(){
                    var $selectSkill = $element.find("[name=skill]");
                    var formGroup = $selectSkill.closest(".form-group");
                    var helpBlock = formGroup.find(".help-block");
                    if(formGroup.hasClass("has-error")){
                        formGroup.removeClass("formGroup");
                    }
                    if(helpBlock.length > 0){
                        helpBlock.remove();
                    }
                }

                $scope.goBack = function(){
                    $scope.post.skill = $scope.selectedSkills;
                    var getAddonService = $('#addonService').dataTable().fnGetData();
                    for (var i = 0; i < getAddonService.length; i++) {
                        if (getAddonService[i].isSelected && getAddonService[i].quantity == null) {
                            getAddonService[i].isSelected = false;
                        }
                    }
                    $scope.post.more.addonService = getAddonService;
                    store.set('post',$scope.post);
                    $location.path("/post");
                }

                $scope.wizard1CompleteCallback = function (wizardData) {
                    $scope.post = store.get('post');
                    checkInputvalue($scope.post.title.toString().toLowerCase(),$scope.post.content.toString().toLowerCase(), function(result) {
                        $scope.request = {};
                        if (result) {
                            $scope.request.status = 3;
                        }
                        $scope.request.username = $scope.post.username;
                        $scope.request.serviceTypeId = $scope.post.category.id;
                        $scope.request.currencyId = 101;
                        var addonServices = [];
                        var getAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < getAddonService.length; i++) {
                            if (getAddonService[i].isSelected && getAddonService[i].quantity != null) {
                                var objAddonService = {};
                                objAddonService.addonServiceId = getAddonService[i].id;
                                objAddonService.quantity = getAddonService[i].quantity;
                                addonServices.push(objAddonService);
                            }
                            if (getAddonService[i].isSelected && getAddonService[i].quantity == null) {
                                getAddonService[i].isSelected = false;
                            }
                        }
                        $scope.request.addonServiceOrders = addonServices;
                        $scope.request.title = $scope.post.title;
                        $scope.request.content = $scope.post.content;
                        var i;
                        var skill = [];
                        if ($scope.selectedSkills != undefined) {
                            for (i = 0; i < $scope.selectedSkills.length; i++) {
                                skill.push($scope.selectedSkills[i].name);
                            }
                        }
                        $scope.request.skill = skill.toString();
                        $scope.request.budgetFrom = $scope.post.budgetTo;
                        $scope.request.budgetTo = null;
                        $scope.request.bonus = null;
                        $scope.request.expiredDate = $scope.post.expiredDate;
                        var dataMore = {
                            "city": $scope.post.city,
                            "position": $scope.post.more.address,
                            "workType": $scope.post.more.category.name,
                            "timeStart": $scope.post.more.timeFrom,
                            "timeFinish": $scope.post.more.timeTo,
                            "dateStart": $scope.post.more.dateStart,
                            "salary": $scope.post.more.budget,
                            "sex": null,
                            "career": null,
                            "addonService": getAddonService
                        };
                        $scope.request.jsonData = JSON.stringify(dataMore);
                        if (addonServices.length > 0) {
                            var modalNotifyAddonService;
                            modalNotifyAddonService = $modal.open({
                                templateUrl: "app/partials/form/notifyAddonService.html",
                                controller: 'NotifyAddonServiceDialogCtrl',
                                resolve: {
                                    data: function () {
                                        return getAddonService;
                                    }
                                }
                            });
                            modalNotifyAddonService.result.then((function (isSubmit) {
                                if (isSubmit) {
                                    $scope.request.addonPayment = 0;
                                    console.log("$scope.request", $scope.request);
                                    api.post('serviceorder/v1', $scope.request).then(function (res) {
                                        console.log("res", res);
                                        store.set('post');
                                        if (res.data.result) {
                                            var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + res.data.serviceOrder.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                                            $rootScope.paymentUrl(url);
                                        } else {
                                            logger.logError(res.data.error.description);
                                        }
                                    });
                                }

                            }), function () {
                            });
                        } else {
                            api.post('serviceorder/v1', $scope.request).then(function (res) {
                                console.log("res", res);
                                store.set('post');
                                if (res.data.result) {
                                    $location.path("/detailpost");
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            });
                        }
                    });
                };

                $scope.validateAddon = function (index, value, element) {
                    var listAddonService = $('#addonService').dataTable().fnGetData();
                    for (var i = 0; i < listAddonService.length; i++) {
                        if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != element.scope().aData.id) {
                            listAddonService[i].isSelected = !listAddonService[i].isSelected;
                        }
                    }
                };

                $scope.setCount = function (aData) {
                    var $input = $element.find('[name=' + aData.type + ']');
                    if (!aData.isSelected) {
                        if (aData.isInput) {
                            aData.quantity = null;
                            var $parent = $input.closest('.form-group');
                            var spanError = $parent.find(".help-block");
                            if ($parent.hasClass("has-error")) {
                                $parent.removeClass("has-error");
                            }
                            if ($parent.hasClass("has-success")) {
                                $parent.removeClass("has-success");
                            }
                            if (spanError.length > 0) {
                                spanError.remove();
                            }
                        }
                    } else {
                        var listAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < listAddonService.length; i++) {
                            if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != aData.id) {
                                listAddonService[i].isSelected = !listAddonService[i].isSelected;
                            }
                        }
                    }

                };

                function getService() {
                    api.get('addonservice/v1').then(function (res) {
                        console.log("resAddon", res);
                        var data;
                        if ($scope.post.more.addonService != undefined) {
                            if($scope.post.isTemplate){
                                for (var i = 0; i < res.data.addonServices.length; i++) {
                                    if (res.data.addonServices[i].id == 1) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                }
                                for (var i = 0; i < res.data.addonServices.length; i++) {
                                    if (res.data.addonServices[i].id == 4) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 5) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    for (var j = 0; j < $scope.post.more.addonService.length; j++) {
                                        if(res.data.addonServices[i].id == $scope.post.more.addonService[j].id){
                                            res.data.addonServices[i].quantity = $scope.post.more.addonService[j].quantity;
                                            res.data.addonServices[i].description = $scope.post.more.addonService[j].description;
                                            res.data.addonServices[i].isSelected = true;
                                        }
                                    }
                                }
                                data = res.data.addonServices;
                            } else{
                                data = $scope.post.more.addonService;
                            }
                        } else {
                            for (var i = 0; i < res.data.addonServices.length; i++) {
                                if (res.data.addonServices[i].id == 4) {
                                    res.data.addonServices.splice(i, 1);
                                }
                                if (res.data.addonServices[i].id == 5) {
                                    res.data.addonServices.splice(i, 1);
                                }
                                if (res.data.addonServices[i].id == 1) {
                                    res.data.addonServices.splice(i, 1);
                                }
                                if (res.data.addonServices[i].id == 2) {
                                    res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay ứng viên tiềm năng của yêu cầu. Yêu cầu đến trực tiếp nhiều đối tượng,dễ dàng nhận được nhiều đề xuất hơn";
                                }
                            }
                            data = res.data.addonServices;
                        }
                        $scope.serviceTable = {
                            "aaData": data,
                            "paging": false,
                            "columns": [
                                {"data": "null"},
                                {"data": "null"},
                                {"data": "null"},
                                {"data": "null"},
                                {"data": "null"}
                            ],
                            "columnDefs": [
                                {
                                    "targets": 0,
                                    "data": null,
                                    "width": "2%",
                                    "class": "id",
                                    "render": function (data, type, full, meta) {
                                        if(!full.isSelected){
                                            full.isSelected = false;
                                        }
                                        var element = angular.element("<div></div>");
                                        element.append('<label class="ui-checkbox"><input ng-model="aData.isSelected" ng-change="$parent.setCount(aData)" type="checkbox"><span></span></label>');
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 1,
                                    "data": null,
                                    "width": "5%",
                                    "class": "name",
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element("<div></div>");
                                        element.append('<span class="label label-warning">' + full.name + '</span>');
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 2,
                                    "data": null,
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element("<div></div>");
                                        element.append(full.description);
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 3,
                                    "data": null,
                                    "width": "20%",
                                    "class": "text-right",
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element("<div></div>");
                                        var formatServiceCost = "";
                                        if(full.type == "so_sms"){
                                            formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/SMS";
                                        } else if(full.type == "so_email"){
                                            formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/Email";
                                        } else{
                                            formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                        }

                                        var formatServiceCostSalse = "";
                                        if(full.serviceOldCost != null){
                                            if(full.type == "so_sms"){
                                                formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/SMS";
                                            } else if(full.type == "so_email"){
                                                formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/Email";
                                            } else{
                                                formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                            }
                                        }
                                        element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 4,
                                    "data": null,
                                    "width": "15%",
                                    "class": "quantity",
                                    "render": function (data, type, full, meta) {
                                        if(full.id != 3){
                                            full.isInput = true;
                                            if(full.quantity == null){
                                                full.quantity = null;
                                            }
                                        } else{
                                            full.isInput = false;
                                            full.quantity = 1;
                                        }
                                        var element = angular.element("<div></div>");
                                        if (full.isInput) {
                                            element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><input ng-disabled="!aData.isSelected" name="' + full.type + '" data-smart-validate-input data-required data-message-required="validate field" ng-model="aData.quantity" maxlength="6" reset-field class="form-control col-md-12" type="tel" maxlength="15" format-currency numbers-only data-i18n="i18n-placeholder" placeholder="enter the number"/></div>');
                                        } else {
                                            element.append('<span>' + full.quantity + '</span>');
                                        }

                                        return element.html();
                                    }
                                }
                            ]
                        };

                    });
                }

                $scope.$on("window.width", function (object, type) {
                    //var rowDetail = $element.find(".row-detail");
                    //rowDetail.remove();
                    //var detailShow = $element.find(".detail-show");
                    //detailShow.removeClass("detail-show");
                    //detailShow.removeClass("active");
                });

                $scope.loadSampleDescription = function(e){
                    if(e == 1){
                        $scope.post.content = 'Tôi cần sửa tivi trong nhà, Tivi nhãn hiệu sony, màn hình phẳng. Tôi rảnh từ 7-9h tối hôm nay hoặc 5-7h chiều ngày mai.';
                    }else if(e == 2){
                        $scope.post.content = 'Tôi cần người vệ sinh nhà cửa. Nhà tôi 3 tầng, diện tích sàn 55m vuông, không nhiều đồ phức tạp. Có thể dọn vào chiều mai hoặc chiều ngày kia.';
                    }else if(e == 3){
                        $scope.post.content = 'Tôi cần ship đồ từ đường Duy Tân Cầu Giấy về bờ Hồ, tôi cần chuyển đồ ngay trong chiều nay.';
                    }
                };
                var chosenservicemore = $('[chosenservicemore]', $element);
                var priceservice = $('[priceservice]', $element);
                var ele;
                var html;
                function languageDeviceType() {
                    $scope.$on("window.size", function (object, type) {
                        if (type === "Desktop") {
                            ele = angular.element('<span data-i18n="chosen service more"></span>');
                            html = $compile(ele)($scope);
                            chosenservicemore.html(html);

                        } else {
                            ele = angular.element('<span data-i18n="chosen service mobile"></span>');
                            html = $compile(ele)($scope);
                            chosenservicemore.html(html);
                        }
                    });
                    if (windowsize.devicetype() != "Desktop") {
                        ele = angular.element('<span data-i18n="chosen service mobile"></span>');
                        html = $compile(ele)($scope);
                        chosenservicemore.html(html);
                    }
                    else {
                        ele = angular.element('<span data-i18n="chosen service more"></span>');
                        html = $compile(ele)($scope);
                        chosenservicemore.html(html);
                    }
                }
            }]);
        module.controller('NotifyAddonServiceDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                var typeAddonService = "";
                var removeValFromIndex = [];
                var totalPayment = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isSelected && data[i].quantity != null) {
                        typeAddonService += data[i].name + ", ";
                        totalPayment += parseInt(data[i].quantity * data[i].serviceCost);
                    } else {
                        removeValFromIndex.push(i);
                    }
                }
                var resultData = $.grep(data, function (n, i) {
                    return $.inArray(i, removeValFromIndex) == -1;
                });
                $scope.typeAddonService = typeAddonService;
                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $scope.totalPaymentAddonService = {
                    "aaData": resultData,
                    "paging": false,
                    "columns": [
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"}
                    ],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "class": "id text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append('<span>' + full.name + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 1,
                            "data": null,
                            "class": "name text-right",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                var formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                var formatServiceCostSalse = "";
                                if(full.serviceOldCost != null){
                                    formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                }
                                element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 2,
                            "data": null,
                            "class": "name text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append('<span>' + full.quantity + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 3,
                            "data": null,
                            "width": "20%",
                            "class": "text-right",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                var total = parseInt(full.quantity * full.serviceCost);
                                element.append('<span>' + total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>');
                                return element.html();
                            }
                        }
                    ]
                };

                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $scope.confirmReject = function () {
                    $modalInstance.close(true);
                };
                $scope.cancelReject = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);

    });