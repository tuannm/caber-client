/**
 * Created by Admin on 5/4/2015.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.countbyuser', []);
        app.useModule(module);

        module.controller('countbyuser', ['$scope', '$rootScope', '$q', '$stateParams', 'api', 'logger', 'USER_ROLES', 'Session',
            function ($scope, $rootScope, $q, $stateParams, api, logger, USER_ROLES, Session) {
                $scope.panelMeta = {
                    status:"Stable",
                    description:"A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
                };
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode:"markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content:"",
                    style:{}
                };
                _.defaults($scope.panel, _d);

                $scope.line1 = {};
                $scope.line1.data = [];
                $scope.line1.options = {
                    series:{
                        lines:{
                            show:true,
                            fill:true,
                            fillColor:{
                                colors:[
                                    {
                                        opacity:0
                                    },
                                    {
                                        opacity:0.3
                                    }
                                ]
                            }
                        },
                        points:{
                            show:true,
                            lineWidth:2,
                            fill:true,
                            fillColor:"#ffffff",
                            symbol:"circle",
                            radius:5
                        }
                    },
                    colors:["#31C0BE", "#8170CA", "#E87352"],
                    tooltip:true,
                    tooltipOpts:{
                        defaultTheme:false
                    },
                    grid:{
                        hoverable:true,
                        clickable:true,
                        tickColor:"#f9f9f9",
                        borderWidth:1,
                        borderColor:"#eeeeee"
                    },
                    xaxis:{
                        ticks:[
                            [1, 'Jan.'],
                            [2, 'Feb.'],
                            [3, 'Mar.'],
                            [4, 'Apr.'],
                            [5, 'May'],
                            [6, 'June'],
                            [7, 'July'],
                            [8, 'Aug.'],
                            [9, 'Sept.'],
                            [10, 'Oct.'],
                            [11, 'Nov.'],
                            [12, 'Dec.']
                        ]
                    }
                };
                $scope.pie = {};
                $scope.pie.data = [];
                $scope.pie.options = {
                    series:{
                        pie:{
                            show:true,
                            radius:1,
                            label:{
                                show:true,
                                radius:3 / 4,
                                formatter:function (label, series) {
                                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
                                },
                                background:{
                                    opacity:0.5
                                }
                            }
                        }
                    },
                    legend:{
                        show:false
                    }
                };

                var totalCustomer;
                var totalSupplier;

                var totalOrderOpening;
                var totalOrderProcessing;
                var totalOrderClosed;
                var totalOrderValidating;

                var totalProposalOpening;
                var totalProposalSubmitting;
                var totalProposalAccepted;
                var totalProposalConfirmed;
                var totalProposalRejected;
                var totalProposalClosed;
                var totalProposalValidating;
                var totalProposalAssigned;

                var getAllOrderForCustomer = function () {
                    var deferred = $q.defer();
                    api.get('count/totals/v1?username=' + Session.userId + '&role=ROLE_CUSTOMER').then(function (res) {
                        console.log("res:", res);
                        if (res.data.result) {
                            if (Session.userRole == "customer") {
                                console.log("ROLE_CUSTOMER len tieng");
                                deferred.resolve(res.data.result);
                                totalOrderOpening = res.data.totalOrderOpening;
                                $scope.totalOrderOpening = totalOrderOpening;
                                totalOrderProcessing = res.data.totalOrderProcessing;
                                $scope.totalOrderProcessing = totalOrderProcessing;
                                totalOrderClosed = res.data.totalOrderClosed;
                                $scope.totalOrderClosed = totalOrderClosed;
                                totalOrderValidating = res.data.totalOrderValidating;
                                $scope.totalOrderValidating = totalOrderValidating;
                                totalCustomer = totalOrderOpening + totalOrderProcessing + totalOrderClosed + totalOrderValidating;
                                console.log("$scope.totalOrderOpening :", totalOrderOpening);
                                console.log("$scope.totalOrderProcessing :", totalOrderProcessing);
                                console.log("$scope.totalOrderClosed :", totalOrderClosed);
                                console.log("$scope.totalOrderValidating :", totalOrderValidating);
                                console.log("$scope.totalCustomer :", totalCustomer);
                            }

                        } else {
                            deferred.reject();
                            logger.logError(res.data.error);
                        }
                    });
                    return deferred.promise;
                }

                var getAllOrderForSupplier = function () {
                    var deferred = $q.defer();
                    api.get('count/totals/v1?username=' + Session.userId + '&role=ROLE_SUPPLIER').then(function (res) {
                        console.log("res total:", res);
                        if (res.data.result) {
                            if (Session.userRole == "supplier") {
                                console.log("ROLE_SUPPLIER len tieng");
                                deferred.resolve(res.data.result);
                                totalProposalOpening = res.data.totalProposalOpening;
                                $scope.totalProposalOpening = totalProposalOpening;
                                totalProposalSubmitting = res.data.totalProposalSubmitting;
                                $scope.totalProposalSubmitting = totalProposalSubmitting;
                                totalProposalAccepted = res.data.totalProposalAccepted;
                                $scope.totalProposalAccepted = totalProposalAccepted;
                                totalProposalConfirmed = res.data.totalProposalConfirmed;
                                $scope.totalProposalConfirmed = totalProposalConfirmed;
                                totalProposalRejected = res.data.totalProposalRejected;
                                $scope.totalProposalRejected = totalProposalRejected;
                                totalProposalClosed = res.data.totalProposalClosed;
                                $scope.totalProposalClosed = totalProposalClosed;
                                totalProposalValidating = res.data.totalProposalValidating;
                                $scope.totalProposalValidating = totalProposalValidating;
                                totalProposalAssigned = res.data.totalProposalAssigned;
                                $scope.totalProposalAssigned = totalProposalAssigned;
                                totalSupplier = totalProposalOpening + totalProposalSubmitting + totalProposalAccepted + totalProposalConfirmed + totalProposalRejected + totalProposalClosed + totalProposalValidating + totalProposalAssigned;
                                console.log("$scope.totalProposalOpening :", totalProposalOpening);
                                console.log("$scope.totalProposalSubmitting :", totalProposalSubmitting);
                                console.log("$scope.totalProposalAccepted :", totalProposalAccepted);
                                console.log("$scope.totalProposalConfirmed :", totalProposalConfirmed);
                                console.log("$scope.totalProposalRejected :", totalProposalRejected);
                                console.log("$scope.totalProposalClosed :", totalProposalClosed);
                                console.log("$scope.totalProposalValidating :", totalProposalValidating);
                                console.log("$scope.totalProposalAssigned :", totalProposalAssigned);
                                console.log("$scope.totalSupplier :", totalSupplier);
                            }

                        } else {
                            deferred.reject();
                            logger.logError(res.data.error);
                        }
                    });
                    return deferred.promise;
                }

                var getAllOrderForBroker = function () {
                    var deferred = $q.defer();
                    api.get('count/totals/v1?username=' + Session.userId + '&role=ROLE_SUPPLIER&userType=B').then(function (res) {
                        console.log("res total:", res);
                        if (res.data.result) {
                            if (Session.userRole == "broker") {
                                console.log("ROLE_Broker len tieng");
                                deferred.resolve(res.data.result);
                                totalProposalOpening = res.data.totalProposalOpening;
                                $scope.totalProposalOpening = totalProposalOpening;
                                totalProposalSubmitting = res.data.totalProposalSubmitting;
                                $scope.totalProposalSubmitting = totalProposalSubmitting;
                                totalProposalAccepted = res.data.totalProposalAccepted;
                                $scope.totalProposalAccepted = totalProposalAccepted;
                                totalProposalConfirmed = res.data.totalProposalConfirmed;
                                $scope.totalProposalConfirmed = totalProposalConfirmed;
                                totalProposalRejected = res.data.totalProposalRejected;
                                $scope.totalProposalRejected = totalProposalRejected;
                                totalProposalClosed = res.data.totalProposalClosed;
                                $scope.totalProposalClosed = totalProposalClosed;
                                totalProposalValidating = res.data.totalProposalValidating;
                                $scope.totalProposalValidating = totalProposalValidating;
                                totalProposalAssigned = res.data.totalProposalAssigned;
                                $scope.totalProposalAssigned = totalProposalAssigned;
                                totalSupplier = totalProposalOpening + totalProposalSubmitting + totalProposalAccepted + totalProposalConfirmed + totalProposalRejected + totalProposalClosed + totalProposalValidating + totalProposalAssigned;
                                console.log("$scope.totalProposalOpening :", totalProposalOpening);
                                console.log("$scope.totalProposalSubmitting :", totalProposalSubmitting);
                                console.log("$scope.totalProposalAccepted :", totalProposalAccepted);
                                console.log("$scope.totalProposalConfirmed :", totalProposalConfirmed);
                                console.log("$scope.totalProposalRejected :", totalProposalRejected);
                                console.log("$scope.totalProposalClosed :", totalProposalClosed);
                                console.log("$scope.totalProposalValidating :", totalProposalValidating);
                                console.log("$scope.totalProposalAssigned :", totalProposalAssigned);
                                console.log("$scope.totalSupplier :", totalSupplier);
                            }

                        } else {
                            deferred.reject();
                            logger.logError(res.data.error);
                        }
                    });
                    return deferred.promise;
                }

                $scope.init = function () {
                    console.log("Session.userRole", Session.userRole);
                    if (Session.userRole == "supplier") {
//                        $('#supplier').removeClass('hide');
                        getAllOrderForSupplier().then(function (data) {
                            if (totalSupplier == 0) {
                                $('#countbyuser').addClass('hide');
                            } else {
                                $('#supplier').removeClass('hide');
                            }
                            console.log("data", data);
                            var data = [
                                {
//                                    label: "Đang được yêu cầu",
                                    data:totalProposalOpening * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang đặt gia",
                                    data:totalProposalSubmitting * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã được chấp ",
                                    data:totalProposalAccepted * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang thanh toán",
                                    data:totalProposalConfirmed * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã từ chối",
                                    data:totalProposalRejected * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã kết thúc",
                                    data:totalProposalClosed * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang validate",
                                    data:totalProposalValidating * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang Assigned",
                                    data: totalProposalAssigned * 100 / totalSupplier
                                }
                            ]
                            if (totalProposalOpening * 100 / totalSupplier != 0) {
                                $('#opening').removeClass('hide');
                            }
                            if (totalProposalSubmitting * 100 / totalSupplier != 0) {
                                $('#submitting').removeClass('hide');
                            }
                            if (totalProposalAccepted * 100 / totalSupplier != 0) {
                                $('#accepted').removeClass('hide');
                            }
                            if (totalProposalConfirmed * 100 / totalSupplier != 0) {
                                $('#confirmed').removeClass('hide');
                            }
                            if (totalProposalRejected * 100 / totalSupplier != 0) {
                                $('#rejected').removeClass('hide');
                            }
                            if (totalProposalClosed * 100 / totalSupplier != 0) {
                                $('#closed').removeClass('hide');
                            }
                            if (totalProposalValidating * 100 / totalSupplier != 0) {
                                $('#validating').removeClass('hide');
                            }
                            if (totalProposalAssigned * 100 / totalSupplier != 0) {
                                $('#Assigned').removeClass('hide');
                            }
                            console.log("data", data);
                            $scope.pie.data = data;
                        });
                    }
                    if (Session.userRole == "customer") {
                        getAllOrderForCustomer().then(function (data) {
                            if (totalCustomer == 0) {
                                $('#countbyuser').addClass('hide');
                            } else {
                                $('#customer').removeClass('hide');
                            }
                            console.log("data", data);
                            var data = [
                                {
//                                    label: "Đang mở(ko PP)",Sdangduocyeucau totalOrderOpening
                                    data:totalOrderOpening * 100 / totalCustomer
                                },
                                {
//                                    label: "Đang mở(có PP)",Sdangdatgia totalOrderProcessing
                                    data:totalOrderProcessing * 100 / totalCustomer
                                },
                                {
//                                    label: "Đã kết thúc",Sdaduocchapnhan totalOrderClosed
                                    data:totalOrderClosed * 100 / totalCustomer
                                },
                                {
//                                    label: "Đang validate",Sdangthanhtoan totalOrderValidating
                                    data:totalOrderValidating * 100 / totalCustomer
                                }
                            ]
                            if (totalOrderOpening * 100 / totalCustomer != 0) {
                                $('#OrderOpening').removeClass('hide');
                            }
                            if (totalOrderProcessing * 100 / totalCustomer != 0) {
                                $('#OrderProcessing').removeClass('hide');
                            }
                            if (totalOrderClosed * 100 / totalCustomer != 0) {
                                $('#OrderClosed').removeClass('hide');
                            }
                            if (totalOrderValidating * 100 / totalCustomer != 0) {
                                $('#OrderValidating').removeClass('hide');
                            }

                            console.log("data", data);
                            console.log("==> % Đang mở(ko PP)", totalOrderOpening * 100 / totalCustomer);
                            console.log("==> % Đang mở(có PP)", totalOrderProcessing * 100 / totalCustomer);
                            console.log("==> % Đã đóng", totalOrderClosed * 100 / totalCustomer);
                            console.log("==> % Đang validate", totalOrderValidating * 100 / totalCustomer);
                            $scope.pie.data = data;

                        });
                    }
                    if (Session.userRole == "broker") {
//                        $('#supplier').removeClass('hide');
                        getAllOrderForBroker().then(function (data) {
                            if (totalSupplier == 0) {
                                $('#countbyuser').addClass('hide');
                            } else {
                                $('#supplier').removeClass('hide');
                            }
                            console.log("data", data);
                            var data = [
                                {
//                                    label: "Đang được yêu cầu",
                                    data: totalProposalOpening * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang đặt gia",
                                    data: totalProposalSubmitting * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã được chấp ",
                                    data: totalProposalAccepted * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang thanh toán",
                                    data: totalProposalConfirmed * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã từ chối",
                                    data: totalProposalRejected * 100 / totalSupplier
                                },
                                {
//                                    label: "Đã kết thúc",
                                    data: totalProposalClosed * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang validate",
                                    data: totalProposalValidating * 100 / totalSupplier
                                },
                                {
//                                    label: "Đang Assigned",
                                    data: totalProposalAssigned * 100 / totalSupplier
                                }
                            ]
                            if (totalProposalOpening * 100 / totalSupplier != 0) {
                                $('#opening').removeClass('hide');
                            }
                            if (totalProposalSubmitting * 100 / totalSupplier != 0) {
                                $('#submitting').removeClass('hide');
                            }
                            if (totalProposalAccepted * 100 / totalSupplier != 0) {
                                $('#accepted').removeClass('hide');
                            }
                            if (totalProposalConfirmed * 100 / totalSupplier != 0) {
                                $('#confirmed').removeClass('hide');
                            }
                            if (totalProposalRejected * 100 / totalSupplier != 0) {
                                $('#rejected').removeClass('hide');
                            }
                            if (totalProposalClosed * 100 / totalSupplier != 0) {
                                $('#closed').removeClass('hide');
                            }
                            if (totalProposalValidating * 100 / totalSupplier != 0) {
                                $('#validating').removeClass('hide');
                            }
                            if (totalProposalAssigned * 100 / totalSupplier != 0) {
                                $('#Assigned').removeClass('hide');
                            }
                            console.log("data", data);
                            $scope.pie.data = data;
                        });
                    }
                }
            }]);

    });