/** @scratch /panels/5
 * include::panels/column.asciidoc[]
 */

/** @scratch /panels/column/0
 * == Column
 * Status: *Stable*
 *
 * A pseudo panel that lets you add other panels to be arranged in a column with defined heights.
 * While the column panel is stable, it does have many limitations, including the inability to drag
 * and drop panels within its borders. It may be removed in a future release.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'config'
    ],
    function (angular, app, _, config) {
        'use strict';

        var module = angular.module('bidproject.panels.container', []);

        app.useModule(module);

        module.controller('selectcv', [ '$scope', '$rootScope', '$timeout', function($scope, $rootScope, $timeout) {
            $scope.panelMeta = {
                status  : "Stable",
                description : "A pseudo panel that lets you add other panels to be arranged in a column with"+
                "defined heights."
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode    : "markdown", // 'html','markdown','text'
                /** @scratch /panels/column/3
                 * === Parameters
                 *
                 * panel:: An array of panel objects
                 */
                panels : []
            };
            _.defaults($scope.panel,_d);

            $scope.init = function(){
                $scope.reset_panel();
            };

            $scope.toggle_row = function(panel) {
                panel.collapse = panel.collapse ? false : true;
                if (!panel.collapse) {
                    $timeout(function() {
                        $scope.send_render();
                    });
                }
            };

            $scope.send_render = function() {
                $scope.$broadcast('render');
            };

            $scope.add_panel = function(panel,newPanel) {
                panel.panels.push(newPanel);
            };

            $scope.reset_panel = function(type) {
                $scope.new_panel = {
                    loading: false,
                    error: false,
                    sizeable: false,
                    draggable: false,
                    removable: false,
                    span: 12,
                    height: "150px",
                    editable: true,
                    type: type
                };
            };

        }]);

        module.directive('markdown', [ '$compile', '$timeout', function($compile,$timeout) {
            return {
                scope : {
                    new_panel:"=panel",
                    row:"=",
                    config:"=",
                    dashboards:"=",
                    type:"=type"
                },
                link: function(scope, elem) {
                    scope.$on('render', function () {
                    });
                }
            };
        }]);

    });