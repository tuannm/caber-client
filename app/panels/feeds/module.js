/**
 * Created by Admin on 5/4/2015.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.feeds', []);
        app.useModule(module);

        module.controller('feeds', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', 'USER_ROLES', '$modal', 'navigations', 'store',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, USER_ROLES, $modal, navigations, store) {
                $scope.panelMeta = {
                    status:"Stable",
                    description:"A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
                };
                // Set and populate defaults

                var _d = {
                    panels:[]
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                $scope.minDate = new Date();
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];
                $scope.searchFeed = {};

                var data;
                var skillTemp = "";
                $scope.newOrder = {

                    "method":"POST",
                    lang:{
                        vi:{
                            "sSearch":"<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                            "sLengthMenu":"_MENU_",
                            "sProcessing":"Đang xử lý...",
                            "sEmptyTable":"Không tìm thấy yêu cầu nào phù hợp",
                            "sZeroRecords":"Không tìm thấy yêu cầu nào phù hợp",
                            "sInfo":"Đang xem _START_ đến _END_ trong tổng số _TOTAL_ yêu cầu",
                            "sInfoEmpty":"Đang xem 0 đến 0 trong tổng số 0 yêu cầu",
                            "sInfoFiltered":"(được lọc từ _MAX_ yêu cầu)",
                            "sInfoPostFix":"",
                            "sUrl":"",
                            "oPaginate":{
                                "sFirst":"Đầu",
                                "sPrevious":"Trước",
                                "sNext":"Tiếp",
                                "sLast":"Cuối"
                            }
                        },
                        en:{
                            "sEmptyTable":"No data available in table",
                            "sInfo":"Showing _START_ to _END_ of _TOTAL_ order",
                            "sInfoEmpty":"Showing 0 to 0 of 0 order",
                            "sInfoFiltered":"(filtered from _MAX_ total order)",
                            "sInfoPostFix":"",
                            "sInfoThousands":",",
                            "sLengthMenu":"_MENU_",
                            "sLoadingRecords":"Loading...",
                            "sProcessing":"Processing...",
                            "sSearch":"<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                            "sZeroRecords":"No matching records found",
                            "oPaginate":{
                                "sFirst":"First",
                                "sLast":"Last",
                                "sNext":"Next",
                                "sPrevious":"Previous"
                            }
                        }
                    },
                    "columns":[
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},
                        {"data":"null"},

                        {"data":"modifiedDate", "bVisible":false, "iDataSort":10}
                    ],
                    "order":[
                        [10, "desc"]
                    ],
                    "columnDefs":[
                        {
                            "targets":0,
                            "data":null,
                            "class":"text-center hide-mobile verticalMiddle",
                            "render":function (data, type, full) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }
                                var element = angular.element("<div></div>");
                                if (full.id != null) {

                                    if (found) {
                                        element.append('<span class="boldRed">' + full.id + '</span>');
                                    }
                                    else {
                                        element.append('<span>' + full.id + '</span>');
                                    }
                                } else {
                                    element.append('<span></span>');
                                }
                                return element.html();
                            }
                        },
                        {
                            "targets":1,
                            "data":null,
                            "width":"20%",
                            "class":"verticalMiddle",
                            "render":function (data, type, full, meta) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }
                                var element = angular.element("<div></div>");
                                var desktop = angular.element("<div></div>");
                                if (full.status != 2) {
                                    if (found) {
                                        desktop.append('<a href="#/detailAdvanceSearch/' + full.id + '" class=" hide-when-mobile boldRed">' + full.title + ' </a>');
                                    }
                                    else {
                                        desktop.append('<a href="#/detailAdvanceSearch/' + full.id + '" class="hide-when-mobile">' + full.title + ' </a>');
                                    }
                                } else {
                                    if (found) {
                                        desktop.append('<span class=" hide-when-mobile boldRed">' + full.title + ' </span>');
                                    }
                                    else {
                                        desktop.append('<span class=" hide-when-mobile">' + full.title + ' </span>');
                                    }
                                }

                                var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                var header = angular.element('<div class="header-mobile"></div>');
                                var body = angular.element('<div class="body"></div>');

                                var pullLeft = angular.element('<div class="col-xs-8 no-margin no-padding footer-pull-left-mobile"></div>');

                                if (found) {
                                    var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile boldRed"></div>');
                                }
                                else {
                                    var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile"></div>');
                                }
                                if (full.expiredDate != null) {
                                    var date = new Date(full.expiredDate);
                                    var day = "";
                                    var month = "";
                                    if (String(date.getDate()).length < 2) {
                                        day = "0" + (date.getDate());
                                    } else {
                                        day = date.getDate();
                                    }
                                    if (String(date.getMonth() + 1).length < 2) {
                                        month = "0" + (date.getMonth() + 1);
                                    } else {
                                        month = (date.getMonth() + 1);
                                    }
                                    if (found) {
                                        header.append('<div class="col-xs-8 no-margin no-padding boldRed"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4 boldRed">' + day + '/' + month + '/' + date.getFullYear() + '</div>');
                                    }
                                    else {
                                        header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4">' + day + '/' + month + '/' + date.getFullYear() + '</div>');
                                    }
                                } else {
                                    if (found) {
                                        header.append('<div class="col-xs-8 no-margin no-padding boldRed"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4 boldRed"></div>');
                                    }
                                    else {
                                        header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4"></div>');
                                    }
                                }

                                if (full.jsonData != null) {
                                    var dataMore = JSON.parse(full.jsonData);
                                    var isMore = false;
                                    if (dataMore.information != undefined && dataMore.information.length > 0) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                isMore = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (isMore) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                if (dataMore.information[i].input != null) {

                                                    if (found) {

                                                        pullLeft.append('<span class="col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + ' - ' + dataMore.information[i].name + '</span>');
                                                    }
                                                    else {
                                                        pullLeft.append('<span class="col-xs-12 no-margin no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + ' - ' + dataMore.information[i].name + '</span>');
                                                    }
                                                }
                                            }
                                        }

                                    } else {
                                        if (full.budgetFrom != null) {

                                            if (found) {
                                                pullLeft.append('<span class="boldRed">' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' VNĐ</span>');
                                            }
                                            else {
                                                pullLeft.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' VNĐ</span>');
                                            }
                                        }
                                    }
                                }
                                pullLeft.append('<div class="col-md-12 col-xs-12 no-margin no-padding"><rating class="ui-rating size-h5 ui-rating-warning" ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');
                                if (full.totalBid == null) {
                                    full.totalBid = 0
                                }
                                if (full.status === 0) {
                                    pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                    pullRight.append('<span class="col-xs-12">' + full.totalBid + ' <span>đề xuất</span></span>');
                                } else if (full.status === 1) {
                                    pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                    pullRight.append('<span class="col-xs-12">' + full.totalBid + ' <span>đề xuất</span></span>');
                                }
                                else if (full.status === 2) {
                                    pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                    pullRight.append('<span class="col-xs-12">' + full.totalBid + ' <span>đề xuất</span></span>');

                                }

                                body.append(pullLeft);
                                body.append(pullRight);
                                mobile.append(header);

                                mobile.append(body);
                                element.append(desktop);
                                element.append(mobile);
                                return element.html();
                            }
                        },
                        {
                            "targets":2,
                            "data":null,
                            "class":"verticalMiddle",
                            "render":function (data, type, full) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }
                                if (found) {
                                    var element = angular.element("<div class=' boldRed'></div>");
                                }
                                else {
                                    var element = angular.element("<div></div>");
                                }

                                element.append('<span>' + full.content.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets":3,
                            "class":"hide-mobile verticalMiddle",
                            "data":null,
                            "render":function (data, type, full) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }
                                var element = angular.element("<div></div>");
                                if (full.jsonData != null) {
                                    var dataMore = JSON.parse(full.jsonData);
                                    var isMore = false;
                                    if (dataMore.information != undefined && dataMore.information.length > 0) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                isMore = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (isMore) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                if (found) {
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].name + '</span>');
                                                }
                                                else {
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding">' + dataMore.information[i].name + '</span>');
                                                }

                                            }
                                        }

                                    } else {
                                        if (full.serviceType != null) {

                                            if (found)
                                                element.append('<span class="boldRed" data-i18n="' + full.serviceType.display + '"></span>');
                                            else {
                                                element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                            }
                                        }

                                    }
                                } else {

                                    if (found)
                                        element.append('<span class="boldRed" data-i18n="' + full.serviceType.display + '"></span>');
                                    else {
                                        element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                    }
                                }
                                return element.html();
                            }
                        },
                        {
                            "targets":4,
                            "data":null,
                            "width":"15%",
                            "class":"text-right hide-mobile verticalMiddle",
                            "render":function (data, type, full) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }

                                var element = angular.element("<div></div>");
                                if (full.jsonData != null) {
                                    var dataMore = JSON.parse(full.jsonData);
                                    var isMore = false;
                                    if (dataMore.information != undefined && dataMore.information.length > 0) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                isMore = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (isMore) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected & dataMore.information[i].input != null) {

                                                if (found)
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + '</span>');
                                                else {
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + '</span>');
                                                }
                                            }
                                        }

                                    } else {
                                        if (full.budgetFrom != null) {

                                            if (found)
                                                element.append('<span class="boldRed">' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + '</span>');
                                            else {
                                                element.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;VNĐ' + '</span>');
                                            }
                                        }
                                    }
                                }

                                return element.html();
                            }
                        },

                        {
                            "targets":5,

                            "data":null,
                            "class":"text-center hide-mobile verticalMiddle",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                if (full.totalBid != null) {
                                    element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + '</span>');
                                } else {
                                    element.append('<span>0</span>');
                                }

                                return element.html();
                            }
                        },
                        {
                            "targets":6,
                            "data":null,
                            "class":"text-center hide-mobile verticalMiddle",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                if (full.expiredDate != null) {
                                    var date = new Date(full.expiredDate);
                                    var day = "";
                                    var month = "";
                                    var time = new Date(date.setHours(date.getHours() - 7));
                                    if (String(date.getDate()).length < 2) {
                                        day = "0" + (date.getDate());
                                    } else {
                                        day = date.getDate();
                                    }
                                    if (String(date.getMonth() + 1).length < 2) {
                                        month = "0" + (date.getMonth() + 1);
                                    } else {
                                        month = (date.getMonth() + 1);
                                    }

                                    element.append('<span>' + day + '/' + month + '/' + date.getFullYear() + '</span>');
                                }

                                return element.html();
                            }
                        },
                        {
                            "targets":7,
                            "data":null,
                            "class":"verticalMiddle hiddenIsNotSkill",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                if (full.skill != null) {
                                    element.append('<span>' + full.skill + '</span>');
                                } else {

                                }
                                return element.html();
                            }
                        },
                        {
                            "targets":8,
                            "data":null,
                            "class":"verticalMiddle",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                if (full.jsonData != null) {
                                    var dataMore = JSON.parse(full.jsonData);
                                    element.append('<span>' + dataMore.city.name + '</span>');
                                }
                                return element.html();
                            }
                        },
                        {
                            "targets":9,
                            "data":null,
                            "width":"20%",
                            "class":"text-center hide-mobile verticalMiddle",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                element.append('<div class="col-md-12"><rating class="ui-rating size-h5 ui-rating-warning" ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');
                                return element.html();
                            }
                        },
                        {
                            "targets":10,
                            "class":"text-center hide-mobile verticalMiddle",
                            "data":null,
                            "render":function (data, type, full) {
                                if (full.addonServices.length > 0) {
                                    var found = false;
                                    for (var i = 0; i < full.addonServices.length; i++) {
                                        if (full.addonServices[i].id == 3) {
                                            found = true;
                                        }
                                    }
                                }
                                var element = angular.element("<div></div>");
                                if (full.status != 2) {
                                    if (found) {
                                        element.append('<span class="boldRed col-xs-12" data-i18n="opening"></span><span>');
                                    }
                                    else {
                                        element.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                    }
                                } else {
                                    if (found) {
                                        element.append('<span class="boldRed" data-i18n="closed" ></span>');
                                    }
                                    else {
                                        element.append('<span data-i18n="closed" ></span>');
                                    }
                                }
                                return element.html();
                            }
                        },
                        {
                            "targets":11,
                            "data":null,
                            "class":"verticalMiddle",
                            "render":function (data, type, full) {
                                var element = angular.element("<div></div>");
                                if (full.status != 2) {
                                    element.append('<div class="col-md-12 col-sm-12"><a href="#/detailAdvanceSearch/' + full.id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="bid"></span></a></div>');
                                } else {
                                }
                                return element.html();
                            }
                        }
                    ]
                };
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    $scope.searchdetails = {};
                    loadCategories();
                    loadCities();
                    $scope.format = 'dd/MM/yyyy';
                    getNewOrderDefault();
                    if (windowsize.devicetype() === 'Desktop') {
                        $scope.showSearchs = true;
                    }
                    else {
                        $scope.showSearchs = false;
                        console.log("searchFeedABC: ", store.get("searchFeed"));
                        data = store.get("searchFeed");
                        if (data) {

                            if (data.childrenCategory) {
                                $scope.childrenCategory = data.childrenCategory;
                            }
                            if (data.parentCities) {
                                $scope.parentCities = data.parentCities;
                            }

                            if (data.idRequest) {
                                $scope.searchdetails.idRequest = data.idRequest.idRequest;
                            }
                            if (data.budgetFrom) {
                                $scope.searchdetails.budgetFrom = data.budgetFrom.budgetFrom;
                            }
                            if (data.budgetTo) {
                                $scope.searchdetails.budgetTo = data.budgetTo.budgetTo;
                            }
                            if (data.expiredDateFrom) {
                                $scope.searchdetails.expiredDateFrom = data.expiredDateFrom.expiredDateFrom;
                            }
                            if (data.expiredDateTo) {
                                $scope.searchdetails.expiredDateTo = data.expiredDateTo.expiredDateTo;
                            }
                            if (data.skills) {
                                $scope.customCategory = data.skills;
                                $scope.skills = data.skills;
                                $("[name=skill]").val($scope.skills).trigger("change");

                            }

                        }

                        if (navigations.current.searchData) {
                            getNewOrder(navigations.current.searchData);
                        }
                    }
                };
                $scope.open1 = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened1 = true;
                    $scope.opened2 = false;
                };
                $scope.open2 = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened2 = true;
                    $scope.opened1 = false;
                };
                var Search = $element.find("#Search");
                var Result = $element.find("#Result");


                $scope.$on("window.size", function (object, type) {
                    if (windowsize.devicetype() != 'Desktop') {
                        $scope.showSearchs = false;

                    } else {
//                        getNewOrderDefault();
                        $scope.showSearchs = true;

                    }
                });
                if (windowsize.devicetype() != 'Desktop') {
                    if ($(window).height() < 568) {
                        $scope.deviceHeight = 181;
                    } else {
                        $scope.deviceHeight = 281;
                    }

                }
                var form = $("#formSearch");
                $scope.searchClick = function () {
                    if (!form.valid()) {
                        return;
                    }
                    var data;

                    var search = {};
                    var obj = {};
                    if ($scope.skills != undefined && $scope.skills.length > 0) {
                        search.skill = "";
                        for (var i = 0; i < $scope.skills.length; i++) {
                            search.skill += $scope.skills[i].name;
                            if (i != $scope.skills.length - 1) {
                                search.skill += ",";
                            }
                        }
                        obj.skills = $scope.skills;
                    }

                    if ($scope.parentCities != undefined) {
                        $scope.searchFeed.location = $scope.parentCities.name;
                        obj.parentCities = $scope.parentCities;
                    } else {
                        $scope.searchFeed.location = undefined;
                    }

                    if ($scope.childrenCategory != undefined) {
                        if (search.skill == undefined) {
                            search.skill = "";
                            if ($scope.childrenCategory != null) {
                                if ($scope.childrenCategory) {
                                    for (var i = 0; i < $scope.childrenCategory.skills.length; i++) {
                                        search.skill += $scope.childrenCategory.skills[i].name;
                                        if (i != $scope.childrenCategory.skills.length - 1) {
                                            search.skill += ",";
                                        }
                                    }
                                } else {
                                    $scope.childrenCategory = undefined;
                                    search.skill = undefined;
                                }
                            } else {
                                $scope.childrenCategory = undefined;
                                search.skill = undefined;
                            }

                        }
                        obj.childrenCategory = $scope.childrenCategory;
                    }

                    if ($scope.parentCategory != undefined) {
                        $scope.searchFeed.serviceTypeId = $scope.parentCategory.id;
                        obj.parentCategory = $scope.parentCategory;
                        obj.indexCategory = $scope.categories.indexOf($scope.parentCategory);
                    } else {
                        $scope.searchFeed.serviceTypeId = undefined;
                        $scope.childrenCategory = undefined;
                        search.skill = undefined;
                    }

                    $scope.searchFeed.skill = search.skill;

                    Date.prototype.addDays = function (days) {
                        this.setDate(this.getDate() + parseInt(days));
                        return this;
                    };

                    if ($scope.searchdetails != undefined) {
                        if ($scope.searchdetails.idRequest != null) {
                            $scope.searchFeed.id = $scope.searchdetails.idRequest;
                            obj.idRequest = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.id = undefined;
                        }

                        if ($scope.searchdetails.expiredDateFrom != null) {
                            var dateString = $scope.searchdetails.expiredDateFrom.toDateString();
                            var date = new Date();
                            var setHours = date.setHours(7, 0, 0);
                            var timeString = new Date(setHours).toTimeString();
                            var expiredDate1 = new Date(dateString + " " + timeString);
                            $scope.searchFeed.expiredDateFrom = expiredDate1.toISOString();
                            obj.expiredDateFrom = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.expiredDateFrom = undefined;
                        }

                        if ($scope.searchdetails.expiredDateTo != null) {
                            var dateString = $scope.searchdetails.expiredDateTo.toDateString();
                            var date = new Date();
                            var setHours = date.setHours(30, 59, 59);
                            var timeString = new Date(setHours).toTimeString();
                            var expiredDate2 = new Date(dateString + " " + timeString);
                            var expiredDate2F = expiredDate2.addDays(1);
                            $scope.searchFeed.expiredDateTo = expiredDate2F.toISOString();
                            obj.expiredDateTo = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.expiredDateTo = undefined;
                        }

                        if ($scope.searchdetails.budgetFrom != null) {
                            $scope.searchFeed.budgetFrom = $scope.searchdetails.budgetFrom;
                            obj.budgetFrom = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.budgetFrom = undefined;
                        }

                        if ($scope.searchdetails.budgetTo != null) {
                            $scope.searchFeed.budgetTo = $scope.searchdetails.budgetTo;
                            obj.budgetTo = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.budgetTo = undefined;
                        }
                    }

                    $scope.searchFeed.username = Session.userId;
                    getNewOrder($scope.searchFeed);
                    data = $scope.searchFeed
                    store.set("searchFeed", obj);

                    if (windowsize.devicetype() != 'Desktop') {
                        $scope.showSearchs = false;
                        navigations.collections[navigations.collections.length - 1].searchData = data
                    }
                }
                var loadCategories = function () {
                    db.get("work.json").then(function (result) {
                        if (result && result.data && result.data.length > 0) {
                            $scope.categories = result.data;
                        }
                        if (windowsize.devicetype() != 'Desktop') {
                            if (data.indexCategory != undefined) {
                                $scope.parentCategory = $scope.categories[data.indexCategory];
                            }
                        }

                    });
                };
                var loadCities = function () {
                    db.get("cities.json").then(function (result) {
                        if (result && result.data && result.data.length > 0) {
                            $scope.cities = result.data;
                        }
                    });
                };

                var found = false

                function getNewOrder(search) {
                    $scope.newOrder.url = "serviceorder/search/v1";
                    $scope.newOrder.remote_data = "serviceOrders";

                    $scope.newOrder.postData = angular.copy(search);
                }

                function getNewOrderDefault() {
                    $scope.searchFeed.username = Session.userId;
                    getNewOrder($scope.searchFeed);
                }

                var modalInstance;
                var rating = {};
                $scope.ratingUser = function (aData) {
                    $scope.currentData = aData;
                    modalInstance = $modal.open({
                        templateUrl:"app/partials/form/ratingUser.html",
                        controller:'ratingUserCtr',
                        resolve:{
                            aData:function () {
                                return aData;


                            }
                        }
                    });
                    modalInstance.result.then((function (rate) {
                        $scope.currentData.ratingMark = rate;
                    }), function () {
                    });
                }
                module.controller('ratingUserCtr', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'aData',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, aData) {
                        $scope.save = function () {
                            var rating = {};
                            var customerRating = {};
                            rating.voter = Session.userId;
                            rating.role = USER_ROLES[Session.userRole];
                            rating.ratings = [];
                            customerRating.id = aData.id;
                            if ($scope.rate.start != undefined) {
                                customerRating.mark = $scope.rate.start;
                            } else {
                                customerRating.mark = 0;
                            }
                            rating.ratings.push(customerRating);
                            api.post('rating/v1', rating).then(function (res) {
                                if (res.data.result) {
                                    $modalInstance.close($scope.rate.start);
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            });

                        };

                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        $scope.cancel = function () {

                            $modalInstance.close();
                        }


                    }
                ]);

                $scope.showDetail = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        var columnValue = hiddenIsNotSkill.find(".columnValue");
                        var hiddenIsNotBoldRed = rowDetail.find(".removeBoldRed");
                        var columnValueBoldRed = hiddenIsNotBoldRed.find(".boldRed");
                        columnValueBoldRed.removeClass("boldRed")
                        if (columnValue.text() == "") {
                            hiddenIsNotSkill.addClass("non-display");
                        }
                        classHideMobile.addClass("non-display");

                    }

                }

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var td = rowDetail.find("td");
                    var ul = rowDetail.find("ul");
                    var li = rowDetail.find("li");
                    var lastLi = li.last();
                    var body = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                    var contentLeft = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                    var contentRight = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                    if (li != undefined && li.length > 0) {
                        li.each(function () {
                            if (!$(this).is(':last-child')) {
                                var content = angular.element("<div class='col-md-12 col-sm-12 no-padding wrap-numberonly'></div>");
                                var columnTitle = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                                var columnValue = angular.element("<div class='col-md-9 col-sm-9'></div>");
                                if ($(this).hasClass("hide-mobile")) {
                                    columnTitle.addClass("hide-mobile");
                                    columnValue.addClass("hide-mobile");
                                }
                                var title = $(this).find("span").closest(".columnTitle");
                                title.removeClass("col-md-2");
                                title.addClass("col-md-12");
                                title.addClass("col-sm-12");
                                var value = $(this).find(".columnValue");
                                if (value.text() == "") {
                                    title.addClass("non-display");
                                    value.addClass("non-display");
                                }
                                value.removeClass("col-md-2");
                                value.addClass("col-md-12");
                                value.addClass("col-sm-12");
                                columnTitle.append(title);
                                columnValue.append(value);
                                content.append(columnTitle);
                                content.append(columnValue);
                                contentLeft.append(content);
                            }

                        });
                        var valueLastLi = lastLi.find(".columnValue");
                        contentRight.append(valueLastLi);
                        body.append(contentLeft);
                        body.append(contentRight);
                        ul.addClass("non-display");
                        td.append(body);
                        if ($(window).width() == 768) {
                            var classHideMobile = rowDetail.find(".hide-mobile");
                            classHideMobile.addClass("non-display");
                        }
                    }
                }
                $scope.hideContent = function (index, value, element) {
                    if (windowsize.devicetype() != "Desktop") {
                        $(".body").removeClass("non-display");
                        $('[data-column = 1]').addClass("non-display");
                        var body = element.find(".body");
                        body.addClass("non-display");
                    } else {
                        if ($(window).width() > 1024) {
                            var rowDetail = element.parent("tbody").find(".row-detail");
                            var li = rowDetail.find("li");
                            li.find(".columnTitle").addClass("non-display");
                            var firstLi = li.first();
                            var lastLi = li.last();
                            var valueLastLi = lastLi.find(".columnValue");
                            valueLastLi.removeClass('col-md-10');
                            valueLastLi.addClass('col-md-2');
                            firstLi.append(valueLastLi);
                        }
                    }

                }

                var unique = function (origArr) {
                    var newArr = [],
                        origLen = origArr.length,
                        found, x, y;

                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if (origArr[x].name === newArr[y].name) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr;
                }

                $scope.chooseParentCategory = function (value) {
                    if (value != null) {
                        if ($scope.childrenValue == undefined) {
                            $scope.childrenCategory = undefined;
                        }
                        if (value.children != undefined) {
                            $scope.childrenCategory = '';
                            return;
                        }
                        else {
                            $timeout(function () {


                                var parentCategory = [];
                                angular.extend(parentCategory, $scope.skills)
                                for (var i = 0; i < value.skills.length; i++) {
                                    parentCategory.push(value.skills[i]);
                                }
                                var uniqueCategory = unique(parentCategory)
                                $scope.customCategory = uniqueCategory;
                                $("#selectedSkills").select2()
                                $scope.$apply()
                                $("#selectedSkills").select2().trigger('change')
                            });

                        }
                    }
                }

                $scope.chooseChildrenCategory = function (childrenValue) {
                    if (childrenValue != undefined) {
                        if (childrenValue.children != undefined) {
                            if (childrenValue.children.length > 0) {
                                return;
                            }
                        }
                        else {
                            $timeout(function () {


                                var childrenCategory = [];
                                angular.extend(childrenCategory, $scope.skills)
                                for (var i = 0; i < childrenValue.skills.length; i++) {
                                    childrenCategory.push(childrenValue.skills[i]);
                                }
                                var uniqueCategory = unique(childrenCategory)
                                $scope.customCategory = uniqueCategory;
                                $("#selectedSkills").select2()
                                if ($scope.skills == null) {
                                    skillTemp = uniqueCategory;
                                }
                                $scope.$apply()
                                $("#selectedSkills").select2().trigger('change')
                            });

                        }
                    }
                }

            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);
    });