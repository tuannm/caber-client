/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'jquery',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, $, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.detailpost', []);
        app.useModule(module);

        module.controller('detailpost', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', '$location', 'Session', 'api', 'USER_ROLES', 'logger', '$modal', 'notification','navigations',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, $location, Session, api, USER_ROLES, logger, $modal, notification,navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);

                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    if (windowsize.devicetype() === "Desktop") {
                        getNewOrder();
                        //getAwaitingPayment();
                    } else {
                        if (navigations.current.index == 0)
                        {
                            getNewOrder();
                        }
                        else if (navigations.current.index == 1)
                        {
                            getFinishOrder();
                        }
                        else if (navigations.current.index == 2){
                            getCompleteOrder();
                        }
                    }

                    if ($scope.newNotification == 0) {
                        $scope.hasNotification = false;
                    }

                };
                $scope.completeOrder = {};
                $scope.hasNotification = false;
                $scope.newNotification = 0;
                $scope.indexChange = null;
                $rootScope.socket.on('notification', function (message) {
                    $rootScope.$apply(function () {
                        angular.extend(message, {id: message.data_id});
                        $scope.indexChange = message;
                        if (message.action == "accept-service-order") {
                            $scope.hasNotification = true;
                            $scope.newNotification += 1;
                        }

                    });
                });

                function getNewOrder() {
                    $scope.newOrder = {
                        "url": "serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&sostatus=0,1,2,3",
                        "remote_data":"serviceOrders",
                        lang:{
                            vi:{
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                "sEmptyTable": "Không có yêu cầu nào",
                                "sZeroRecords": "Không có yêu cầu nào",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ yêu cầu",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 yêu cầu",
                                "sInfoFiltered": "(được lọc từ _MAX_ yêu cầu)",
                                "sInfoPostFix": "",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en:{
                                "sEmptyTable":     "No data available in table",
                                "sInfo":           "Showing _START_ to _END_ of _TOTAL_ order",
                                "sInfoEmpty":      "Showing 0 to 0 of 0 order",
                                "sInfoFiltered":   "(filtered from _MAX_ total order)",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing":     "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords":    "No matching records found",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            //{"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "12%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.addonServices.length > 0) {
                                        var option = angular.element('<ul class="promotions hide-when-mobile"></ul>');
                                        _.forEach(full.addonServices, function (n, key) {
                                            option.append('<li data-promotion="' + n.type.toLowerCase() + '">' + n.name + '</li>');
                                        });
                                        element.append(option);
                                    }
                                    element.append('<span>' + full.id + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "20%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.addonServices.length > 0) {
                                        var option = angular.element('<ul class="promotions hide-when-desktop"></ul>');
                                        _.forEach(full.addonServices, function (n, key) {
                                            option.append('<li data-promotion="' + n.type.toLowerCase() + '">' + n.name + '</li>');
                                        });
                                        element.append(option);
                                    }
                                    var desktop = angular.element("<div></div>");
                                    desktop.append('<a href="#/viewdetailpost/' + full.id + '" class=" hide-when-mobile">' + full.title + ' </a>');
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile text-bold"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-padding footer-pull-left-mobile text-bold"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-padding footer-pull-right-mobile"></div>');
                                    if (full.expiredDate != null) {
                                        var arrDateTime = full.expiredDate.toString().split("T");
                                        var arrDate = arrDateTime[0].toString().split("-");
                                        var day = arrDate[2].toString();
                                        var month = arrDate[1].toString();
                                        var year = arrDate[0].toString();
                                        if (day.length < 2) {
                                            day = "0" + day;
                                        }
                                        if (month.length < 2) {
                                            month = "0" + month;
                                        }
                                        header.append('<div class="col-xs-8 no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4">' + day + '/' + month + '/' + year + '</div>');
                                    } else {
                                        header.append('<div class="col-xs-8 no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4"></div>');
                                    }

                                    //content.append('<span>' + full.content + '</span>');

                                    if (full.jsonData != null) {
                                        var dataMore = $.parseJSON(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    pullLeft.append('<span class="col-xs-12 no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + ' - ' + dataMore.information[i].name + '</span>');
                                                }
                                            }

                                        } else {
                                            if (full.budgetFrom != null) {
                                                pullLeft.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                            }
                                        }
                                    }
                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }

                                    var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                                    if (full.status == 0) {
                                        if(full.isOrderOpenning != undefined && full.isOrderOpenning){
                                            pullRight.append('<span class="col-xs-12 toast-title" data-i18n="opening"></span><span>');
                                        } else{
                                            pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                        }

                                        if(full.countBidding != undefined && full.countDelete != undefined){
                                            if(full.countBidding > 0 && full.countDelete > 0){
                                                pullRight.append('<span class="col-xs-12"><span>' + proposalsMatched + ' <span data-i18n="proposal"></span></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span> <span class="color-danger"><i class="fa fa-level-down"></i></span><span class="color-danger">' + parseInt(full.countDelete) + '</span></span></span>');
                                            }
                                        } else if(full.countBidding != undefined){
                                            if(full.countBidding > 0){
                                                pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span></span></span>');
                                            }
                                        } else if(full.countDelete != undefined){
                                            if(full.countDelete > 0){
                                                pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countDelete) + '</span></span></span>');
                                            }
                                        } else{
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span></span>');
                                        }

                                    } else if (full.status == 1) {
                                        if (proposalsMatched > 0 || (full.isChangeStatus != undefined && full.isChangeStatus)) {
                                            if(full.isChangeStatus != undefined && full.isChangeStatus){
                                                pullRight.append('<span class="col-xs-12 toast-title" data-i18n="bidding"></span><span>');
                                            } else{
                                                pullRight.append('<span class="col-xs-12" data-i18n="bidding"></span><span>');
                                            }

                                            if(full.countBidding != undefined && full.countDelete != undefined){
                                                if(full.countBidding > 0 && full.countDelete > 0){
                                                    pullRight.append('<span class="col-xs-12"><span>' + proposalsMatched + ' <span data-i18n="proposal"></span></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span> <span class="color-danger"><i class="fa fa-level-down"></i></span><span class="color-danger">' + parseInt(full.countDelete) + '</span></span></span>');
                                                }
                                            } else if(full.countBidding != undefined){
                                                if(full.countBidding > 0){
                                                    pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span></span></span>');
                                                }
                                            } else if(full.countDelete != undefined){
                                                if(full.countDelete > 0){
                                                    pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span>&nbsp; <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countDelete) + '</span></span></span>');
                                                }
                                            } else{
                                                pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span></span>');
                                            }


                                        } else {
                                            pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span></span>');
                                        }
                                    } else if (full.status == 2) {
                                        pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                        pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal"></span></span>');
                                    } else if (full.status == 3) {
                                        pullRight.append('<span class="col-xs-12" data-i18n="verifying"></span><span>');
                                    }
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>' + full.content.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "10%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }

                                    if(full.countBidding != undefined && full.countDelete != undefined){
                                        if(full.countBidding > 0 && full.countDelete > 0){
                                            element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + ' <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span> <span class="color-danger"><i class="fa fa-level-down"></i></span><span class="color-danger">' + parseInt(full.countDelete) + '</span></span></span>');
                                        }
                                    } else if(full.countBidding != undefined){
                                        if(full.countBidding > 0){
                                            element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + ' <span class="color-warning"><i class="fa fa-level-up"></i></span><span class="color-warning">' + parseInt(full.countBidding) + '</span></span></span>');
                                        }
                                    } else if(full.countDelete != undefined){
                                        if(full.countDelete > 0){
                                            element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + ' <span class="color-danger"><i class="fa fa-level-down"></i></span><span class="color-danger">' + parseInt(full.countDelete) + '</span></span></span>');
                                        }
                                    } else{
                                        element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "class": "hiddenIsNotSkill",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.skill != null) {
                                        var skills = full.skill.toString().split(",");
                                        var skill = "";
                                        for (var i = 0; i < skills.length; i++) {
                                            if (i != skills.length - 1) {
                                                skill += skills[i].toString() + ", ";
                                            } else {
                                                skill += skills[i].toString();
                                            }
                                        }
                                        element.append(skill);
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "15%",
                                "class": "text-left hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = $.parseJSON(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding">' + dataMore.information[i].name + '</span>');
                                                }
                                            }
                                        } else {
                                            element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                        }
                                    } else {
                                        element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                    }

                                    return element.html();

                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "width": "15%",
                                "class": "text-right hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = $.parseJSON(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                                }
                                            }

                                        } else {
                                            if (full.budgetFrom != null) {
                                                element.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                            }
                                        }
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "width": "15%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.expiredDate != null) {
                                        var arrDateTime = full.expiredDate.toString().split("T");
                                        var arrDate = arrDateTime[0].toString().split("-");
                                        var day = arrDate[2].toString();
                                        var month = arrDate[1].toString();
                                        var year = arrDate[0].toString();
                                        if (day.length < 2) {
                                            day = "0" + day;
                                        }
                                        if (month.length < 2) {
                                            month = "0" + month;
                                        }
                                        element.append('<span>' + day + '/' + month + '/' + year + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = $.parseJSON(full.jsonData);
                                        element.append('<span>' + dataMore.city.name + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            //{
                            //    "targets": 9,
                            //    "data": null,
                            //    "render": function (data, type, full) {
                            //        var element = angular.element("<div></div>");
                            //        element.append('<span>' + full.serviceType.display + '</span>');
                            //        return element.html();
                            //    }
                            //},
                            {
                                "targets": 9,
                                "data": null,
                                "width": "13%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }
                                    var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                                    if (full.status == 0) {
                                        if(full.isOrderOpenning != undefined && full.isOrderOpenning){
                                            element.append('<span class="toast-title" data-i18n="opening"></span>');
                                        } else{
                                            element.append('<span data-i18n="opening"></span>');
                                        }

                                    } else if (full.status == 1) {
                                        if (proposalsMatched > 0 || (full.isChangeStatus != undefined && full.isChangeStatus)) {
                                            if(full.isChangeStatus != undefined && full.isChangeStatus){
                                                element.append('<span class="toast-title" data-i18n="bidding"></span>');
                                            } else{
                                                element.append('<span data-i18n="bidding"></span>');
                                            }

                                        } else {
                                            element.append('<span data-i18n="opening"></span>');
                                        }
                                    } else if (full.status == 2) {
                                        element.append('<span data-i18n="closed"></span>');
                                    } else if (full.status == 3) {
                                        element.append('<span data-i18n="verifying"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 10,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div class='col-md-12'></div>");
                                    //element.append('<div data-ng-show="aData.addonPayment == 0 && aData.status != 2 && aData.status != 3" class="col-md-12 no-padding"><a data-ng-click="$parent.paymentAO(aData)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="payment"></span></a></div>');
                                    element.append('<div class="col-md-12 no-padding"><a href="#/viewdetailpost/' + full.id + '" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    //element.append('<div data-ng-show="aData.addonPayment != 0 &&aData.addonServices.length == 0 && aData.status != 2 && aData.status != 3" class="col-md-12 no-padding"><a data-ng-click="$parent.upgradeOrder(aData)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="upgrade order"></span></a></div>');
                                    return element.html();
                                }
                            }

                        ]
                    };
                }

                function getFinishOrder() {
                    $scope.finishOrder = {
                        "url": "payment/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole]+ "&status=1,2",
                        "remote_data":"payments",
                        lang:{
                            vi:{
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                "sEmptyTable": "Không có yêu cầu nào được thanh toán",
                                "sZeroRecords": "Không có yêu cầu nào được thanh toán",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ yêu cầu thanh toán",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 yêu cầu thanh toán",
                                "sInfoFiltered": "(được lọc từ _MAX_ yêu cầu thanh toán)",
                                "sInfoPostFix": "",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en:{
                                "sEmptyTable":     "No data available in table",
                                "sInfo":           "Showing _START_ to _END_ of _TOTAL_ order payment",
                                "sInfoEmpty":      "Showing 0 to 0 of 0 order payment",
                                "sInfoFiltered":   "(filtered from _MAX_ total order payment)",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing":     "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords":    "No matching records found",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "13%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    if(full.transactionId  != null){
                                        desktop.append('<a class="color-primary" href="#/payment/'+full.id+'"><span> '+ full.transactionId +'</span></a>')
                                    }
                                    var mobile = angular.element("<div class='hide-when-desktop text-left'></div>");
                                    var header = angular.element('<div class="header-mobile col-xs-12 no-padding"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-12 no-padding"></div>');
                                    //var pullRight = angular.element('<div class="col-xs-3 no-padding footer-pull-right-mobile"></div>');
                                    if(full.transactionId  != null){
                                        if (full.status === 0) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span data-i18n="transaction id"></span>: <span>'+full.transactionId+'</span></div><div class="col-xs-12 no-padding footer-pull-left-mobile"><span data-i18n="total payment"></span>: <span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="unpaid"></span></div>');
                                        }
                                        if (full.status === 1) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span data-i18n="transaction id"></span>: <span>'+full.transactionId+'</span></div><div class="col-xs-12 no-padding footer-pull-left-mobile"><span data-i18n="total payment"></span>: <span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="paid"></span></div>');
                                        }
                                        if (full.status === 2) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span data-i18n="transaction id"></span>: <span>'+full.transactionId+'</span></div><div class="col-xs-12 no-padding footer-pull-left-mobile"><span data-i18n="total payment"></span>: <span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="failed paid"></span></div>');
                                        }
                                    } else{
                                        if (full.status === 0) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span>'+full.serviceOrder.id+'</span> - <span>'+full.serviceOrder.title+'</span></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="unpaid"></span></div>');
                                        }
                                        if (full.status === 1) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span>'+full.serviceOrder.id+'</span> - <span>'+full.serviceOrder.title+'</span></div></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="paid"></span></div>');
                                        }
                                        if (full.status === 2) {
                                            header.append('<div class="col-xs-9 no-padding"><div class="col-xs-12 no-padding"><span>'+full.serviceOrder.id+'</span> - <span>'+full.serviceOrder.title+'</span></div></div></div><div class="col-xs-3 no-padding footer-pull-right-mobile"><span data-i18n="failed paid"></span></div>');
                                        }
                                    }
                                    if(full.type == "SO"){
                                        content.append('<span>Thanh toán cho yêu cầu đã trao</span>');
                                    } else if(full.type == "AO"){
                                        content.append('<span>Thanh toán cho dịch vụ nâng cao</span>');
                                    }
                                    pullLeft.append('<div class="col-xs-12 no-padding"><span>' + full.serviceOrder.title + '</span></div>');
                                    if(full.totalPayment != null){
                                        pullLeft.append('<div class="col-xs-12 no-padding footer-pull-left-mobile"><span data-i18n="total payment"></span>: <span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span></div>');
                                    }

                                    body.append(pullLeft);
                                   // body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    //mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();

                                }
                            },
                            {
                                "targets":1,
                                "data": null,
                                "width": "13%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>'+full.serviceOrder.id +'</span>');
                                    return element.html();

                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": "20%",
                                "class":"text-center",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>'+full.serviceOrder.title +'</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "20%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if(full.type == "SO"){
                                        element.append('<span>Thanh toán cho yêu cầu đã trao</span>');
                                    }
                                    if(full.type == "AO"){
                                        element.append('<span>Thanh toán cho dịch vụ nâng cao</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "15%",
                                "class":"text-right hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if(full.totalPayment != null){
                                        element.append('<span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "15%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if(full.modifiedDate != null){
                                        var arrDate = full.modifiedDate.toString().split("-");
                                        var day = arrDate[2].toString();
                                        var month = arrDate[1].toString();
                                        var year = arrDate[0].toString();
                                        if (day.length < 2) {
                                            day = "0" + day;
                                        }
                                        if (month.length < 2) {
                                            month = "0" + month;
                                        }
                                        element.append('<span>' + day + '/' + month + '/' + year + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "width": "15%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.status === 0) {
                                        element.append('<span data-i18n="unpaid"></span>');
                                    }
                                    if (full.status === 1) {
                                        element.append('<span data-i18n="paid"></span>');
                                    }
                                    if (full.status === 2) {
                                        element.append('<span data-i18n="failed paid"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "width": "15%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div class='col-md-12'></div>");
                                    element.append('<div class="col-md-12 no-padding"><a data-ng-click="$parent.viewpayment(aData.id)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    return element.html();
                                }
                            }
                        ]
                    };

                }
                $scope.viewpayment = function(paymentID)
                {
                    navigations.push({
                        title: "transaction info",
                        path: "payment",
                        params: paymentID,
                        hasChildrenInMobile: false,
                        isClick: true
                    });
                }
                function getCompleteOrder() {
                    $scope.completeOrder = {
                        "url": "proposal/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=1",
                        "remote_data":"proposals",
                        lang:{
                            vi:{
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                "sEmptyTable": "Không có thông tin liên hệ nào",
                                "sZeroRecords": "Không có thông tin liên hệ nào",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ thông tin liên hệ",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 thông tin liên hệ",
                                "sInfoFiltered": "(được lọc từ _MAX_ thông tin liên hệ)",
                                "sInfoPostFix": "",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en:{
                                "sEmptyTable":     "No data available in table",
                                "sInfo":           "Showing _START_ to _END_ of _TOTAL_ order contact",
                                "sInfoEmpty":      "Showing 0 to 0 of 0 order contact",
                                "sInfoFiltered":   "(filtered from _MAX_ total order contact)",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing":     "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords":    "No matching records found",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "13%",
                                "class":"text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    desktop.append('<span>{{aData.serviceOrderId}}</span>');
                                    var mobile = angular.element("<div class='hide-when-desktop text-left'></div>");
                                    var header = angular.element('<div class="header-mobile col-xs-12 no-padding"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-12 no-padding"></div>');
                                    //var pullRight = angular.element('<div class="col-xs-3 no-padding footer-pull-right-mobile"></div>');
                                    header.append('<div class="col-xs-12 no-padding">{{aData.serviceOrderId}} - {{aData.serviceOrderTitle}}</div>');
                                    header.append('<div class="col-xs-12 no-padding"><span data-i18n="proposal id"></span>: {{aData.id}}</div>');
                                    header.append('<div class="col-xs-12 no-padding"><span data-i18n="proposer"></span>: {{aData.aliasName}}</div>');
                                    header.append('<div class="col-xs-12 no-padding"><div class="col-xs-6 no-padding padding-right-2 padding-top-5"><a href="tel:'+full.phoneNumber+'" class="btn btn-primary btn-sm col-xs-12"><span class="glyphicon glyphicon-earphone"></span>&nbsp;<span data-i18n="contact"></span></a></div><div class="col-xs-6 no-padding padding-left-2 padding-top-5"><a href="mailto:'+full.email+'" class="btn btn-primary btn-sm col-xs-12"> <span class="glyphicon glyphicon-envelope"></span>&nbsp; <span data-i18n="contact"></span></a></div></div>');
                                    //header.append('<div class="col-xs-12 no-padding"></div>');
                                    body.append(pullLeft);
                                    // body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    //mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();

                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "15%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>{{aData.serviceOrderTitle}}</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": "15%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>{{aData.id}}</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "15%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                     element.append('<span>{{aData.aliasName}}</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "20%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>{{aData.email}}</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "20%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div>{{aData.phoneNumber}}</div>");
                                    //element.append('<span></span>');
                                    return element.html();
                                }
                            }
                        ]
                    };

                }

                $scope.notifyChange = function(data){
                    if($scope.indexChange.action == "submit-proposal" || $scope.indexChange.action == "create-proposal"){
                        if(data.aData.countBidding != undefined && data.aData.countBidding > 0){
                            data.aData.countBidding += 1;
                        } else{
                            data.aData.countBidding = 1;
                        }
                        data.aData.status = 1;
                        data.aData.isChangeStatus = true;
                        data.aData.totalProposalSubmitted += 1;
                    }
                    if($scope.indexChange.action == "delete-proposal"){
                        if(data.aData.countDelete != undefined && data.aData.countDelete > 0){
                            data.aData.countDelete += 1;
                        } else{
                            data.aData.countDelete = 1;
                        }
                        data.aData.totalProposalSubmitted -= 1;
                        var totalBid = parseInt(data.aData.totalProposalSubmitted + data.aData.totalProposalAccepted + data.aData.totalProposalConfirmed + data.aData.totalProposalRejected + data.aData.totalProposalClosed);
                        var currentCountBid = totalBid -  data.aData.countDelete;
                        if(currentCountBid <= 0){
                            data.aData.status = 0;
                            data.aData.isOrderOpenning = true;
                        }

                    }

                }

                $scope.reloadFinishOrder = function () {
                        getFinishOrder();
                };

                $scope.reloadNewOrder = function () {
                        getNewOrder();
                };

                $scope.reloadCompleteOrder = function () {
                    if ($scope.newNotification > 0) {
                        $scope.newNotification = 0
                        $scope.hasNotification = false;

                        $scope.completeOrder.url = new String("proposal/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=1");
                        getCompleteOrder();
                    }
                    else {
                        getCompleteOrder();
                    }

                };

                $scope.upgradeOrder = function (aData) {
                    var modalUpgradeOrder;
                    modalUpgradeOrder = $modal.open({
                        templateUrl: "app/partials/form/upgradeOrder.html",
                        controller: 'UpgradeOrderCtrl',
                        resolve: {
                            data: function () {
                                return aData;
                            }
                        }
                    });
                    modalUpgradeOrder.result.then((function () {
                        var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + aData.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                        $rootScope.paymentUrl(url);

                    }), function () {
                    });
                };

                $scope.paymentAO = function(aData){
                    var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + aData.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                    $rootScope.paymentUrl(url);
                };

                $scope.showDetail = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        var columnValue = hiddenIsNotSkill.find(".columnValue");
                        if (columnValue.text() == "") {
                            hiddenIsNotSkill.addClass("non-display");
                        }
                        classHideMobile.addClass("non-display");

                    }

                }

                $scope.redirectDetail = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        rowDetail.remove();
                        if(value.transactionId != null){
                            $location.path("payment/" + value.id);
                        }

                    }

                }

                //$scope.$on("window.size", function (object, type) {
                //    var dataTables_scrollBody = $element.find(".dataTables_scrollBody");
                //
                //    if(dataTables_scrollBody.length > 0){
                //
                //        dataTables_scrollBody.css("height","auto");
                //
                //    }
                //});

                $scope.$on("window.width", function (object, type) {
                    var rowDetail = $element.find(".row-detail");
                    rowDetail.remove();
                    var detailShow = $element.find(".detail-show");
                    detailShow.removeClass("detail-show");
                    detailShow.removeClass("active");
                });

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var td = rowDetail.find("td");
                    var ul = rowDetail.find("ul");
                    var li = rowDetail.find("li");
                    var lastLi = li.last();
                    var body = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                    var contentLeft = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                    var contentRight = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                    if (li != undefined && li.length > 0) {
                        li.each(function () {
                            if (!$(this).is(':last-child')) {
                                var content = angular.element("<div class='col-md-12 col-sm-12 no-padding wrap-numberonly   '></div>");
                                var columnTitle = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                                var columnValue = angular.element("<div class='col-md-9 col-sm-9'></div>");
                                if($(this).hasClass("hide-mobile")){
                                    columnTitle.addClass("hide-mobile");
                                    columnValue.addClass("hide-mobile");
                                }
                                var title = $(this).find("span").closest(".columnTitle");
                                title.removeClass("col-md-2");
                                title.addClass("col-md-12");
                                title.addClass("col-sm-12");
                                var value = $(this).find(".columnValue");
                                if (value.text() == "") {
                                    title.addClass("non-display");
                                    value.addClass("non-display");
                                }
                                value.removeClass("col-md-2");
                                value.addClass("col-md-12");
                                value.addClass("col-sm-12");
                                columnTitle.append(title);
                                columnValue.append(value);
                                content.append(columnTitle);
                                content.append(columnValue);
                                contentLeft.append(content);
                            }

                        });
                        var valueLastLi = lastLi.find(".columnValue");
                        contentRight.append(valueLastLi);
                        body.append(contentLeft);
                        body.append(contentRight);
                        ul.addClass("non-display");
                        td.append(body);
                        if($(window).width() == 768){
                            var classHideMobile = rowDetail.find(".hide-mobile");
                            classHideMobile.addClass("non-display");
                        }

                    }
                }


                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };

                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };

                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };

                $scope.selectedRow = function (value, element) {
                    if (windowsize.devicetype() !== 'Desktop') {
                        $location.path("/viewdetailpost");
                    } else {
                        var rowDetail = element.next('.row-detail');
                        var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                        var compiledContents = $compile(content);
                        compiledContents($scope, function (clone) {
                            $('ul', rowDetail).append(clone);
                        });
                    }


                };

            }
        ]);
        module.controller('UpgradeOrderCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session', 'db',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session, db) {

                $scope.totalPayment = 0;
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                api.get('addonservice/v1').then(function (res) {
                    var AddonService;
                    if (data.serviceType.id == 1) {
                        for (var i = 0; i < res.data.addonServices.length; i++) {
                            if (res.data.addonServices[i].type == "email") {
                                res.data.addonServices.splice(i, 1);
                            }
                            if (res.data.addonServices[i].type == "sms") {
                                res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay ứng viên tiềm năng của yêu cầu. Yêu cầu trực tiếp nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                        }
                        AddonService = res.data.addonServices;
                    }
                    if (data.serviceType.id == 2) {
                        for (var i = 0; i < res.data.addonServices.length; i++) {
                            if (res.data.addonServices[i].type == "email") {
                                res.data.addonServices[i].description = "Gửi email tới các lập trình viên tiềm năng của yêu cầu. Yêu cầu của bạn sẽ được gửi tới nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                            if (res.data.addonServices[i].type == "sms") {
                                res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay lập trình viên tiềm năng của yêu cầu. Yêu cầu trực tiếp nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                        }
                        AddonService = res.data.addonServices;
                    }
                    if (data.serviceType.id == 3) {
                        for (var i = 0; i < resService.data.length; i++) {
                            if (res.data.addonServices[i].type == "email") {
                                res.data.addonServices.splice(i, 1);
                            }
                            if (res.data.addonServices[i].type == "sms") {
                                res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay người giúp việc tiềm năng của yêu cầu. Yêu cầu trực tiếp nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                        }
                        AddonService = res.data.addonServices;
                    }
                    if (data.serviceType.id == 5) {
                        for (var i = 0; i < res.data.addonServices.length; i++) {
                            if (res.data.addonServices[i].type == "email") {
                                res.data.addonServices[i].description = "Gửi email tới các gia sư tiềm năng của yêu cầu. Yêu cầu của bạn sẽ được gửi tới nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                            if (res.data.addonServices[i].type == "sms") {
                                res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay gia sư tiềm năng của yêu cầu. Yêu cầu trực tiếp nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                            }
                        }
                        AddonService = res.data.addonServices;
                    }
                    $scope.serviceTable = {
                        "aaData": AddonService,
                        "paging": false,
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "2%",
                                "class": "id",
                                "render": function (data, type, full, meta) {
                                    full.isSelected = false;
                                    var element = angular.element("<div></div>");
                                    element.append('<label class="ui-checkbox"><input ng-model="aData.isSelected" ng-change="$parent.setCount(aData)" type="checkbox"><span></span></label>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "5%",
                                "class": "name",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span class="label label-warning">' + full.name + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append(full.description);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "20%",
                                "class": "text-right",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    var formatServiceCost = "";
                                    if(full.type == "sms"){
                                        formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName + "/SMS";
                                    } else if(full.type == "email"){
                                        formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName + "/Email";
                                    } else{
                                        formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName;
                                    }

                                    var formatServiceCostSalse = "";
                                    if(full.serviceOldCost != null){
                                        if(full.type == "sms"){
                                            formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName + "/SMS";
                                        } else if(full.type == "email"){
                                            formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName + "/Email";
                                        } else{
                                            formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currencyName;
                                        }
                                    }
                                    element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "15%",
                                "class": "quantity",
                                "render": function (data, type, full, meta) {
                                    if(full.id != 3){
                                        full.isInput = true;
                                        full.quantity = null;
                                    } else{
                                        full.isInput = false;
                                        full.quantity = 1;
                                    }
                                    var element = angular.element("<div></div>");
                                    if (full.isInput) {
                                        element.append('<div class="form-group col-md-12 no-padding"><input ng-disabled="!aData.isSelected" ng-change="$parent.totalCalc(aData)" name="' + full.type + '" data-smart-validate-input data-required data-message-required="Bắt buộc phải nhập" ng-model="aData.quantity" maxlength="6" reset-field class="form-control col-md-12" type="text" maxlength="15" format-currency numbers-only placeholder="Nhập số lượng bạn muốn gửi"/></div>');
                                    } else {
                                        element.append('<span>' + full.quantity + '</span>');
                                    }

                                    return element.html();
                                }
                            }
                        ]
                    };

                });
                var totalPayment = 0;
                var addonEmail = 0;
                var addonSMS = 0;
                var boldRed = 0;
                $scope.setCount = function (aData) {
                    var oldCalc = parseInt(aData.quantity * aData.serviceCost);
                    if (!aData.isSelected) {
                        if (aData.isInput) {
                            if (aData.id == 1) {
                                totalPayment = parseInt(totalPayment - addonEmail);
                                addonEmail = 0;
                                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            } else if (aData.id == 2) {
                                totalPayment = parseInt(totalPayment - addonSMS);
                                addonSMS = 0;
                                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            }

                            aData.quantity = null;
                        } else {
                            boldRed = oldCalc;
                            totalPayment = parseInt(totalPayment - boldRed);
                            boldRed = 0;
                            $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                        }
                    } else {
                        if (!aData.isInput) {
                            boldRed = oldCalc;
                            totalPayment = parseInt(totalPayment + boldRed);
                            $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        }
                        var listAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < listAddonService.length; i++) {
                            if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != aData.id) {
                                listAddonService[i].isSelected = !listAddonService[i].isSelected;
                            }
                        }
                    }

                };

                $scope.validateAddon = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var title = rowDetail.find("span").closest(".columnTitle");
                    title.removeClass("col-md-2");
                    title.addClass("col-md-3");
                    var value = rowDetail.find(".columnValue");
                    value.removeClass("col-md-10");
                    value.addClass("col-md-9");
                    var listAddonService = $('#addonService').dataTable().fnGetData();
                    for (var i = 0; i < listAddonService.length; i++) {
                        if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != element.scope().aData.id) {
                            listAddonService[i].isSelected = !listAddonService[i].isSelected;
                        }
                    }
                };
                $scope.totalCalc = function (aData) {
                    if (aData.id == 1) {
                        addonEmail = parseInt(aData.quantity * aData.serviceCost);
                    } else if (aData.id == 2) {
                        addonSMS = parseInt(aData.quantity * aData.serviceCost);
                    }
                    totalPayment = parseInt(addonEmail + addonSMS + boldRed);
                    $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                };


                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $scope.confirmPayment = function () {
                    var listAddonService = $('#addonService').dataTable().fnGetData();
                    var isError = false;
                    for (var i = 0; i < listAddonService.length; i++) {
                        if (listAddonService[i].isSelected && listAddonService[i].quantity == null) {
                            isError = true;
                            break;
                        }
                    }
                    if(parseInt(totalPayment) == 0){
                        isError = true;
                    }
                    if (!isError) {
                        $modalInstance.close();
                    } else {
                        logger.logWarning("Chưa chọn dịch vụ nâng cấp hoặc chưa nhập số lượng cụ thể");
                    }

                };
                $scope.cancelPayment = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);

    })
;

