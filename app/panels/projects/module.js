/** @scratch /panels/5
 * include::panels/column.asciidoc[]
 */

/** @scratch /panels/column/0
 * == Column
 * Status: *Stable*
 *
 * A pseudo panel that lets you add other panels to be arranged in a column with defined heights.
 * While the column panel is stable, it does have many limitations, including the inability to drag
 * and drop panels within its borders. It may be removed in a future release.
 *
 */
define([
    'angular',
    'app',
    'lodash',
    'config',
    'css!./module.css'
],
function (angular, app, _, config) {
    'use strict';

    var module = angular.module('bidproject.panels.projects', []);

    app.useModule(module);

    module.controller('projects', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db',
        function ($scope, $rootScope, $compile, $timeout, $stateParams, db) {
            $scope.panelMeta = {
                status: "Stable",
                description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
            };
            // Set and populate defaults
            var _d = {
                panels: []
            };
            _.defaults($scope.panel, _d);

            $scope.init = function () {
                $scope.ready = false;
                $scope.reset_panel();
                getProducts();
            };

            function getProducts() {
                db.get("projects.json").then(function (result) {
                    if (result && result.data) {
                        $scope.custom = {
                            "data": result.data.projects.items,
                            "columns": [
                                { "data": "null" },
                                { "data": "name"},
                                { "data": "short_descr" },
                                { "data": "null" },
                                { "data": "start_date" },
                                { "data": "null"}
                            ],
                            "columnDefs": [ 
                                {
                                "targets": 0,
                                "data": null,
                                "render": function ( data, type, full, meta ) {
                                    var element = angular.element("<div></div>");
                                    if(full.options){
                                        var option = angular.element('<ul class="promotions"></ul>');
                                        _.forEach(full.options, function(n, key) {
                                            if(n){
                                                option.append('<li data-promotion="'+ key +'">' + key + '</li>');
                                            }
                                        });
                                        element.append(option);
                                    }
                                    if(full.url){
                                        element.append('<a href="' + full.url + '">'+ full.name +' </a>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "render": function ( data, type, full, meta ) {
                                    var element = angular.element("<div></div>");
                                    if(full.jobsDetails){
                                        _.forEach(full.jobsDetails, function(item) {
                                            element.append('<a class = "hiddenlink" href="/skill/'+ item.id +'">' + item.name + ', </li>');
                                        });
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "render": function ( data, type, full, meta ) {
                                    var element = angular.element("<div></div>");
                                    if(full.budget){
                                        element.append(full.budget.min +' - ' + full.budget.max);
                                    }
                                    return element.html();
                                }
                            }
                            
                        ]
                        };
                    }
                    
                });
            }

            $scope.$on('render', function () {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            });

            $scope.toggle_row = function (panel) {
                panel.collapse = panel.collapse ? false : true;
                if (!panel.collapse) {
                    $timeout(function () {
                        $scope.send_render();
                    });
                }
            };

            $scope.send_render = function () {
                $scope.$broadcast('render');
            };

            $scope.add_panel = function (panel, newPanel) {
                panel.panels.push(newPanel);
            };

            $scope.reset_panel = function (type) {
                $scope.new_panel = {
                    loading: false,
                    error: false,
                    sizeable: false,
                    draggable: false,
                    removable: false,
                    span: 12,
                    height: "150px",
                    editable: true,
                    type: type
                };
            };
            
            $scope.selectedRow = function(value, element){
                var rowDetail = element.next('.row-detail');
                var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                var compiledContents = $compile(content);
                compiledContents($scope, function (clone) {
                    $('ul', rowDetail).append(clone);
                });
                
            };
            
            $scope.showProjects = function(){
                console.log("ba dao");
            }
            

        }]);
    module.filter('withoutContainer', [function () {
        return function () {
            return _.without(config.panel_names, 'container');
        };
    }]);
});