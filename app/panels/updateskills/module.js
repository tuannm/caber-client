/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
        'angular',
        'app',
        'lodash',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, config) {
        'use strict';

        var module = angular.module('bidproject.panels.updateskills', []);

        app.useModule(module);
        module.controller('updateskills', ['$scope', '$rootScope', '$timeout', '$stateParams', '$compile', '$location', 'db', 'windowsize', 'api', 'Session', 'logger',
            function ($scope, $rootScope, $timeout, $stateParams, $compile, $location, db, windowsize, api, Session, logger) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);

                $scope.finishUpdated = function () {

                    var list = {};
                    list.skill = "";
                    if (windowsize.devicetype() != 'Desktop') {
                        if ($scope.selectedSkills == undefined ||$scope.selectedSkills.length == 0 ) {
                            logger.logError("Bạn phải chọn ít nhất 1 kỹ năng!");
                            return;
                        }
                        else if ( $scope.selectedSkills !=undefined)
                        {
                            for (var i = 0; i < $scope.selectedSkills.length; i++) {
                                list.skill += $scope.selectedSkills[i].name;

                                if (i != $scope.selectedSkills.length - 1) {
                                    list.skill += ", ";
                                }
                            }
                        }
                    }
                    else {
                    for (var i = 0; i < $scope.skills.length; i++) {
                        list.skill += $scope.skills[i].name;
                        if (i != $scope.skills.length - 1) {
                            list.skill += ", ";
                        }
                    }
                    if ($scope.skills.length == 0) {
                        logger.logError("Bạn phải chọn ít nhất 1 kỹ năng!");
                        return;
                    }

                    if ($scope.skills.length >= 50) {
                        logger.logError("Không được chọn nhiều hơn 50 kỹ năng!");
                        return;
                    }
                    }
                    api.put('user/' + Session.id + '/v1', list).then(function (res) {

                        if (res.data.result) {
                            $location.path("/detailSupplier");
                        } else {
                            logger.logError(res.data.error.description);
                        }
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    })
                };
                $scope.selectedSkills = [];
                $scope.customCategory = [];
                $scope.skills = [];
                $scope.parents = [];
                $scope.init = function () {

                    $scope.ready = false;
                    $scope.reset_panel();
                    if (windowsize.devicetype() === "Desktop") {
                        setMultiTouch(false);
                    } else {
                        setMultiTouch(true);
                    }
                    $scope.childrenskills = {
                        "aoColumns": [
                            {"data": "name"}
                        ],
                        "aaData": [],
                        paging: false,
                        scrollY: "200px",
                        scrollCollapse: true
                    };

                    $scope.parentskills = {
                        "aoColumns": [
                            {
                                "data": "null"
                            }
                        ],
                        columnDefs: [
                            {
                                "targets": 0,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div><span data-i18n='"+full.name+"'></span></div>");
                                    return element.html();
                                }
                            }
                        ],
                        children: {
                            "aoColumns": [
                                {
                                    "data": "name"
                                }
                            ]
                        },
                        paging: false,
                        scrollY: "200px",
                        scrollCollapse: true

                    };
                    $scope.skillstable = {
                        "aoColumns": [
                            {"data": "name"}
                        ],
                        paging: false,
                        scrollY: "200px",
                        scrollCollapse: true
                    };
                    getParentSkills();
                    api.get('user/' + Session.id + '/v1').then(function (res) {
                        if (res.data.result) {
                            //$scope.skills = res.data.user.skill.split(', ');
                            if (res.data.user.skill == null) {
                                return;
                            }
                            var skills = _.filter(res.data.user.skill.split(', '), function(item){
                                return {name: item};
                            })
                            for(var i = 0; i< skills.length; i++){
                                $scope.skills.push({name: skills[i]});
                            }
                            if (windowsize.devicetype() != 'Desktop') {

                                $timeout(function() {
                                    $scope.customCategory  = $scope.skills
                                    $scope.selectedSkills = $scope.skills
                                    $("select").val($scope.skills).trigger("change");
                                })

                            }
                            $scope.skillstable.aaData = $scope.skills;
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };

                function getParentSkills() {
                    db.get("work.json").then(function (result) {
                        if (result && result.data) {

                            $scope.parentskills.aaData = result.data;
                            $scope.categories = result.data;
                        }
                    });
                }
                var parenCategoryValue = {};
                $scope.selectedIndex = function (index, value) {
                    parenCategoryValue = value
                    $scope.$apply(function(){

                        if (windowsize.devicetype() != 'Desktop') {
                            if ($scope.parentskills) {
                                var skills = [];
                                if (value && value.skills && value.skills.length > 0) {
                                    if (!$scope.childrenskills.aaData || $scope.childrenskills.aaData.length == 0) {
                                        skills = _.filter(value.skills, function(item){
                                            return !skillsHasItem(item);
                                        });
                                    }
                                    else {
                                        var check = _.filter($scope.childrenskills.aaData, function(item){
                                            return value.skills.indexOf(item) == -1 && !skillsHasItem(item);
                                        });
                                        if(check.length > 0 && check.length == $scope.childrenskills.aaData.length){
                                            skills = $scope.childrenskills.aaData.concat(value.skills);
                                            skills = _.filter(value.skills, function (item) {
                                                return !skillsHasItem(item);
                                            });
                                        }else{
                                            skills = check;
                                        }
                                    }

                                    $scope.childrenskills.aaData = skills;
                                }
                            }
                        } else {
                            if ($scope.parentskills) {
                                $scope.childrenskills.aaData = _.filter(value.skills, function(item){
                                    return !skillsHasItem(item);
                                });
                            }
                        }
                    })

                };

                var skillsHasItem = function(item){
                    for(var i = 0; i < $scope.skills.length; i ++){
                        if($scope.skills[i].name == item.name){
                            return true;
                        }
                    }
                    return false;
                }
                var unique = function(origArr) {
                    var newArr = [],
                        origLen = origArr.length,
                        found, x, y;

                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if (origArr[x].name === newArr[y].name) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr;
                }
                $scope.chooseParentCategory = function (value)
                {
                    if (value != null)
                    {
                        if (value.children != undefined)
                        {
                            $scope.childrenCategory = '';
                            return;
                        }
                        else{

                            $timeout(function() {
                                var parentCategory =[];
                                angular.extend(parentCategory,$scope.selectedSkills)
                                for (var i=0;i<value.skills.length;i++)
                                {
                                    parentCategory.push(value.skills[i]);
                                }
                                console.log("parentCategory 2",parentCategory)
                                var uniqueCategory = unique(parentCategory)

                                console.log("$scope.customCategory",$scope.customCategory)
                                $scope.customCategory = uniqueCategory
                                $("#selectedSkill").select2();
                                $scope.$apply()
                                $("#selectedSkill").select2().trigger('change')
                            });

                        }
                    }
                }
                $scope.$watch('selectedSkills', function(newValue,oldValue) {
                    if (windowsize.devicetype() != 'Desktop') {
                        if ($scope.selectedSkills != undefined) {

                            if ($scope.selectedSkills.length >= 50) {
                                $scope.selectedSkills = oldValue;
                                logger.logWarning("Bạn không được nhập quá 50 kĩ năng")
                                return
                            }
                        }
                    }
                });
                $scope.$watch('chooseSkill', function(newValue,oldValue) {
                    if (windowsize.devicetype() == 'Desktop')
                    {
                    if ($scope.skills != undefined)
                    {

                        if ($scope.skills.length >= 50)
                        {   $scope.skills = oldValue;
                            logger.logWarning("Bạn không được nhập quá 50 kĩ năng")
                            return
                        }
                    }
                    }
                });
                $scope.chooseChildrenCategory = function(childrenValue)
                {

                    if (windowsize.devicetype() == 'Desktop')
                    {
                        return
                    }
                    else
                    {
                        console.log("childrenValue",childrenValue)
                        if (childrenValue != undefined)
                        {
                        if (childrenValue.children != undefined)
                        {
                            if (childrenValue.children.length > 0) {
                                return;
                            }
                        }
                        else {
                            $timeout(function () {

                                var childrenCategory =[];
                                angular.extend(childrenCategory,$scope.selectedSkills)
                                for (var i=0;i<childrenValue.skills.length;i++)
                                {
                                    childrenCategory.push(childrenValue.skills[i]);
                                }
                                var uniqueCategory = unique(childrenCategory)
                                $scope.customCategory = uniqueCategory;
                                $("#selectedSkill").select2()
                                $scope.$apply()
                                $("#selectedSkill").select2().trigger('change')
                            });

                        }
                        }
                    }
                }
                $scope.chooseSkill = function (index, value) {
                    if ($scope.childrenskills) {
                        if (windowsize.devicetype() == 'Desktop')
                        {
                            if ($scope.skills != undefined)
                            {

                                if ($scope.skills.length >= 50)
                                {
                                    logger.logWarning("Bạn không được nhập quá 50 kĩ năng")
                                }
                                else
                                {
                                    $scope.skills.push(value);
                                    var position = _.findIndex($scope.childrenskills.aaData, value);
                                    $scope.childrenskills.aaData.splice(position, 1);
                                    var childrenskills = angular.copy($scope.childrenskills.aaData);
                                    var skills = angular.copy($scope.skills);
                                    $scope.skillstable.aaData = skills;
                                    $scope.childrenskills.aaData = childrenskills;
                                    if (windowsize.devicetype() != 'Desktop') {
                                        notify(value.name);
                                    }
                                }
                            }
                        }
                        $scope.$apply();
                    }
                };

                $scope.editSkill = function (index, value, element) {
                    if (windowsize.devicetype() == 'Desktop') {
                        $(".dSkill").remove();
                        if (element.hasClass("active")) {
                            var deleteButton = angular.element('<a ng-click="deleteSkill()" class="dSkill pull-right glyphicon glyphicon-remove-sign"></a>');
                            var content = $compile(deleteButton)($scope);
                            $("td", element).append(content);
                            $scope.deleteSkillValue = value;
                        }
                    }
                };

                $scope.deleteSkill = function () {
                    var position = _.findIndex($scope.skills, $scope.deleteSkillValue);
                    $scope.skills.splice(position, 1);
                    var lastChidrens = $scope.childrenskills.aaData;
                    lastChidrens.push($scope.deleteSkillValue);
                    var skills = angular.copy($scope.skills);
                    $scope.skillstable.aaData = skills;
                    $scope.childrenskills.aaData = angular.copy(lastChidrens);
                    $scope.deleteSkillValue = null;
                };

                $scope.$on("window.size", function (object, type) {
                    if (type !== "Desktop") {
                        $("#select2-selectedSkill-results").removeClass("non-display");
                        setMultiTouch(true);
                        $timeout(function() {
                            $scope.customCategory  = $scope.skills
                            $scope.selectedSkills = $scope.skills
                            $("select").val($scope.selectedSkills).trigger("change");
                        })
                    } else {

                        $("#select2-selectedSkill-results").addClass("non-display");
                        setMultiTouch(false);
                        var skills = angular.copy($scope.selectedSkills);
                        $scope.skillstable.aaData = skills;
                        //var table2 = angular.copy($scope.childrenCategory);
                        //$scope.childrenskills.aaData = angular.copy(table2);
                    }
                });

                function setMultiTouch(isMulti) {
                    if($scope.childrenskills) $scope.childrenskills.multiTouch = isMulti;
                    if($scope.parentskills)$scope.parentskills.multiTouch = isMulti;
                }

                function notify(selected) {
                    return logger.logSuccess("Bạn vừa chọn: " + selected);
                };

            }]);
    });