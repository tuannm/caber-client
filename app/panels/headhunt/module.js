/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.headhunt', []);
        app.useModule(module);

        module.controller('headhunt', ['$scope', '$rootScope', '$stateParams', 'db', 'AuthService', 'AUTH_EVENTS', 'windowsize', '$location', 'Session', 'api', 'logger', 'store', '$modal', '$element', '$timeout',
            function ($scope, $rootScope, $stateParams, db, AuthService, AUTH_EVENTS, windowsize, $location, Session, api, logger, store, $modal, $element, $timeout) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                $scope.slides = [];
                $scope.categories = [];
                $scope.skills = [];
                $scope.currency = [];
                $scope.selected = {};
                $scope.cv = [];
                $scope.selectedSkills = [];
                var username = "";
                $scope.address1 = "";
                $scope.post = store.get("post") ? store.get("post") : {};
                console.log("get store",$scope.post);
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];
                $scope.init = function () {
                    $scope.ready = true;
                    $scope.selected = store.get('selected');
                    if (jQuery.isEmptyObject($scope.post) && jQuery.isEmptyObject($scope.selected)) {
                        $location.path("post");
                    } else {
                        getDescription();
                        getService();
                        $scope.minDate = new Date();
                        username = Session.userId;
                        if ($scope.post.skill != undefined) {
                            if($scope.post.isTemplate){
                                var skills = []
                                for (var i = 0; i < $scope.post.skill.length; i++) {
                                    skills.push($scope.selected.category.children[0].skills[$scope.post.skill[i]]);
                                }
                                $scope.selectedSkills = skills;
                            } else{
                                $scope.selectedSkills = $scope.post.skill;
                                $("select").val($scope.selectedSkills).trigger("change");
                            }

                        }
                        if (angular.isUndefined($scope.post.more)) {
                            $scope.post.more = {};
                        }
                    }
                };
                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    username = Session.userId;
                });
                $scope.account = {};
                $scope.account.selection = "personal";
                $scope.selection = [];

                $scope.selectSkills = function () {
                    $scope.skills = $scope.parentCategory.skills;
                }
                $scope.clearSkill = function () {
                    $scope.selected.skill = undefined;
                    if ($scope.parentCategory.children != undefined) {
                        $scope.childrenCategory = $scope.parentCategory.children[0];
                    }
                }
                $scope.goBack = function () {
                    $scope.posts = store.get('post');
                    $scope.posts.skill = $scope.selectedSkills;
                    var getAddonService = $('#addonService').dataTable().fnGetData();
                    for (var i = 0; i < getAddonService.length; i++) {
                        if (getAddonService[i].isSelected && getAddonService[i].quantity == null) {
                            getAddonService[i].isSelected = false;
                        }
                    }
                    var getInformation = $('#infoMore').dataTable().fnGetData();
                    for (var i = 0; i < getInformation.length; i++) {
                        if (getInformation[i].isSelected && getInformation[i].input == null) {
                            getInformation[i].isSelected = false;
                        }
                    }
                    $scope.posts.more.info = getInformation;
                    $scope.posts.more.addonService = getAddonService;
                    // $scope.posts.more.address= $scope.post.more.address;
                    store.set('post', $scope.posts);
                    $location.path("/post");
                }

                function checkInputvalue(value1, value2, callback) {
                    var valueReplace1 = value1.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    var valueReplace2 = value2.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    db.get("validatePost.json").then(function (result) {
                        if (result.data.length > 0) {
                            var check = result.data;
                            var found = false;
                            var validateNumber1 = valueReplace1.replace(/\s+/g, '').match(/\d+/g);
                            var validateNumber2 = valueReplace2.replace(/\s+/g, '').match(/\d+/g);
                            if(!found) {
                                if(validateNumber1 != null){
                                    for (var i = 0; i < validateNumber1.length; i++) {
                                        if (validateNumber1[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                if(validateNumber2 != null){
                                    for (var i = 0; i < validateNumber2.length; i++) {
                                        if(validateNumber2[i].length > 7){
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                for (var j = 0; j < check.length; j++) {
                                    if (valueReplace1.search(check[j].name) != -1 || valueReplace2.search(check[j].name) != -1) {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found) {
                                callback(true);
                                return true;
                            } else {
                                callback(false);
                                return false;
                            }
                        }
                    });
                }

                $scope.wizard1CompleteCallback = function (wizardData) {
                    $scope.posts = store.get('post');
                    checkInputvalue($scope.post.title.toString().toLowerCase(), $scope.post.content.toString().toLowerCase(), function (result) {
                        $scope.request = {};
                        if (result) {
                            $scope.request.status = 3;
                        }
                        $scope.request.username = $scope.posts.username;
                        $scope.request.serviceTypeId = $scope.posts.category.id;
                        $scope.request.currencyId = 101;
                        var addonServices = [];
                        var getAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < getAddonService.length; i++) {
                            if (getAddonService[i].isSelected && getAddonService[i].quantity != null) {
                                var objAddonService = {};
                                objAddonService.addonServiceId = getAddonService[i].id;
                                objAddonService.quantity = getAddonService[i].quantity;
                                addonServices.push(objAddonService);
                            }
                            if (getAddonService[i].isSelected && getAddonService[i].quantity == null) {
                                getAddonService[i].isSelected = false;
                            }
                        }
                        $scope.request.addonServiceOrders = addonServices;
                        $scope.request.title = $scope.posts.title;
                        $scope.request.content = $scope.post.content;

                        var i;
                        var skill = [];
                        if ($scope.selectedSkills != undefined) {
                            for (i = 0; i < $scope.selectedSkills.length; i++) {
                                skill.push($scope.selectedSkills[i].name);
                            }
                        }

                        $scope.request.skill = skill.toString();
                        $scope.request.budgetFrom = $scope.posts.budgetTo;
                        $scope.request.budgetTo = null;
                        $scope.request.bonus = null;
                        $scope.request.expiredDate = $scope.posts.expiredDate;
                        var getInformation = $('#infoMore').dataTable().fnGetData();
                        for (var i = 0; i < getInformation.length; i++) {
                            if (getInformation[i].isSelected && getInformation[i].input == null) {
                                getInformation[i].isSelected = false;
                            }
                        }
                        var dataMore = {
                            "city": $scope.posts.city,
                            "information": getInformation,
                            "position": null,
                            "workType": null,
                            "timeStart": null,
                            "timeFinish": null,
                            "dateStart": null,
                            "salary": null,
                            "sex": null,
                            "career": null,
                            "addonService": getAddonService
                        };
                        $scope.request.jsonData = JSON.stringify(dataMore);
                        if (addonServices.length > 0) {
                            var modalNotifyAddonService;
                            modalNotifyAddonService = $modal.open({
                                templateUrl: "app/partials/form/notifyAddonService.html",
                                controller: 'NotifyAddonServiceDialogCtrl',
                                resolve: {
                                    data: function () {
                                        return getAddonService;
                                    }
                                }
                            });
                            modalNotifyAddonService.result.then((function (isSubmit) {
                                if (isSubmit) {
                                    $scope.request.addonPayment = 0;
                                    api.post('serviceorder/v1', $scope.request).then(function (res) {
                                        console.log("res", res);
                                        store.set('post');
                                        if (res.data.result) {
                                            var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + res.data.serviceOrder.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                                            $rootScope.paymentUrl(url);
                                        } else {
                                            logger.logError(res.data.error.description);
                                        }
                                    });
                                }

                            }), function () {
                            });
                        } else {
                            api.post('serviceorder/v1', $scope.request).then(function (res) {
                                console.log("res", res);
                                store.set('post');
                                if (res.data.result) {
                                    $location.path("/detailpost");
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            });
                        }
                    });

                };

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var columnTitle = rowDetail.find(".columnTitle");
                    var columnValue = rowDetail.find(".columnValue");
                    columnTitle.addClass("non-display");
                    columnValue.removeClass("col-xs-9");
                    columnValue.addClass("col-xs-12");
                    if (windowsize.devicetype() !== 'Desktop') {
                        var listInformation = $('#infoMore').dataTable().fnGetData();
                        for (var i = 0; i < listInformation.length; i++) {
                            if (listInformation[i].isSelected && listInformation[i].input == null && listInformation[i].id != element.scope().aData.id) {
                                listInformation[i].isSelected = !listInformation[i].isSelected;
                            }
                            if (listInformation[i].isSelected && listInformation[i].input < 10000 && listInformation[i].id != element.scope().aData.id) {
                                listInformation[i].isSelected = !listInformation[i].isSelected;
                                listInformation[i].input = null;
                            }
                        }
                    }
                };

                $scope.removeHasError = function () {
                    var $selectSkill = $element.find("[name=skills]");
                    var formGroup = $selectSkill.closest(".form-group");
                    var helpBlock = formGroup.find(".help-block");
                    if (formGroup.hasClass("has-error")) {
                        formGroup.removeClass("formGroup");
                    }
                    if (helpBlock.length > 0) {
                        helpBlock.remove();
                    }
                }

                $scope.validateAddon = function (index, value, element) {
                    var listAddonService = $('#addonService').dataTable().fnGetData();
                    for (var i = 0; i < listAddonService.length; i++) {
                        if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != element.scope().aData.id) {
                            listAddonService[i].isSelected = !listAddonService[i].isSelected;
                        }
                    }
                };

                $scope.setSelection = function (value) {
                    $scope.account.selection = value;
                    if (value === 'personal') {
                        angular.element("#personal").addClass("active");
                        angular.element("#organization").removeClass("active");
                    } else if (value === 'organization') {
                        angular.element("#personal").removeClass("active");
                        angular.element("#organization").addClass("active");
                    }
                };

                $scope.$on("window.width", function (object, type) {
                    //var rowDetail = $element.find(".row-detail");
                    //rowDetail.remove();
                    //var detailShow = $element.find(".detail-show");
                    //detailShow.removeClass("detail-show");
                    //detailShow.removeClass("active");
                });

                $scope.showProjects = function () {
                    var credentials = {};
                    if ($scope.username === 'customer') {
                        credentials.user_role = 'customer';
                    } else if ($scope.username === 'supplier') {
                        credentials.user_role = 'supplier';
                    }
                    credentials.user_id = "1";
                    AuthService.authenticate(credentials);
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                };
                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };

                $scope.setBudget = function (aData) {
                    var $input = $element.find('[name=' + aData.type + ']');
                    var $parent = $input.closest('.form-group');
                    var spanError = $parent.find(".help-block");
                    var $inputOther = $element.find('[name=input_other]');
                    var $parentOther = $inputOther.closest('.form-group');
                    var spanErrorOther = $parentOther.find(".help-block");
                    if (!aData.isSelected) {
                        aData.input = null;
                        if (aData.name == 'Other') {
                            aData.description = "";
                            if ($parentOther.hasClass("has-error")) {
                                $parentOther.removeClass("has-error");
                            }
                            if ($parentOther.hasClass("has-success")) {
                                $parentOther.removeClass("has-success");
                            }
                            if (spanErrorOther.length > 0) {
                                spanErrorOther.remove();
                            }
                        }
                        if ($parent.hasClass("has-error")) {
                            $parent.removeClass("has-error");
                        }
                        if ($parent.hasClass("has-success")) {
                            $parent.removeClass("has-success");
                        }
                        if (spanError.length > 0) {
                            spanError.remove();
                        }
                    } else {
                        if (!$parent.hasClass("has-error")) {
                            $parent.addClass("has-error");
                        }
                        if (windowsize.devicetype() !== 'Desktop') {
                            var listInformation = $('#infoMore').dataTable().fnGetData();
                            for (var i = 0; i < listInformation.length; i++) {
                                if (listInformation[i].isSelected && listInformation[i].input == null && listInformation[i].id != aData.id) {
                                    listInformation[i].isSelected = !listInformation[i].isSelected;
                                }
                                if (listInformation[i].isSelected && listInformation[i].input < 10000 && listInformation[i].id != aData.id) {
                                    listInformation[i].isSelected = !listInformation[i].isSelected;
                                    listInformation[i].input = null;
                                }
                            }
                        }
                    }
                };

                $scope.setCount = function (aData) {
                    var $input = $element.find('[name=' + aData.type + ']');
                    var $parent = $input.closest('.form-group');
                    if (!aData.isSelected) {
                        if (aData.isInput) {
                            aData.quantity = null;
                            var spanError = $parent.find(".help-block");
                            if ($parent.hasClass("has-error")) {
                                $parent.removeClass("has-error");
                            }
                            if ($parent.hasClass("has-success")) {
                                $parent.removeClass("has-success");
                            }
                            if (spanError.length > 0) {
                                spanError.remove();
                            }
                        }
                    } else {
                        var listAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < listAddonService.length; i++) {
                            if (listAddonService[i].isSelected && listAddonService[i].quantity == null && listAddonService[i].id != aData.id) {
                                listAddonService[i].isSelected = !listAddonService[i].isSelected;
                            }
                        }
                    }

                };


                var loadPostMore = function () {
                    db.get("work.json").then(function (result) {
                        if (result.data.length > 0) {
                            for (var i = 0; i < result.data.length; i++) {
                                if (result.data[i].name == 'Nhân sự') {
                                    $scope.categories = result.data[i].children;
                                    $scope.parentCategory = $scope.categories[0];
                                    $scope.skills = $scope.parentCategory.skills;
                                }
                            }
                        }
                    });
                };

                $scope.selectedRow = function (index, data, element) {
                    data.isSelected = !data.isSelected;

                };
                $scope.loadSampleDescription = function (e) {
                    if (e == 1) {
                        $scope.post.content = 'Cần 20 số điện thoại của ứng viên làm về J2EE. Ứng viên có trên 2 năm kinh nghiệm, người giới thiệu cần ghi rõ công ty nơi ứng viên làm việc, tóm tắt kinh nghiệm. Nếu ai giới thiệu được tôi sẵn sàng trả 500.000 cho yêu cầu, không cần gửi CV';
                    } else if (e == 2) {
                        $scope.post.content = 'Tôi cần 5 CV Java, biết tiếng Anh. Tôi có thể trả 1 triệu cho ai giới thiệu được CV này';
                    } else if (e == 3) {
                        $scope.post.content = 'Tôi cần tuyển dụng 2 ứng viên Java. Ứng viên trên 2 năm kinh nghiệm, biết về String, Strut. Ứng viên cần có tiếng Nhật. Ứng viên có mức lương từ 10-20 triệu, trải qua 2 vòng phỏng vấn. Tôi sẵn sàng trả 2 tháng lương của ứng viên cho người tuyển dụng thành công ứng viên';
                    }
                };

                function getDescription() {
                    db.get('headhunt.json').then(function (res) {
                        var data;
                        if ($scope.post.more.info != undefined) {
                            if($scope.post.isTemplate){
                                for (var i = 0; i < res.data.length; i++) {
                                    for (var j = 0; j < $scope.post.more.info.length; j++) {
                                        if(res.data[i].id == $scope.post.more.info[j].id){
                                            res.data[i].input = $scope.post.more.info[j].input;
                                            res.data[i].description = $scope.post.more.info[j].description;
                                            res.data[i].isSelected = true;
                                        }
                                    }
                                }
                                data = res.data;
                            } else{
                                data = $scope.post.more.info;
                            }
                        } else {
                            data = res.data;
                        }
                        $scope.descriptionTable = {
                            "aaData": data,
                            "ordering": false,
                            "search": false,
                            "paging": false,
                            "filter": false,
                            "info": false,
                            "columns": [
                                {"data": "null"},
                                {"data": "null"},
                                {"data": "null"}
                            ],
                            "columnDefs": [
                                {
                                    "targets": 0,
                                    "data": null,
                                    "width": "10%",
                                    "class": "text-center width10Percent",
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element("<div></div>");
                                        element.append('<label class="ui-checkbox"><input name="checkbox"  ng-model="aData.isSelected" ng-change="$parent.setBudget(aData)"  type="checkbox"><span></span></label>');
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 1,
                                    "data": null,
                                    "width": "50%",
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element("<div></div>");
                                        if (full.name == 'Other') {
                                            element.append('<div class="form-group col-md-12 no-padding"><input ng-disabled="!aData.isSelected" type="text" id="input_other" name="input_other" data-smart-validate-input data-required data-message-required="validate field" reset-field ng-model="aData.description" class="form-control col-md-12" data-i18n="i18n-placeholder" placeholder="other fields" /></div>');
                                        } else {
                                            element.append('<strong>' + full.description + '</strong>');
                                        }
                                        return element.html();
                                    }
                                },
                                {
                                    "targets": 2,
                                    "data": null,
                                    "class": "setWidth-35",
                                    "render": function (data, type, full, meta) {
                                        var element = angular.element('<div ></div>');
                                        element.append('<div class="form-group"><div class="input-group"><input ng-disabled="!aData.isSelected" data-smart-validate-input requireField="validate budget" name="' + full.type + '"' +
                                        ' class="form-control" reset-field data-i18n="i18n-placeholder" placeholder="post deal" type="tel" maxlength="15" ng-model="aData.input" format-currency numbers-only "/> ' +
                                        '<span class="input-group-btn"><button type="button" class="btn btn-default">&nbsp;<u>đ</u></button></span></div></div>');
                                        return element.html();
                                    }
                                }

                            ]
                        };
                    });

                }
                $scope.serviceTable = {
                    "paging": false,
                    "columns": [
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"}
                    ],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "width": "2%",
                            "class": "id",
                            "render": function (data, type, full, meta) {
                                if(!full.isSelected){
                                    full.isSelected = false;
                                }
                                var element = angular.element("<div></div>");
                                element.append('<label class="ui-checkbox"><input ng-model="aData.isSelected" ng-change="$parent.setCount(aData)" type="checkbox"><span></span></label>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 1,
                            "data": null,
                            "width": "5%",
                            "class": "name",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append('<span class="label label-warning">' + full.name + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 2,
                            "data": null,
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append(full.description);
                                return element.html();
                            }
                        },
                        {
                            "targets": 3,
                            "data": null,
                            "width": "20%",
                            "class": "text-right",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                var formatServiceCost = "";
                                if(full.type == "so_sms"){
                                    formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/SMS";
                                } else if(full.type == "so_email"){
                                    formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/Email";
                                } else{
                                    formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                }

                                var formatServiceCostSalse = "";
                                if(full.serviceOldCost != null){
                                    if(full.type == "so_sms"){
                                        formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/SMS";
                                    } else if(full.type == "so_email"){
                                        formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫/Email";
                                    } else{
                                        formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                    }
                                }
                                element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 4,
                            "data": null,
                            "width": "15%",
                            "class": "quantity",
                            "render": function (data, type, full, meta) {
                                if(full.id != 3){
                                    full.isInput = true;
                                    if(full.quantity == null){
                                        full.quantity = null;
                                    }
                                } else{
                                    full.isInput = false;
                                    full.quantity = 1;
                                }
                                var element = angular.element("<div></div>");
                                if (full.isInput) {
                                    element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><input ng-disabled="!aData.isSelected" name="' + full.type + '" data-smart-validate-input data-required data-message-required="validate field" ng-model="aData.quantity" maxlength="6" reset-field class="form-control col-md-12" type="tel" maxlength="15" format-currency numbers-only data-i18n="i18n-placeholder" placeholder="enter the number"/></div>');
                                } else {
                                    element.append('<span>' + full.quantity + '</span>');
                                }

                                return element.html();
                            }
                        }
                    ]
                };

                function getService() {
                    if ($scope.post.more != undefined && $scope.post.more.addonService != undefined) {
                        if($scope.post.isTemplate){
                            api.get('addonservice/v1').then(function (res) {
                                for (var i = 0; i < res.data.addonServices.length; i++) {
                                    if (res.data.addonServices[i].id == 4) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 5) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    for (var j = 0; j < $scope.post.more.addonService.length; j++) {
                                        if(res.data.addonServices[i].id == $scope.post.more.addonService[j].id){
                                            res.data.addonServices[i].quantity = $scope.post.more.addonService[j].quantity;
                                            res.data.addonServices[i].description = $scope.post.more.addonService[j].description;
                                            res.data.addonServices[i].isSelected = true;
                                        }
                                    }

                                }
                                $scope.serviceTable.aaData = res.data.addonServices;
                            });
                        } else{
                            $scope.serviceTable.aaData = $scope.post.more.addonService;
                        }

                    } else {
                        api.get('addonservice/v1').then(function (res) {
                            console.log("resAddon", res);
                            for (var i = 0; i < res.data.addonServices.length; i++) {
                                if (res.data.addonServices[i].id == 1) {
                                    res.data.addonServices[i].description = "Gửi email tới các lập trình viên tiềm năng của yêu cầu. Yêu cầu của bạn sẽ đến được với nhiều đối tượng,dễ dàng nhận được nhiều đề xuất hơn";
                                }
                                if (res.data.addonServices[i].id == 2) {
                                    res.data.addonServices[i].description = "Gửi tin nhắn điện thoại đến tận tay lập trình viên tiềm năng của yêu cầu. Yêu cầu đến trực tiếp nhiều đối tượng,dễ dàng nhận được nhiều đề xuất hơn";
                                }
                                if (res.data.addonServices[i].id == 4) {
                                    res.data.addonServices.splice(i, 1);
                                }
                                if (res.data.addonServices[i].id == 5) {
                                    res.data.addonServices.splice(i, 1);
                                }
                            }
                            $scope.serviceTable.aaData = res.data.addonServices;
                        });
                    }

                    //db.get('service_headhunt.json').then(function (resService) {
                    //    var data;
                    //    if ($scope.post.more.addonService != undefined) {
                    //        data = $scope.post.more.addonService;
                    //    } else {
                    //        for (var i = 0; i < resService.data.length; i++) {
                    //            if (resService.data[i].type == "email") {
                    //                resService.data[i].description = "Gửi email tới các lập trình viên tiềm năng của yêu cầu. Yêu cầu của bạn sẽ được gửi tới nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                    //            }
                    //            if (resService.data[i].type == "sms") {
                    //                //resService.data.splice(i, 1);
                    //                resService.data[i].description = "Gửi tin nhắn điện thoại đến tận tay lập trình viên tiềm năng của yêu cầu. Yêu cầu trực tiếp nhiều đối tượng,dễ dàng nhận được yêu cầu đề xuất hơn";
                    //            }
                    //        }
                    //        data = resService.data;
                    //    }
                    //    $scope.serviceTable = {
                    //        "aaData": data,
                    //        "paging": false,
                    //        "columns": [
                    //            {"data": "null"},
                    //            {"data": "null"},
                    //            {"data": "null"},
                    //            {"data": "null"},
                    //            {"data": "null"}
                    //        ],
                    //        "columnDefs": [
                    //            {
                    //                "targets": 0,
                    //                "data": null,
                    //                "width": "2%",
                    //                "class": "id",
                    //                "render": function (data, type, full, meta) {
                    //                    var element = angular.element("<div></div>");
                    //                    element.append('<label class="ui-checkbox"><input ng-model="aData.isSelected" ng-change="$parent.setCount(aData)" type="checkbox"><span></span></label>');
                    //                    return element.html();
                    //                }
                    //            },
                    //            {
                    //                "targets": 1,
                    //                "data": null,
                    //                "width": "5%",
                    //                "class": "name",
                    //                "render": function (data, type, full, meta) {
                    //                    var element = angular.element("<div></div>");
                    //                    element.append('<span class="label label-warning">' + full.name + '</span>');
                    //                    return element.html();
                    //                }
                    //            },
                    //            {
                    //                "targets": 2,
                    //                "data": null,
                    //                "render": function (data, type, full, meta) {
                    //                    var element = angular.element("<div></div>");
                    //                    element.append(full.description);
                    //                    return element.html();
                    //                }
                    //            },
                    //            {
                    //                "targets": 3,
                    //                "data": null,
                    //                "width": "20%",
                    //                "class": "text-right",
                    //                "render": function (data, type, full, meta) {
                    //                    var element = angular.element("<div></div>");
                    //                    var formatServiceCost = full.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currency;
                    //                    var formatServiceCostSalse = full.price_old.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " " + full.currency;
                    //                    element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                    //                    return element.html();
                    //                }
                    //            },
                    //            {
                    //                "targets": 4,
                    //                "data": null,
                    //                "width": "15%",
                    //                "class": "quantity",
                    //                "render": function (data, type, full, meta) {
                    //                    var element = angular.element("<div></div>");
                    //                    if (full.isInput) {
                    //                        element.append('<div class="form-group col-md-12 no-padding"><input ng-disabled="!aData.isSelected" name="' + full.type + '" data-smart-validate-input data-required data-message-required="Bắt buộc phải nhập" ng-model="aData.quantity" maxlength="6" reset-field class="form-control col-md-12" type="text" maxlength="15" format-currency numbers-only placeholder="Nhập số lượng bạn muốn gửi"/></div>');
                    //                    } else {
                    //                        element.append('<span>' + full.quantity + '</span>');
                    //                    }
                    //
                    //                    return element.html();
                    //                }
                    //            }
                    //        ]
                    //    };
                    //});
                }

                var loadCurrency = function () {
                    api.get('currency/v1').then(function (res) {
                        if (res.data.result) {
                            $scope.currency = res.data.currencies;
                        } else {
                            logger.logError(res.data.error.description);
                        }
                        $scope.parentCurrency = $scope.currency[100];

                    });
                };

            }]);
        module.controller('NotifyAddonServiceDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                var typeAddonService = "";
                var removeValFromIndex = [];
                var totalPayment = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].isSelected && data[i].quantity != null) {
                        typeAddonService += data[i].name + ", ";
                        totalPayment += parseInt(data[i].quantity * data[i].serviceCost);
                    } else {
                        removeValFromIndex.push(i);
                    }
                }
                var resultData = $.grep(data, function (n, i) {
                    return $.inArray(i, removeValFromIndex) == -1;
                });
                $scope.typeAddonService = typeAddonService;
                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $scope.totalPaymentAddonService = {
                    "aaData": resultData,
                    "paging": false,
                    "columns": [
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "null"}
                    ],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "class": "id text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append('<span>' + full.name + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 1,
                            "data": null,
                            "class": "name text-right",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                var formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                var formatServiceCostSalse = "";
                                if(full.serviceOldCost != null){
                                    formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                }
                                element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 2,
                            "data": null,
                            "class": "name text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                element.append('<span>' + full.quantity + '</span>');
                                return element.html();
                            }
                        },
                        {
                            "targets": 3,
                            "data": null,
                            "width": "20%",
                            "class": "text-right",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div></div>");
                                var total = parseInt(full.quantity * full.serviceCost);
                                element.append('<span>' + total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>');
                                return element.html();
                            }
                        }
                    ]
                };

                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $scope.confirmReject = function () {
                    $modalInstance.close(true);
                };
                $scope.cancelReject = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);

    });
