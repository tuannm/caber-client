/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.survey', []);
        app.useModule(module);

        module.controller('survey', [ '$scope', '$rootScope', function($scope, $rootScope) {
            $scope.panelMeta = {
                status  : "Stable",
                description : "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode    : "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content : "",
                style: {}
            };
            _.defaults($scope.panel,_d);

            $scope.init = function() {
                $scope.ready = true;
            };

            $scope.startSurvey = function(){
                if($(".question-1").hasClass("non-display")){
                    $(".question-1").removeClass("non-display");
                    $(".question-0").addClass("non-display");
                }
            }

            $scope.nextQuestion2 = function(){
                if($(".question-2").hasClass("non-display")){
                    $(".question-2").removeClass("non-display");
                    $(".question-1").addClass("non-display");
                }
            }
            $scope.prevQuestion0 = function(){
                if($(".question-0").hasClass("non-display")){
                    $(".question-0").removeClass("non-display");
                    $(".question-1").addClass("non-display");
                }
            }
            $scope.nextQuestion3 = function(){
                if($(".question-3").hasClass("non-display")){
                    $(".question-3").removeClass("non-display");
                    $(".question-2").addClass("non-display");
                }
            }
            $scope.prevQuestion1 = function(){
                if($(".question-1").hasClass("non-display")){
                    $(".question-1").removeClass("non-display");
                    $(".question-2").addClass("non-display");
                }
            }

            $scope.nextQuestion4 = function(){
                if($(".question-4").hasClass("non-display")){
                    $(".question-4").removeClass("non-display");
                    $(".question-3").addClass("non-display");
                }
            }
            $scope.prevQuestion2 = function(){
                if($(".question-2").hasClass("non-display")){
                    $(".question-2").removeClass("non-display");
                    $(".question-3").addClass("non-display");
                }
            }

            $scope.nextQuestion5 = function(){
                if($(".question-5").hasClass("non-display")){
                    $(".question-5").removeClass("non-display");
                    $(".question-4").addClass("non-display");
                }
            }
            $scope.prevQuestion3 = function(){
                if($(".question-3").hasClass("non-display")){
                    $(".question-3").removeClass("non-display");
                    $(".question-4").addClass("non-display");
                }
            }
            $scope.prevQuestion4 = function(){
                if($(".question-4").hasClass("non-display")){
                    $(".question-4").removeClass("non-display");
                    $(".question-5").addClass("non-display");
                }
            }



        }]);
    });
