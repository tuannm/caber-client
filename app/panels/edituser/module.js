/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.edituser', []);
        app.useModule(module);

        module.controller('edituser', ['$scope', '$rootScope', 'USER_ROLES', 'AuthService', 'db', '$stateParams', 'api', function ($scope, $rootScope, USER_ROLES, AuthService, db, $stateParams, api) {
            $scope.panelMeta = {
                status: "Stable",
                description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };
            $scope.saveClick = function () {
                console.log($scope.accountdetails);
            }
            $scope.updateAccountClick = function () {
                console.log($scope.accountsettings);
            }
            $scope.updateEmailClick = function () {
                console.log($scope.notifications);
            }
            var loadCountries = function () {
                db.get("countries.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.countries = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentCountries = $scope.countries[$stateParams.params - 1];
                    } else {
                        $scope.parentCountries = $scope.countries[235];
                    }
                });
            };

            var loadTimezones = function () {
                db.get("timezones.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.timezones = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentTimezones = $scope.timezones[$stateParams.params - 1];
                    } else {
                        $scope.parentTimezones = $scope.timezones[75];
                    }
                });
            };

            var loadCurrency = function () {
                api.get('currency/v1').then(function (res) {
                    if (res.data.result) {
                        $scope.currency = res.data.currencies;
                    } else {
                        logger.logError(res.data.error.description);
                    }
                    $scope.parentCurrency = $scope.currency[100];

                });
            };

            var loadLanguage = function () {
                db.get("language.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.language = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentLanguage = $scope.language[$stateParams.params - 1];
                    } else {
                        $scope.parentLanguage = $scope.language[79];
                    }
                });
            };
            var loadEmailfrequency = function () {
                db.get("emailfrequency.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailfrequency = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailfrequency = $scope.emailfrequency[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailfrequency = $scope.emailfrequency[0];
                    }
                });
            };
            var loadEmailfrequencyactivity = function () {
                db.get("emailfrequencyactivity.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailfrequencyactivity = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailfrequencyactivity = $scope.emailfrequencyactivity[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailfrequencyactivity = $scope.emailfrequencyactivity[0];
                    }
                });
            };
            var loadEmailformat = function () {
                db.get("emailformat.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailformat = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailformat = $scope.emailformat[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailformat = $scope.emailformat[0];
                    }
                });
            };

            //$scope.saveClick = function () {
            //    console.log($scope.user);
            //}
            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode: "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content: "",
                style: {}
            };
            _.defaults($scope.panel, _d);
            $scope.slides = [];
            $scope.countries = [];
            $scope.timezones = [];
            $scope.currency = [];
            $scope.language = [];
            $scope.emailfrequency = [];
            $scope.emailfrequencyactivity = [];
            $scope.emailformat = [];
            $scope.init = function () {
                $scope.ready = true;
                $scope.userRoles = USER_ROLES;
                $scope.isAuthorized = AuthService.isAuthorized;
                loadCountries();
                loadTimezones();
                loadCurrency();
                loadLanguage();
                loadEmailfrequency();
                loadEmailfrequencyactivity();
                loadEmailformat();
            };
        }]);

    });

