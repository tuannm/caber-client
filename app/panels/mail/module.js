
define([
        'angular',
        'app',
        'css!./module.css'
    ],
    function (angular, app) {
        var module = angular.module('bidproject.panels.mail', []);
        app.useModule(module);

        module.controller('mail', ['$scope', '$rootScope', 'socketServices',
            function ($scope, $rootScope, socketServices) {
                socketServices.chatService();
                $scope.message = '';

                $scope.openChat = function (user) {
                    if ($rootScope.chat_id != user.chat_id) {
                        $rootScope.chat_id = user.chat_id;
                    }
                }

                $scope.sendMessage = function (message) {
                    if (message != '') {
                        socketServices.sendMessage(message);
                        $scope.message = '';
                    }
                }

                $scope.messageTo = function (user_id) {
                    $scope.message += $scope.message == '' ? user_id : ' ' + user_id;
                }
            }
        ]);
    }
);