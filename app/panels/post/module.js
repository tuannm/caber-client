/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.post', []);
        app.useModule(module);

        module.controller('post', ['$scope', '$rootScope', '$stateParams', 'db', 'AuthService', 'AUTH_EVENTS', 'windowsize', '$location', 'Session', 'api', 'logger', 'store',
            function ($scope, $rootScope, $stateParams, db, AuthService, AUTH_EVENTS, windowsize, $location, Session, api, logger, store) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };

                // Set and populate defaults
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                //init variables
                $scope.slides = [];
                $scope.categories = [];
                $scope.post = store.get('post') ? store.get('post') : {};
                $scope.selected = store.get('selected');
                $scope.skills = [];
                $scope.currency = [];
                $scope.selected = {};
                $scope.account = {};
                $scope.dataTemplate = {};
                $scope.account.selection = "personal";
                $scope.selection = [];
                $scope.isTemplate = false;
                $scope.checkboxRule = false;
                var username = "";
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];
                $scope.init = function () {
                    $scope.ready = true;
                    loadCities();
                    loadCategories();
                    getTemplate();
                    var date = new Date();
                    if ($scope.post.expiredDate != undefined) {
                        $scope.date_expired = $scope.post.expiredDate;
                    } else {
                        //$scope.date_expired = new Date(date.setDate(date.getDate() + 7));
                    }
                    $scope.format = 'dd/MM/yyyy';
                    $scope.time_expired = date;
                    $scope.hstep = 1;
                    $scope.mstep = 1;
                    $scope.ismeridian = false;
                    username = Session.userId;
                    $scope.parentCities = $scope.post.city;
                    if (!angular.isUndefined($scope.post.more)) {
                        $scope.checkboxRule = true;
                        var check = false;
                        if ($scope.post.skill != undefined) {
                            if ($scope.post.skill.length != 0) {
                                check = true;
                            }
                        }

                        for (var key in $scope.post.more) {
                            if ($scope.post.more.hasOwnProperty(key)) {
                                if ($scope.post.more[key] != undefined
                                    && $scope.post.more[key] != ""
                                    && $scope.post.more[key] != null) {
                                    if (key == "info" || key == "addonService") {
                                        for (var i = 0; i < $scope.post.more[key].length; i++) {
                                            if ($scope.post.more[key][i].isSelected) {
                                                check = true;
                                                break;
                                            }
                                        }
                                        if (check) {
                                            break;
                                        }

                                    } else {
                                        check = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (check) {
                            $scope.moreEnabled = true;
                        } else {
                            $scope.moreEnabled = false;
                        }
                    }
                    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
                        $("#isMobile").removeClass("non-display")
                        $("#isNotMobile").addClass("non-display")
                        //$scope.isMobile = true;
                    } else {
                        $("#isNotMobile").removeClass("non-display")
                        $("#isMobile").addClass("non-display")
                        //$scope.isMobile = false;
                    }
                };
                //$('#date_expired').mask("99/99/9999", {placeholder: 'MM/DD/YYYY' });
                //$('#date_expired').val('');
                $scope.changeCategory = function (valueSelected) {
                    if (!angular.isUndefined($scope.post.more)) {
                        $scope.post.more = {};
                        store.set("post", $scope.post);
                        $scope.checkboxRule = false;
                        $scope.moreEnabled = false;
                    }
                    if(valueSelected != undefined){
                        if (valueSelected.id == 7) {
                            $("[data-smart-click=wizardPostMoreCompleteCallback]").css("display", "none");
                        } else {
                            $("[data-smart-click=wizardPostMoreCompleteCallback]").css("display", "block");
                        }

                        var data = _.filter($scope.dataTemplate, function(item){
                            return item.data.id == valueSelected.id
                        })
                        $scope.template = data;
                    } else{
                        $scope.template = $scope.dataTemplate;
                    }
                };
                $scope.toggleMode = function () {
                    $scope.ismeridian = !$scope.ismeridian;
                };
                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    username = Session.userId;
                });

                $scope.toggleSelection = function (addonServiceId) {
                    var idx = $scope.selection.indexOf(addonServiceId);
                    if (idx > -1) {
                        $scope.selection.splice(idx, 1);
                    }
                    else {
                        $scope.selection.push(addonServiceId);
                    }
                };
                $scope.wizardPostMoreCompleteCallback = function () {
                    $scope.post.isTemplate = $scope.isTemplate;
                    $scope.post.username = username;
                    $scope.post.currencyId = 101;
                    $scope.post.addonServiceIds = [];
                    $scope.post.budgetFrom = null;
                    var dateString = new Date($scope.date_expired).toDateString();
                    var date = new Date();
                    var setHours = date.setHours(23, 59, 59);
                    var timeString = new Date(setHours).toTimeString();
                    var expiredDate = new Date(dateString + " " + timeString);
                    $scope.post.expiredDate = expiredDate;
                    $scope.post.bonus = null;
                    $scope.post.city = $scope.parentCities;
                    $scope.post.category = $scope.selected.category;
                    $scope.post.indexCategory = $scope.categories.indexOf($scope.selected.category);
                    store.set("post", $scope.post);
                    store.set("selected", $scope.selected);
                    $location.path($scope.selected.category.url);
                }
                function checkInputvalue(value1, value2, callback) {
                    var valueReplace1 = value1.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    var valueReplace2 = value2.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    db.get("validatePost.json").then(function (result) {
                        if (result.data.length > 0) {
                            var check = result.data;
                            var found = false;
                            var validateNumber1 = valueReplace1.replace(/\s+/g, '').match(/\d+/g);
                            var validateNumber2 = valueReplace2.replace(/\s+/g, '').match(/\d+/g);
                            if(!found) {
                                if(validateNumber1 != null){
                                    for (var i = 0; i < validateNumber1.length; i++) {
                                        if (validateNumber1[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                if(validateNumber2 != null){
                                    for (var i = 0; i < validateNumber2.length; i++) {
                                        if(validateNumber2[i].length > 7){
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if(!found){
                                for (var j = 0; j < check.length; j++) {
                                    if (valueReplace1.search(check[j].name) != -1 || valueReplace2.search(check[j].name) != -1) {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found) {
                                callback(true);
                                return true;
                            } else {
                                callback(false);
                                return false;
                            }
                        }
                    });
                }

                $scope.wizard1CompleteCallback = function (wizardData) {
                    checkInputvalue($scope.post.title.toString().toLowerCase(), $scope.post.content.toString().toLowerCase(), function (result) {
                        $scope.request = {};
                        if (result) {
                            $scope.request.status = 3;
                        }
                        $scope.request.username = username;
                        $scope.request.serviceTypeId = $scope.selected.category.id;
                        $scope.request.currencyId = 101;
                        $scope.request.addonServiceOrders = [];
                        $scope.request.title = $scope.post.title;
                        $scope.request.content = $scope.post.content;
                        $scope.request.skill = "";
                        $scope.request.budgetFrom = $scope.post.budgetTo;
                        $scope.request.budgetTo = null;
                        $scope.request.bonus = null;
                        var dateString = new Date($scope.date_expired).toDateString();
                        var date = new Date();
                        var setHours = date.setHours(23, 59, 59);
                        var timeString = new Date(setHours).toTimeString();
                        var expiredDate = new Date(dateString + " " + timeString);
                        $scope.request.expiredDate = expiredDate;
                        var dataMore = {
                            "city": $scope.parentCities,
                            "information": null,
                            "position": null,
                            "workType": null,
                            "timeStart": null,
                            "timeFinish": null,
                            "dateStart": null,
                            "salary": null,
                            "diploma": null,
                            "sex": null,
                            "career": null,
                            "addonService": null
                        };

                        $scope.request.jsonData = JSON.stringify(dataMore);
                        api.post('serviceorder/v1', $scope.request).then(function (res) {
                            store.set('post');
                            if (res.data.result) {
                                $location.path("/detailpost");
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });
                    });

                };

                $scope.selectTemplate = function(data){
                    if(data != undefined){
                        $scope.post = data;
                        var date = new Date();
                        date.setDate(date.getDate() + data.date_expired);
                        $scope.date_expired = date;
                        $scope.parentCities = $scope.cities[data.city];
                        $scope.selected.category = $scope.categories[data.service];
                        if(!jQuery.isEmptyObject(data.more)){
                            data.more.dateStart =  date;
                            $scope.post.more = data.more;
                            $scope.isTemplate = true;
                            $scope.moreEnabled = true;
                        } else{
                            $scope.post.more = {};
                            $scope.moreEnabled = false;
                        }
                        $scope.checkboxRule = true;
                        if (data.id == 7) {
                            $("[data-smart-click=wizardPostMoreCompleteCallback]").css("display", "none");
                        } else {
                            $("[data-smart-click=wizardPostMoreCompleteCallback]").css("display", "block");
                        }
                    } else{
                        $scope.post = data;
                        $scope.date_expired = undefined;
                        $scope.parentCities = undefined;
                        $scope.selected.category = undefined;
                        $scope.checkboxRule = false;
                        $scope.moreEnabled = false;
                    }

                }
                function getTemplate() {
                    db.get('template.json').then(function (result) {
                        if (result && result.data && result.data.length > 0) {
                            $scope.dataTemplate = result.data;
                            if($scope.post != undefined &&  $scope.post.category != undefined){
                                var data = _.filter(result.data, function(item){
                                    return item.data.id == $scope.post.category.id
                                })
                                $scope.template = data;
                            } else{
                                $scope.template = result.data;
                            }

                        }
                    });

                }

                var loadCategories = function () {
                    db.get("work.json").then(function (result) {
                        if (result && result.data && result.data.length > 0) {
                            $scope.categories = result.data;
                        }
                        if($scope.post.indexCategory != undefined){
                            $scope.selected.category = $scope.categories[$scope.post.indexCategory];
                        }
                    });
                };
                var loadCities = function () {
                    db.get("cities.json").then(function (result) {

                        if (result && result.data && result.data.length > 0) {
                            $scope.cities = result.data;
                        }
                    });
                };
                $scope.setSelection = function (value) {
                    $scope.account.selection = value;
                    if (value === 'personal') {
                        angular.element("#personal").addClass("active");
                        angular.element("#organization").removeClass("active");
                    } else if (value === 'organization') {
                        angular.element("#personal").removeClass("active");
                        angular.element("#organization").addClass("active");
                    }
                };

                $scope.showProjects = function () {
                    var credentials = {};
                    if ($scope.username === 'customer') {
                        credentials.user_role = 'customer';
                    } else if ($scope.username === 'supplier') {
                        credentials.user_role = 'supplier';
                    }
                    credentials.user_id = "1";
                    AuthService.authenticate(credentials);
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                };
                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };
            }]);

    });
