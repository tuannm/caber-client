/**
 * Created by Admin on 5/28/2015.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.dashboard', []);
        app.useModule(module);

        module.controller('dashboard', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', 'USER_ROLES', 'AUTH_EVENTS', '$location', 'store',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, USER_ROLES, AUTH_EVENTS, $location, store) {
                $scope.panelMeta = {
                    status:"Stable",
                    description:"A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
                };
                // Set and populate defaults
                var loadUser = function () {
                    api.get('user/' + Session.id + '/v1').then(function (res) {
                        console.log("res:", res);
                        if (res.data.result) {
                            $scope.username = res.data.user.username;
                            if (res.data.user.jsonData) {
                                var dataMore = JSON.parse(res.data.user.jsonData);
                            }
                            if (!dataMore) {
                                dataMore = {};
                            }
                            console.log("dataMore", dataMore);
                            if (dataMore.avatar != undefined) {
                                $scope.myImage = dataMore.avatar;
                            } else {
                                $scope.myImage = "images/unknown.png";
                            }
                            $('#avatar').attr('src', $scope.myImage);
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };

                var loadratingUser = function () {
                    console.log("Session.id", Session.userId);
                    api.get('rating/v1?username=' + Session.userId).then(function (res) {
                        console.log("res:", res);
                        if (res.data.result) {
                            console.log("res.data.mark", res.data.mark);
                            $scope.headerRating = res.data.mark;
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };

                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    if (!Session.data) {
                        Session.data = {};
                    }

                    if (Session.data.avatar != undefined) {
                        $scope.myImage = Session.data.avatar;
                    } else {
                        $scope.myImage = "images/unknown.png";
                    }
                    $('#avatar').attr('src', $scope.myImage + '?time=' + new Date());
                });

                $scope.$on("updateAvatar", function () {
                    console.log("aaaa");
                    if (!Session.data) {
                        Session.data = {};
                    }

                    if (Session.data.avatar != undefined) {
                        $scope.myImage = Session.data.avatar;
                    } else {
                        $scope.myImage = "images/unknown.png";
                    }
                    $('#avatar').attr('src', $scope.myImage + '?time=' + new Date());

                });
                $scope.urlrecent = function () {
                    if (Session.userRole == "supplier" || Session.userRole == "broker") {
                        $location.path('detailSupplier');
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    if (Session.userRole == "customer") {
                        store.set('post');
                        $location.path('detailpost');
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                };
                $scope.urlClick = function () {
                    if (Session.userRole == "supplier" || Session.userRole == "broker") {
                        $location.path('feeds');
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    if (Session.userRole == "customer") {
                        store.set('post');
                        $location.path('post');
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                };
                $scope.urlUpdateUser = function () {
                    $location.path('updateuser');
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                };
                var _d = {
                    panels:[]
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                //loadUser();
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    loadUser();
                    loadratingUser();
                    console.log("Session.userRole", Session.userRole);
                    if (Session.userRole == "supplier" || Session.userRole == "broker") {
                        $scope.userrole = "supplier";
                        $scope.iconproject = "glyphicon glyphicon-search";
                        $scope.createproject = "search a new request";
                    }
                    if (Session.userRole == "customer") {
                        $scope.userrole = "customer";
                        $scope.iconproject = "glyphicon glyphicon-plus";
                        $scope.createproject = "create a new request";
                    }
                };
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);


    });