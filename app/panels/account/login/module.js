/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.login', []);
        app.useModule(module);

        module.controller('login', ['$scope', '$rootScope', '$element', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'ezfb', '$modal', 'api', 'logger', 'Session', 'socketServices', 'pushNotification',
            function ($scope, $rootScope, $element, $location, USER_ROLES, AuthService, AUTH_EVENTS, ezfb, $modal, api, logger, Session, socketServices, pushNotification) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };

                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown",
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);

                $scope.isShow = true;
                $scope.init = function () {
                    $scope.ready = true;
                };

                var form = $("#formLogin")
                $scope.login = function () {
                    if (!form.valid()) {
                        return;
                    }
                    $scope.isShow = false;
                };
            }]);
    });
