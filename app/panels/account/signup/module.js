/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.signup', []);
        app.useModule(module);

        module.controller('signup', ['$scope', '$rootScope', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'api', 'logger', '$modal', 'windowsize', 'store', function ($scope, $rootScope, $location, USER_ROLES, AuthService, AUTH_EVENTS, api, logger, $modal, windowsize, store) {
            $scope.panelMeta = {
                status:"Stable",
                description:"A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode:"markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content:"",
                style:{}
            };
            _.defaults($scope.panel, _d);
            $scope.slides = [];
            $scope.init = function () {
                $scope.ready = true;
                $scope.userRoles = USER_ROLES;
                $scope.isAuthorized = AuthService.isAuthorized;
                store.get("prop");
                if (store.get("prop") === true) {
                    $scope.role.supplier = true;
                }
                //$(window).on("resize", function(event){
                //    var w = $(this).width();
                //    if(w < 330)
                //    {
                //        $('#signup').flowtype({
                //            minimum: 1,
                //            maximum: 330,
                //            minFont: 10,
                //            maxFont: 14,
                //            fontRatio: 30,
                //            lineRatio: 1.45
                //        });
                //    }else{
                //        $('#signup').flowtype({
                //            minFont: 14,
                //            maxFont: 14,
                //            fontRatio: 30,
                //            lineRatio: 1.45
                //        });
                //    }
                //});
                //if (windowsize.devicetype() === 'Desktop') {
                //    $('#signup').flowtype({
                //        minFont: 14,
                //        maxFont: 14,
                //        fontRatio: 30,
                //        lineRatio: 1.45
                //    });
                //}

            };
            function openChoserole(res) {
                var _modalChoseRole;
                _modalChoseRole = $modal.open({
                    templateUrl:"app/partials/form/choseRole.html",
                    controller:'ChoseRoleCtrl',
                    resolve:{
                        res:function () {
                            return res;
                        }
                    }
                });

            }

            var modalInstance;
            $scope.newAccount = {};
            $scope.role = {};
            var form = $("#formSignup")
            $scope.signupClick = function () {
                if (!form.valid() || !$scope.role.customer && !$scope.role.supplier) {
                    return;
                }
                var credentials = {};
                var _modalChoseRole;

                $scope.newAccount.role = [];
                var signupUser = {};

                var credentials = {};
                var _modalChoseRole;

                if ($scope.role.customer) {
                    $scope.newAccount.role.push(USER_ROLES.customer);
                }
                if ($scope.role.supplier) {
                    $scope.newAccount.role.push(USER_ROLES.supplier);
                }


                signupUser.username = $scope.newAccount.username;
                signupUser.password = $scope.newAccount.password;
                signupUser.email = $scope.newAccount.email;
                signupUser.roles = $scope.newAccount.role;
                signupUser.phoneNumber = $scope.newAccount.phoneNumber;

                api.post('user/register/v1', signupUser).then(function (res) {
                    if (res.data.result) {
                        credentials.user_id = res.data.user.username;
                        credentials.session_id = res.data.user.id;
                        credentials.accessToken = res.data.accessToken;
                        if (res.data.user.jsonData) {
                            credentials.data = JSON.parse(res.data.user.jsonData);
                        }
                        if (res.data.user.roles.length > 1) {
                            openChoserole(res)
                        }
                        else {
                            _.filter(USER_ROLES, function (value, key) {
                                if (res.data.user.roles.indexOf(value) != -1) {
                                    credentials.user_role = key;
                                }
                            });
                            if (res.data.user.roles.indexOf(USER_ROLES.customer) != -1) {
                                AuthService.authenticate(credentials);
                                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                //$location.path("/post");
                                $location.path("/dashboard");
                            } else if (res.data.user.roles.indexOf(USER_ROLES.supplier) != -1) {
                                if (res.data.user.skill === null) {
                                    AuthService.authenticate(credentials);
                                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                    $location.path("/updateskills");

                                }
                                else {
                                    AuthService.authenticate(credentials);

                                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                    //$location.path("/detailSupplier");
                                    //$location.path("/feeds");
                                    $location.path("/dashboard");
                                }

                            }
                        }

                    }
                    else {
                        logger.logError(res.data.error.description);
                    }
                });
//        $location.path("/mockup/file/updateskills");
            };
        }]);

    });
