/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';
        var module = angular.module('bidproject.panels.managecv', []);
        app.useModule(module);

        module.controller('managecv', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', '$location', '$modal', 'store', 'USER_ROLES', 'navigations',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, $location, $modal, store, USER_ROLES, navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };

                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                var urlCV = "";
                var selectedParentCV = [];
                $scope.selection = [];
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.isParent = true
                    $scope.reset_panel();
                    getCV();
                    $scope.selectCV.url = new String("cv/v1?userId=" + Session.id + "")

                    getDataNavigation();
                    if ($stateParams.params != null || $stateParams.params != null) {
                        $scope.hasParam = true;
                        urlCV = $stateParams.routeId.toString() + "/" + $stateParams.params
                    }
                    else {
                        urlCV = $stateParams.routeId
                        $scope.hasParam = false;
                    }
                };

                $scope.getValueChecked = {};
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()

                });

                $scope.$on('render', function () {
                    tabResponsive(windowsize.divicetype())

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };
                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };
                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };
                $scope.selectedRow = function (value, element) {
                    var rowDetail = element.next('.row-detail');
                    var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                    var compiledContents = $compile(content);
                    compiledContents($scope, function (clone) {
                        $('ul', rowDetail).append(clone);
                    });

                };
                $scope.vaberFee = "50,000";
                $scope.price = {}
                $scope.hasCV = false;
                var isSelect = false;
                var getDataNavigation = function () {
                    console.log("navigations", navigations)
                    if (navigations.collections.length > 0) {
                        if ($stateParams.routeId == "selectcv") {
                            $scope.isParent = true
                            selectedParentCV = JSON.parse(localStorage.getItem("selectedParentCV")) || [];
                            //localStorage.removeItem("selectedParentCV");
                            //for (var i=0;i<selectedParentCV.length;i++) {
                            //    var contentCVS = JSON.parse(selectedParentCV[i].content)
                            //    $scope.selection.push()
                            //}

                            isSelect = true
                            var selections = store.get('selections') ? store.get('selections') : [];

                            if (navigations.current.selectedParentCV != undefined || navigations.current.selectedParentCV != null) {
                                $scope.selection = navigations.current.selectedParentCV;
                            }
                            else {
                                if (selections.length > 0) {
                                    $scope.selection = selections
                                }
                                else {
                                    $scope.selection = selectedParentCV;
                                }
                            }
                            var newIndex = navigations.collections.indexOf(navigations.current);
                            $scope.$on('listCV', function (event, data) {
                                if (data.cvs.length > 0) {
                                    if (windowsize.devicetype() == "Desktop") {
                                        $scope.hasCV = true;
                                    }
                                    else {
                                        navigations.pushAtIndex(newIndex, {
                                            actionLabel: "<img class='img30_30 position-img' src='images/pop.png'>",
                                            title: "select cv",
                                            path: "selectcv",
                                            params: "",
                                            isParent: true,
                                            selectedParentCV: $scope.selection,
                                            actionClick: function () {
                                                $scope.selectedCv();
                                            }
                                        });
                                    }
                                }
                                else {
                                    if (windowsize.devicetype() != "Desktop") {
                                        navigations.pushAtIndex(newIndex, {
                                            actionClick: function () {
                                                $scope.selectedCv();
                                            },
                                            title: "select cv",
                                            path: "selectcv",
                                            params: "",
                                            isParent: true,
                                            selectedParentCV: $scope.selection
                                        });
                                    }


                                }
                            });
                        }
                        else {
                            $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                            $scope.isParent = false;
                        }
                    }
                }
                var bid = $('[bid]', $element);
                var inputDetailsOrder = {};
                $scope.hideRadio = false;
                $scope.showTable = true;
                $scope.bids = {};
                $scope.proposalRes = {};
                $scope.proposalRes.types = [];
                $scope.proposalRes.budgetTo = [];
                $scope.parentCv = [];
                $scope.selectedCV = [];
                $scope.showInfo = function () {
                    var span = $("#showInfo").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-down")) {
                        $("#hideBrief").removeClass("non-display");
                        $("#viewBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-down");
                        $("#icon").addClass("glyphicon-chevron-up");
                        $("#detailInfo").slideDown();
                    } else {
                        $("#viewBrief").removeClass("non-display");
                        $("#hideBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-up");
                        $("#icon").addClass("glyphicon-chevron-down");
                        $("#detailInfo").slideUp();
                    }
                };
                $scope.$watch('price.net', function (newVal, oldVal) {
                    if (newVal != null) {
                        if (newVal.toString().length > 12) {
                            $scope.price.net = oldVal;
                        }
                    }
                });

                $scope.deleteTags = function (value) {
                    if ($scope.disable) {
                        return
                    }
                    else {
                        var index = $scope.listCVs.indexOf(value);
                        if (index > -1) {
                            $scope.listCVs.splice(index, 1);
                        }
                    }
                }
                $scope.showDetail = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        hiddenIsNotSkill.each(function () {
                            var columnValue = $(this).find(".columnValue");
                            if (columnValue.text() == "") {
                                $(this).addClass("non-display");
                            }
                        });

                        classHideMobile.addClass("non-display");

                    }

                }
                $scope.hideContent = function (index, value, element) {
                    if (windowsize.devicetype() != "Desktop") {
                        $(".body").removeClass("non-display");
                        $('[data-column = 1]').addClass("non-display");
                        var body = element.find(".body");
                        body.addClass("non-display");
                    } else {
                        if ($(window).width() > 1024) {
                            var rowDetail = element.parent("tbody").find(".row-detail");
                            var li = rowDetail.find("li");
                            li.find(".columnTitle").addClass("non-display");
                            var firstLi = li.first();
                            var lastLi = li.last();
                            var valueLastLi = lastLi.find(".columnValue");
                            valueLastLi.removeClass('col-md-10');
                            valueLastLi.addClass('col-md-2');
                            firstLi.append(valueLastLi);
                        }
                    }

                }
                $scope.isUpload = false;
                document.getElementById("customButton").addEventListener("click", function () {
                    document.getElementById("fileUpload").click();  // trigger the click of actual file upload button
                });

                document.getElementById("fileUpload").addEventListener("change", function () {
                    var fullPath = document.getElementById('fileUpload').value;
                    var fileName = fullPath.split(/(\\|\/)/g).pop();  // fetch the file name
                    document.getElementById("fileName").innerHTML = fileName;  // display the file name

                }, false);
                $timeout(function () {
                    jQuery("input#fileUpload").change(function () {
                        if (jQuery(this).val() != null) {
                            $("#isUploaded").removeClass("non-display")

                        }
                    });
                });
                var files;
                var fileReader = new FileReader();
                var contentCV = "";
                $element.on('change', function (e) {

                    files = document.querySelector('input[type=file]').files[0];
                    var reader = new FileReader();

                    reader.onloadend = function () {
                        contentCV = reader.result.split("base64,").pop()
                    }
                    if (files) {
                        reader.readAsDataURL(files);
                    }

                });
                $scope.uploadCv = function () {
                    var file = files.name.split(".");
                    var fileName = file[0];
                    var fileType = file[1];
                    if (!fileType.match("xlsx") || !fileType.match("xls")) {
                        logger.logWarning("Vui lòng chọn file excel");
                        return;
                    }
                    api.post("attachment/v1?type=cv", {
                        userId: Session.id,
                        fileName: fileName,
                        fileType: fileType,
                        content: contentCV
                    }).then(function (res) {
                        if (res.data.result) {
                            $scope.selectCV.url = new String("cv/v1?userId=" + Session.id + "");
                            logger.logSuccess("Bạn vừa đăng thành công file " + file)
                            $("#isUploaded").addClass("non-display")
                        }
                        else {
                            logger.logError(res.data.error.description);
                        }
                    });

                }
                var getCV = function () {
                    $scope.selectCV = {
                        //"url": "cv/v1?userId=" + Session.id + "",
                        "remote_data": "cvs",
                        lang: {
                            vi: {
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                //"sLengthMenu":   "Xem _MENU_ mục",
                                "sZeroRecords": "Không có CV nào được chọn",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ CV",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 CV",
                                "sInfoFiltered": "(được lọc từ _MAX_ CV)",
                                "sInfoPostFix": "",
                                //"sSearch":       "Tìm:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en: {
                                "sEmptyTable": "No data available in table",
                                "sInfo": "Showing _START_ to _END_ of _TOTAL_ CV",
                                "sInfoEmpty": "Showing 0 to 0 of 0 CV",
                                "sInfoFiltered": "(filtered from _MAX_ total CV)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ",",
                                "sLengthMenu": "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing": "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords": "No matching records found",
                                "oPaginate": {
                                    "sFirst": "First",
                                    "sLast": "Last",
                                    "sNext": "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null", width: '15%'},
                            {"data": "null", width: '10%'},
                            {"data": "null", width: '10%'},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                width: '10%',
                                "class": "text-center img",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hidden-xs hidden-sm'></div>");
                                    var content = angular.element("<div></div>");
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.image != undefined && contentCVS.image != null) {
                                        desktop.append('<span><img src="' + contentCVS.image + '" class="img64_64 img-thumbnail"></span>');
                                    }
                                    else {
                                        desktop.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    }
                                    desktop.append(content);
                                    element.append(desktop);

                                    var mobile = angular.element("<div class='hidden-lg hidden-md no-padding col-xs-12'></div>");

                                    var bodyLeft = angular.element("<div class='col-xs-12 no-padding'></div>");
                                    var bodyRight = angular.element("<div class='bodyRight pull-right col-xs-3 no-padding text-center'></div>");
                                    var info = angular.element('<div class="col-xs-10 header-mobile"></div>');
                                    var checkbox = angular.element('<div class="col-xs-2 header-mobile checkbox-mobile"></div>');
                                    var body = angular.element('<div class="col-xs-9 "></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="pull-left footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-3 position-checkbox no-padding pull-left"></div>');
                                    var img = angular.element('<div class="col-xs-3 no-padding"></div>');
                                    if (contentCVS.image != undefined && contentCVS.image != null) {
                                        img.append('<span><img src="' + contentCVS.image + '" class="img64_64 img-thumbnail"></span>');
                                    }
                                    else {
                                        img.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    }

                                    if (contentCVS.fullname != null) {
                                        info.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="full name"></span>: <span>' + contentCVS.fullname + '</span></div>');
                                    }
                                    if (contentCVS.gender != null) {
                                        info.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="gender"></span>: <span data-i18n="' + contentCVS.gender + '"></span></div>');
                                    }
                                    if (contentCVS.dob != null) {
                                        var date = new Date(contentCVS.dob);
                                        var day = "";
                                        var month = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        info.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="yob"></span>: <span>' + day + '/' + month + '/' + date.getFullYear() + '</span></div>');
                                    }
                                    if (contentCVS.yearofexperience != undefined) {
                                        if (contentCVS.yearofexperience != null) {
                                            info.append('<div class="col-xs-12 no-padding"><span class="col-md-12 col-sm-12 col-xs-12 no-padding"><span>Kinh nghiệm: </span>' + contentCVS.yearofexperience + ' <span>năm</span></span></div>');
                                        }
                                    }
                                    if (isSelect) {
                                        checkbox.append('<label class="ui-checkbox margin-checkbox"><input name="checkbox"  ng-checked="aData.isChecked"  ng-model="aData.isChecked" data-ng-click="$parent.toggleSelection(aData)" type="checkbox"><span></span></label>');
                                    }

                                    body.append(info);
                                    body.append(checkbox);
                                    mobile.append(img);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "class": "hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.gender != null) {
                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding" data-i18n="' + contentCVS.fullname + '"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "class": "hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.gender != null) {
                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding" data-i18n="' + contentCVS.gender + '"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.dob != null) {
                                        var date = new Date(contentCVS.dob);
                                        var day = "";
                                        var month = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        element.append('<span>' + day + '/' + month + '/' + date.getFullYear() + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "class": "hiddenIsNotSkill",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.introductionVO.skill != undefined) {
                                        if (contentCVS.introductionVO.skill != null) {
                                            var skill = "";
                                            for (var i = 0; i < contentCVS.introductionVO.skill.length; i++) {
                                                if (i != contentCVS.introductionVO.skill.length - 1) {
                                                    skill += contentCVS.introductionVO.skill[i].name.toString() + ", ";
                                                } else {
                                                    skill += contentCVS.introductionVO.skill[i].name.toString();
                                                }
                                            }
                                            element.append(skill);
                                        }
                                    }
                                    return element.html();
                                }
                            },

                            {
                                "targets": 5,
                                "data": null,
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.introductionVO.experienceYears != undefined) {
                                        if (contentCVS.introductionVO.experienceYears != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + contentCVS.introductionVO.experienceYears + '</span>');
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "class": "hiddenIsNotSkill",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    full.content = full.content.replace(/&quot;/g, '"');
                                    var contentCVS = JSON.parse(full.content)
                                    if (contentCVS.introductionVO.jobTarget != undefined) {
                                        if (contentCVS.introductionVO.jobTarget != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + contentCVS.introductionVO.jobTarget + '</span>');
                                        }
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "class": "text-center hide-mobile verticalMiddle",
                                "width": "15%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    full.isChecked = false;
                                    if ($scope.selection != undefined) {
                                        for (var i = 0; i < $scope.selection.length; i++) {
                                            if ($scope.selection[i].id == full.id) {
                                                full.isChecked = true;
                                                break;
                                            }
                                        }
                                    }
                                    element.append('<label class="ui-checkbox"><input name="checkbox"  ng-checked="aData.isChecked"  ng-model="aData.isChecked" data-ng-click="$parent.toggleSelection(aData)" type="checkbox"><span></span></label>');
                                    //element.append('<span ng-show="aData.status == 2" data-i18n="awarded"></span>');
                                    //element.append('<span ng-show="aData.status == 3" data-i18n="complete"></span>');
                                    //element.append('<span ng-show="aData.status == 4" data-i18n="transaction failed"></span>');
                                    //element.append('<span ng-show="aData.status == 5" data-i18n="rejected"></span>');
                                    return element.html();

                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<a  data-ng-click="$parent.editCV(aData.id)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="update"></span></a>');
                                    element.append('<a data-ng-click="$parent.deleteCV(aData,aData.id,iDataIndex)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="delete"></span></a>');
                                    return element.html();
                                }
                            },

                        ]
                    };
                    var modalInstance;
                    var rowIndex;
                    $scope.deleteCV = function (aData, cvID, index) {
                        var table = $('#selectCV').DataTable();
                        rowIndex = index
                        console.log("table", table.rows().data().length)
                        modalInstance = $modal.open({
                            templateUrl: "app/partials/form/deletecv.html",
                            controller: 'DeleteDialogCtrl',
                            resolve: {
                                dataCV: function () {
                                    return aData;
                                },
                                cvID: function () {
                                    return cvID;
                                }
                            }
                        });
                        modalInstance.result.then((function (education) {
                            //getCV();
                            //var idx = $scope.listCVs.indexOf(aData);
                            //if (idx > -1) {
                            //    $scope.listCVs.splice(idx, 1);
                            //}

                            //var table = $('#selectCV').DataTable();
                            //if (rowIndex == (table.rows().data().length - 1))
                            //$('#selectCV tbody').on('click', "#remove", function () {
                            //    table
                            //        .row($(this).parents('tr'))
                            //        .remove()
                            //        .draw();
                            //});
                            //{

                            //$('#selectCV'+rowIndex).closest('tr').remove();
                            //table.destroy();
                            //table.fnDraw();
                            //}
                            //else{
                            //$("tbody tr:eq(" + rowIndex +")").remove();
                            getCV();
                            $scope.selectCV.url = new String("cv/v1?userId=" + Session.id + "")
                            //}
                            //table.fnDraw();
                        }), function () {
                        });
                    };

                    module.controller('DeleteDialogCtrl', [
                        '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'dataCV', 'cvID',
                        function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, dataCV, cvID) {
                            var contentCVS = JSON.parse(dataCV.content)
                            $rootScope.$on('$locationChangeSuccess', function () {
                                $modalInstance.close();
                            });
                            $scope.close = function () {
                                $modalInstance.dismiss("cancel");
                            };
                            $scope.nameOfCV = contentCVS.fullname
                            $scope.deleteCv = function () {
                                api.delete('cv/' + cvID + '/v1').then(function (res) {
                                    if (res.data.result) {
                                        logger.logSuccess("Bạn vừa xóa CV thành công")

                                    }
                                    else {
                                        logger.logError(res.data.error.description);
                                    }

                                });
                                $modalInstance.close();
                            };

                            $scope.cancelDelete = function () {
                                $modalInstance.close();
                            };
                        }
                    ]);
                    var loggerShow = true;
                    $scope.formatContent = function (index, value, element) {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var td = rowDetail.find("td");

                        //console.log(td.context.cells[0])
                        //var img = td.context.cells[0].("img")
                        //img.css("cursor","pointer");
                        //img.animate({width: "500px"}, 'slow');
                        var ul = rowDetail.find("ul");
                        var li = rowDetail.find("li");
                        var lastLi = li.last();
                        var body = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                        var contentLeft = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                        var contentRight = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                        if (li != undefined && li.length > 0) {
                            li.each(function () {
                                if (!$(this).is(':last-child')) {
                                    var content = angular.element("<div class='col-md-12 col-sm-12 no-padding wrap-numberonly'></div>");
                                    var columnTitle = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                                    var columnValue = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                                    if ($(this).hasClass("hide-mobile")) {
                                        columnTitle.addClass("hide-mobile");
                                        columnValue.addClass("hide-mobile");
                                    }
                                    var title = $(this).find("span").closest(".columnTitle");
                                    title.removeClass("col-md-2");
                                    title.addClass("col-md-12");
                                    title.addClass("col-sm-12");
                                    var value = $(this).find(".columnValue");
                                    if (value.text() == "") {
                                        title.addClass("non-display");
                                        value.addClass("non-display");
                                    }
                                    value.removeClass("col-md-2");
                                    value.addClass("col-md-12");
                                    value.addClass("col-sm-12");
                                    columnTitle.append(title);
                                    columnValue.append(value);
                                    content.append(columnTitle);
                                    content.append(columnValue);
                                    contentLeft.append(content);
                                }

                            });
                            var valueLastLi = lastLi.find(".columnValue");
                            contentRight.append(valueLastLi);
                            body.append(contentLeft);
                            body.append(contentRight);
                            ul.addClass("non-display");
                            td.append(body);
                            if ($(window).width() == 768) {
                                var classHideMobile = rowDetail.find(".hide-mobile");
                                classHideMobile.addClass("non-display");
                            }
                        }
                    }

                    $scope.editCV = function (cvID) {

                        loggerShow = false;
                        store.set("selections", $scope.selection);
                        localStorage.setItem("urlCV", JSON.stringify(urlCV));
                        navigations.push({
                            title: "update cv",
                            path: "addnewcv",
                            params: cvID,
                            isParent: true,
                            selectedParentCV: $scope.selection
                        });
                    }
                    $scope.addCV = function () {
                        localStorage.setItem("urlCV", JSON.stringify(urlCV));
                        store.set("selections", $scope.selection);
                        navigations.push({
                            title: "add new cv",
                            path: "addnewcv",
                            params: "",
                            selectedParentCV: $scope.selection,
                            isParent: true
                        });
                    }
                    //$('#selectCV tbody').on('click', 'tr', function () {
                    //$('body').delegate('#selectCV tbody tr', "click", function () {
                    //
                    //    var aPos = $('#selectCV').dataTable().fnGetPosition(this);
                    //    var aData = $('#selectCV').dataTable().fnGetData(aPos);
                    //    var contentCVS = JSON.parse(aData.content)
                    //    contentCVS.id = aData.id
                    //    var i;
                    //    var found = false;
                    //    if ($scope.disable) {
                    //        return;
                    //    }
                    //    else {
                    //        if ($scope.listCVs !== undefined && $scope.listCVs.length > 0) {
                    //
                    //            for (i = 0; i < $scope.listCVs.length; i++) {
                    //                if ($scope.listCVs[i].id === aData.id) {
                    //                    found = true;
                    //                    break;
                    //                }
                    //            }
                    //        } else {
                    //            found = false;
                    //        }
                    //        if (!found) {
                    //            if ($scope.listCVs === undefined) {
                    //                $scope.listCVs = [];
                    //                $scope.listCVs.push(contentCVS);
                    //            }
                    //            else {
                    //                $scope.listCVs.push(contentCVS);
                    //            }
                    //            $scope.$apply($scope.listCVs)
                    //        }
                    //        else if (found) {
                    //            if (loggerShow) {
                    //                //logger.logWarning("Đã thêm CV này rồi");
                    //            }
                    //        }
                    //    }
                    //})
                };
                $scope.selectedCv = function () {
                    //localStorage.removeItem("selectedParentCV");
                    //localStorage.setItem("selectedParentCV", JSON.stringify($scope.selection));
                    for (var i = 0; i < $scope.selection.length; i++) {
                        if ($scope.selection[i].content != undefined) {
                            var contentCVS = JSON.parse($scope.selection[i].content)

                            $scope.selection[i].image = contentCVS.image
                            $scope.selection[i].fullname = contentCVS.fullname;
                            $scope.selection[i].goal = contentCVS.introductionVO.jobTarget;
                            $scope.selection[i].sex = contentCVS.gender;
                            $scope.selection[i].startDate = contentCVS.introductionVO.workable;
                            if (contentCVS.introductionVO.skill != null) {
                                var skill = "";
                                for (var j = 0; j < contentCVS.introductionVO.skill.length; j++) {
                                    if (j != contentCVS.introductionVO.skill.length - 1) {
                                        skill += contentCVS.introductionVO.skill[j].name.toString() + ", ";
                                    } else {
                                        skill += contentCVS.introductionVO.skill[j].name.toString();
                                    }
                                }
                                $scope.selection[i].skill = skill;
                            }
                            $scope.selection[i].yearofexperience = contentCVS.introductionVO.experienceYears
                            if (contentCVS.dob != null) {
                                var date = new Date(contentCVS.dob);
                                $scope.selection[i].dob = date.getFullYear();
                            }
                            delete $scope.selection[i].content;
                        }
                    }
                    navigations.back({
                        selectedParentCV: $scope.selection,
                        isParent: false
                    });

                }

                $scope.toggleSelection = function (aData) {
                    var idx = -1;
                    for (var i = 0; i < $scope.selection.length; i++) {
                        if ($scope.selection[i].id == aData.id) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx > -1) {
                        $scope.selection.splice(idx, 1);
                    }
                    else {
                        $scope.selection.push(aData);

                    }
                };

                $scope.cancel = function () {
                    navigations.back({
                        selectedParentCV: $scope.selection,
                        isParent: false
                    });
                }
                $scope.detailCV = function () {

                    $("#cvSelected").removeClass("non-display-mobile");
                    $("#tablecvSelected").removeClass("non-display-mobile");

                }


                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };

                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };

                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };

            }]);
    });

