/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.other', []);
        app.useModule(module);

        module.controller('other', ['$scope', '$rootScope', '$stateParams', 'db', 'AuthService', 'AUTH_EVENTS', 'windowsize', '$location', 'Session', 'api', 'logger','store',
            function ($scope, $rootScope, $stateParams, db, AuthService, AUTH_EVENTS, windowsize, $location, Session, api, logger,store) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                $scope.slides = [];
                $scope.categories = [];
                $scope.skills = [];
                $scope.currency = [];
                $scope.post = store.get('post') ? store.get('post'):{};
                $scope.selected = {};
                $scope.account = {};
                $scope.account.selection = "personal";
                $scope.selection = [];
                var username = "";
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];
                $scope.selectedSkills=[];
                $scope.init = function () {
                    $scope.ready = true;
                    loadPostMore();
                    username = Session.userId;
                    $scope.selected = store.get('selected');
                    if(angular.isUndefined($scope.post.more)){
                        $scope.post.more ={};
                        $scope.post.more.gender = 'male';
                        $scope.post.more.description = $scope.post.content;
                    }
                    if($scope.post.skill != undefined){
                        $scope.selectedSkills = $scope.post.skill;
                        $("select").val($scope.selectedSkills).trigger("change");
                    }
                    $scope.post.more.dateStart = new Date();

                };



                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    username = Session.userId;
                });

                $scope.clearSkill = function () {
                    $scope.selected.skill = undefined;
                    if($scope.parentCategory.children != undefined){
                        $scope.childrenCategory = $scope.parentCategory.children[0];
                    }

                }

                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };

                var loadPostMore = function () {
                        db.get("work.json").then(function (result) {
                           if(result.data.length >0){
                               for(var i = 0; i < result.data.length; i++){
                                   if(result.data[i].name=='Yêu cầu khác'){
                                        $scope.skills = result.data[i].skills;
                                    }
                               }
                           }
                        });
                };

                $scope.goBack = function(){
                    store.set('post',$scope.post);
                    $location.path("/post");
                }


                $scope.wizard1CompleteCallback = function (wizardData) {
                    var i;
                    var skill = [];
                    console.log("skills", $scope.selectedSkills);
                    if ($scope.selectedSkills != undefined) {
                        for (i = 0; i < $scope.selectedSkills.length; i++) {
                            skill.push($scope.selectedSkills[i].name);
                        }
                    }
                    $scope.posts.skill = skill.toString();
                    api.post('serviceorder/v1', $scope.posts).then(function (res) {
                        console.log("res", res);
                        if (res.data.result) {
                            store.set('post',{});
                            store.set('more',{});
                            $location.path("/detailpost");
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });
                };
                $scope.loadSampleDescription = function(e){
                    if(e == 1){
                        $scope.post.more.description = 'Cần 20 số điện thoại của ứng viên làm về J2EE. Ứng viên có trên 2 năm kinh nghiệm, người giới thiệu cần ghi rõ công ty nơi ứng viên làm việc, tóm tắt kinh nghiệm. Nếu ai giới thiệu được tôi sẵn sàng trả 500.000 cho yêu cầu, không cần gửi CV';
                    }else if(e == 2){
                        $scope.post.more.description = 'Tôi cần 5 CV Java, biết tiếng Anh. Tôi có thể trả 1 triệu cho ai giới thiệu được CV này';
                    }else if(e == 3){
                        $scope.post.more.description = 'Tôi cần tuyển dụng 2 ứng viên Java. Ứng viên trên 2 năm kinh nghiệm, biết về String, Strut. Ứng viên cần có tiếng Nhật. Ứng viên có mức lương từ 10-20 triệu, trải qua 2 vòng phỏng vấn. Tôi sẵn sàng trả 2 tháng lương của ứng viên cho người tuyển dụng thành công ứng viên';
                    }
                };

            }]);

    });
