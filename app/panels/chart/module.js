/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.chart', []);
        app.useModule(module);

        module.controller('chart', ['$scope', '$rootScope', '$q', '$stateParams', 'api', 'logger', 'USER_ROLES', 'Session',
            function ($scope, $rootScope, $q, $stateParams, api, logger, USER_ROLES, Session) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown", // 'html','markdown','text'
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                $scope.line1 = {};
                $scope.line1.data = [];
                $scope.line1.options = {
                    series: {
                        lines: {
                            show: true,
                            fill: true,
                            fillColor: {
                                colors: [
                                    {
                                        opacity: 0
                                    }, {
                                        opacity: 0.3
                                    }
                                ]
                            }
                        },
                        points: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: "#ffffff",
                            symbol: "circle",
                            radius: 5
                        }
                    },
                    colors: ["#31C0BE", "#8170CA", "#E87352"],
                    tooltip: true,
                    tooltipOpts: {
                        defaultTheme: false
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#f9f9f9",
                        borderWidth: 1,
                        borderColor: "#eeeeee"
                    },
                    xaxis: {
                        ticks: [[1, 'Jan.'], [2, 'Feb.'], [3, 'Mar.'], [4, 'Apr.'], [5, 'May'], [6, 'June'], [7, 'July'], [8, 'Aug.'], [9, 'Sept.'], [10, 'Oct.'], [11, 'Nov.'], [12, 'Dec.']]
                    }
                };
                $scope.pie = {};
                $scope.pie.data = [];
                $scope.pie.options = {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                formatter: function (label, series) {
                                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                                },
                                background: {
                                    opacity: 0.5
                                }
                            }
                        }
                    },
                    legend: {
                        show: false
                    }
                };

                var count;
                var status4;
                var status7;
                var status8;
                var month1;
                var month2;
                var month3;
                var month4;
                var month5;
                var month6;
                var month7;
                var month8;
                var month9;
                var month10;
                var month11;
                var month12;
                var getAllOrderForCustomer = function () {
                    var deferred = $q.defer();
                    api.get('serviceorder/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole] + '&sostatus=0,1,2,3,4,5,6,7,8&page=0&pagesize=1000').then(function (res) {
                        if (res.data.result) {
                            console.log("Order: ", res.data);
                            if (res.data.serviceOrders.length > 0) {
                                deferred.resolve(res.data.serviceOrders);
                                month1 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 1;
                                }).length;
                                month2 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 2;
                                }).length;
                                month3 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 3;
                                }).length;
                                month4 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 4;
                                }).length;
                                month5 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 5;
                                }).length;
                                month6 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 6;
                                }).length;
                                month7 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 7;
                                }).length;
                                month8 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 8;
                                }).length;
                                month9 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 9;
                                }).length;
                                month10 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 10;
                                }).length;
                                month11 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 11;
                                }).length;
                                month12 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 12;
                                }).length;
                                console.log("month1: ", month1);
                                console.log("month2: ", month2);
                                console.log("month3: ", month3);
                                console.log("month4: ", month4);
                                console.log("month5: ", month5);
                                console.log("month6: ", month6);
                                console.log("month7: ", month7);
                                console.log("month8: ", month8);
                                console.log("month9: ", month9);
                                console.log("month10: ", month10);
                                console.log("month11: ", month11);
                                console.log("month12: ", month12);
                                count = res.data.serviceOrders.length;
                                console.log("count all status: ", count);
                                status4 = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status == 4;
                                }).length;
                                console.log("status4(tu choi): ", status4);
                                status7 = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status == 7;
                                }).length;
                                console.log("status7(thanh cong): ", status7);
                                if (status4 == 0 && status7 == 0) {
                                    $('#datachart').addClass('col-md-12');
                                    $('#piechart').addClass('hide');
                                } else {
                                    $('#datachart').addClass('col-md-6');
                                }
                            }
                            if (res.data.serviceOrders.length == 0) {
                                $('#chart').addClass('hide');
                            }
                        }
                        else {
                            deferred.reject();
                            logger.logError(res.data.error.description);
                        }
                    });
                    return deferred.promise;
                }

                var getAllOrderForSupplier = function () {
                    var deferred = $q.defer();
                    api.get('serviceorder/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole] + '&pstatus=3&page=0&pagesize=1000').then(function (res) {
                        console.log('serviceorder/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole] + '&pstatus=3&page=0&pagesize=1000');
                        if (res.data.result) {
                            console.log("Order: ", res.data);
                            if (res.data.serviceOrders.length > 0) {
                                deferred.resolve(res.data.serviceOrders);
                                count = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status != 1;
                                }).length;
                                console.log("count all status: ", count);
                                month1 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 1 && n.status != 1;
                                }).length;
                                month2 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 2 && n.status != 1;
                                }).length;
                                month3 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 3 && n.status != 1;
                                }).length;
                                month4 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 4 && n.status != 1;
                                }).length;
                                month5 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 5 && n.status != 1;
                                }).length;
                                month6 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 6 && n.status != 1;
                                }).length;
                                month7 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 7 && n.status != 1;
                                }).length;
                                month8 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 8 && n.status != 1;
                                }).length;
                                month9 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 9 && n.status != 1;
                                }).length;
                                month10 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 10 && n.status != 1;
                                }).length;
                                month11 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 11 && n.status != 1;
                                }).length;
                                month12 = _.filter(res.data.serviceOrders, function (n) {
                                    return new Date(n.createdDate).getMonth() + 1 == 12 && n.status != 1;
                                }).length;
                                console.log("month1: ", month1);
                                console.log("month2: ", month2);
                                console.log("month3: ", month3);
                                console.log("month4: ", month4);
                                console.log("month5: ", month5);
                                console.log("month6: ", month6);
                                console.log("month7: ", month7);
                                console.log("month8: ", month8);
                                console.log("month9: ", month9);
                                console.log("month10: ", month10);
                                console.log("month11: ", month11);
                                console.log("month12: ", month12);
                                status4 = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status == 4;
                                }).length;
                                console.log("status4(tu choi): ", status4);
                                status7 = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status == 7;
                                }).length;
                                console.log("status7(thanh cong): ", status7);
                                status8 = _.filter(res.data.serviceOrders, function (n) {
                                    return n.status == 8;
                                }).length;
                                console.log("status8(bi tu choi): ", status8);
                                if (status4 == 0 && status7 == 0 && status8 == 0) {
                                    $('#datachart').addClass('col-md-12');
                                    $('#piechart').addClass('hide');
                                } else {
                                    $('#datachart').addClass('col-md-6');
                                }
                            }
                            if (res.data.serviceOrders.length == 0) {
                                $('#chart').addClass('hide');
                            }
                        }
                        else {
                            deferred.reject();
                            logger.logError(res.data.error.description);
                        }
                    });
                    return deferred.promise;
                }

                $scope.init = function () {
                    console.log("USER_ROLES[Session.userRole]", USER_ROLES[Session.userRole]);
                    if (USER_ROLES[Session.userRole] == "ROLE_SUPPLIER") {
                        getAllOrderForSupplier().then(function (data) {
                            console.log("data", data);
                            var lineChart1 = {};
                            lineChart1.data1 = [[1, month1], [2, month2], [3, month3], [4, month4], [5, month5], [6, month6], [7, month7], [8, month8], [9, month9], [10, month10], [11, month11], [12, month12]];
                            $scope.line1.data = [
                                {
                                    data: lineChart1.data1,
                                    label: ' Tổng số yêu cầu'
                                }
                            ];
                            var data = [{
                                label: "Từ chối",
                                data: status4 * 100 / count
                            },
                                {
                                    label: "Thành công",
                                    data: status7 * 100 / count
                                },
                                {
                                    label: "Bị từ chối",
                                    data: status8 * 100 / count
                                }]
                            console.log("data", data);
                            console.log("==> % bi tu choi", status8 * 100 / count);
                            console.log("==> % tu choi", status4 * 100 / count);
                            console.log("==> % thanh cong", status7 * 100 / count);
                            $scope.pie.data = data;
                        });
                    }
                    if (USER_ROLES[Session.userRole] == "ROLE_CUSTOMER") {
                        getAllOrderForCustomer().then(function (data) {
                            console.log("data", data);
                            var lineChart1 = {};
                            lineChart1.data1 = [[1, month1], [2, month2], [3, month3], [4, month4], [5, month5], [6, month6], [7, month7], [8, month8], [9, month9], [10, month10], [11, month11], [12, month12]];
                            $scope.line1.data = [
                                {
                                    data: lineChart1.data1,
                                    label: ' Tổng số yêu cầu'
                                }
                            ];
                            var data = [{
                                label: "Từ chối",
                                data: status4 * 100 / count
                            },
                                {
                                    label: "Thành công",
                                    data: status7 * 100 / count
                                }]
                            console.log("data", data);
                            console.log("==> % tu choi", status4 * 100 / count);
                            console.log("==> % thanh cong", status7 * 100 / count);
                            $scope.pie.data = data;

                        });
                    }
                }
            }]);

    });
