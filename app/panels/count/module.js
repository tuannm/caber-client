/**
 * Created by Admin on 5/4/2015.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'css!./module.css'
],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.count', []);
        app.useModule(module);

        module.controller('count', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger) {
                $scope.panelMeta = {
                    status:"Stable",
                    description:"A pseudo panel that lets you add other panels to be arranged in a column with" +
                        "defined heights."
                };
                // Set and populate defaults

                var _d = {
                    panels:[]
                };
                var loadCount = function () {
                    api.get('count/totals/v1').then(function (res) {
                        console.log("res:", res);
                        if (res.data.result) {
                            $scope.TotalUsers = res.data.totalUsers.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $scope.TotalOrders = res.data.totalOrders.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $scope.TotalFinishOrders = res.data.totalFinishOrders.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    loadCount();
                };
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);
    });