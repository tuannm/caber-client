/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    'angular',
    'app',
    'lodash',
    'require',
    'config',
    'css!./module.css'
],
    function (angular, app, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.updateuser', []);
        app.useModule(module);

        module.controller('updateuser', ['$scope', '$rootScope', 'USER_ROLES', 'AuthService', 'db', '$stateParams', 'api', 'Session', 'logger', 'AUTH_EVENTS', 'navigations', 'windowsize', function ($scope, $rootScope, USER_ROLES, AuthService, db, $stateParams, api, Session, logger, AUTH_EVENTS, navigations, windowsize) {
            $scope.panelMeta = {
                status:"Stable",
                description:"A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };
            $scope.checkcustomer = false;
            $scope.checksupllier = false;
            $scope.accountdetails = {};
            $scope.role = {};
            var credentials = {};
            var rolecurrent = {};
            var fileUrl = "";
            console.log("config.js", config)
            function convertImgToBase64(url, callback, outputFormat) {
                var img = new Image();
                img.crossOrigin = 'Anonymous';
                img.onload = function () {
                    var canvas = document.createElement('CANVAS');
                    var ctx = canvas.getContext('2d');
                    canvas.height = this.height;
                    canvas.width = this.width;
                    ctx.drawImage(this, 0, 0);
                    var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                    callback(dataURL);
                    canvas = null;
                };
                img.src = url;
            }

            var loadUser = function () {
                api.get('user/' + Session.id + '/v1').then(function (res) {
                    if (res.data.result) {
                        $scope.accountdetails.fullname = res.data.user.fullname;
                        $scope.accountdetails.email = res.data.user.email;
                        $scope.accountdetails.address = res.data.user.address;
                        $scope.accountdetails.phonenumber = res.data.user.phoneNumber;
                        $scope.accountdetails.company = res.data.user.organization;

                        if (res.data.user.jsonData) {
                            var dataMore = JSON.parse(res.data.user.jsonData);
                        }
                        if (!dataMore) {
                            dataMore = {};
                        }
                        if (dataMore.avatar != undefined) {
                            $(".image-crop-final").attr("src", dataMore.avatar);
                            fileUrl = dataMore.avatar;
                        } else {
                            $(".image-crop-final").attr("src", "images/unknown.png");
                            fileUrl = 'images/unknown.png';
                        }

                        if (res.data.user.roles.indexOf(USER_ROLES.supplier) != -1) {
                            $("#supplier").prop("checked", true);
                            $scope.role.supplier = true;

                            $scope.checksupllier = true;
                        }
                        if (res.data.user.roles.indexOf(USER_ROLES.customer) != -1) {
                            $("#customer").prop("checked", true);
                            $scope.role.customer = true;

                            $scope.checkcustomer = true;
                        }
                        rolecurrent = res.data.user.roles;
                    } else {
                        logger.logError(res.data.error.description);
                    }
                })
            };
            $scope.chooseImage = function () {
                if ($scope.cropper.croppedImage != null) {
                    console.log("choose");
                    $('#canvasImg').removeClass('hide');
                    $('#imgFinal').addClass('hide');
                }
            }
            $scope.saveClick = function () {
                $('#canvasImg').addClass('hide');
                $('#imgFinal').removeClass('hide');
                var list = {};
                list.fullname = $scope.accountdetails.fullname;
                list.address = $scope.accountdetails.address;
                list.phoneNumber = $scope.accountdetails.phonenumber;
                list.organization = $scope.accountdetails.company;
                list.email = $scope.accountdetails.email;

                if ($scope.accountdetails.phonenumber != null) {
                    if ($scope.cropper.croppedImage != null) {
                        api.post("attachment/v1", {
                            userId:Session.id,
                            fileName:Session.userId,
                            fileType:"png",
                            content:$scope.cropper.croppedImage.split("base64,").pop()
                        }, false).then(function (res) {
                                if (res.data.result) {
                                    $scope.myImage = config.resources + res.data.attachment.fileUrl.split("cber-resource/resources/").pop();
                                    if (!Session.data) {
                                        Session.data = {};
                                    }
                                    Session.data.avatar = $scope.myImage;
                                    credentials.user_id = Session.userId;
                                    credentials.session_id = Session.id;
                                    credentials.user_role = Session.userRole;
                                    credentials.multi = Session.multi;
                                    credentials.accessToken = Session.accessToken;
                                    credentials.data = Session.data;
                                    AuthService.authenticate(credentials);
                                    $rootScope.$broadcast("updateAvatar");
                                    var jsonData = {
                                        "avatar":$scope.myImage
                                    };
                                    list.jsonData = JSON.stringify(jsonData);
                                    api.put('user/' + Session.id + '/v1', list).then(function (res) {
                                        if (res.data.result) {
                                        }
                                    })
                                }
                            });
                    } else {
                        var jsonData = {
                            "avatar":fileUrl
                        };
                        list.jsonData = JSON.stringify(jsonData);
                    }
                    api.put('user/' + Session.id + '/v1', list).then(function (res) {
                        if (res.data.result) {
                            logger.logSuccess("Bạn đã lưu thành công!");
                            $('#notifiDL').addClass('hide');
                            $('#notifiSDD').addClass('hide');
                            $('#notifiSuccess').addClass('hide');
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                }
            }
            var accountSettingsForm = $("#accountSettingsForm");
            $scope.saveSettingClick = function () {
                if (!accountSettingsForm.valid()) {
                    return;
                }
                $scope.accountdetails.role = [];
                var list = {};
                if ($scope.role.customer) {
                    $scope.accountdetails.role.push(USER_ROLES.customer);
                }
                if ($scope.role.supplier) {
                    $scope.accountdetails.role.push(USER_ROLES.supplier);
                }


                var jsonData = {
                    "avatar":fileUrl
                };
                list.jsonData = JSON.stringify(jsonData);
                if ($scope.accountdetails.role.length == 0) {
                    logger.logError("Chọn loại tài khoản !");
                    return;
                }

                list.password = $scope.accountdetails.newpassword;
                list.oldPassword = $scope.accountdetails.oldPassword;
                list.roles = $scope.accountdetails.role;

                if (list.password != undefined && list.oldPassword == undefined) {
                    logger.logError("Vui lòng điền đầy đủ thông tin");
                    return;
                } else if (list.password == undefined && list.oldPassword != undefined) {
                    logger.logError("Vui lòng điền đầy đủ thông tin");
                    return;
                } else if (list.password != "" && list.oldPassword == "") {
                    logger.logError("Vui lòng điền đầy đủ thông tin");
                    return;
                } else if (list.password == "" && list.oldPassword != "") {
                    logger.logError("Vui lòng điền đầy đủ thông tin");
                    return;
                } else {
                    api.put('user/' + Session.id + '/v1', list).then(function (res) {
                        if (res.data.result) {
                            if ($scope.accountdetails.role.length == 2) {
                                $("#btn-switchRole").removeClass("non-display");
                                if (!Session.data) {
                                    Session.data = {};
                                }
                                Session.data.avatar = fileUrl;
                                credentials.user_id = Session.userId;
                                credentials.session_id = Session.id;
                                credentials.user_role = Session.userRole;
                                credentials.multi = 1;
                                credentials.accessToken = Session.accessToken;
                                credentials.data = Session.data;
                                AuthService.authenticate(credentials);
//                                $rootScope.$broadcast("updateAvatar");
                            }
                            logger.logSuccess("Bạn đã lưu thành công!");
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                }
            }
            if (Session.userRole == "broker") {
                $("#accountType").addClass("hide");
            }
            if (windowsize.devicetype() != 'Desktop') {
                $("#home").removeClass("tab-pane");
                $("#info").removeClass("tab-pane");
                $("#home").removeClass("active");
                $("#info").removeClass("active");
                $("#tabcontent").removeClass("tab-content");
            }
            $scope.$on("window.size", function (object, type) {
                if (windowsize.devicetype() != 'Desktop') {
                    $("#home").removeClass("tab-pane");
                    $("#info").removeClass("tab-pane");
                    $("#home").removeClass("active");
                    $("#info").removeClass("active");
                    $("#tabcontent").removeClass("tab-content");
                } else {
                    $("#home").addClass("tab-pane");
                    $("#info").addClass("tab-pane");
                    $("#tabcontent").addClass("tab-content");
                    $("#home").addClass("active");
                }
            });
            $scope.showHome = function () {
                if ($("#home").hasClass("hidden-xs")) {
                    $("#home").removeClass("hidden-xs");
                    $("#info").addClass("hidden-xs");
                } else {
                    $("#home").addClass("hidden-xs");
                }

            }

            $scope.showInfo = function () {
                if ($("#info").hasClass("hidden-xs")) {
                    $("#info").removeClass("hidden-xs");
                    $("#home").addClass("hidden-xs");
                } else {
                    $("#info").addClass("hidden-xs");
                }

            }

            $scope.updateAccountClick = function () {
            }
            $scope.updateEmailClick = function () {
            }
            var loadCountries = function () {
                db.get("countries.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.countries = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentCountries = $scope.countries[$stateParams.params - 1];
                    } else {
                        $scope.parentCountries = $scope.countries[235];
                    }
                });
            };

            var loadTimezones = function () {
                db.get("timezones.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.timezones = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentTimezones = $scope.timezones[$stateParams.params - 1];
                    } else {
                        $scope.parentTimezones = $scope.timezones[75];
                    }
                });
            };

            var loadCurrency = function () {
                api.get('currency/v1').then(function (res) {
                    if (res.data.result) {
                        $scope.currency = res.data.currencies;
                    } else {
                        logger.logError(res.data.error.description);
                    }
                    $scope.parentCurrency = $scope.currency[100];

                });
            };

            var loadLanguage = function () {
                db.get("language.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.language = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentLanguage = $scope.language[$stateParams.params - 1];
                    } else {
                        $scope.parentLanguage = $scope.language[79];
                    }
                });
            };

            var loadEmailfrequency = function () {
                db.get("emailfrequency.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailfrequency = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailfrequency = $scope.emailfrequency[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailfrequency = $scope.emailfrequency[0];
                    }
                });
            };
            var loadEmailfrequencyactivity = function () {
                db.get("emailfrequencyactivity.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailfrequencyactivity = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailfrequencyactivity = $scope.emailfrequencyactivity[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailfrequencyactivity = $scope.emailfrequencyactivity[0];
                    }
                });
            };
            var loadEmailformat = function () {
                db.get("emailformat.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.emailformat = result.data;
                    }
                    if ($stateParams.params) {
                        $scope.parentEmailformat = $scope.emailformat[$stateParams.params - 1];
                    } else {
                        $scope.parentEmailformat = $scope.emailformat[0];
                    }
                });
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode:"markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content:"",
                style:{}
            };
            _.defaults($scope.panel, _d);
            $scope.slides = [];
            $scope.countries = [];
            $scope.timezones = [];
            $scope.currency = [];
            $scope.language = [];
            $scope.emailfrequency = [];
            $scope.emailfrequencyactivity = [];
            $scope.emailformat = [];
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            $scope.init = function () {
                $scope.ready = true;
                $scope.userRoles = USER_ROLES;
                $scope.isAuthorized = AuthService.isAuthorized;
                loadCountries();
                loadTimezones();
                loadCurrency();
                loadLanguage();
                loadEmailfrequency();
                loadEmailfrequencyactivity();
                loadEmailformat();
                loadUser();

            };
            var imageFile;
        }]);

    });

