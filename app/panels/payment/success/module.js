/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.success', []);
        app.useModule(module);

        module.controller('success', ['$scope', '$rootScope', '$element', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'ezfb', '$modal', 'api', 'logger', 'Session', '$stateParams','navigations',
            function ($scope, $rootScope, $element, $location, USER_ROLES, AuthService, AUTH_EVENTS, ezfb, $modal, api, logger, Session, $stateParams,navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };

                var _d = {
                    /** @scratch /panels/text/5
                     * === Parameters
                     *
                     * mode:: `html', `markdown' or `text'
                     */
                    mode: "markdown",
                    /** @scratch /panels/text/5
                     * content:: The content of your panel, written in the mark up specified in +mode+
                     */
                    content: "",
                    style: {}
                };
                _.defaults($scope.panel, _d);
                $scope.init = function () {
                    $scope.ready = true;
                    loadPayment();
                    $scope.oneAtATime = true;
                    if(navigations.current.isClick != undefined && navigations.current.isClick){
                        $(".ng-back").removeClass("non-display");
                    }
                };

                function loadPayment() {
                    if ($stateParams.params != undefined) {
                        api.get('payment/' + $stateParams.params + '/v1').then(function (res) {
                            if (res.data.result) {
                                $scope.transactionID = res.data.payment.transactionId;
                                if (res.data.payment.status === 0) {
                                    if ($(".wait-payment").hasClass("non-display")) {
                                        $(".wait-payment").removeClass("non-display")
                                    }
                                }
                                if (res.data.payment.status === 1) {
                                    if ($(".paid").hasClass("non-display")) {
                                        $(".paid").removeClass("non-display")
                                    }
                                }
                                if (res.data.payment.status === 2) {
                                    if ($(".failed-paid").hasClass("non-display")) {
                                        $(".failed-paid").removeClass("non-display")
                                    }
                                }
                                if (res.data.payment.type == "AO") {
                                    $scope.description = "Thanh toán cho dịch vụ nâng cao";
                                } else if (res.data.payment.type == "SO") {
                                    $scope.description = "Thanh toán cho yêu cầu " + res.data.payment.serviceOrder.id;
                                } else if (res.data.payment.type == "PP") {
                                    if ($(".payment-service-order").hasClass("non-display")) {
                                        $(".payment-service-order").removeClass("non-display");
                                    }
                                    $scope.postID = res.data.payment.serviceOrder.id;
                                    $scope.poster = res.data.payment.serviceOrder.aliasName;
                                    $scope.titleOrder = res.data.payment.serviceOrder.title;
                                    $scope.description = "Thanh toán cho yêu cầu "+ res.data.payment.serviceOrder.id;
                                }
                                $scope.paymentID = res.data.payment.id;
                                $scope.totalPayment = res.data.payment.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                if (res.data.payment.type == "AO") {
                                    if ($(".add-on-service").hasClass("non-display")) {
                                        $(".add-on-service").removeClass("non-display");
                                    }
                                    api.get('serviceorder/' + res.data.payment.serviceOrder.id + '/v1').then(function (res) {
                                        if (res.data.serviceOrder.jsonData != null) {
                                            var dataMore = JSON.parse(res.data.serviceOrder.jsonData);
                                            var removeValFromIndex = [];
                                            for (var i = 0; i < dataMore.addonService.length; i++) {
                                                if (!dataMore.addonService[i].isSelected) {
                                                    removeValFromIndex.push(i);
                                                }
                                            }
                                            var resultData = $.grep(dataMore.addonService, function (n, i) {
                                                return $.inArray(i, removeValFromIndex) == -1;
                                            });

                                            $scope.custom = {
                                                "aaData": resultData,
                                                "paging": false,
                                                "columns": [
                                                    {"data": "null"},
                                                    {"data": "null"},
                                                    {"data": "null"},
                                                    {"data": "null"},
                                                    {"data": "null"}
                                                ],
                                                "columnDefs": [
                                                    {
                                                        "targets": 0,
                                                        "data": null,
                                                        "width": "5%",
                                                        "class": "name",
                                                        "render": function (data, type, full, meta) {
                                                            var element = angular.element("<div></div>");
                                                            var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                                            desktop.append('<span class="label label-warning">' + full.name + '</span>')
                                                            var mobile = angular.element("<div class='hide-when-desktop text-left padding-left-4'></div>");
                                                            var header = angular.element('<div class="header-mobile col-xs-12 no-padding"></div>');
                                                            var body = angular.element('<div class="body"></div>');
                                                            var content = angular.element('<div class="content-mobile"></div>');
                                                            var pullLeft = angular.element('<div class="col-xs-12 no-padding"></div>');
                                                            //var pullRight = angular.element('<div class="col-xs-3 no-padding footer-pull-right-mobile"></div>');
                                                            header.append('<span data-i18n="kind"></span>:  <span>' + full.name + '</span>');
                                                            content.append('<span>' + full.description + '</span>');
                                                            var formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                            pullLeft.append('<div class="col-xs-12 no-padding"><span data-i18n="price"></span>: <span class="price">' + formatServiceCost + '</span></div>');
                                                            pullLeft.append('<div class="col-xs-12 no-padding"><span data-i18n="quantity"></span>: <span>' + full.quantity + '</span></div>');
                                                            var totalPayment = 0;
                                                            if (full.isSelected && full.quantity != null) {
                                                                totalPayment += parseInt(full.quantity * full.serviceCost);
                                                            }
                                                            pullLeft.append('<div class="col-xs-12 no-padding"><span data-i18n="total"></span>: <span>' + totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫</span></div>');
                                                            body.append(pullLeft);
                                                            // body.append(pullRight);
                                                            mobile.append(header);
                                                            mobile.append(content);
                                                            mobile.append(body);
                                                            element.append(desktop);
                                                            element.append(mobile);
                                                            return element.html();
                                                        }
                                                    },
                                                    {
                                                        "targets": 1,
                                                        "data": null,
                                                        "width": "40%",
                                                        "render": function (data, type, full, meta) {
                                                            var element = angular.element("<div></div>");
                                                            element.append('<span>' + full.description + '</span>');
                                                            return element.html();
                                                        }
                                                    },
                                                    {
                                                        "targets": 2,
                                                        "data": null,
                                                        "width": "15%",
                                                        "class":"text-right",
                                                        "render": function (data, type, full, meta) {
                                                            var element = angular.element("<div></div>");
                                                            var formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                            var formatServiceCostSalse = "";
                                                            if(full.serviceOldCost != null){
                                                                formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                            }
                                                            element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                                            return element.html();
                                                        }
                                                    },
                                                    {
                                                        "targets": 3,
                                                        "data": null,
                                                        "width": "15%",
                                                        "class": "quantity",
                                                        "render": function (data, type, full, meta) {
                                                            var element = angular.element("<div></div>");
                                                            element.append('<span data-ng-show="aData.quantity == null">0</span>');
                                                            element.append('<span data-ng-show="aData.quantity != null">' + full.quantity + '</span>');
                                                            return element.html();
                                                        }
                                                    },
                                                    {
                                                        "targets": 4,
                                                        "data": null,
                                                        "width": "15%",
                                                        "class":"text-right",
                                                        "render": function (data, type, full, meta) {
                                                            var element = angular.element("<div></div>");
                                                            var totalPayment = 0;
                                                            if (full.isSelected && full.quantity != null) {
                                                                totalPayment += parseInt(full.quantity * full.serviceCost);
                                                            }
                                                            element.append('<span>'+ totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫</span>');
                                                            return element.html();
                                                        }
                                                    }
                                                ]
                                            };
                                        }

                                    });


                                }
                                if (res.data.payment.proposal.length > 0 && res.data.payment.type == "SO") {
                                    if ($(".proposal-award").hasClass("non-display")) {
                                        $(".proposal-award").removeClass("non-display");
                                    }
                                    $scope.proposalsAward = {
                                        "aaData": res.data.payment.proposal,
                                        "paging": false,
                                        "columns": [
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"}
                                        ],
                                        "columnDefs": [
                                            {
                                                "targets": 0,
                                                "data": null,
                                                "width": "15%",
                                                "class": "text-center",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                                    desktop.append('<span> ' + full.id + '</span>')
                                                    var mobile = angular.element("<div class='hide-when-desktop text-left padding-left-4'></div>");
                                                    var header = angular.element('<div class="header-mobile col-xs-12 no-padding"></div>');
                                                    var body = angular.element('<div class="body"></div>');
                                                    var content = angular.element('<div class="content-mobile"></div>');
                                                    var pullLeft = angular.element('<div class="col-xs-12 no-padding"></div>');
                                                    //var pullRight = angular.element('<div class="col-xs-3 no-padding footer-pull-right-mobile"></div>');
                                                    header.append('<span>' + full.id + ' - ' + full.aliasName + '</span>');
                                                    content.append('<span>' + full.proposal + '</span>');
                                                    pullLeft.append('<span>' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                                    body.append(pullLeft);
                                                    // body.append(pullRight);
                                                    mobile.append(header);
                                                    mobile.append(content);
                                                    mobile.append(body);
                                                    element.append(desktop);
                                                    element.append(mobile);
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 1,
                                                "data": null,
                                                "width": "25%",
                                                "class": "text-center",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span>' + full.aliasName + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 2,
                                                "data": null,
                                                "width": "30%",
                                                "class": "text-center",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span>' + full.proposal + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 3,
                                                "data": null,
                                                "width": "20%",
                                                "class": "text-center",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span>' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                                    return element.html();
                                                }
                                            }
                                        ]
                                    };
                                }


                            } else {
                                logger.logError(res.data.error.description);
                            }

                        });
                    }

                }

            }]);
    });
