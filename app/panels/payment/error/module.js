/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.error', []);
        app.useModule(module);

        module.controller('error', ['$scope', '$rootScope', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'api', 'logger', '$modal', function ($scope, $rootScope, $location, USER_ROLES, AuthService, AUTH_EVENTS, api, logger, $modal) {
            $scope.panelMeta = {
                status: "Stable",
                description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode: "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content: "",
                style: {}
            };
            _.defaults($scope.panel, _d);
            $scope.slides = [];
            $scope.init = function () {
                $scope.ready = true;
                $scope.userRoles = USER_ROLES;
                $scope.isAuthorized = AuthService.isAuthorized;
                $scope.isAgree = true;
                $scope.register = {
                    username: "tiennm2",
                    email: "tiennm2@123.com",
                    password: "123456",
                    type: 1
                }


            };

            $scope.$watch("reference_code", function(newVal, oldVal){
                console.log(newVal, oldVal);
                if(newVal && newVal.length == 8 && oldVal != newVal){
                    api.get('oauth/user/reference/'+ $scope.reference_code).then(function (res) {
                        console.log("reference", res);
                        if (res.data) {
                            $scope.register.reference_id = res.data.id;
                            $scope.reference_info = res.data.meta.fullname || res.data.username;
                        }
                    });
                }
            })

            $scope.registerClick = function () {
                console.log($scope.register);
                if(!$scope.isReference){
                    delete $scope.register.reference_id;
                }
                api.post('/oauth/user', $scope.register).then(function (res) {
                    if (res.data) {
                        $location.path("login");
                    }
                    else {
                        logger.logError(res.data.error.description);
                    }
                });
            };
        }]);

    });
