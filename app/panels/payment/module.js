define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.payment', []);
        app.useModule(module);

        module.controller('payment', ['$scope', '$rootScope', 'db', '$modal', '$stateParams', 'api', 'logger', 'Session', 'USER_ROLES','navigations',
            function ($scope, $rootScope, db, $modal, $stateParams, api, logger, Session, USER_ROLES,navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };

                $scope.url = "http://118.70.74.174/services/payment/";

                $scope.bank_id = false;
                $scope.allbanks = [];
                $scope.slides = [];
                $scope.categories = [];
                $scope.skills = [];
                $scope.currency = [];
                $scope.init = function () {
                    $scope.ready = true;
                    loadBanks();
                    getUserInfo();
                    getDetailService();
                    console.log("navigations",navigations);
                };
                $scope.payment = "0";
                $scope.account = {};
                $scope.account.selection = "personal";
                var loadBanks = function () {
                    api.get("http://118.70.74.174/services/oauth/baokim/banks").then(function (result) {
                        if(result.data){
                            $scope.allbanks = result.data;
                        }else{
                            logger.logError(result.data.error.description);
                        }
                    });
                };

                //var loadCurrency = function () {
                //    api.get('currency/v1').then(function (res) {
                //        if (res.data.result) {
                //            $scope.currency = res.data.currencies;
                //        } else {
                //            logger.logError(res.data.error.description);
                //        }
                //        $scope.parentCurrency = $scope.currency[100];
                //
                //    });
                //};
                var getUserInfo = function () {
                    console.log("session", Session)
                    api.get('user/' + Session.id + '/v1').then(function (res) {
                        if (res.data.result) {

                            $scope.payer_name = res.data.user.fullname;
                            $scope.payer_phone_no = res.data.user.phoneNumber;
                            $scope.payer_email = res.data.user.email;
                            $scope.payer_address = res.data.user.address;

                        }
                    });
                };
                var currencyName = '';
                var getDetailService = function () {

                    api.get('serviceorder/' + $stateParams.params + '/v1').then(function (res) {
                        if (res.data.result) {
                            infoTransaction.title = res.data.serviceOrder.title;

                            var data  =  _.filter(res.data.serviceOrder.proposals, function(proposal){
                                return proposal.status == 2;
                            })
                            console.log("data",data);
                            if (data[0].currency === null)
                            {
                                $scope.total_amount  = data[0].expectedCost

                            }
                            else
                            {

                                $scope.total_amount  = data[0].expectedCost + data[0].currency.name ;
                                currencyName = data[0].currency.name;
                            }
                        }

                    else {
                        logger.logError(res.data.error.description);
                    }

                    });
                };

                $scope.ChooseBank = function(bank_id, event){
                    $("img.img-active").removeClass("img-active");
                    $(event.currentTarget).addClass('img-active');
                    $scope.bank_id = bank_id;
                };

                $scope.selectPaymentBank = function (payment_method_type) {
                    $scope.banks = [];
                    angular.forEach($scope.allbanks, function (item) {
                        if (item.payment_method_type == payment_method_type) {
                            $scope.banks.push(item);
                        }
                    });
                };
                var modalInstance;

                var infoTransaction = {};
                $scope.checkout = function () {
                    var paymethod = $("#"+$scope.payment).text();
                    var totalamount = $scope.total_amount.replace(currencyName, '');
                    console.log("totalamount", totalamount);
                    var url = $scope.url + 'baokim/checkout?';
                    url += "payer_name=" + $scope.payer_name;
                    url += '&payer_email=' + $scope.payer_email;
                    url += '&payer_phone_no=' + $scope.payer_phone_no;
                    url += '&payer_address=' + $scope.payer_address;
                    url += '&total_amount=' + totalamount;
                    url += '&bank_payment_method_id=' + $scope.bank_id;
                    $scope.popup = window;
                    $scope.popup.open(url);
                    $scope.popup.focus();
                    $scope.popup.popupCallback = function (str) {
                        if (str.status === false)
                        {
                            logger.logError(str.error);
                        }
                        else {
                            $scope.popup.close();
                            var transaction = JSON.parse(str);

                            infoTransaction.id = transaction.transaction_id;
                            infoTransaction.id_Service = $stateParams.params;
                            infoTransaction.paymethod = paymethod;
                            infoTransaction.sum = sum;
                            infoTransaction.supplier = $scope.payer_name;

                            modalInstance = $modal.open({
                                templateUrl: "app/partials/form/finishOrder.html",
                                controller: 'finishOrder',
                                resolve: {
                                    infoTransaction: function () {
                                        return infoTransaction;
                                    }
                                }
                            });
                        }
                    }
                };
            }
        ]);
        module.controller('finishOrder', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'infoTransaction',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, infoTransaction) {

                console.log("idPost",infoTransaction.id_Service);

                        $scope.tiltle = infoTransaction.title;
                        $scope.supplier = infoTransaction.supplier;
                        $scope.paymethod = infoTransaction.paymethod;
                        $scope.sum = infoTransaction.sum;


                $scope.showHistory = function () {

                    $modalInstance.close();
                }
                $scope.accept = function () {
                    {
                        var finishOrder = {};
                        finishOrder.username = Session.userId;

                        finishOrder.status = 5;
                        console.log("finishOrder",finishOrder);
                        api.put('serviceorder/'+ infoTransaction.id_Service +'/v1', finishOrder).then(function (res) {

                        if (res.data.result) {
                            console.log("res finish",res.data.result);
                            $location.path("/detailSupplier");
                        }
                        else {
                            logger.logError(res.data.error.description);
                        }
                    });
                        $modalInstance.close();
                    }

                };
            }
        ]);
    }
);