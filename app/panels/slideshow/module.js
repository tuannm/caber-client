/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
  'angular',
  'app',
  'lodash',
  'require',
  'css!./module.css',
  'css!./angular-carousel.min.css'
],
function (angular, app, _, require) {
  'use strict';

  var module = angular.module('bidproject.panels.slideshow', []);
  app.useModule(module);

  module.controller('slideshow', [ '$scope', '$rootScope', '$element', 'windowsize', function($scope, $rootScope, $element, windowsize) {
    $scope.panelMeta = {
      status  : "Stable",
      description : "A static text panel that can use plain text, markdown, or (sanitized) HTML"
    };

    // Set and populate defaults
    var _d = {
      /** @scratch /panels/text/5
       * === Parameters
       *
       * mode:: `html', `markdown' or `text'
       */
      mode    : "markdown", // 'html','markdown','text'
      /** @scratch /panels/text/5
       * content:: The content of your panel, written in the mark up specified in +mode+
       */
      content : "",
      style: {}
    };
    _.defaults($scope.panel,_d);
    $scope.slides = [];

    $scope.init = function() {
      	$scope.ready = true;
        for (var i = 0; i < $scope.panel.slides.length; i++) {
          addSlide($scope.slides, $scope.panel.slides[i]);
        }
      $scope.carouselIndex = 0;
      console.log("$scope.slides", $scope);
    };

    $scope.colors = ["#fc0003", "#f70008", "#f2000d", "#ed0012", "#e80017", "#e3001c", "#de0021", "#d90026", "#d4002b", "#cf0030", "#c90036", "#c4003b", "#bf0040", "#ba0045", "#b5004a", "#b0004f", "#ab0054", "#a60059", "#a1005e", "#9c0063", "#960069", "#91006e", "#8c0073", "#870078", "#82007d", "#7d0082", "#780087", "#73008c", "#6e0091", "#690096", "#63009c", "#5e00a1", "#5900a6", "#5400ab", "#4f00b0", "#4a00b5", "#4500ba", "#4000bf", "#3b00c4", "#3600c9", "#3000cf", "#2b00d4", "#2600d9", "#2100de", "#1c00e3", "#1700e8", "#1200ed", "#0d00f2", "#0800f7", "#0300fc"];

    function getSlide(target, style) {
      var i = target.length;
      return _.extend(style, {
        id: (i + 1),
        label: 'slide #' + (i + 1),
        color: $scope.colors[ (i * 10) % $scope.colors.length],
        odd: (i % 2 === 0)
      });
    }

    $scope.$watch('carouselIndex', function(newValue) {
      if(newValue < 0){
        return;
      }
      $('ul', $element).css({"background-color": $scope.slides[newValue]["background-color"],"height": windowsize.getWindowDimensions().h - 50 });
    });

    function addSlide(target, style) {
      target.push(getSlide(target, style));
    };
  }]);

});
