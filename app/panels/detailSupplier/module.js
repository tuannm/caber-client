/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.detailSupplier', []);
        app.useModule(module);

        module.controller('detailSupplier', ['$scope', '$rootScope', '$state', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', '$location', 'Session', 'api', '$modal', 'USER_ROLES', 'logger', 'notification', 'navigations', 'store',

            function ($scope, $rootScope, $compile, $state, $timeout, $stateParams, db, windowsize, $element, $location, Session, api, $modal, USER_ROLES, logger, notification, navigations, store) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.searchFeed = {};

                $scope.init = function () {
                    if (Session.userRole == "supplier") {
                        $scope.supplier = true
                    }
                    else {
                        $scope.supplier = false
                    }
                    $scope.searchdetails = {};
                    $scope.role = Session.userRole;
                    $scope.ready = false;
                    $scope.showSearchs = false;
                    $scope.reset_panel();
                    if (windowsize.devicetype() === "Desktop") {
                        getNewOrder();
                        getbrokerAssignedDefault();
                    } else {
                        if (navigations.current.index == 0) {
                            getNewOrder();
                            getbrokerAssignedDefault();
                        }
                        else if (navigations.current.index == 1) {
                            getAwaitingPayment();
                            getAwaitingPaymentBroker();
                        }
                        else if (navigations.current.index == 2) {
                            getFinishOrder();
                            getFinishOrderBroker();
                        }
                    }

                    if ($scope.newNotification == 0) {
                        $scope.hasNotification = false;
                    }
                    if ($scope.newNotificationPayment == 0) {
                        $scope.hasNotificationPayment = false;
                    }
                };

                $scope.hasNotification = false;
                $scope.newNotification = 0;
                $scope.newNotificationPayment = 0;
                $scope.hasNotificationPayment = false;
                $scope.openStartDate = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.openedStartDate = true;
                };
                var found = false
                $scope.indexChange = null;
                $rootScope.socket.on('notification', function (message) {
                    $rootScope.$apply(function () {
                        angular.extend(message, {id: message.data_id});
                        $scope.indexChange = message;
                        if (message.action == "create-service-order") {
                            $scope.hasNotification = true;
                            $scope.newNotification += 1;
                        }
                        if (message.action == "accept-proposal") {
                            $scope.hasNotificationPayment = true;
                            $scope.newNotificationPayment += 1;
                        }


                    });
                });
                $scope.awaitingPayment = {};
                $scope.notifyChange = function (data) {
                    if ($scope.indexChange.action == "create-service-order") {
                        $scope.hasNotification = true;
                        $scope.newNotificationPayment += 1;
                    }
                    if ($scope.indexChange.action == "accept-proposal") {
                        $scope.hasNotificationPayment = true;
                        $scope.newNotificationPayment += 1;
                        if(Session.userRole == "supplier"){
                            data.aData.proposals[0].status = 2;
                        }
                        data.aData.isChangeStatus = true;
                    }
                    if ($scope.indexChange.action == "close-service-order") {
                        data.aData.status = 2;
                        data.aData.isChangeStatus = true;
                    }

                }

                $scope.searchClick = function () {
                    //if (!form.valid()) {
                    //    return;
                    //}
                    var data;

                    //var search = {};
                    var obj = {};

                    if ($scope.searchdetails != undefined) {
                        if ($scope.searchdetails.idRequestAssigned != null) {
                            $scope.searchFeed.seviceOrderId = $scope.searchdetails.idRequestAssigned;
                            obj.idRequestAssigned = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.seviceOrderId = "";
                        }

                        if ($scope.searchdetails.idProposalAssigned != null) {
                            $scope.searchFeed.proposalId = $scope.searchdetails.idProposalAssigned;
                            obj.idProposalAssigned = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.proposalId = "";
                        }

                        if ($scope.searchdetails.titleRequestAssigned != null) {
                            $scope.searchFeed.title = $scope.searchdetails.titleRequestAssigned;
                            obj.titleRequestAssigned = $scope.searchdetails;
                        } else {
                            $scope.searchFeed.title = "";
                        }
                    }

                    //$scope.searchFeed.username = Session.userId;
                    getbrokerAssigned($scope.searchFeed);
                    data = $scope.searchFeed
                    store.set("searchFeed", obj);

                    if (windowsize.devicetype() != 'Desktop') {
                        $scope.showSearchs = false;
                        navigations.collections[navigations.collections.length - 1].searchData = data
                    }
                }

                function getbrokerAssigned(search) {
                    console.log("search: ", search);
                    getbrokerAssignedList();
                    $scope.brokerAssignedList.url = "proposal/v1?userType=B&pstatus=7,1,3,4,5&serviceOrderId=" + search.seviceOrderId + "&title=" + search.title + "&proposalId=" + search.proposalId;
                    $scope.brokerAssignedList.remote_data = "proposals";
                    $scope.brokerAssignedList.postData = angular.copy(search);

                }

                function getbrokerAssignedDefault() {
                    getbrokerAssignedList();
                    $scope.brokerAssignedList.url = "proposal/v1?userType=B&pstatus=7,1,3,4,5";
                    $scope.brokerAssignedList.remote_data = "proposals";

                }

                function getNewOrder() {
                    $scope.newOrder = {
                        "url": "serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=0,1,3,4,5,6,7",
                        "remote_data": "serviceOrders",
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "modifiedDate", "bVisible": false, "iDataSort": 10}
                        ],
                        "order": [[10, "desc"]],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "class": "text-center hide-mobile verticalMiddle",
                                "width": "13%",
                                "render": function (data, type, full) {
                                    //console.log("full", full)
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].jsonData) {
                                        var dataMore = JSON.parse(full.proposals[0].jsonData.replace(/&quot;/g, '"'));
                                    }
                                    if (dataMore) {
                                        if (dataMore.addonService != undefined && dataMore.addonService != null) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }

                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }

                                    if (full.id != null) {
                                        if (found) {
                                            element.append('<span class="boldRed">' + full.id + '</span>');
                                        }
                                        else {
                                            element.append('<span>' + full.id + '</span>');
                                        }
                                    } else {
                                        element.append('<span></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "20%",
                                "class": "verticalMiddle",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].jsonData) {
                                        var dataMore = JSON.parse(full.proposals[0].jsonData.replace(/&quot;/g, '"'));
                                    }
                                    if (dataMore) {
                                        if (dataMore.addonService != undefined && dataMore.addonService != null) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }

                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }

                                    var desktop = angular.element("<div></div>");

                                    if (found) {
                                        desktop.append('<a href="#/detailOrder/' + full.proposals[0].id + '" class="hide-when-mobile boldRed">' + full.title + ' </a>');
                                    }
                                    else {
                                        desktop.append('<a href="#/detailOrder/' + full.proposals[0].id + '" class=" hide-when-mobile">' + full.title + ' </a>');
                                    }
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-margin no-padding footer-pull-left-mobile"></div>');
                                    if (found) {
                                        var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile boldRed"></div>');
                                    }
                                    else {
                                        var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile"></div>');
                                    }

                                    if (full.expiredDate != null) {
                                        var date = new Date(full.expiredDate);
                                        var day = "";
                                        var month = "";
                                        var time = new Date(date.setHours(date.getHours() - 7));
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        if (found) {
                                            header.append('<div class="col-xs-8 no-margin no-padding boldRed"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4 boldRed">' + day + '/' + month + '/' + date.getFullYear() + '</div>');
                                        }
                                        else {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4">' + day + '/' + month + '/' + date.getFullYear() + '</div>');
                                        }
                                    } else {
                                        if (found) {
                                            header.append('<div class="col-xs-8 no-margin no-padding boldRed"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4 boldRed"></div>');
                                        }
                                        else {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4"></div>');
                                        }
                                    }

                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    if (dataMore.information[i].input != null) {
                                                        if (found) {

                                                            pullLeft.append('<span class="col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + ' - ' + dataMore.information[i].name + '</span>');
                                                        }
                                                        else {
                                                            pullLeft.append('<span class="col-xs-12 no-margin no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + ' - ' + dataMore.information[i].name + '</span>');
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            if (full.budgetFrom != null) {
                                                if (found) {
                                                    pullLeft.append('<span class="boldRed">' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                                }
                                                else {
                                                    pullLeft.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                                }
                                            }
                                        }
                                    }

                                    pullLeft.append('<div class="col-md-12 col-xs-12 no-margin no-padding"><rating class="ui-rating size-h5 ui-rating-warning" ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');

                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }
                                    var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                                    if (full.status === 1) {
                                        if (full.proposals[0].status === 1) {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                pullRight.append('<span class="col-xs-12 toast-title" data-i18n="bidding"></span><span>');
                                            }
                                            else {
                                                pullRight.append('<span class="col-xs-12" data-i18n="bidding"></span><span>');
                                            }
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                        else if (full.proposals[0].status === 0) {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                pullRight.append('<span class="col-xs-12 toast-title" data-i18n="opening"></span><span>');
                                            }
                                            else {
                                                pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                            }
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                        else if (full.proposals[0].status === 3) {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                pullRight.append('<span class="col-xs-12 toast-title" data-i18n="complete"></span><span>');
                                            }
                                            else {
                                                pullRight.append('<span class="col-xs-12" data-i18n="complete"></span><span>');
                                            }
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');

                                        }
                                        else if (full.proposals[0].status === 4) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="reject"></span><span>');
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                        else if (full.proposals[0].status === 5) {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                pullRight.append('<span class="col-xs-12 toast-title" data-i18n="closed"></span><span>');
                                            }
                                            else {
                                                pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                            }
                                            pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                        else if (full.proposals[0].status === 6) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="verifying"></span><span>');
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                        else if (full.proposals[0].status === 7) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="assign broker"></span><span>');
                                            pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                        }
                                    }
                                    else if (full.status === 2) {
                                        pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                        pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                    }
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "class": "verticalMiddle",
                                "render": function (data, type, full) {
                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }
                                    if (found) {
                                        var element = angular.element("<div class=' boldRed'></div>");
                                    }
                                    else {
                                        var element = angular.element("<div></div>");
                                    }

                                    element.append('<span> ' + full.content.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "class": "hide-mobile verticalMiddle",
                                "data": null,
                                "render": function (data, type, full) {
                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    if (found) {
                                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].name + '</span>');
                                                    }
                                                    else {
                                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding">' + dataMore.information[i].name + '</span>');
                                                    }
                                                }
                                            }

                                        } else {
                                            if (found)
                                                element.append('<span class="boldRed" data-i18n="' + full.serviceType.display + '"></span>');
                                            else {
                                                element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                            }
                                        }
                                    } else {
                                        if (found)
                                            element.append('<span class="boldRed" data-i18n="' + full.serviceType.display + '"></span>');
                                        else {
                                            element.append('<span data-i18n="' + full.serviceType.display + '"></span>');
                                        }
                                    }

                                    return element.html();

                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "15%",
                                "class": "text-right hide-mobile verticalMiddle",
                                "render": function (data, type, full) {
                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        var isMore = false;
                                        if (dataMore.information != undefined && dataMore.information.length > 0) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected) {
                                                    isMore = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (isMore) {
                                            var i;
                                            for (i = 0; i < dataMore.information.length; i++) {
                                                if (dataMore.information[i].isSelected & dataMore.information[i].input != null) {
                                                    if (found)
                                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding boldRed">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                                    else {
                                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-margin no-padding">' + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                                    }
                                                }
                                            }

                                        } else {
                                            if (full.budgetFrom != null) {
                                                if (found)
                                                    element.append('<span class="boldRed">' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                                else {
                                                    element.append('<span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                                }
                                            }
                                        }
                                    }

                                    return element.html();
                                }
                            },

                            {
                                "targets": 5,

                                "data": null,
                                "class": "text-center hide-mobile verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }
                                    element.append('<span>' + parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed) + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "class": "text-center hide-mobile verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.expiredDate != null) {
                                        var date = new Date(full.expiredDate);
                                        var day = "";
                                        var month = "";
                                        var time = new Date(date.setHours(date.getHours() - 7));
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }

                                        element.append('<span>' + day + '/' + month + '/' + date.getFullYear() + '</span>');

                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "class": "verticalMiddle hiddenIsNotSkill",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    if (full.skill != null) {
                                        var skills = full.skill.toString().split(",");
                                        var skill = "";
                                        for (var i = 0; i < skills.length; i++) {
                                            if (i != skills.length - 1) {
                                                skill += skills[i].toString() + ", ";
                                            } else {
                                                skill += skills[i].toString();
                                            }
                                        }
                                        element.append(skill);
                                    }


                                    return element.html();
                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "class": "verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);

                                        element.append('<span>' + dataMore.city.name + '</span>');

                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 9,
                                "data": null,
                                "width": "16%",
                                "class": "text-center hide-mobile verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<div class="col-md-12"><rating class="ui-rating size-h5 ui-rating-warning" ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 10,
                                "data": null,
                                "class": "verticalMiddle removeBoldRed hiddenIsNull text-right",
                                "width": "13%",
                                "render": function (data, type, full) {
                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].expectedCost !== null) {

                                        if (found) {
                                            element.append('<span class="boldRed">' + full.proposals[0].expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                        }
                                        else {
                                            element.append('<span>' + full.proposals[0].expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                        }
                                    } else {
                                        element.append('<span></span>');
                                    }
                                    return element.html();

                                }
                            },
                            {

                                "targets": 11,
                                "class": "text-center hide-mobile verticalMiddle",
                                "data": null,
                                "render": function (data, type, full) {
                                    if (full.addonServices.length > 0) {
                                        var found = false;
                                        for (var i = 0; i < full.addonServices.length; i++) {
                                            if (full.addonServices[i].id == 3 && full.addonPayment == 1) {
                                                found = true;
                                            }
                                        }
                                    }
                                    var element = angular.element("<div></div>");


                                    if (full.status === 1) {
                                        if (full.proposals[0].status === 1) {
                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="bidding" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="bidding" ></span>');
                                            }
                                        }
                                        else if (full.proposals[0].status === 0) {
                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="opening" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="opening" ></span>');
                                            }
                                        }
                                        else if (full.proposals[0].status === 3) {
                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="complete" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="complete" ></span>');
                                            }
                                        }
                                        else if (full.proposals[0].status === 4) {

                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="reject" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="reject" ></span>');
                                            }
                                        }
                                        else if (full.proposals[0].status === 5) {
                                            if (found) {
                                                if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                    element.append('<span class="boldRed toast-title"  data-i18n="closed" ></span>');
                                                }
                                                else {
                                                    element.append('<span class="boldRed" data-i18n="closed" ></span>');
                                                }
                                            }
                                            else {
                                                if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                    element.append('<span class="toast-title" data-i18n="closed" ></span>');
                                                }
                                                else {
                                                    element.append('<span data-i18n="closed" ></span>');
                                                }
                                            }
                                        }
                                        else if (full.proposals[0].status === 6) {
                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="verifying" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="verifying" ></span>');
                                            }
                                        }
                                        else if (full.proposals[0].status === 7) {
                                            if (found) {
                                                element.append('<span class="boldRed" data-i18n="assign broker" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="assign broker" ></span>');
                                            }
                                        }
                                    }
                                    else if (full.status === 2) {
                                        if (found) {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                element.append('<span class="boldRed toast-title"  data-i18n="closed" ></span>');
                                            }
                                            else {
                                                element.append('<span class="boldRed" data-i18n="closed" ></span>');
                                            }
                                        }
                                        else {
                                            if (full.isChangeStatus != undefined && full.isChangeStatus) {
                                                element.append('<span class="toast-title" data-i18n="closed" ></span>');
                                            }
                                            else {
                                                element.append('<span data-i18n="closed" ></span>');
                                            }
                                        }
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 12,
                                "data": null,
                                "class": "verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.status === 1) {

                                        if (full.proposals[0].status === 1) {
                                            if (full.proposals[0].jsonData) {
                                                var dataMore = JSON.parse(full.proposals[0].jsonData.replace(/&quot;/g, '"'));
                                            }
                                            if (dataMore.addonService != undefined && dataMore.addonService != null) {
                                                element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                            } else {
                                                element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + ' " class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="update"></span></a></div>');
                                            }


                                        }
                                        else if (full.proposals[0].status === 0) {
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + ' " class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="bid"></span></a></div>');

                                        }
                                        else if (full.proposals[0].status === 3) {

                                            element.append('<div class="col-md-12 col-sm-12"><a class="margin-top6 pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.ratingUser(aData)"><span data-i18n="rating"></span></a></div>');
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                        }
                                        else if (full.proposals[0].status === 4) {

                                            element.append('<div class="col-md-12 col-sm-12"><a class="margin-top6 pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.ratingUser(aData)"><span data-i18n="rating"></span></a></div>');
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                        }
                                        else if (full.proposals[0].status === 5) {

                                            element.append('<div class="col-md-12 col-sm-12"><a class="margin-top6 pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.ratingUser(aData)"><span data-i18n="rating"></span></a></div>');
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                        }
                                        else if (full.proposals[0].status === 6) {
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                        }
                                        else if (full.proposals[0].status === 7) {
                                            element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                        }
                                    }
                                    else if (full.status === 2) {
                                        element.append('<div class="col-md-12 col-sm-12"><a class="margin-top6 pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.ratingUser(aData)"><span data-i18n="rating"></span></a></div>');
                                        element.append('<div class="col-md-12 col-sm-12"><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    }
                                    if(full.proposals[0].brokerId != null){
                                        full.proposals[0].userRole = Session.userRole;
                                        element.append('<div class="col-md-12 col-sm-12" id="' + full.proposals[0].brokerId + '"><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.viewContact(aData.proposals[0])"><span data-i18n="view contact broker"></span></a></div>');
                                    }

                                    return element.html();
                                }
                            },
                        ]
                    };
                }

                var modalInstance;
                var rating = {};
                $scope.ratingUser = function (aData) {
                    $scope.currentData = aData;
                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/ratingUser.html",
                        controller: 'ratingUserCtr',
                        resolve: {
                            data: function () {
                                return $scope.currentData;

                                //    idProposal: function () {
                                //        return idPro;
                                //    }
                            }
                        }
                    });
                    modalInstance.result.then((function (rate) {
                        $scope.currentData.ratingMark = rate;
                    }), function () {
                    });
                }
                module.controller('ratingUserCtr', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data) {

                        $scope.rate = {};
                        $scope.rate.start = 0;
                        $scope.close = function () {
                            $modalInstance.dismiss("close");
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss("close");
                        };

                        $scope.getSetStatus = function () {
                            switch ($scope.rate.start) {
                                case 1:
                                    if ($(".very-bad").hasClass("non-display"))
                                        $(".very-bad").removeClass("non-display");
                                    if (!$(".bad").hasClass("non-display"))
                                        $(".bad").addClass("non-display");
                                    if (!$(".normal").hasClass("non-display"))
                                        $(".normal").addClass("non-display");
                                    if (!$(".good").hasClass("non-display"))
                                        $(".good").addClass("non-display");
                                    if (!$(".very-good").hasClass("non-display"))
                                        $(".very-good").addClass("non-display");
                                    break;
                                case 2:
                                    if ($(".bad").hasClass("non-display"))
                                        $(".bad").removeClass("non-display");
                                    if (!$(".very-bad").hasClass("non-display"))
                                        $(".very-bad").addClass("non-display");
                                    if (!$(".normal").hasClass("non-display"))
                                        $(".normal").addClass("non-display");
                                    if (!$(".good").hasClass("non-display"))
                                        $(".good").addClass("non-display");
                                    if (!$(".very-good").hasClass("non-display"))
                                        $(".very-good").addClass("non-display");
                                    break;
                                case 3:
                                    if ($(".normal").hasClass("non-display"))
                                        $(".normal").removeClass("non-display");
                                    if (!$(".very-bad").hasClass("non-display"))
                                        $(".very-bad").addClass("non-display");
                                    if (!$(".bad").hasClass("non-display"))
                                        $(".bad").addClass("non-display");
                                    if (!$(".good").hasClass("non-display"))
                                        $(".good").addClass("non-display");
                                    if (!$(".very-good").hasClass("non-display"))
                                        $(".very-good").addClass("non-display");
                                    break;
                                case 4:
                                    if ($(".good").hasClass("non-display"))
                                        $(".good").removeClass("non-display");
                                    if (!$(".very-bad").hasClass("non-display"))
                                        $(".very-bad").addClass("non-display");
                                    if (!$(".bad").hasClass("non-display"))
                                        $(".bad").addClass("non-display");
                                    if (!$(".normal").hasClass("non-display"))
                                        $(".normal").addClass("non-display");
                                    if (!$(".very-good").hasClass("non-display"))
                                        $(".very-good").addClass("non-display");
                                    break;
                                case 5:
                                    if ($(".very-good").hasClass("non-display"))
                                        $(".very-good").removeClass("non-display");
                                    if (!$(".very-bad").hasClass("non-display"))
                                        $(".very-bad").addClass("non-display");
                                    if (!$(".bad").hasClass("non-display"))
                                        $(".bad").addClass("non-display");
                                    if (!$(".normal").hasClass("non-display"))
                                        $(".normal").addClass("non-display");
                                    if (!$(".good").hasClass("non-display"))
                                        $(".good").addClass("non-display");
                                    break;
                            }
                        }


                        $scope.save = function () {
                            var rating = {};
                            var proposalRating = {};
                            rating.voter = Session.userId;
                            rating.role = USER_ROLES[Session.userRole];
                            rating.ratings = [];
                            proposalRating.id = data.id;
                            proposalRating.mark = $scope.rate.start;
                            proposalRating.comments = $scope.rate.comment;
                            rating.ratings.push(proposalRating);
                            api.post('rating/v1', rating).then(function (res) {
                                if (res.data.result) {
                                    $modalInstance.close($scope.rate.start);
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            });

                        };


                    }
                ]);
                function getbrokerAssignedList() {
                    $scope.brokerAssignedList = {

                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            //{"data": "null", "class": "verticalMiddle"},
                            {"data": "id", "class":"verticalMiddle text-center hide-mobile", "width": "10%"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "class": "verticalMiddle  hide-mobile text-center",
                                "data": null,
                                "width": "15%",
                                "render": function (data, type, full) {
                                    console.log("full", full)
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        if (dataMore.addonService) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }
                                    element.append('<span>' + full.serviceOrderId + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "25%",
                                "class": "verticalMiddle",
                                "render": function (data, type, full, meta) {

                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div></div>");
                                    desktop.append('<a href="#/detailOrder/' + full.id + '" class=" hide-when-mobile">' + full.serviceOrderTitle + ' </a>');
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-margin no-padding footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile"></div>');
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);

                                        if (dataMore.addonService) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                        if (dataMore.expiredDate != null) {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrderId + ' - ' + full.serviceOrderTitle + '</span></div><div class="col-xs-4">' + dataMore.expiredDate + '</div>');
                                        } else {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrderId + ' - ' + full.serviceOrderTitle + '</span></div><div class="col-xs-4"></div>');
                                        }

                                        pullLeft.append('<div class="col-xs-12 no-margin no-padding"><span class="col-xs-4 no-padding" data-i18n="proposal id"></span><span class="col-xs-8 no-padding">'+full.id+'</span></div>');
                                        if (full.status === 1) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="bidding"></span><span>');
                                        }
                                        else if (full.status === 0) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="opening"></span><span>');
                                        }
                                        else if (full.status === 3) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="complete"></span><span>');
                                        }
                                        else if (full.status === 4) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="reject"></span><span>');
                                        }
                                        else if (full.status === 5) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="closed"></span><span>');
                                        }
                                        else if (full.status === 6) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="verifying"></span><span>');
                                        }
                                        else if (full.status === 7) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="assign broker"></span><span>');
                                        }
                                    }
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": "12%",
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        if (dataMore.budget) {
                                            if (dataMore.budget.length > 0) {
                                                element.append('<span>' + dataMore.budget[0] + '</span>');
                                            }
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            //{
                            //    "targets": 3,
                            //    "data": null,
                            //    "width": "16%",
                            //    "class": "verticalMiddle hide-mobile text-center",
                            //    "render": function (data, type, full) {
                            //        console.log("full", full)
                            //        var element = angular.element("<div></div>");
                            //        var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                            //        element.append('<span>' + proposalsMatched + '</span>');
                            //        return element.html();
                            //    }
                            //},
                            {
                                "targets": 4,
                                "data": null,
                                "width": "20%",
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        element.append('<span>' + dataMore.title + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "12%",
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.expectedCost !== null) {
                                        element.append('<span>' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫" + '</span>');
                                    } else {
                                        element.append('<span></span>');
                                    }
                                    return element.html();

                                }
                            },

                            {
                                "targets": 6,
                                "class": "verticalMiddle text-center",
                                "data": null,
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    element.append('<span>' + full.aliasName + '</span>');

                                    return element.html();

                                }
                            },
                            {

                                "targets": 7,
                                "class": "text-center hide-mobile verticalMiddle",
                                "data": null,
                                "width": "15%",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.status === 1) {
                                        element.append('<span data-i18n="bidding" ></span>');
                                    }
                                    else if (full.status === 0) {
                                        element.append('<span data-i18n="opening" ></span>');
                                    }
                                    else if (full.status === 3) {
                                        element.append('<span data-i18n="complete" ></span>');
                                    }
                                    else if (full.status === 4) {
                                        element.append('<span data-i18n="reject" ></span>');
                                    }
                                    else if (full.status === 5) {
                                        element.append('<span data-i18n="closed" ></span>');
                                    }
                                    else if (full.status === 6) {
                                        element.append('<span data-i18n="verifying" ></span>');
                                    }
                                    else if (full.status === 7) {
                                        element.append('<span data-i18n="assign broker" ></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 8,
                                "class": "verticalMiddle",
                                "data": null,
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    //element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.acceptProposal(aData,aData.proposals[0].id)"><span data-i18n="accept">Đồng ý</span></a></div>');
                                    //element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.rejectProposal(aData.proposals[0].id)"><span data-i18n="reject">Từ chối</span></a></div>');
                                    element.append('<div><a href="#/detailOrder/' + full.id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    full.userRole = Session.userRole;
                                    element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.viewContact(aData)"><span data-i18n="view contact supplier"></span></a></div>');

                                    return element.html();
                                }
                            }
                        ]
                    };


                }

                function getAwaitingPaymentBroker() {
                    $scope.awaitingPaymentBroker = {
                        "url": "proposal/v1?userType=B&pstatus=2",
                        "remote_data": "proposals",
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            //{"data": "null", "class": "verticalMiddle"},
                            {"data": "id", "class":"verticalMiddle text-center hide-mobile", "width": "10%"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "class": "verticalMiddle  hide-mobile text-center",
                                "data": null,
                                "width": "15%",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        if (dataMore.addonService) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }
                                    element.append('<span>' + full.serviceOrderId + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "25%",
                                "class": "verticalMiddle",
                                "render": function (data, type, full, meta) {

                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div></div>");
                                    desktop.append('<a href="#/detailOrder/' + full.id + '" class=" hide-when-mobile">' + full.serviceOrderTitle + ' </a>');
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-margin no-padding footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile"></div>');
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);

                                        if (dataMore.addonService) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                        if (dataMore.expiredDate != null) {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrderId + ' - ' + full.serviceOrderTitle + '</span></div><div class="col-xs-4">' + dataMore.expiredDate + '</div>');
                                        } else {
                                            header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrderId + ' - ' + full.serviceOrderTitle + '</span></div><div class="col-xs-4"></div>');
                                        }

                                        pullLeft.append('<div class="col-xs-12 no-margin no-padding"><span class="col-xs-4 no-padding" data-i18n="proposal id"></span><span class="col-xs-8 no-padding">'+full.id+'</span></div>');
                                        if (full.status === 7) {
                                            pullRight.append('<span class="col-xs-12" data-i18n="assign broker"></span><span>');
                                        }
                                    }
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": "12%",
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        if (dataMore.budget) {
                                            if (dataMore.budget.length > 0) {
                                                element.append('<span>' + dataMore.budget[0] + '</span>');
                                            }
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            //{
                            //    "targets": 3,
                            //    "data": null,
                            //    "width": "16%",
                            //    "class": "verticalMiddle hide-mobile text-center",
                            //    "render": function (data, type, full) {
                            //        console.log("full", full)
                            //        var element = angular.element("<div></div>");
                            //        var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                            //        element.append('<span>' + proposalsMatched + '</span>');
                            //        return element.html();
                            //    }
                            //},
                            {
                                "targets": 4,
                                "data": null,
                                "width": "20%",
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        element.append('<span>' + dataMore.title + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "12%",
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.expectedCost !== null) {
                                        element.append('<span>' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫" + '</span>');
                                    } else {
                                        element.append('<span></span>');
                                    }
                                    return element.html();

                                }
                            },

                            {
                                "targets": 6,
                                "class": "verticalMiddle text-center",
                                "data": null,
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    element.append('<span>' + full.aliasName + '</span>');

                                    return element.html();

                                }
                            },
                            {

                                "targets": 7,
                                "class": "text-center hide-mobile verticalMiddle",
                                "data": null,
                                "width": "15%",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div><span data-i18n='wait payment'></span></div>");
                                    return element.html();
                                }
                            },
                            {
                                "targets": 8,
                                "class": "verticalMiddle",
                                "data": null,
                                "render": function (data, type, full) {
                                    console.log("full", full)
                                    var element = angular.element("<div></div>");
                                    var dataJson = JSON.parse(full.jsonData)
                                    if(dataJson.addonService != undefined){
                                        if(dataJson.addonService.type != "pp_broker_basic"){
                                            full.userRole = Session.userRole;
                                            element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.acceptProposal(aData,aData.id)"><span data-i18n="accept">Đồng ý</span></a></div>');
                                            element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.rejectProposal(aData.id)"><span data-i18n="reject">Từ chối</span></a></div>');
                                        }
                                    }
                                    element.append('<div><a href="#/detailOrder/' + full.id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    return element.html();
                                }
                            }
                        ]
                    };


                }

                function getFinishOrderBroker() {
                    $scope.finishOrderBroker = {
                        "url": "payment/v1?role=" + USER_ROLES[Session.userRole] + "&status=1,2",
                        "remote_data": "payments",
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "16%",
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {
                                    console.log("full",full);
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0].addonServiceType != undefined && full.proposal[0].addonServiceType != null) {
                                        var name;
                                        if(full.proposal[0].addonServiceType == "pp_broker_advance"){
                                            name = "Broker advance";
                                        } else if(full.proposal[0].addonServiceType == "pp_broker_basic") {
                                            name = "Broker basic"
                                        }
                                        var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                        option.append('<li data-promotion="' + full.proposal[0].addonServiceType.toLowerCase() + '">' + name + '</li>');
                                        element.append(option);
                                    }

                                    if (full.transactionId != null) {
                                        element.append('<span> ' + full.transactionId + '</span>')
                                    }
                                    else {
                                        element.append('<span></span>')
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.serviceOrder != undefined) {
                                        element.append('<span>' + full.serviceOrder.id + '</span>');
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "class": "verticalMiddle hide-mobile ",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>' + full.serviceOrder.title.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "10%",
                                "class": "verticalMiddle text-center text-left-on-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0].addonServiceType != undefined && full.proposal[0].addonServiceType != null) {
                                        var name;
                                        if(full.proposal[0].addonServiceType == "pp_broker_advance"){
                                            name = "Broker advance";
                                        } else if(full.proposal[0].addonServiceType == "pp_broker_basic") {
                                            name = "Broker basic"
                                        }
                                        var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                        option.append('<li data-promotion="' + full.proposal[0].addonServiceType.toLowerCase() + '">' + name + '</li>');
                                        element.append(option);
                                    }
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-padding footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-padding footer-pull-right-mobile"></div>');
                                    if (full.proposal != undefined) {
                                        if (full.proposal.length > 0) {
                                            desktop.append('<span>' + full.proposal[0].id + '</span>');
                                        }
                                    }
                                    header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrder.id + ' - ' + full.serviceOrder.title + '</span></div><div class="col-xs-4 no-padding">' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</div>');
                                    //}
                                    if (full.serviceOrder != undefined) {
                                        pullLeft.append('<span>' + full.serviceOrder.aliasName + '</span>');
                                    }
                                    if (full.status === 0) {
                                        pullRight.append('<span data-i18n="wait payment"></span>');
                                    }
                                    if (full.status === 1) {
                                        pullRight.append('<span data-i18n="paid"></span>');
                                    }
                                    if (full.status === 2) {
                                        pullRight.append('<span data-i18n="failed paid"></span>');
                                    }
                                    //}
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "class": "verticalMiddle ",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0] != undefined) {
                                        element.append('<span>' + full.proposal[0].proposal.replace(/\r\n|\r|\n/g, "<br />") + '</span>');

                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "15%",
                                "class": "text-center verticalMiddle text-right",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0] != undefined) {
                                        if (full.proposal[0].expectedCost !== null) {
                                            element.append('<span>' + full.proposal[0].expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                        } else {
                                            element.append('<span></span>');
                                        }
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    element.append('<span>' + full.serviceOrder.aliasName + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "class": "verticalMiddle text-right",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.totalPayment != undefined || full.totalPayment != null) {
                                        element.append('<span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                    }
                                    else {
                                        full.totalPayment = 0;
                                        element.append('<span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.status === 0) {
                                        element.append('<span data-i18n="wait payment"></span>');
                                    }
                                    if (full.status === 1) {
                                        element.append('<span data-i18n="paid"></span>');
                                    }
                                    if (full.status === 2) {
                                        element.append('<span data-i18n="failed paid"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 9,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.transactionId != null) {
                                        if (full.proposal[0]) {
                                            element.append('<div id="' + full.proposal[0].id + '"><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.viewContactDetail(aData.proposal[0].id)"><span data-i18n="view contact detail">Thông tin liên lạc</span></a></div>');
                                            element.append('<div id="' + full.proposal[0].id + 'tel"></div>');
                                            element.append('<div id="' + full.proposal[0].id + 'email"></div>');
                                        }
                                        //element.append('<a href="#/payment/' + full.id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="detail payment"></span></a>');
                                        element.append('<a data-ng-click="$parent.viewpayment(aData.id)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="detail payment"></span></a>');
                                    }
                                    return element.html();
                                }
                            }
                        ]
                    };
                }

                function getAwaitingPayment() {
                    $scope.awaitingPayment = {
                        "url": "serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=2",
                        "remote_data": "serviceOrders",
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "content", "class": "verticalMiddle"},
                            {"data": "skill", "class": "verticalMiddle hiddenIsNotSkill"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "class": "verticalMiddle  hide-mobile text-center",
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].jsonData) {
                                        var dataMore = JSON.parse(full.proposals[0].jsonData.replace(/&quot;/g, '"'));
                                    }
                                    if (dataMore) {
                                        if (dataMore.addonService != undefined && dataMore.addonService != null) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }
                                    element.append('<span>' + full.id + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "20%",
                                "class": "verticalMiddle",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].jsonData) {
                                        var dataMore = JSON.parse(full.proposals[0].jsonData.replace(/&quot;/g, '"'));
                                    }
                                    if (dataMore) {
                                        if (dataMore.addonService != undefined && dataMore.addonService != null) {
                                            var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                            //_.forEach(dataMore.addonService, function (n, key) {
                                            option.append('<li data-promotion="' + dataMore.addonService.type.toLowerCase() + '">' + dataMore.addonService.name + '</li>');
                                            //});
                                            element.append(option);
                                        }
                                    }
                                    var desktop = angular.element("<div></div>");
                                    desktop.append('<a href="#/detailOrder/' + full.proposals[0].id + '" class=" hide-when-mobile">' + full.title + ' </a>');
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-margin no-padding footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-margin no-padding footer-pull-right-mobile"></div>');

                                    if (full.expiredDate != null) {
                                        var date = new Date(full.expiredDate);
                                        var day = "";
                                        var month = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4">' + day + '/' + month + '/' + date.getFullYear() + '</div>');
                                    } else {
                                        header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.id + ' - ' + full.title + '</span></div><div class="col-xs-4"></div>');
                                    }
                                    if (full.proposals[0].jsonData != null) {
                                        var dataMore = JSON.parse(full.proposals[0].jsonData);
                                        //pullLeft.append('<div class="col-xs-12 no-margin no-padding"><span>' + dataMore.title + '</span></div>');
                                        if (full.budgetFrom != null) {
                                            pullLeft.append('<div class="col-xs-12 no-margin no-padding"><span>' + full.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span></div>');
                                        }
                                    }

                                    pullLeft.append('<div class="col-md-12 col-xs-12 no-margin no-padding"><rating class="ui-rating size-h5 ui-rating-warning" ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');
                                    if (full.totalProposalSubmitted == null) {
                                        full.totalProposalSubmitted = 0;
                                    }
                                    if (full.totalProposalAccepted == null) {
                                        full.totalProposalAccepted = 0;
                                    }
                                    if (full.totalProposalConfirmed == null) {
                                        full.totalProposalConfirmed = 0;
                                    }
                                    if (full.totalProposalClosed == null) {
                                        full.totalProposalClosed = 0;
                                    }
                                    if (full.totalProposalRejected == null) {
                                        full.totalProposalRejected = 0;
                                    }
                                    var proposalsMatched = parseInt(full.totalProposalSubmitted + full.totalProposalAccepted + full.totalProposalConfirmed + full.totalProposalRejected + full.totalProposalClosed);
                                    if (full.proposals[0].status === 2) {
                                        pullRight.append('<span class="col-xs-12" data-i18n="wait payment"></span><span>');
                                        pullRight.append('<span class="col-xs-12">' + proposalsMatched + ' <span data-i18n="proposal supplier">đề xuất</span></span>');
                                    }
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "16%",
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.expiredDate != null) {
                                        var date = new Date(full.expiredDate);
                                        var day = "";
                                        var month = "";
                                        var time = new Date(date.setHours(date.getHours() - 7));
                                        //var hours = time.getHours();
                                        //var minutes = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        //if (String(hours).length < 2) {
                                        //    hours = "0" + hours;
                                        //}
                                        //if (String(date.getMinutes()).length < 2) {
                                        //    minutes = "0" + (date.getMinutes());
                                        //} else {
                                        //    minutes = (date.getMinutes());
                                        //}
                                        element.append('<span>' + day + '/' + month + '/' + date.getFullYear() + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "class": "verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.jsonData != null) {
                                        var dataMore = JSON.parse(full.jsonData);
                                        element.append('<span>' + dataMore.city.name + '</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "width": "20%",
                                "class": "verticalMiddle text-right",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposals[0].expectedCost !== null) {
                                        element.append('<span>' + full.proposals[0].expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                    } else {
                                        element.append('<span></span>');
                                    }
                                    return element.html();

                                }
                            },

                            {
                                "targets": 7,
                                "class": "verticalMiddle text-center",
                                "data": null,
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    element.append('<span>' + full.aliasName + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 8,
                                "class": "verticalMiddle text-center hide-mobile",
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.proposals[0].status == 2) {

                                        element.append('<span data-i18n="wait payment"></span>');
                                    }
                                    return element.html()
                                }

                            },
                            {
                                "targets": 9,
                                "class": "verticalMiddle",
                                "data": null,
                                "render": function (data, type, full) {
                                    console.log("full", full)
                                    var element = angular.element("<div></div>");
                                    var dataJson;
                                    console.log("dataJson", dataJson);
                                    console.log("full.proposals.jsonData", full.proposals.jsonData);
                                    if (full.proposals[0].jsonData != null) {
                                        dataJson = JSON.parse(full.proposals[0].jsonData);
                                    }
                                    if (dataJson != undefined && dataJson.addonService != undefined) {
                                        if (dataJson.addonService.type != "pp_broker_advance") {
                                            element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.acceptProposal(aData,aData.proposals[0].id)"><span data-i18n="accept">Đồng ý</span></a></div>');
                                            element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.rejectProposal(aData.proposals[0].id)"><span data-i18n="reject">Từ chối</span></a></div>');
                                        }

                                    } else {
                                        element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.acceptProposal(aData,aData.proposals[0].id)"><span data-i18n="accept">Đồng ý</span></a></div>');
                                        element.append('<div><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.rejectProposal(aData.proposals[0].id)"><span data-i18n="reject">Từ chối</span></a></div>');
                                    }
                                    element.append('<div><a href="#/detailOrder/' + full.proposals[0].id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    return element.html();
                                }
                            }
                        ]
                    };


                }


                $scope.acceptProposal = function (dataSer, idPro) {
                    //var acceptOrder = {};
                    //acceptOrder.username = Session.userId;
                    //acceptOrder.status = 3;
                    //
                    //api.put('serviceorder/' + dataSer.id + '/v1', acceptOrder).then(function (res) {
                    //
                    //    if (res.data.result) {
                    //
                    //    }
                    //    else {
                    //        logger.logError(res.data.error.description);
                    //    }
                    //});
                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/confirmProposal.html",
                        controller: 'confirmProposal',
                        resolve: {
                            dataSer: function () {
                                return dataSer;
                            },
                            idProposal: function () {
                                return idPro;
                            }
                        }

                    });

                };
                module.controller('confirmProposal', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'dataSer', 'idProposal',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, dataSer, idProposal) {
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        console.log("dataSer",dataSer)
                        $scope.isHide = false;
                        if(dataSer.userRole != undefined && dataSer.userRole == "broker"){
                            $scope.isHide = true;
                        }
                        $scope.title = dataSer.title;
                        $scope.customer = dataSer.aliasName;
                        $scope.cancel = function () {

                            $modalInstance.close();
                        }
                        $scope.type = "youPay";
                        $scope.accept = function () {

                            $modalInstance.close();
                            var url;
                            if(dataSer.userRole != undefined && dataSer.userRole == "broker"){
                                url = config.admin + "payment?paymentType=PP&username=" + Session.userId + "&serviceOrderId=" + dataSer.serviceOrderId + "&proposalIds=" + idProposal + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                            } else{
                                url = config.admin + "payment?paymentType=PP&username=" + Session.userId + "&serviceOrderId=" + dataSer.id + "&proposalIds=" + idProposal + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                            }

                            $rootScope.paymentUrl(url);
                        };

                    }
                ]);
                $scope.rejectProposal = function (id) {

                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/rejectProposal.html",
                        controller: 'rejectProposal',
                        resolve: {
                            idProposal: function () {
                                return id;
                            }
                        }
                    });
                    modalInstance.result.then((function (flagReject) {
                        if (flagReject) {
                            if (Session.userRole == "supplier") {
                                getAwaitingPayment();
                                $scope.awaitingPayment.url = new String("serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=2");
                            } else if (Session.userRole == "broker") {
                                getAwaitingPaymentBroker();
                                $scope.awaitingPaymentBroker.url = new String("serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=2");
                            }
                        }

                    }), function () {
                    });
                };
                module.controller('rejectProposal', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idProposal',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idProposal) {
                        var flagReject = false;
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.cancel = function () {
                            $modalInstance.close(flagReject);
                        }

                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.reject = function () {
                            var rejectOrder = {};
                            rejectOrder.username = Session.userId;
                            rejectOrder.status = 4;
                            api.put('proposal/' + idProposal + '/v1', rejectOrder).then(function (res) {
                                if (res.data.result) {
                                    flagReject = true
                                    $modalInstance.close(flagReject);
                                    $location.path("/detailSupplier");
                                    return logger.logWarning("Bạn vừa từ chối 1 Order");
                                }
                                else {
                                    logger.logError(res.data.error.description);
                                }
                            });
                        };

                    }
                ]);
                function getFinishOrder() {
                    $scope.finishOrder = {
                        "url": "payment/v1?role=" + USER_ROLES[Session.userRole] + "&status=1,2",
                        "remote_data": "payments",
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}

                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "16%",
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {
                                    //console.log("full",full)
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0].addonServiceType != undefined && full.proposal[0].addonServiceType != null) {
                                        var name;
                                        if(full.proposal[0].addonServiceType == "pp_broker_advance"){
                                            name = "Broker advance";
                                        } else if(full.proposal[0].addonServiceType == "pp_broker_basic") {
                                            name = "Broker basic"
                                        }
                                        var option = angular.element('<ul class="margin-add-on promotions hide-when-mobile"></ul>');
                                        option.append('<li data-promotion="' + full.proposal[0].addonServiceType.toLowerCase() + '">' + name + '</li>');
                                        element.append(option);
                                    }
                                    if (full.transactionId != null) {
                                        element.append('<span> ' + full.transactionId + '</span>')
                                    }
                                    else {
                                        element.append('<span></span>')
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.serviceOrder != undefined) {
                                        element.append('<span>' + full.serviceOrder.id + '</span>');
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "class": "verticalMiddle hide-mobile ",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>' + full.serviceOrder.title.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "10%",
                                "class": "verticalMiddle text-center text-left-on-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0].addonServiceType != undefined && full.proposal[0].addonServiceType != null) {
                                        var name;
                                        if(full.proposal[0].addonServiceType == "pp_broker_advance"){
                                            name = "Broker advance";
                                        } else if(full.proposal[0].addonServiceType == "pp_broker_basic") {
                                            name = "Broker basic"
                                        }
                                        var option = angular.element('<ul class="margin-add-on promotions hide-when-desktop"></ul>');
                                        option.append('<li data-promotion="' + full.proposal[0].addonServiceType.toLowerCase() + '">' + name + '</li>');
                                        element.append(option);
                                    }
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    var mobile = angular.element("<div class='hide-when-desktop'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="body"></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="col-xs-8 no-padding footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-4 no-padding footer-pull-right-mobile"></div>');
                                    if (full.proposal != undefined) {
                                        if (full.proposal.length > 0) {
                                            desktop.append('<span>' + full.proposal[0].id + '</span>');
                                        }
                                    }
                                    header.append('<div class="col-xs-8 no-margin no-padding"><span>' + full.serviceOrder.id + ' - ' + full.serviceOrder.title + '</span></div><div class="col-xs-4 no-padding">' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</div>');
                                    //}
                                    if (full.serviceOrder != undefined) {
                                        pullLeft.append('<span>' + full.serviceOrder.aliasName + '</span>');
                                    }
                                    if (full.status === 0) {
                                        pullRight.append('<span data-i18n="wait payment"></span>');
                                    }
                                    if (full.status === 1) {
                                        pullRight.append('<span data-i18n="paid"></span>');
                                    }
                                    if (full.status === 2) {
                                        pullRight.append('<span data-i18n="failed paid"></span>');
                                    }
                                    //}
                                    body.append(pullLeft);
                                    body.append(pullRight);
                                    mobile.append(header);
                                    //mobile.append(content);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "class": "verticalMiddle ",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0] != undefined) {
                                        element.append('<span>' + full.proposal[0].proposal.replace(/\r\n|\r|\n/g, "<br />") + '</span>');

                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "15%",
                                "class": "text-center verticalMiddle text-right",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.proposal[0] != undefined) {
                                        if (full.proposal[0].expectedCost !== null) {
                                            element.append('<span>' + full.proposal[0].expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                        } else {
                                            element.append('<span></span>');
                                        }
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "class": "verticalMiddle text-center",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");

                                    element.append('<span>' + full.serviceOrder.aliasName + '</span>');

                                    return element.html();

                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "class": "verticalMiddle text-right",
                                "render": function (data, type, full) {

                                    var element = angular.element("<div></div>");
                                    if (full.totalPayment != undefined || full.totalPayment != null) {
                                        element.append('<span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                    }
                                    else {
                                        full.totalPayment = 0;
                                        element.append('<span>' + full.totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;₫' + '</span>');
                                    }
                                    return element.html();

                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "class": "verticalMiddle hide-mobile text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.status === 0) {
                                        element.append('<span data-i18n="wait payment"></span>');
                                    }
                                    if (full.status === 1) {
                                        element.append('<span data-i18n="paid"></span>');
                                    }
                                    if (full.status === 2) {
                                        element.append('<span data-i18n="failed paid"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 9,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.transactionId != null) {
                                        if (full.proposal[0]) {
                                            element.append('<div id="' + full.proposal[0].id + '"><a class="pull-right btn btn-sm btn-gap btn-primary no-border" data-ng-click="$parent.viewContactDetail(aData.proposal[0].id)"><span data-i18n="view contact detail">Thông tin liên lạc</span></a></div>');
                                            element.append('<div id="' + full.proposal[0].id + 'tel"></div>');
                                            element.append('<div id="' + full.proposal[0].id + 'email"></div>');
                                        }
                                        //element.append('<a href="#/payment/' + full.id + '" class=" pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="detail payment"></span></a>');
                                        element.append('<a data-ng-click="$parent.viewpayment(aData.id)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="detail payment"></span></a>');
                                    }
                                    return element.html();
                                }
                            }
                        ]
                    };
                }

                $scope.viewpayment = function (paymentID) {
                    navigations.push({
                        title: "transaction info",
                        path: "payment",
                        params: paymentID,
                        hasChildrenInMobile: false,
                        isClick: true
                    });
                }

                $scope.viewContact = function (aData) {
                        modalInstance = $modal.open({
                            templateUrl: "app/partials/form/viewContact.html",
                            controller: 'viewContactCtrl',
                            resolve: {
                                data: function () {
                                    return aData
                                }

                            }
                        });
                };
                module.controller('viewContactCtrl', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data) {
                        $scope.isSupplier = true;
                        $scope.isBroker = true;
                        var id;
                        if(data.userRole == "broker"){
                            id = data.supplierId;
                            $scope.isSupplier = false;
                        } else if (data.userRole == "supplier"){
                            id = data.brokerId;
                            $scope.isBroker = false;
                        }
                        api.get('user/'+id+'/v1').then(function (res) {
                            console.log("res", res)
                            if (res.data.result) {
                                $scope.aliasName = res.data.user.aliasName;
                                $scope.phoneNo = res.data.user.phoneNumber;
                                $scope.email = res.data.user.email;

                            } else {
                                logger.logError(res.data.error.description);
                            }
                        })
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.cancel = function () {
                            $modalInstance.close();
                        }
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };

                    }
                ]);

                $scope.viewContactDetail = function (proposalID) {
                    if (windowsize.devicetype() == "Desktop") {
                        modalInstance = $modal.open({
                            templateUrl: "app/partials/form/viewContactDetail.html",
                            controller: 'viewContactDetailCtrl',
                            resolve: {
                                idProposal: function () {
                                    return proposalID
                                }

                            }
                        });
                    }
                    else {
                        api.get('proposal/' + proposalID + '/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole]).then(function (res) {

                            if (res.data.result) {
                                $scope.hasContact = false;
                                $scope.customerTel = res.data.proposal.customerPhoneNumber
                                $scope.customerEmail = res.data.proposal.customerEmail
                                $("#" + proposalID).addClass("non-display")
                                $("#" + proposalID + "tel").append('<a href="tel:' + $scope.customerTel + '" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span class="glyphicon glyphicon-earphone"></span>&nbsp;<span data-i18n="contact">Liên hệ</span></a>')
                                $("#" + proposalID + "email").append('<a href="mailto:' + $scope.customerEmail + '" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span class="glyphicon glyphicon-envelope"></span>&nbsp;<span data-i18n="contact">Liên hệ</span></a>')
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        })
                    }
                };
                module.controller('viewContactDetailCtrl', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idProposal',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idProposal) {
                        api.get('proposal/' + idProposal + '/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole]).then(function (res) {
                            console.log("res", res)
                            if (res.data.result) {
                                $scope.serviceOrderTitle = res.data.proposal.serviceOrderTitle;
                                $scope.expectedCost = res.data.proposal.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                $scope.aliasName = res.data.proposal.customerAliasName;
                                $scope.phoneNo = res.data.proposal.customerPhoneNumber;
                                $scope.email = res.data.proposal.customerEmail;

                            } else {
                                logger.logError(res.data.error.description);
                            }
                        })
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.cancel = function () {
                            $modalInstance.close();
                        }
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };

                    }
                ]);
                $scope.hoveringOver = function (value) {
                    $scope.overStar = value;
                };
                $scope.rating = function (score, customer) {
                    rating.candidate = customer;
                    rating.voter = Session.userId;
                    rating.mark = score;
                    api.post('rating/v1', rating).then(function (res) {
                        if (res.data.result) {
                        }
                        else {
                            logger.logError(res.data.error.description);
                        }
                    });
                };
                $scope.$on("window.width", function (object, type) {
                    var rowDetail = $element.find(".row-detail");
                    rowDetail.remove();
                    var detailShow = $element.find(".detail-show");
                    detailShow.removeClass("detail-show");
                    detailShow.removeClass("active");
                });
                $scope.showDetail = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        var columnValue = hiddenIsNotSkill.find(".columnValue");
                        var hiddenIsNotBoldRed = rowDetail.find(".removeBoldRed");
                        var columnValueBoldRed = hiddenIsNotBoldRed.find(".boldRed");
                        columnValueBoldRed.removeClass("boldRed")
                        var hiddenIsNull = rowDetail.find(".hiddenIsNull");
                        var columnValueNull = hiddenIsNull.find(".columnValue");
                        var hiddenExpectedCost = columnValueNull.find("span")
                        if (columnValue.text() == "" || columnValue.text() == "null") {
                            hiddenIsNotSkill.addClass("non-display");
                        }
                        if (hiddenExpectedCost.text() == "" || hiddenExpectedCost.text() == "null") {
                            hiddenIsNull.addClass("non-display");
                        }
                        classHideMobile.addClass("non-display");

                    }

                }

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var td = rowDetail.find("td");
                    var ul = rowDetail.find("ul");
                    var li = rowDetail.find("li");
                    var lastLi = li.last();
                    var body = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                    var contentLeft = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                    var contentRight = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                    if (li != undefined && li.length > 0) {
                        li.each(function () {
                            if (!$(this).is(':last-child')) {
                                var content = angular.element("<div class='col-md-12 col-sm-12 no-padding wrap-numberonly'></div>");
                                var columnTitle = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                                var columnValue = angular.element("<div class='col-md-9 col-sm-9'></div>");
                                if ($(this).hasClass("hide-mobile")) {
                                    columnTitle.addClass("hide-mobile");
                                    columnValue.addClass("hide-mobile");
                                }
                                var title = $(this).find("span").closest(".columnTitle");
                                title.removeClass("col-md-2");
                                title.addClass("col-md-12");
                                title.addClass("col-sm-12");
                                var value = $(this).find(".columnValue");
                                if (value.text() == "") {
                                    title.addClass("non-display");
                                    value.addClass("non-display");
                                }
                                value.removeClass("col-md-2");
                                value.addClass("col-md-12");
                                value.addClass("col-sm-12");
                                columnTitle.append(title);
                                columnValue.append(value);
                                content.append(columnTitle);
                                content.append(columnValue);
                                contentLeft.append(content);
                            }

                        });
                        var valueLastLi = lastLi.find(".columnValue");
                        contentRight.append(valueLastLi);
                        body.append(contentLeft);
                        body.append(contentRight);
                        ul.addClass("non-display");
                        td.append(body);
                        if ($(window).width() == 768) {
                            var classHideMobile = rowDetail.find(".hide-mobile");
                            classHideMobile.addClass("non-display");
                        }
                    }
                }
                $scope.hideContent = function (index, value, element) {
                    if (windowsize.devicetype() != "Desktop") {
                        $(".body").removeClass("non-display");
                        $('[data-column = 1]').addClass("non-display");
                        var body = element.find(".body");
                        body.addClass("non-display");
                    } else {
                        if ($(window).width() > 1024) {
                            var rowDetail = element.parent("tbody").find(".row-detail");
                            var li = rowDetail.find("li");
                            li.find(".columnTitle").addClass("non-display");
                            var firstLi = li.first();
                            var lastLi = li.last();
                            var valueLastLi = lastLi.find(".columnValue");
                            valueLastLi.removeClass('col-md-10');
                            valueLastLi.addClass('col-md-2');
                            firstLi.append(valueLastLi);
                        }
                    }

                }
                $scope.reloadNewOrder = function () {
                    if ($scope.newNotification > 0) {
                        $scope.newNotification = 0
                        $scope.hasNotification = false;
                        getNewOrder();
                        $scope.newOrder.url = new String("serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=0,1,3,4,5,6");

                    }
                    else {
                        getNewOrder();
                    }
                };
                $scope.reloadAwaitingPayment = function () {
                    if ($scope.newNotificationPayment > 0) {
                        $scope.newNotificationPayment = 0
                        $scope.hasNotificationPayment = false;
                        getAwaitingPayment();
                        $scope.awaitingPayment.url = new String("serviceorder/v1?username=" + Session.userId + "&role=" + USER_ROLES[Session.userRole] + "&pstatus=2");
                    }
                    else {
                        getAwaitingPayment();
                    }
                };
                $scope.reloadBrokerAssignedList = function () {
                    getbrokerAssignedDefault();
                };
                $scope.reloadAwaitingPaymentBroker = function () {
                    if ($scope.newNotificationPayment > 0) {
                        $scope.newNotificationPayment = 0
                        $scope.hasNotificationPayment = false;
                        getAwaitingPaymentBroker();
                        $scope.awaitingPaymentBroker.url = new String("proposal/v1?userType=B&pstatus=2");
                    }
                    else {
                        getAwaitingPaymentBroker();
                    }

                };
                $scope.reloadFinishOrderBroker = function () {
                    getFinishOrderBroker();
                };

                $scope.reloadFinishOrder = function () {
                    getFinishOrder();
                };
                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };
                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };
                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };
                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };
                $scope.selectedRow = function (value, element) {
                    if (windowsize.devicetype() !== 'Desktop') {
                        $location.path("/viewdetailpost");
                    } else {
                        var rowDetail = element.next('.row-detail');
                        var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                        var compiledContents = $compile(content);
                        compiledContents($scope, function (clone) {
                            $('ul', rowDetail).append(clone);
                        });
                    }


                };
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);

    });

