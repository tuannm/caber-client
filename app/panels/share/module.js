/**
 * Created by Admin on 5/4/2015.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.share', []);
        app.useModule(module);

        module.controller('share', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults

                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                };
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);
    });