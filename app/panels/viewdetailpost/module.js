/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';

        var module = angular.module('bidproject.panels.viewdetailpost', []);
        app.useModule(module);

        module.controller('viewdetailpost', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'localize', '$modal', '$location', 'api', 'logger', 'Session', 'USER_ROLES', 'navigations', '$window', 'notification', 'store',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, localize, $modal, $location, api, logger, Session, USER_ROLES, navigations, $window, notification, store) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);

                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    $scope.isOpen = false;
                    loadViewDetailPost();
                    if (windowsize.devicetype() == 'Desktop') {
                        getProposal();
                        if(navigations.current.title == "view bidding"){
                            var newIndex = navigations.collections.indexOf(navigations.current);
                            if (newIndex > -1) {
                                navigations.collections.splice(newIndex, 1);
                            }
                            navigations.current = navigations.collections[newIndex -1];
                        }
                    }
                    if ($(window).width() == 768) {
                        $scope.isTablet = true;
                    }

                    if(navigations.current.isCallBack == undefined){
                        $("#titleDetail").removeClass("hide");
                        $("#contentDetail").removeClass("hide");
                        $("#btnBidinfo").removeClass("hide");

                    }
                };
                for (var i = 0; i < notification.messages.length; i++) {
                    if ($stateParams.params == notification.messages[i].data_id) {
                        if (!notification.messages[i].is_read) {
                            $rootScope.socket.emit('updateNotification', [notification.messages[i]._id], Session.id);
                        }
                    }
                }
                $scope.oneAtATime = true;

                $scope.showInfo = function () {
                    var span = $("#showInfo").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-down")) {
                        $("#hideBrief").removeClass("non-display");
                        $("#viewBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-down");
                        $("#icon").addClass("glyphicon-chevron-up");
                        $("#detailInfo").slideDown();
                        $("#viewService").removeClass("non-display");
                        $("#hideServices").addClass("non-display");
                        $("#iconService").removeClass("glyphicon-chevron-up");
                        $("#iconService").addClass("glyphicon-chevron-down");
                        $("#updateOrder").slideUp();
                    } else {
                        $("#viewBrief").removeClass("non-display");
                        $("#hideBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-up");
                        $("#icon").addClass("glyphicon-chevron-down");
                        $("#detailInfo").slideUp();
                    }
                };

                $scope.showUpdateOrder = function () {
                    var span = $("#showUpdateOrder").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-down")) {
                        $("#hideServices").removeClass("non-display");
                        $("#viewService").addClass("non-display");
                        $("#iconService").removeClass("glyphicon-chevron-down");
                        $("#iconService").addClass("glyphicon-chevron-up");
                        $("#updateOrder").slideDown();
                        $("#viewBrief").removeClass("non-display");
                        $("#hideBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-up");
                        $("#icon").addClass("glyphicon-chevron-down");
                        $("#detailInfo").slideUp();
                    } else {
                        $("#viewService").removeClass("non-display");
                        $("#hideServices").addClass("non-display");
                        $("#iconService").removeClass("glyphicon-chevron-up");
                        $("#iconService").addClass("glyphicon-chevron-down");
                        $("#updateOrder").slideUp();
                    }
                };

                function loadViewDetailPost() {
                    if ($stateParams.params != undefined) {
                        api.get('serviceorder/' + $stateParams.params + '/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole] + '').then(function (res) {
                            if (res.data.result) {
                                if (res.data.serviceOrder.jsonData != null) {
                                    var dataMore = JSON.parse(res.data.serviceOrder.jsonData);
                                    $scope.titleViewdetailpost = " - " + res.data.serviceOrder.title;
                                    $scope.id = res.data.serviceOrder.id;
                                    if (res.data.serviceOrder.addonServices != undefined && res.data.serviceOrder.addonServices.length > 0) {
                                        if ($("#showUpdateOrder").hasClass("non-display")) {
                                            $("#showUpdateOrder").removeClass("non-display")
                                        }
                                        if ($("#upgradeOrderMobile").hasClass("non-display")) {
                                            $("#upgradeOrderMobile").removeClass("non-display")
                                        }
                                    }
                                    if (res.data.serviceOrder.totalProposalSubmitted == null) {
                                        res.data.serviceOrder.totalProposalSubmitted = 0;
                                    }
                                    if (res.data.serviceOrder.totalProposalAccepted == null) {
                                        res.data.serviceOrder.totalProposalAccepted = 0;
                                    }
                                    if (res.data.serviceOrder.totalProposalConfirmed == null) {
                                        res.data.serviceOrder.totalProposalConfirmed = 0;
                                    }
                                    if (res.data.serviceOrder.totalProposalClosed == null) {
                                        res.data.serviceOrder.totalProposalClosed = 0;
                                    }
                                    if (res.data.serviceOrder.totalProposalRejected == null) {
                                        res.data.serviceOrder.totalProposalRejected = 0;
                                    }
                                    $scope.proposalsMatched = parseInt(res.data.serviceOrder.totalProposalSubmitted + res.data.serviceOrder.totalProposalAccepted + res.data.serviceOrder.totalProposalConfirmed + res.data.serviceOrder.totalProposalRejected + res.data.serviceOrder.totalProposalClosed);
                                    $scope.proposalsAwaiting = res.data.serviceOrder.totalProposalSubmitted;
                                    $scope.proposalsAgreed = parseInt(res.data.serviceOrder.totalProposalAccepted + res.data.serviceOrder.totalProposalConfirmed + res.data.serviceOrder.totalProposalRejected);
                                    $scope.proposalSubmited = res.data.serviceOrder.totalProposalSubmitted;
                                    $scope.statusOrder = res.data.serviceOrder.status;
                                    var modalWarningPayment;
                                    if ($scope.proposalsMatched > 0) {
                                        $(".view-list-bidding").removeClass("non-display");
                                    }
                                    if (res.data.serviceOrder.status == 0) {
                                        if (res.data.serviceOrder.addonPayment != 0) {
                                            if ($(".active-status-opening").hasClass("non-display")) {
                                                $(".active-status-opening").removeClass("non-display")
                                            }
                                            if (!$(".active-status-bidding").hasClass("non-display")) {
                                                $(".active-status-bidding").addClass("non-display")
                                            }
                                            if (!$(".active-status-closed").hasClass("non-display")) {
                                                $(".active-status-closed").addClass("non-display")
                                            }
                                            //if (!$(".active-status-unpaid").hasClass("non-display")) {
                                            //    $(".active-status-unpaid").addClass("non-display")
                                            //}
                                            if (!$(".active-status-verifying").hasClass("non-display")) {
                                                $(".active-status-verifying").addClass("non-display")
                                            }
                                        } else {
                                            modalWarningPayment = $modal.open({
                                                templateUrl: "app/partials/form/warningPayment.html",
                                                controller: 'WarningPaymentDialogCtrl'
                                            });
                                            modalWarningPayment.result.then((function () {
                                                var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + $scope.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                                                $rootScope.paymentUrl(url);

                                            }), function () {
                                            });

                                            $scope.isOpen = true;
                                            var span = $("#showUpdateOrder").children(".glyphicon");
                                            span.removeClass("glyphicon-chevron-down");
                                            span.addClass("glyphicon-chevron-up");
                                            $("#hideServices").removeClass("non-display");
                                            $("#viewService").addClass("non-display");
                                            $("#updateOrder").slideDown();
                                            var totalPayment = 0;
                                            for (var i = 0; i < dataMore.addonService.length; i++) {
                                                if (dataMore.addonService[i].isSelected && dataMore.addonService[i].quantity != null) {
                                                    totalPayment += parseInt(dataMore.addonService[i].quantity * dataMore.addonService[i].serviceCost);
                                                }
                                            }

                                            $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";

                                            if ($(".payment-addon-service").hasClass("non-display")) {
                                                $(".payment-addon-service").removeClass("non-display")
                                            }
                                            //if ($(".active-status-unpaid").hasClass("non-display")) {
                                            //    $(".active-status-unpaid").removeClass("non-display")
                                            //}
                                            if (!$(".active-status-bidding").hasClass("non-display")) {
                                                $(".active-status-bidding").addClass("non-display")
                                            }
                                            if (!$(".active-status-closed").hasClass("non-display")) {
                                                $(".active-status-closed").addClass("non-display")
                                            }
                                            if ($(".active-status-opening").hasClass("non-display")) {
                                                $(".active-status-opening").removeClass("non-display")
                                            }
                                            if (!$(".active-status-verifying").hasClass("non-display")) {
                                                $(".active-status-verifying").addClass("non-display")
                                            }
                                        }

                                    } else if (res.data.serviceOrder.status == 1) {
                                        if ($scope.proposalsMatched > 0) {
                                            if (res.data.serviceOrder.addonPayment != 0) {
                                                if (!$(".active-status-opening").hasClass("non-display")) {
                                                    $(".active-status-opening").addClass("non-display")
                                                }
                                                if ($(".active-status-bidding").hasClass("non-display")) {
                                                    $(".active-status-bidding").removeClass("non-display")
                                                }
                                                if (!$(".active-status-closed").hasClass("non-display")) {
                                                    $(".active-status-closed").addClass("non-display")
                                                }
                                                //if (!$(".active-status-unpaid").hasClass("non-display")) {
                                                //    $(".active-status-unpaid").addClass("non-display")
                                                //}
                                                if (!$(".active-status-verifying").hasClass("non-display")) {
                                                    $(".active-status-verifying").addClass("non-display")
                                                }
                                            } else {
                                                modalWarningPayment = $modal.open({
                                                    templateUrl: "app/partials/form/warningPayment.html",
                                                    controller: 'WarningPaymentDialogCtrl'
                                                });
                                                modalWarningPayment.result.then((function () {
                                                    var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + $scope.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                                                    $rootScope.paymentUrl(url);

                                                }), function () {
                                                });
                                                $scope.isOpen = true;
                                                var span = $("#showUpdateOrder").children(".glyphicon");
                                                span.removeClass("glyphicon-chevron-down");
                                                span.addClass("glyphicon-chevron-up");
                                                $("#hideServices").removeClass("non-display");
                                                $("#viewService").addClass("non-display");
                                                $("#updateOrder").slideDown();
                                                var totalPayment = 0;
                                                for (var i = 0; i < dataMore.addonService.length; i++) {
                                                    if (dataMore.addonService[i].isSelected && dataMore.addonService[i].quantity != null) {
                                                        totalPayment += parseInt(dataMore.addonService[i].quantity * dataMore.addonService[i].serviceCost);
                                                    }
                                                }
                                                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                if ($(".payment-addon-service").hasClass("non-display")) {
                                                    $(".payment-addon-service").removeClass("non-display")
                                                }
                                                //if ($(".active-status-unpaid").hasClass("non-display")) {
                                                //    $(".active-status-unpaid").removeClass("non-display")
                                                //}
                                                if ($(".active-status-bidding").hasClass("non-display")) {
                                                    $(".active-status-bidding").removeClass("non-display")
                                                }
                                                if (!$(".active-status-closed").hasClass("non-display")) {
                                                    $(".active-status-closed").addClass("non-display")
                                                }
                                                if (!$(".active-status-opening").hasClass("non-display")) {
                                                    $(".active-status-opening").addClass("non-display")
                                                }
                                                if (!$(".active-status-verifying").hasClass("non-display")) {
                                                    $(".active-status-verifying").addClass("non-display")
                                                }
                                            }
                                        } else {
                                            if (res.data.serviceOrder.addonPayment != 0) {
                                                if ($(".active-status-opening").hasClass("non-display")) {
                                                    $(".active-status-opening").removeClass("non-display")
                                                }
                                                if (!$(".active-status-bidding").hasClass("non-display")) {
                                                    $(".active-status-bidding").addClass("non-display")
                                                }
                                                if (!$(".active-status-closed").hasClass("non-display")) {
                                                    $(".active-status-closed").addClass("non-display")
                                                }
                                                //if (!$(".active-status-unpaid").hasClass("non-display")) {
                                                //    $(".active-status-unpaid").addClass("non-display")
                                                //}
                                                if (!$(".active-status-verifying").hasClass("non-display")) {
                                                    $(".active-status-verifying").addClass("non-display")
                                                }
                                            } else {
                                                modalWarningPayment = $modal.open({
                                                    templateUrl: "app/partials/form/warningPayment.html",
                                                    controller: 'WarningPaymentDialogCtrl'
                                                });
                                                modalWarningPayment.result.then((function () {
                                                    var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + $scope.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                                                    $rootScope.paymentUrl(url);
                                                    ;

                                                }), function () {
                                                });
                                                $scope.isOpen = true;
                                                var span = $("#showUpdateOrder").children(".glyphicon");
                                                span.removeClass("glyphicon-chevron-down");
                                                span.addClass("glyphicon-chevron-up");
                                                $("#hideServices").removeClass("non-display");
                                                $("#viewService").addClass("non-display");
                                                $("#updateOrder").slideDown();
                                                var totalPayment = 0;
                                                for (var i = 0; i < dataMore.addonService.length; i++) {
                                                    if (dataMore.addonService[i].isSelected && dataMore.addonService[i].quantity != null) {
                                                        totalPayment += parseInt(dataMore.addonService[i].quantity * dataMore.addonService[i].serviceCost);
                                                    }
                                                }
                                                $scope.totalPayment = totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";

                                                if ($(".payment-addon-service").hasClass("non-display")) {
                                                    $(".payment-addon-service").removeClass("non-display")
                                                }
                                                //if ($(".active-status-unpaid").hasClass("non-display")) {
                                                //    $(".active-status-unpaid").removeClass("non-display")
                                                //}
                                                if (!$(".active-status-bidding").hasClass("non-display")) {
                                                    $(".active-status-bidding").addClass("non-display")
                                                }
                                                if (!$(".active-status-closed").hasClass("non-display")) {
                                                    $(".active-status-closed").addClass("non-display")
                                                }
                                                if ($(".active-status-opening").hasClass("non-display")) {
                                                    $(".active-status-opening").removeClass("non-display")
                                                }
                                                if (!$(".active-status-verifying").hasClass("non-display")) {
                                                    $(".active-status-verifying").addClass("non-display")
                                                }
                                            }

                                        }
                                    } else if (res.data.serviceOrder.status == 2) {
                                        if (!$(".active-status-opening").hasClass("non-display")) {
                                            $(".active-status-opening").addClass("non-display")
                                        }
                                        if (!$(".active-status-bidding").hasClass("non-display")) {
                                            $(".active-status-bidding").addClass("non-display")
                                        }
                                        if ($(".active-status-closed").hasClass("non-display")) {
                                            $(".active-status-closed").removeClass("non-display")
                                        }
                                        if (!$(".active-status-unpaid").hasClass("non-display")) {
                                            $(".active-status-unpaid").addClass("non-display")
                                        }
                                        if (!$(".active-status-verifying").hasClass("non-display")) {
                                            $(".active-status-verifying").addClass("non-display")
                                        }

                                    } else if (res.data.serviceOrder.status == 3) {
                                        if (!$(".active-status-opening").hasClass("non-display")) {
                                            $(".active-status-opening").addClass("non-display")
                                        }
                                        if (!$(".active-status-bidding").hasClass("non-display")) {
                                            $(".active-status-bidding").addClass("non-display")
                                        }
                                        if ($(".active-status-verifying").hasClass("non-display")) {
                                            $(".active-status-verifying").removeClass("non-display")
                                        }
                                        if (!$(".active-status-unpaid").hasClass("non-display")) {
                                            $(".active-status-unpaid").addClass("non-display")
                                        }
                                        if (!$(".active-status-closed").hasClass("non-display")) {
                                            $(".active-status-closed").addClass("non-display")
                                        }

                                    }

                                    if (res.data.serviceOrder.budgetFrom != null) {
                                        $scope.postDeal = res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                    }
                                    $scope.city = dataMore.city.name;
                                    var isMore = false;

                                    if (dataMore.information != undefined && dataMore.information.length > 0) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                isMore = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (isMore) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                if (dataMore.information[i].id == 4) {
                                                    $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].description + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫' + '</span>');

                                                } else {
                                                    $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].name + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫' + '</span>');

                                                }

                                            }
                                        }

                                        if ($(".show-headhunt").hasClass("non-display")) {
                                            $(".show-headhunt").removeClass("non-display");
                                        }
                                        if (!$(".hide-headhunt").hasClass("non-display")) {
                                            $(".hide-headhunt").addClass("non-display");
                                        }

                                    } else {
                                        if ($(".hide-headhunt").hasClass("non-display")) {
                                            $(".hide-headhunt").removeClass("non-display");
                                        }
                                        if (!$(".show-headhunt").hasClass("non-display")) {
                                            $(".show-headhunt").addClass("non-display");
                                        }
                                    }


                                    $scope.services = res.data.serviceOrder.serviceType.display;
                                    $scope.servicesType = res.data.serviceOrder.serviceType;
                                    $scope.currency = res.data.serviceOrder.currency.name;
                                    $scope.description = res.data.serviceOrder.content.replace(/\r\n|\r|\n/g, "<br />");
                                    var isMore = false;
                                    if (dataMore.information != undefined && dataMore.information.length > 0) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                isMore = true;
                                                break;
                                            }
                                        }
                                    }
                                    var serviceType = "";
                                    if (isMore) {
                                        var i;
                                        for (i = 0; i < dataMore.information.length; i++) {
                                            if (dataMore.information[i].isSelected) {
                                                serviceType += dataMore.information[i].name + ", ";
                                            }
                                        }

                                    } else {
                                        serviceType = res.data.serviceOrder.serviceType.display;
                                    }
                                    $scope.serviceType = serviceType;
                                    if (res.data.serviceOrder.skill != null) {
                                        var skills = res.data.serviceOrder.skill.toString().split(",");
                                        var skill = "";
                                        for (var i = 0; i < skills.length; i++) {
                                            if (i != skills.length - 1) {
                                                skill += skills[i].toString() + ", ";
                                            } else {
                                                skill += skills[i].toString();
                                            }
                                        }
                                        $scope.skills = skill;
                                        if ($(".hide-is-not-content").hasClass("non-display")) {
                                            $(".hide-is-not-content").removeClass("non-display");
                                        }

                                    } else {
                                        if (!$(".hide-is-not-content").hasClass("non-display")) {
                                            $(".hide-is-not-content").addClass("non-display");
                                        }
                                    }

                                    if ((res.data.serviceOrder.status == 1 || res.data.serviceOrder.status == 0) && res.data.serviceOrder.totalProposalSubmitted > 0) {
                                        $("#btnPayment").removeClass("non-display");
                                    } else {
                                        if (!$("#btnPayment").hasClass("non-display")) {
                                            $("#btnPayment").addClass("non-display");
                                        }
                                    }

                                    if (res.data.serviceOrder.status == 0 || res.data.serviceOrder.status == 1) {
                                        $(".is-so-close").removeClass("non-display");
                                    }
                                    if (res.data.serviceOrder.expiredDate != null) {
                                        var arrDateTime = res.data.serviceOrder.expiredDate.toString().split("T");
                                        var arrDate = arrDateTime[0].toString().split("-");
                                        var day = arrDate[2].toString();
                                        var month = arrDate[1].toString();
                                        var year = arrDate[0].toString();
                                        if (day.length < 2) {
                                            day = "0" + day;
                                        }
                                        if (month.length < 2) {
                                            month = "0" + month;
                                        }
                                        $scope.expiredDate = day + '/' + month + '/' + year;
                                    }

                                    if (res.data.serviceOrder.serviceType.id == 3) {
                                        if ($(".show-hide-helper").hasClass("non-display")) {
                                            $(".show-hide-helper").removeClass("non-display");
                                        }
                                        if (dataMore.workType == null) {
                                            $(".helper-work-type").addClass("non-display");
                                        }
                                        $scope.workType = dataMore.workType;
                                        var dateHelper = "";
                                        if (dataMore.dateStart != null) {
                                            var date = new Date(dataMore.dateStart);
                                            var day = "";
                                            var month = "";

                                            if (String(date.getDate()).length < 2) {
                                                day = "0" + (date.getDate());
                                            } else {
                                                day = date.getDate();
                                            }
                                            if (String(date.getMonth() + 1).length < 2) {
                                                month = "0" + (date.getMonth() + 1);
                                            } else {
                                                month = (date.getMonth() + 1);
                                            }
                                            dateHelper = day + "/" + month + "/" + date.getFullYear();
                                        }
                                        var timeStartHelper = "";
                                        if (dataMore.timeStart != null) {
                                            var date = new Date(dataMore.timeStart);
                                            var time = new Date(date.setHours(date.getHours() - 5));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeStartHelper = hours + ":" + minutes;
                                        }
                                        var timeFinishHelper = "";
                                        if (dataMore.timeFinish != null) {
                                            var date = new Date(dataMore.timeFinish);
                                            var time = new Date(date.setHours(date.getHours() - 5));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeFinishHelper = hours + ":" + minutes;
                                        }
                                        if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                            $(".helper-start-date-time").addClass("non-display");
                                        }
                                        if (timeStartHelper == "" || timeFinishHelper == "") {
                                            $scope.startDateHelper = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                        } else {
                                            $scope.startDateHelper = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                        }
                                        if (dataMore.salary != null) {
                                            $scope.postCurrencyHelper = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                        } else {
                                            $(".helper-salary").addClass("non-display");
                                        }
                                        if (dataMore.position == null) {
                                            $(".helper-location").addClass("non-display");
                                        }
                                        $scope.location = dataMore.position;
                                    } else {
                                        if (!$(".show-hide-helper").hasClass("non-display")) {
                                            $(".show-hide-helper").addClass("non-display");
                                        }
                                    }

                                    if (res.data.serviceOrder.serviceType.id == 1) {
                                        if ($(".show-hide-quick").hasClass("non-display")) {
                                            $(".show-hide-quick").removeClass("non-display");
                                        }
                                        if (dataMore.workType == null) {
                                            $(".quick-work-type").addClass("non-display");
                                        }
                                        $scope.servicesMore = dataMore.workType;
                                        var dateHelper = "";
                                        if (dataMore.dateStart != null) {
                                            var date = new Date(dataMore.dateStart);
                                            var day = "";
                                            var month = "";

                                            if (String(date.getDate()).length < 2) {
                                                day = "0" + (date.getDate());
                                            } else {
                                                day = date.getDate();
                                            }
                                            if (String(date.getMonth() + 1).length < 2) {
                                                month = "0" + (date.getMonth() + 1);
                                            } else {
                                                month = (date.getMonth() + 1);
                                            }
                                            dateHelper = day + "/" + month + "/" + date.getFullYear();
                                        }
                                        var timeStartHelper = "";
                                        if (dataMore.timeStart != null) {
                                            var date = new Date(dataMore.timeStart);
                                            var time = new Date(date.setHours(date.getHours() - 5));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeStartHelper = hours + ":" + minutes;
                                        }
                                        var timeFinishHelper = "";
                                        if (dataMore.timeFinish != null) {
                                            var date = new Date(dataMore.timeFinish);
                                            var time = new Date(date.setHours(date.getHours() - 5));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeFinishHelper = hours + ":" + minutes;
                                        }
                                        if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                            $(".quick-start-date-time").addClass("non-display");
                                        }
                                        if (timeStartHelper == "" || timeFinishHelper == "") {
                                            $scope.startDateQuick = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                        } else {
                                            $scope.startDateQuick = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                        }
                                        if (dataMore.salary != null) {
                                            $scope.priceService = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                        } else {
                                            $(".quick-salary").addClass("non-display");
                                        }
                                        if (dataMore.position == null) {
                                            $(".quick-location").addClass("non-display");
                                        }
                                        $scope.location = dataMore.position;
                                    } else {
                                        if (!$(".show-hide-quick").hasClass("non-display")) {
                                            $(".show-hide-quick").addClass("non-display");
                                        }
                                    }

                                    if (res.data.serviceOrder.serviceType.id == 5) {
                                        if ($(".show-hide-tutoring").hasClass("non-display")) {
                                            $(".show-hide-tutoring").removeClass("non-display");
                                        }

                                        var dateHelper = "";
                                        if (dataMore.dateStart != null) {
                                            var date = new Date(dataMore.dateStart);
                                            var day = "";
                                            var month = "";

                                            if (String(date.getDate()).length < 2) {
                                                day = "0" + (date.getDate());
                                            } else {
                                                day = date.getDate();
                                            }
                                            if (String(date.getMonth() + 1).length < 2) {
                                                month = "0" + (date.getMonth() + 1);
                                            } else {
                                                month = (date.getMonth() + 1);
                                            }
                                            dateHelper = day + "/" + month + "/" + date.getFullYear();
                                        }
                                        var timeStartHelper = "";
                                        if (dataMore.timeStart != null) {
                                            var date = new Date(dataMore.timeStart);
                                            var time = new Date(date.setHours(date.getHours()));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeStartHelper = hours + ":" + minutes;
                                        }
                                        var timeFinishHelper = "";
                                        if (dataMore.timeFinish != null) {
                                            var date = new Date(dataMore.timeFinish);
                                            var time = new Date(date.setHours(date.getHours()));
                                            var hours = time.getHours();
                                            var minutes = "";

                                            if (String(hours).length < 2) {
                                                hours = "0" + hours;
                                            }
                                            if (String(date.getMinutes()).length < 2) {
                                                minutes = "0" + (date.getMinutes());
                                            } else {
                                                minutes = (date.getMinutes());
                                            }
                                            timeFinishHelper = hours + ":" + minutes;
                                        }
                                        if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                            $(".tutoring-start-date-time").addClass("non-display");
                                        }
                                        if (timeStartHelper == "" || timeFinishHelper == "") {

                                            $scope.startDate = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                        } else {
                                            $scope.startDate = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                        }
                                        if (dataMore.sex == "" || dataMore.sex == null) {
                                            $(".tutoring-gender").addClass("non-display");
                                        }
                                        if (dataMore.career == "" || dataMore.career == null) {
                                            $(".tutoring-career").addClass("non-display");
                                        }
                                        $scope.gender = dataMore.sex;
                                        $scope.career = dataMore.career;
                                        if (dataMore.salary != null) {
                                            $scope.salaryTutoring = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                        } else {
                                            $(".tutoring-salary").addClass("non-display");
                                        }
                                        if (dataMore.position == null) {
                                            $(".tutoring-location").addClass("non-display");
                                        }
                                        else {
                                            $scope.location = dataMore.position;
                                        }
                                    } else {
                                        if (!$(".show-hide-tutoring").hasClass("non-display")) {
                                            $(".show-hide-tutoring").addClass("non-display");
                                        }
                                    }

                                    $scope.custom = {
                                        "aaData": dataMore.addonService,
                                        "paging": false,
                                        "columns": [
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"},
                                            {"data": "null"}
                                        ],
                                        "columnDefs": [
                                            {
                                                "targets": 0,
                                                "data": null,
                                                "width": "2%",
                                                "class": "id",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<label class="ui-checkbox"><input name="checkboxSV"  ng-model="aData.isSelected"  type="checkbox" onclick="return false"><span></span></label>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 1,
                                                "data": null,
                                                "width": "5%",
                                                "class": "name",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span class="label label-warning">' + full.name + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 2,
                                                "data": null,
                                                "width": "40%",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span>' + full.description + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 3,
                                                "data": null,
                                                "width": "20%",
                                                "class": "text-right",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    var formatServiceCost = "";
                                                    if (full.serviceCost != null) {
                                                        formatServiceCost = full.serviceCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                    }
                                                    var formatServiceCostSalse = "";
                                                    if (full.serviceOldCost != null) {
                                                        formatServiceCostSalse = full.serviceOldCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                                    }
                                                    element.append('<span class="price_old">' + formatServiceCostSalse + '</span>' + '<br/><span class="price">' + formatServiceCost + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 4,
                                                "data": null,
                                                "width": "10%",
                                                "class": "quantity",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<span data-ng-show="aData.quantity == null">0</span>');
                                                    element.append('<span data-ng-show="aData.quantity != null">' + full.quantity + '</span>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 5,
                                                "data": null,
                                                "width": "15%",
                                                "class": "text-right",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    var totalPayment = 0;
                                                    if (full.isSelected && full.quantity != null) {
                                                        totalPayment += parseInt(full.quantity * full.serviceCost);
                                                    }
                                                    element.append('<span>' + totalPayment.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫</span>');
                                                    return element.html();
                                                }
                                            }
                                        ]
                                    };
                                }
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });
                    }
                    else {
                        $scope.isValue = true;
                    }
                }

                function getProposal() {
                    $scope.userBid = {
                        "url": "proposal/v1?serviceOrderId=" + $stateParams.params,
                        "remote_data": "proposals",
                        lang:{
                            vi:{
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                "sEmptyTable": "Không tìm thấy ứng viên nào phù hợp",
                                "sZeroRecords": "Không tìm thấy ứng viên nào phù hợp",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ ứng viên",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 ứng viên",
                                "sInfoFiltered": "(được lọc từ _MAX_ ứng viên)",
                                "sInfoPostFix": "",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en:{
                                "sEmptyTable":     "No data available in table",
                                "sInfo":           "Showing _START_ to _END_ of _TOTAL_ candidate",
                                "sInfoEmpty":      "Showing 0 to 0 of 0 candidate",
                                "sInfoFiltered":   "(filtered from _MAX_ total candidate)",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing":     "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords":    "No matching records found",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            //{"data": "proposal"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "class": "text-center hide-mobile",
                                "width": "10%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span>' + full.id + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "30%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    var content = angular.element("<div></div>");
                                    var name = angular.element("<div class='col-lg-12 no-padding'></div>");
                                    var description = angular.element("<div class='col-lg-12 no-padding'></div>");
                                    var dataJson;
                                    if (full.jsonData != null) {
                                        if (typeof full.jsonData == "object") {
                                            dataJson = full.jsonData;
                                        } else {
                                            full.jsonData = full.jsonData.replace(/&quot;/g, '"')
                                            dataJson = JSON.parse(full.jsonData);
                                        }
                                        name.append('<span>' + dataJson.title + '</span>');
                                    }

                                    content.append(name);
                                    desktop.append(content);
                                    element.append(desktop);

                                    var mobile = angular.element("<div class='hide-when-desktop no-padding col-xs-12'></div>");
                                    var bodyLeft = angular.element("<div class='col-xs-9 no-padding'></div>");
                                    var bodyRight = angular.element("<div class='bodyRight pull-right col-xs-3 no-padding text-right'></div>");
                                    var header = angular.element('<div class="col-xs-12 no-padding"></div>');
                                    var headerTitle = angular.element('<div class="header-tiltle col-xs-12 no-padding"></div>');
                                    var headerRating = angular.element('<div class="col-xs-12 no-padding"></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="pull-left footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="pull-right footer-pull-right-mobile"></div>');
                                    headerTitle.append('<span>' + full.id + ' - ' + dataJson.title + '</span>');
                                    //if (full.jsonData != null) {
                                    //    var dataJson;
                                    //    if(typeof full.jsonData == "object"){
                                    //        dataJson = full.jsonData;
                                    //    } else{
                                    //        dataJson =  JSON.parse(full.jsonData);
                                    //    }
                                    //
                                    //}
                                    content.append('<span>' + full.proposal + '</span>');
                                    if (full.expectedCost != null) {
                                        pullLeft.append('<span class="col-xs-12 no-padding" data-ng-show="aData.expectedCost != null && aData.currency != null">' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;' + ' ₫</span>');
                                    }
                                    //if (full.jsonData != null) {
                                    //    var dataJson = JSON.parse(full.jsonData);
                                    //    if (dataJson.cv.length > 0 && dataJson.cv != undefined) {
                                    //       // pullLeft.append('<span class="col-xs-12 no-padding hide-768" data-i18n="list cv"></span>');
                                    //    }
                                    //
                                    //
                                    //}


                                    headerRating.append(
                                        '<div class="col-xs-12 no-padding"><span>' + full.aliasName + '</span></div><div class="col-xs-12 no-padding"><rating class="ui-rating size-h6 ui-rating-warning"  ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div><div data-ng-show="aData.status == 3 || aData.status == 4 || aData.status == 5"><a data-ng-click="$parent.selectionSupRating(aData)" class="btn btn-xs btn-primary no-border"><span data-i18n="rating"></span></a></div>'
                                    );
                                    if ($scope.selection.indexOf(full.id) != -1) {
                                        full.isChecked = true;
                                    } else {
                                        full.isChecked = false;
                                    }
                                    bodyRight.append('<label class="ui-checkbox choose-proposal" ng-show="aData.status == 1"><input name="checkbox"  ng-checked="aData.isChecked"  ng-model="aData.isChecked" data-ng-click="$parent.toggleSelection(aData)" type="checkbox"><span></span></label>');
                                    bodyRight.append('<span ng-show="aData.status == 2" data-i18n="awarded"></span>');
                                    bodyRight.append('<span ng-show="aData.status == 3" data-i18n="complete"></span>');
                                    bodyRight.append('<span ng-show="aData.status == 4" data-i18n="transaction failed"></span>');
                                    bodyRight.append('<span ng-show="aData.status == 5" data-i18n="closed"></span>');
                                    //bodyRight.append('<span data-ng-show="aData.status == 2" data-i18n="awarded"></span>');
                                    //bodyRight.append('<span ng-show="aData.status == 3" data-i18n="complete"></span>');
                                    //bodyRight.append('<span ng-show="aData.status == 4" data-i18n="transaction failed"></span>');
                                    //bodyRight.append('<span ng-show="aData.status == 5" data-i18n="rejected"></span>');

                                    header.append(headerTitle);
                                    header.append(headerRating);
                                    bodyLeft.append(header);
                                    //bodyLeft.append(content);
                                    bodyLeft.append(pullLeft);
                                    bodyLeft.append(pullRight);
                                    mobile.append(bodyLeft);
                                    mobile.append(bodyRight);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "class": "text-right hide-mobile",
                                "width": "20%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.expectedCost != null) {
                                        element.append('<span data-ng-show="aData.expectedCost != null && aData.currency != null">' + full.expectedCost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '&nbsp;' + ' ₫</span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "class": "text-center hide-mobile",
                                "width": "25%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<div class="col-md-12 no-padding">' + full.aliasName + '</div><div class="col-md-12"><rating class="ui-rating size-h5 ui-rating-warning"  ng-model="aData.ratingMark" max="5" readonly="true" state-on="\'fa fa-star\'" state-off="\'fa fa-star-o\'"></rating></div>');
                                    element.append('<div ng-show="aData.status == 3 || aData.status == 4 || aData.status == 5" class="col-md-6 col-md-offset-3 no-padding"><a data-ng-click="$parent.selectionSupRating(aData)" class="btn btn-xs btn-primary no-border col-md-12"><span data-i18n="rating"></span></a></div>');
                                    return element.html();
                                }
                            },

                            {
                                "targets": 4,
                                "data": null,
                                "class": "",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    //element.append('<a class="action jqvmap-region" data-ng-click="$parent.showCvPopup(1)"><span>Nguyễn Văn A</span></a>');
                                    element.append('<span>' + full.proposal.replace(/\r\n|\r|\n/g, "<br />") + '</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "class": "hiddenIsNotSkill",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");

                                    if (full.jsonData != null) {
                                        if (typeof full.jsonData != "object") {
                                            full.jsonData = JSON.parse(full.jsonData);
                                        }
                                        if (full.jsonData.cv != undefined) {
                                            if (full.jsonData.cv.length > 0) {
                                                element.append('' + full.jsonData.cv.length + ' <span data-i18n="candidate"></span>');
                                            }
                                        }

                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "class": "hiddenIsNotServiceType",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");

                                    if (full.jsonData != null) {
                                        if (typeof full.jsonData != "object") {
                                            full.jsonData = JSON.parse(full.jsonData);
                                        }
                                        if (full.jsonData.serviceType.id != undefined) {
                                            element.append('' + full.jsonData.serviceType.description + '');
                                        }

                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "class": "text-center action-selection hide-mobile",
                                "width": "15%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if ($scope.selection.indexOf(full.id) != -1) {
                                        full.isChecked = true;
                                    } else {
                                        full.isChecked = false;
                                    }
                                    element.append('<label class="ui-checkbox" ng-show="aData.status == 1"><input name="checkbox"  ng-checked="aData.isChecked"  ng-model="aData.isChecked" data-ng-click="$parent.toggleSelection(aData)" type="checkbox"><span></span></label>');
                                    element.append('<span ng-show="aData.status == 2" data-i18n="awarded"></span>');
                                    element.append('<span ng-show="aData.status == 3" data-i18n="complete"></span>');
                                    element.append('<span ng-show="aData.status == 4" data-i18n="transaction failed"></span>');
                                    element.append('<span ng-show="aData.status == 5" data-i18n="closed"></span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 8,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div class='col-md-12'></div>");
                                    element.append('<div class="col-md-12 no-padding"><a data-ng-click="$parent.viewdetailproposal(aData)" class="pull-right btn btn-sm btn-gap btn-primary no-border"><span data-i18n="view detail"></span></a></div>');
                                    return element.html();
                                }
                            }
                        ]
                    };
                }

                $scope.viewdetailproposal = function (aData) {
                    var tableUserBib = $('#tbUserBid').dataTable().fnGetData();
                    var isAwardContinue = false;
                    for (var i = 0; i < tableUserBib.length; i++) {
                        if (tableUserBib[i].status == 2 || tableUserBib[i].status == 3) {
                            isAwardContinue = true;
                            break;
                        }
                    }

                    var data = {};
                    data.proposal = aData;
                    data.serviceOrderID = $scope.id;
                    data.serviceOrderTitle = $scope.titleViewdetailpost;
                    data.isAwardContinue = isAwardContinue;
                    data.serviceType = $scope.servicesType;
                    localStorage.setItem("dataProposal", JSON.stringify(data));
                    $location.path("viewdetailproposal");

                }


                var modalShowListCV;
                $scope.showListCV = function (dataCV) {
                    modalShowListCV = $modal.open({
                        templateUrl: "app/partials/form/showListCV.html",
                        controller: 'ShowListCVDialogCtrl',
                        resolve: {
                            data: function () {
                                return dataCV;
                            }
                        }
                    });
                    modalShowListCV.result.then((function (rate) {

                    }), function () {
                    });
                }

                $scope.previous = function () {
                    $("#listCV").addClass("hide");
                    listUserBid.removeClass("hide");
                    contents.removeClass("hide");
                    contents1.removeClass("hide");
                    $("#btnPayment").removeClass("hide");
                }

                $scope.viewListCV = function (data) {
                    $("#listCV").removeClass("hide");
                    listUserBid.addClass("hide");
                    contents.addClass("hide");
                    contents1.addClass("hide");
                    if ($(window).width() == 768) {
                        $("#btnPayment").addClass("hide");
                    }
                    navigations.push({
                        title: "view detail cv",
                        path: $stateParams.routeId + "/" + $stateParams.params,
                        backCallback: function () {
                            listUserBid.removeClass("hide");
                            $("#listCV").addClass("hide");
                            // contents.removeClass("hide");
                            //contents1.removeClass("hide");
                        },
                        actionCallback: function () {

                        }
                    });
                    $scope.listCV = {
                        "aaData": data.cv,
                        "paging": false,
                        "scrollCollapse": true,
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "yearofexperience", "width": "10%", "class": "text-center"},
                            {"data": "skill", "width": "25%", "class": "text-center"},
                            {"data": "dob"},
                            {"data": "null"},
                            {"data": "goal"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "11%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div>Ứng viên {{iDataIndex + 1}}</div>");
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "12%",
                                "class": "text-center",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div><span data-i18n='{{aData.sex}}'></span></div>");
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "12%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div><span data-i18n='{{aData.startDate}}'></span></div>");
                                    return element.html();
                                }
                            }

                        ]
                    };
                    $("#listCV").removeClass("non-display");
                }

                var tabContent = $element.find("ul.nav-tabs");
                var contents = $element.find("#titleDetail");
                var contents1 = $element.find("#contentDetail");
                var listUserBid = $element.find("#listUserBid");
                tabContent.removeClass("nav-tabs");
                if (windowsize.devicetype() != 'Desktop') {
                    var tabClass = "col-xs-12 btn btn-primary btn-gap";
                    var position = 0;
                    listUserBid.addClass("hide");
                    var tabs = tabContent.find("li");
                    if (navigations.current.isCallBack == true) {
                        $timeout(function () {
                            $(tabs[navigations.current.index]).click()
                            var newIndex = navigations.collections.indexOf(navigations.current);
                            if (newIndex > -1) {
                                navigations.collections.splice(newIndex, 1);
                            }
                        }, 1000);
                    }
                    else {
                        $(tabs[0]).click()
                    }

                    $(tabs).on("click", function (event) {
                        if ($scope.proposalSubmited > 0 && $scope.statusOrder != 2) {
                            $timeout(function () {
                                navigations.push({
                                    title: $('a span', tabs).last().attr("data-i18n") || $('a span', tabs).last().html(),
                                    path: $stateParams.routeId,
                                    params: $stateParams.params,
                                    isCallBack: true,
                                    index: 0,
                                    actionLabel: "<img class='img30_30 position-img' src='images/pop.png'>",
                                    backCallback: function () {
                                        $("li", tabContent).removeClass("hide");
                                        $("li", tabContent).removeClass("active");
                                        $(contents).removeClass("hide");
                                        $(contents1).removeClass("hide");
                                        $("#btnBidinfo").removeClass("hide");
                                        $(listUserBid).addClass("hide");
                                        $("#listCV").addClass("hide");
                                    },
                                    actionCallback: function () {
                                        $scope.$apply(function () {
                                            getProposal();
                                            $("li", tabContent).addClass("hide");
                                            $(contents).addClass("hide");
                                            $(contents1).addClass("hide");
                                            $(listUserBid).removeClass("hide");
                                        });
                                    },
                                    actionClick: function () {
                                        $scope.submitAwardOrder();
                                    }
                                });
                            }, 0);

                        }
                        else {
                            $timeout(function () {
                                navigations.push({
                                    title: $('a span', tabs).last().attr("data-i18n") || $('a span', tabs).last().html(),
                                    path: $stateParams.routeId,
                                    params: $stateParams.params,
                                    isCallBack: true,
                                    index: 0,
                                    backCallback: function () {
                                        $("li", tabContent).removeClass("hide");
                                        $("li", tabContent).removeClass("active");
                                        $("#btnBidinfo").removeClass("hide");
                                        $(contents).removeClass("hide");
                                        $(contents1).removeClass("hide");
                                        $(listUserBid).addClass("hide");
                                        $("#listCV").addClass("hide");

                                    },
                                    actionCallback: function () {
                                        $scope.$apply(function () {
                                            getProposal();
                                            $("li", tabContent).addClass("hide");
                                            $(contents).addClass("hide");
                                            $(contents1).addClass("hide");
                                            $(listUserBid).removeClass("hide");
                                        })
                                    }
                                });
                            }, 0);
                        }
                    });
                    $(tabs).removeClass("active");
                    $(tabs).addClass(tabClass);

                }

                $scope.$on('window.size', function (object, type) {
                    if (type !== 'Desktop') {
                        var tabClass = "col-xs-12 btn btn-primary btn-gap";
                        var position = 0;
                        listUserBid.addClass("hide");
                        //contents.removeClass("hide");
                        //contents1.removeClass("hide");
                        //$("li", tabContent).removeClass("hide");
                        var tabs = tabContent.find("li");
                        if (navigations.current.isCallBack == true) {
                            $timeout(function () {
                                $(tabs[navigations.current.index]).click()
                                var newIndex = navigations.collections.indexOf(navigations.current);
                                if (newIndex > -1) {
                                    navigations.collections.splice(newIndex, 1);
                                }
                            }, 1000);
                        }
                        else {
                            $(tabs[0]).click()
                        }

                        $(tabs).on("click", function (event) {
                            if ($scope.proposalSubmited > 0 && $scope.statusOrder != 2) {
                                $timeout(function () {
                                    navigations.push({
                                        title: $('a span', tabs).last().attr("data-i18n") || $('a span', tabs).last().html(),
                                        path: $stateParams.routeId,
                                        params: $stateParams.params,
                                        isCallBack: true,
                                        index: 0,
                                        backCallback: function () {
                                            $("li", tabContent).removeClass("hide");
                                            $("li", tabContent).removeClass("active");
                                            $(contents).removeClass("hide");
                                            $(contents1).removeClass("hide");
                                            $(listUserBid).addClass("hide");
                                            $("#listCV").addClass("hide");
                                        },
                                        actionCallback: function () {
                                            $scope.$apply(function () {
                                                getProposal();
                                                $("li", tabContent).addClass("hide");
                                                $(contents).addClass("hide");
                                                $(contents1).addClass("hide");
                                                $(listUserBid).removeClass("hide");

                                            });
                                        },
                                        actionLabel: "<img class='img30_30 position-img' src='images/pop.png'>",
                                        actionClick: function () {
                                            $scope.submitAwardOrder();
                                        }
                                    });
                                }, 0);

                            }
                            else {
                                $timeout(function () {
                                    navigations.push({
                                        title: $('a span', tabs).last().attr("data-i18n") || $('a span', tabs).last().html(),
                                        path: $stateParams.routeId,
                                        params: $stateParams.params,
                                        isCallBack: true,
                                        index: 0,
                                        backCallback: function () {
                                            $("li", tabContent).removeClass("hide");
                                            $("li", tabContent).removeClass("active");
                                            $(contents).removeClass("hide");
                                            $(contents1).removeClass("hide");
                                            $(listUserBid).addClass("hide");
                                            $("#listCV").addClass("hide");

                                        },
                                        actionCallback: function () {
                                            $scope.$apply(function () {
                                                getProposal();
                                                $("li", tabContent).addClass("hide");
                                                $(contents).addClass("hide");
                                                $(contents1).addClass("hide");
                                                $(listUserBid).removeClass("hide");
                                            })
                                        }
                                    });
                                }, 0);
                            }
                        });
                        $(tabs).removeClass("active");
                        $(tabs).addClass(tabClass);

                    } else {
                        if(navigations.current.title == "view bidding"){
                            var newIndex = navigations.collections.indexOf(navigations.current);
                            if (newIndex > -1) {
                                navigations.collections.splice(newIndex, 1);
                            }
                            navigations.current = navigations.collections[newIndex -1];
                        }
                        contents.removeClass("hide");
                        getProposal();
                        $timeout(function () {
                            listUserBid.removeClass("hide");
                        }, 3000);
                    }
                });


                $scope.hrefPayment = function () {
                    var url = config.admin + "payment?paymentType=AO&username=" + Session.userId + "&serviceOrderId=" + $scope.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                    $rootScope.paymentUrl(url);
                }

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var td = rowDetail.find("td");
                    var ul = rowDetail.find("ul");
                    var li = rowDetail.find("li");
                    var lastLi = li.last();
                    var body = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                    var contentLeft = angular.element("<div class='col-md-9 col-sm-9 no-padding'></div>");
                    var contentRight = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                    if (li != undefined && li.length > 0) {
                        li.each(function () {
                            if (!$(this).is(':last-child')) {
                                var content = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                                var columnTitle = angular.element("<div class='col-md-3 col-sm-3 no-padding'></div>");
                                var columnValue = angular.element("<div class='col-md-9 col-sm-9 colValue'></div>");
                                if ($(this).hasClass("hide-mobile")) {
                                    columnTitle.addClass("hide-mobile");
                                    columnValue.addClass("hide-mobile");
                                }
                                var spanValue = $(this).find(".columnValue");
                                spanValue.removeClass("col-md-10");
                                spanValue.addClass("col-md-12");
                                //var tbListCV = spanValue.find(".tbListCV");
                                var title = $(this).find("span").closest(".columnTitle");
                                title.removeClass("col-md-2");
                                title.addClass("col-md-12");
                                title.addClass("col-sm-12");
                                var value = $(this).find(".columnValue");
                                if (value.text() == "") {
                                    title.addClass("non-display");
                                    value.addClass("non-display");
                                }
                                value.removeClass("col-md-2");
                                value.addClass("col-md-12");
                                value.addClass("col-sm-12");
                                columnTitle.append(title);
                                columnValue.append(value);
                                content.append(columnTitle);
                                content.append(columnValue);
                                contentLeft.append(content);
                            }
                            //if(tbListCV.length > 0){
                            //    var parentListCV = tbListCV.closest(".colValue");
                            //    parentListCV.removeClass("col-md-9");
                            //    parentListCV.removeClass("col-sm-9");
                            //    parentListCV.addClass("col-md-12");
                            //    parentListCV.addClass("col-sm-12");
                            //}

                        });
                        var valueLastLi = lastLi.find(".columnValue");
                        contentRight.append(valueLastLi);
                        body.append(contentLeft);
                        body.append(contentRight);
                        ul.addClass("non-display");
                        td.append(body);
                        if ($(window).width() == 768) {
                            var classHideMobile = rowDetail.find(".hide-mobile");
                            classHideMobile.addClass("non-display");
                            //rowDetail.remove();
                        }
                    }
                }

                $scope.selection = [];
                $scope.toggleSelection = function (aData) {
                    var idx = $scope.selection.indexOf(aData.id);
                    if (idx > -1) {
                        $scope.selection.splice(idx, 1);
                    }
                    else {
                        $scope.selection.push(aData.id);
                    }
                    console.log("$scope.selection",$scope.selection);
                };


                $scope.selectionSupRating = function (supplier) {
                    $scope.currentData = supplier;
                    var modalRating;
                    modalRating = $modal.open({
                        templateUrl: "app/partials/form/ratingUser.html",
                        controller: 'RatingDialogCtrl',
                        resolve: {
                            data: function () {
                                return $scope.currentData;
                            }
                        }
                    });
                    modalRating.result.then((function (rate) {
                        $scope.currentData.ratingMark = rate;
                        $scope.currentData.currentRate = true;
                    }), function () {
                    });


                };

                $scope.selectionProposal = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        //value.isChecked = !value.isChecked;
                        //$scope.toggleSelection(value);
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        var columnValue = hiddenIsNotSkill.find(".columnValue");
                        if (columnValue.text() == "") {
                            hiddenIsNotSkill.addClass("non-display");
                        }
                        classHideMobile.addClass("non-display");
                        var hiddenIsNotServiceType = rowDetail.find(".hiddenIsNotServiceType");
                        var columnValueServiceType = hiddenIsNotServiceType.find(".columnValue");
                        if (columnValueServiceType.text() == "") {
                            hiddenIsNotServiceType.addClass("non-display");
                        }

                    }
                }

                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };

                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };

                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };

                var modalInstance;
                $scope.submitAwardOrder = function () {
                    var tableUserBib = $('#tbUserBid').dataTable().fnGetData();
                    var isAwardContinue = false;
                    for (var i = 0; i < tableUserBib.length; i++) {
                        if (tableUserBib[i].status == 2 || tableUserBib[i].status == 3) {
                            isAwardContinue = true;
                            break;
                        }
                    }


                    var serviceOrder = {};
                    serviceOrder.id = $scope.id;
                    serviceOrder.proposalSelection = $scope.selection;
                    serviceOrder.isAwardContinue = isAwardContinue;

                    if ($scope.selection != undefined && $scope.selection.length > 0) {
                        modalInstance = $modal.open({
                            templateUrl: "app/partials/form/payment.html",
                            controller: 'PaymentDialogCtrl',
                            resolve: {
                                data: function () {
                                    return serviceOrder;
                                }
                            }
                        });
                        modalInstance.result.then((function () {
                            var proposalId = $scope.selection.toString();
                            var url = config.admin + "payment?paymentType=SO&username=" + Session.userId + "&serviceOrderId=" + $scope.id + "&proposalIds=" + proposalId + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                            $rootScope.paymentUrl(url);
                        }), function () {
                        });
                    } else {
                        logger.logWarning("Bạn hãy chọn ít nhất 1 người để trao yêu cầu");
                    }

                };

                $scope.$on("window.width", function (object, type) {
                    var rowDetail = $element.find(".row-detail");
                    rowDetail.remove();
                    var detailShow = $element.find(".detail-show");
                    detailShow.removeClass("detail-show");
                    if (type > 768) {
                        detailShow.removeClass("active");
                    }
                    if (type < 769 && modalShowListCV !== undefined) {
                        modalShowListCV.dismiss("cancel");
                    }
                });

                $scope.$on('window.size', function (object, type) {
                    if (type !== 'Desktop' && modalInstance !== undefined) {
                        modalInstance.dismiss("cancel");
                    }
                });

                $scope.deletePost = function (id) {
                    var deletePopup;
                    deletePopup = $modal.open({
                        templateUrl: "app/partials/form/deletePost.html",
                        controller: 'DeleteDialogCtrl',
                        resolve: {
                            idPost: function () {
                                return id;
                            }
                        }
                    });
                    deletePopup.result.then((function () {
                        $location.path('detailpost');
                    }), function () {
                    });
                };

                $scope.extendPost = function (id) {
                    var extendPopup;
                    extendPopup = $modal.open({
                        templateUrl: "app/partials/form/extend.html",
                        controller: 'ExtendDialogCtrl',
                        resolve: {
                            idPost: function () {
                                return id;
                            }
                        }
                    });
                    extendPopup.result.then((function (expiredDate) {
                        var day = "";
                        var month = "";

                        if (String(expiredDate.getDate()).length < 2) {
                            day = "0" + (expiredDate.getDate());
                        } else {
                            day = expiredDate.getDate();
                        }
                        if (String(expiredDate.getMonth() + 1).length < 2) {
                            month = "0" + (expiredDate.getMonth() + 1);
                        } else {
                            month = (expiredDate.getMonth() + 1);
                        }
                        $scope.expiredDate = day + '/' + month + '/' + expiredDate.getFullYear();
                    }), function () {
                    });
                };

                $scope.editPost = function (id) {
                    $location.path("postedit/" + id);
                };

                $scope.closePost = function (id) {
                    var closePopup;
                    closePopup = $modal.open({
                        templateUrl: "app/partials/form/closePost.html",
                        controller: 'CloseDialogCtrl',
                        resolve: {
                            idPost: function () {
                                return id;
                            }
                        }
                    });
                    closePopup.result.then((function () {
                        $location.path("/detailpost");
                    }), function () {
                    });
                };

                $scope.showCvPopup = function (data) {
                    var CvPopup;
                    CvPopup = $modal.open({
                        templateUrl: "app/partials/form/cvPopup.html",
                        controller: 'CVDialogCtrl',
                        resolve: {
                            data: function () {
                                return data;
                            }
                        }
                    });
                    CvPopup.result.then((function () {

                    }), function () {
                    });
                };

            }]);
        module.controller('PaymentDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {

                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };

                $scope.init = function () {
                    loadPayment();
                };

                $scope.cancelAwardOrder = function () {
                    $modalInstance.dismiss("cancel");
                };

                function loadPayment() {
                    $scope.countSelection = data.proposalSelection.length;
                    if (data.isAwardContinue) {
                        $scope.vaberFee = "0";
                    } else {
                        $scope.vaberFee = "50,000";
                    }

                }

                $scope.confirmAwardOrder = function () {
                    //var awardOrder = {};
                    //awardOrder.username = Session.userId;
                    //awardOrder.serviceOrderId = data.id;
                    //awardOrder.status = 2;
                    //awardOrder.proposalIds = data.proposalSelection;
                    //api.put('proposal/v1', awardOrder).then(function (res) {
                    //    if (res.data.result) {
                    //        $modalInstance.close();
                    //    } else {
                    //        logger.logError(res.data.error.description);
                    //    }
                    //});
                    $modalInstance.close();

                };

            }
        ]);
        module.controller('ExtendDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idPost',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idPost) {
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.init = function () {
                    loadExtent();
                    $scope.extent_time_expired = new Date();
                    $scope.hstep = 1;
                    $scope.mstep = 1;
                    $scope.ismeridian = false;
                };
                $scope.toggleMode = function () {
                    $scope.ismeridian = !$scope.ismeridian;
                };
                $scope.disabled = function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                };
                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.opened = true;
                };
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];

                $scope.wizardCompleteCallback = function (wizardData) {
                    if ($scope.expiredDate != undefined) {
                        var dateString = $scope.expiredDate.toDateString();
                        var date = new Date();
                        var setHours = date.setHours(23, 59, 59);
                        var timeString = new Date(setHours).toTimeString();
                        var expiredDate = new Date(dateString + " " + timeString);
                        var post = {};
                        post.expiredDate = expiredDate;
                        api.put('serviceorder/' + idPost + '/v1', post).then(function (res) {
                            if (res.data.result) {
                                $modalInstance.close(expiredDate);
                                $scope.bidEndDate = $scope.expiredDate;
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });

                    }
                };
                $scope.cancelExtend = function () {
                    $modalInstance.dismiss("cancel");
                };

                function loadExtent() {
                    api.get('serviceorder/' + idPost + '/v1').then(function (res) {
                        if (res.data.result) {
                            var arrDateTime = res.data.serviceOrder.expiredDate.toString().split("T");
                            var arrDate = arrDateTime[0].toString().split("-");
                            var day = arrDate[2].toString();
                            var month = arrDate[1].toString();
                            var year = arrDate[0].toString();
                            if (day.length < 2) {
                                day = "0" + day;
                            }
                            if (month.length < 2) {
                                month = "0" + month;
                            }
                            $scope.bidEndDate = day + '/' + month + '/' + year;
                            $scope.minDate = new Date();
                        } else {
                            logger.logError(res.data.error.description);
                        }


                    });

                }
            }
        ]);
        module.controller('CloseDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idPost', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idPost, Session) {
                $scope.confirmClose = function () {
                    var serviceOrder = {};
                    serviceOrder.username = Session.userId;
                    serviceOrder.status = "2";
                    api.put('serviceorder/' + idPost + '/v1', serviceOrder).then(function (res) {
                        if (res.data.result) {
                            $modalInstance.close();
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });

                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.cancelClose = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);
        module.controller('DeleteDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idPost',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idPost) {
                $scope.confirmDelete = function () {
                    api.delete('serviceorder/' + idPost + '/v1').then(function (res) {
                        if (res.data.result) {
                            $modalInstance.close();
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });

                };
                $scope.cancelDelete = function () {
                    $modalInstance.dismiss("cancel");
                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
            }
        ]);
        module.controller('CVDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data) {
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
            }
        ]);
        module.controller('RatingDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {

                $scope.rate = {};
                $scope.rate.start = 0;
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.cancel = function () {
                    $modalInstance.dismiss("close");
                };

                $scope.getSetStatus = function () {
                    switch ($scope.rate.start) {
                        case 1:
                            if($(".very-bad").hasClass("non-display"))
                                $(".very-bad").removeClass("non-display");
                            if(!$(".bad").hasClass("non-display"))
                                $(".bad").addClass("non-display");
                            if(!$(".normal").hasClass("non-display"))
                                $(".normal").addClass("non-display");
                            if(!$(".good").hasClass("non-display"))
                                $(".good").addClass("non-display");
                            if(!$(".very-good").hasClass("non-display"))
                                $(".very-good").addClass("non-display");
                            break;
                        case 2:
                            if($(".bad").hasClass("non-display"))
                                $(".bad").removeClass("non-display");
                            if(!$(".very-bad").hasClass("non-display"))
                                $(".very-bad").addClass("non-display");
                            if(!$(".normal").hasClass("non-display"))
                                $(".normal").addClass("non-display");
                            if(!$(".good").hasClass("non-display"))
                                $(".good").addClass("non-display");
                            if(!$(".very-good").hasClass("non-display"))
                                $(".very-good").addClass("non-display");
                            break;
                        case 3:
                            if($(".normal").hasClass("non-display"))
                                $(".normal").removeClass("non-display");
                            if(!$(".very-bad").hasClass("non-display"))
                                $(".very-bad").addClass("non-display");
                            if(!$(".bad").hasClass("non-display"))
                                $(".bad").addClass("non-display");
                            if(!$(".good").hasClass("non-display"))
                                $(".good").addClass("non-display");
                            if(!$(".very-good").hasClass("non-display"))
                                $(".very-good").addClass("non-display");
                            break;
                        case 4:
                            if($(".good").hasClass("non-display"))
                                $(".good").removeClass("non-display");
                            if(!$(".very-bad").hasClass("non-display"))
                                $(".very-bad").addClass("non-display");
                            if(!$(".bad").hasClass("non-display"))
                                $(".bad").addClass("non-display");
                            if(!$(".normal").hasClass("non-display"))
                                $(".normal").addClass("non-display");
                            if(!$(".very-good").hasClass("non-display"))
                                $(".very-good").addClass("non-display");
                            break;
                        case 5:
                            if($(".very-good").hasClass("non-display"))
                                $(".very-good").removeClass("non-display");
                            if(!$(".very-bad").hasClass("non-display"))
                                $(".very-bad").addClass("non-display");
                            if(!$(".bad").hasClass("non-display"))
                                $(".bad").addClass("non-display");
                            if(!$(".normal").hasClass("non-display"))
                                $(".normal").addClass("non-display");
                            if(!$(".good").hasClass("non-display"))
                                $(".good").addClass("non-display");
                            break;
                    }
                }


                $scope.save = function () {
                    var rating = {};
                    var proposalRating = {};
                    rating.voter = Session.userId;
                    rating.role = USER_ROLES[Session.userRole];
                    rating.ratings = [];
                    proposalRating.id = data.id;
                    proposalRating.mark = $scope.rate.start;
                    proposalRating.comments = $scope.rate.comment;
                    rating.ratings.push(proposalRating);
                    api.post('rating/v1', rating).then(function (res) {
                        if (res.data.result) {
                            $modalInstance.close($scope.rate.start);
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });

                };
            }
        ]);
        module.controller('WarningPaymentDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger) {
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $scope.paymentNow = function () {
                    $modalInstance.close();
                };
                $scope.paymentLater = function () {
                    $modalInstance.dismiss("close");
                };
            }
        ]);
        module.controller('ShowListCVDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.listCV = {
                    "aaData": data,
                    "paging": false,
                    "scrollCollapse": true,
                    "columns": [
                        {"data": "null"},
                        {"data": "null"},
                        {"data": "yearofexperience", "width": "10%", "class": "text-center"},
                        {"data": "skill", "width": "25%", "class": "text-center"},
                        {"data": "dob"},
                        {"data": "null"},
                        {"data": "goal"}
                    ],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "width": "11%",
                            "class": "text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div>Ứng viên {{iDataIndex + 1}}</div>");
                                return element.html();
                            }
                        },
                        {
                            "targets": 1,
                            "data": null,
                            "width": "12%",
                            "class": "text-center",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div><span data-i18n='{{aData.sex}}'></span></div>");
                                return element.html();
                            }
                        },
                        {
                            "targets": 5,
                            "data": null,
                            "width": "12%",
                            "render": function (data, type, full, meta) {
                                var element = angular.element("<div><span data-i18n='{{aData.startDate}}'></span></div>");
                                return element.html();
                            }
                        }

                    ]
                };


            }
        ]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);

    });

