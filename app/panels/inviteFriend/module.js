/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.introduction', []);
        app.useModule(module);

        module.controller('inviteFriend', [ '$scope', '$rootScope','ezfb', '$window', function($scope, $rootScope, ezfb, $window) {
            $scope.panelMeta = {
                status  : "Stable",
                description : "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode    : "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content : "",
                style: {}
            };
            _.defaults($scope.panel,_d);

            var AppRate = window.AppRate;

            $scope.init = function() {
                if(AppRate){
                    AppRate.preferences.storeAppURL.ios = '1019060579';
                    AppRate.preferences.storeAppURL.android = 'market://details?id=vn.itsol.vaber';
                    AppRate.preferences.useLanguage = 'vi';
                }

                $scope.ready = true;
                if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
                    $scope.isIOS = true;
                }
                else if(navigator.userAgent.match(/(Android|BlackBerry|IEMobile)/))
                {
                    $scope.isAndroid =true;
                }
                else
                {
                    $scope.isIOS = true;
                    $scope.isAndroid =true;
                }
            };

            $scope.showRate = function(){
                if($("#rate").hasClass("hidden-xs")){
                    $("#rate").removeClass("hidden-xs");
                    $("#share").addClass("hidden-xs");
                } else{
                    $("#rate").addClass("hidden-xs");
                }

            }

            $scope.showShare = function(){
                if($("#share").hasClass("hidden-xs")){
                    $("#share").removeClass("hidden-xs");
                    $("#rate").addClass("hidden-xs");
                } else{
                    $("#share").addClass("hidden-xs");
                }

            }

            $scope.iosRate = function(){
                if($scope.isIOS && AppRate){
                    AppRate.promptForRating(true);
                }else{
                    $window.location.href = "https://itunes.apple.com/vn/app/vaber/id1019060579?mt=8";
                }
            }

            $scope.androidRate = function(){
                if($scope.isAndroid && AppRate){
                    AppRate.promptForRating(true);
                }else{
                    $window.location.href = "https://play.google.com/store/apps/details?id=vn.itsol.vaber";
                }
            }

            $scope.inviteFacebook = function () {
                ezfb.getLoginStatus(function (res) {

                    /**
                     * Calling FB.login with required permissions specified
                     * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
                     */
                    ezfb.login(function (res) {
                        console.log('res', res);
                        /**
                         * no manual $scope.$apply, I got that handled
                         */
                        if (res.authResponse) {

                            updateLoginStatus(updateApiMe);
                            if (top.location!= self.location) {
                                top.location = self.location.href
                            }
                        }
                    }, {scope: 'email,user_friends'});
                    function updateLoginStatus(more) {
                        ezfb.getLoginStatus(function (res) {
                            $scope.loginStatus = res;
                            console.log("res",res);
                            (more || angular.noop)();
                        });
                    }

                    function updateApiMe() {
                        ezfb.api('/me/friends', function (res) {

                            $scope.apiMe = res;
                            $scope.account = $scope.apiMe;
                            console.log(" $scope.apiMe", $scope.apiMe);
                            console.log("res",res);
                            ezfb.ui({

                                //method: 'send',
                                //name: 'doanhdk',
                                //to:'123',
                                //picture: 'http://pngamesmobile.info/images/upload/2015/05/07/1821-ww2.jpg',
                                //link: 'http://118.70.74.174/vaber',
                                //description: 'description'
                                //method: 'share',
                                //name: '[name]',
                                //picture: '[Picture URL]',
                                //link: 'http://118.70.74.174/vaber',
                                //description: '[description]'
                                //method: 'feed',
                                //link: 'http://118.70.74.174/vaber',
                                //caption: 'An example caption'
                                method: 'apprequests',

                                message: "Invite friends to Vaber.com",

                                title: "Who woulds you like to invite?"


                            });
                        });

                    }

                });
            }

        }]);
    });
