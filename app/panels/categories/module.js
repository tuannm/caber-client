/** @scratch /panels/5
 * include::panels/column.asciidoc[]
 */

/** @scratch /panels/column/0
 * == Column
 * Status: *Stable*
 *
 * A pseudo panel that lets you add other panels to be arranged in a column with defined heights.
 * While the column panel is stable, it does have many limitations, including the inability to drag
 * and drop panels within its borders. It may be removed in a future release.
 *
 */
define([
  'angular',
  'app',
  'lodash',
  'config',
  'css!./module.css'
],
function (angular, app, _, config) {
  'use strict';

  var module = angular.module('bidproject.panels.categories', []);

  app.useModule(module);

  module.controller('categories', [ '$scope', '$rootScope', '$timeout', '$stateParams', 'db',
  function($scope, $rootScope, $timeout, $stateParams, db) {
    $scope.panelMeta = {
      status  : "Stable",
      description : "A pseudo panel that lets you add other panels to be arranged in a column with"+
        "defined heights."
    };
    // Set and populate defaults
    var _d = {
      panels : []
    };
    _.defaults($scope.panel,_d);
    
    $scope.init = function(){
        $scope.ready = false;
        $scope.reset_panel();
            getProducts();
    };
    
    function getProducts(){
        db.get("categories.json").then(function(result) {
            if (result && result.data && result.data.length > 0) {
                for(var i = 0; i < result.data.length; i++){
                    
                    $scope.add_panel($scope.panel,{
                        "error": false,                    
                        "editable": true,
                        "draggable": false,
                        "group": [
                            "default"
                        ],
                        "type": "categories.item",
                        "status": "Stable",
                        "items" : result.data[i]
                    });
                    $scope.ready = true;
                    $scope.send_render();
                }
            }
        });
    }
    
    $scope.$on('render', function() {
        if(!$scope.$$phase) {
            $scope.$apply();
        }
    });

    $scope.toggle_row = function(panel) {
      panel.collapse = panel.collapse ? false : true;
      if (!panel.collapse) {
        $timeout(function() {
          $scope.send_render();
        });
      }
    };

    $scope.send_render = function() {
      $scope.$broadcast('render');
    };

    $scope.add_panel = function(panel,newPanel) {
      panel.panels.push(newPanel);
    };
	
    $scope.reset_panel = function(type) {
      $scope.new_panel = {
        loading: false,
        error: false,
        sizeable: false,
        draggable: false,
        removable: false,
        span: 12,
        height: "150px",
        editable: true,
        type: type
      };
    };

  }]);

  module.directive('markdown', [ '$compile', '$timeout', function($compile,$timeout) {
    return {
      scope : {
        new_panel:"=panel",
        row:"=",
        config:"=",
        dashboards:"=",
        type:"=type"
      },
      link: function(scope, elem) {
        scope.$on('render', function () {          
        });
      }
    };
  }]);

  module.filter('withoutContainer', [ function() {
    return function() {
      return _.without(config.panel_names,'container');
    };
  }]);
});