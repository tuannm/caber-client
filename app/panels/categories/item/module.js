/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
  'angular',
  'app',
  'lodash',
  'require'
],
function (angular, app, _, require) {
  'use strict';

  var module = angular.module('bidproject.panels.item', []);
  app.useModule(module);

  module.controller('item', [ '$scope', '$rootScope','$location', function($scope, $rootScope, $location) {
    $scope.panelMeta = {
      status  : "Stable",
      description : "A static text panel that can use plain text, markdown, or (sanitized) HTML"
    };

    // Set and populate defaults
    var _d = {
    };
    _.defaults($scope.panel,_d);

    $scope.init = function() {
      	$scope.ready = true;
        $scope.$on('render', function() {
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        });
    };
    
    
    
    $scope.onClick = function(id){
        $location.path('/product/'+id);
    }

  }]);
});
