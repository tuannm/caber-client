/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';
        var module = angular.module('bidproject.panels.detailAdvanceSearch', []);
        app.useModule(module);

        module.controller('detailAdvanceSearch', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', '$location', '$modal', 'store', 'USER_ROLES', 'navigations',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, $location, $modal, store, USER_ROLES, navigations) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                var urlCV = "";
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    getServiceDetail()
                    loadCurrency();
                    getSelectedCV();
                    getDescription();
                    $scope.listCVs = navigations.current.selectedParentCV
                    getSelectedCV($scope.listCVs)
                    inputDetailsOrder.cv = null;
                    //$scope.showTitleProposal = false;
                    //$scope.hideProposal = false;
                    //$scope.hidePrice = false;
                    //$('.btn-payment').hide();
                    //$('.btn-delete').show();
                    //$('.btn-bid').show();
                    //$('.btn-accept').hide();
                    //$('.btn-reject').hide();
                    if ($stateParams.params != null || $stateParams.params != null) {
                        urlCV = $stateParams.routeId.toString() + "/" + $stateParams.params
                    }
                    else {
                        urlCV = $stateParams.routeId
                    }
                };
                $scope.brokerList = [];
                $scope.getValueChecked = {};
                var brokerselected = {};

                function getDescription() {
                    api.get('user/v1?userType=B').then(function (res1) {
                        if (res1.data.result) {
                            $scope.brokerList = res1.data.brokers;
                            api.get('addonservice/v1').then(function (res) {
                                //var data;
                                //if ($scope.post.more.info != undefined) {
                                //    if($scope.post.isTemplate){
                                //        for (var i = 0; i < res.data.length; i++) {
                                //            for (var j = 0; j < $scope.post.more.info.length; j++) {
                                //                if(res.data[i].id == $scope.post.more.info[j].id){
                                //                    res.data[i].input = $scope.post.more.info[j].input;
                                //                    res.data[i].description = $scope.post.more.info[j].description;
                                //                    res.data[i].isSelected = true;
                                //                }
                                //            }
                                //        }
                                //        data = res.data;
                                //    } else{
                                //        data = $scope.post.more.info;
                                //    }
                                //} else {
                                //    data = res.data;
                                //}
                                for (var i = 0; i < res.data.addonServices.length; i++) {
                                    if (res.data.addonServices[i].id == 1) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 2) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 3) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                }

                                $scope.data = res.data.addonServices;
                                $scope.serviceTable = {
                                    "aaData": res.data.addonServices,
                                    "paging": false,
                                    "columns": [
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"}
                                    ],
                                    "columnDefs": [
                                        {
                                            "targets": 0,
                                            "data": null,
                                            "width": "2%",
                                            "class": "id",
                                            "render": function (data, type, full, meta) {
                                                if (!full.isSelected) {
                                                    full.isSelected = false;
                                                }
                                                var element = angular.element("<div></div>");
                                                element.append('<label class="ui-checkbox"><input id="aData.id" ng-model="aData.isSelected" ng-click="$parent.setCount(aData,$parent.data)" type="checkbox"><span></span></label>');
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 1,
                                            "data": null,
                                            "width": "7%",
                                            "class": "name text-center",
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append('<span class="label label-warning">' + full.name + '</span>');
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 2,
                                            "data": null,
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append(full.description);
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 3,
                                            "data": null,
                                            "width": "20%",
                                            "class": "text-center",
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append(full.serviceCost + "%");
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 4,
                                            "data": null,
                                            "width": "15%",
                                            "class": "quantity",
                                            "render": function (data, type, full, meta) {
                                                $scope.chosenBroker = $scope.brokerList[0]
                                                brokerselected = $scope.brokerList[0];
                                                var element = angular.element("<div></div>");
                                                element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><select  class="form-control width100percent" style="color:black" ng-model="$parent.chosenBroker" ng-options="broker.username for broker in $parent.brokerList" name="chosenBroker"  data-required data-message-required="validate choose city post" data-smart-validate-input></select></div>');
                                                return element.html();
                                            }
                                        }
                                    ]
                                };
                            });
                        } else {
                            logger.logError(res1.data.error.description);
                        }
                    })
                }

                $scope.setCount = function (aData, allData) {
                    for (var i = 0; i < allData.length; i++) {
                        if (allData[i].id != aData.id) {
                            allData[i].isSelected = false;
                            brokerselected = {};
                        }
                    }
                };
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()

                });
                $scope.$on('render', function () {
                    tabResponsive(windowsize.divicetype())

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };
                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };
                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };
                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };
                $scope.selectedRow = function (value, element) {
                    var rowDetail = element.next('.row-detail');
                    var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                    var compiledContents = $compile(content);
                    compiledContents($scope, function (clone) {
                        $('ul', rowDetail).append(clone);
                    });

                };

                $scope.vaberFee = "50,000";
                $scope.price = {}
                var inputDetails = store.get('inputDetailsOrder') ? store.get('inputDetailsOrder') : {};
                var bids = {};
                var idSer;
                var dataSer;
                var currency = {};
                var bid = $('[bid]', $element);
                var inputDetailsOrder = {};
                var modalInstance;
                var form = $("#formDetailOrder")
                var disable = false;
                var found = false;

                $scope.hideRadio = false;
                $scope.showTable = true;
                $scope.bids = {};
                $scope.proposalRes = {};
                $scope.proposalRes.types = [];
                $scope.proposalRes.budgetTo = [];
                $scope.listCVs = [];
                $scope.parentCv = [];
                $scope.selectedCV = [];
                $scope.showInfo = function () {
                    var span = $("#showInfo").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-down")) {
                        $("#hideBrief").removeClass("non-display");
                        $("#viewBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-down");
                        $("#icon").addClass("glyphicon-chevron-up");
                        $("#detailInfo").slideDown();
                    } else {
                        $("#viewBrief").removeClass("non-display");
                        $("#hideBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-up");
                        $("#icon").addClass("glyphicon-chevron-down");
                        $("#detailInfo").slideUp();
                    }
                };
                function getServiceDetail() {
                    $scope.price.net = inputDetails.expectedCost;
                    $scope.titleProposal = inputDetails.title;
                    //inputDetailsOrder.cv = $scope.listCVs;
                    $scope.bids.proposal = inputDetails.proposal;
                    api.get('serviceorder/' + $stateParams.params + '/v1?username= ' + Session.userId + ' &role= ' + USER_ROLES[Session.userRole] + '').then(function (res) {
                        if (res.data.result) {
                            dataSer = res.data.serviceOrder;
                            var id = res.data.serviceOrder.id

                            $scope.titleHeader = id.toString().concat(' - ', res.data.serviceOrder.title);
                            if (res.data.serviceOrder.totalBid === null) {
                                res.data.serviceOrder.totalBid = 0
                            }
                            $scope.proposalRes.bids = parseInt(res.data.serviceOrder.totalProposalSubmitted + res.data.serviceOrder.totalProposalAccepted + res.data.serviceOrder.totalProposalConfirmed + res.data.serviceOrder.totalProposalRejected + res.data.serviceOrder.totalProposalClosed);
                            if (res.data.serviceOrder.jsonData != null) {
                                dataMore = JSON.parse(res.data.serviceOrder.jsonData);

                                var isMore = false;
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {

                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                if (isMore) {
                                    var descriptionTable = [];
                                    $('#descriptionTableTitle').removeClass("non-display");
                                    $('#descriptionTable').removeClass("non-display");
                                    $scope.showTable = false;
                                    //$('#content-addCV').removeClass("non-display");
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            $scope.proposalRes.types.push(dataMore.information[i].name);
                                            dataMore.information[i].isChecked = false;
                                            descriptionTable.push(dataMore.information[i]);
                                            if (dataMore.information[i].input != null) {
                                                $scope.proposalRes.budgetTo.push(dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫");
                                            }
                                            else {
                                                $scope.proposalRes.budgetTo.push(0 + '₫');
                                            }
                                        }
                                    }
                                    if (inputDetails.serviceType != undefined && !jQuery.isEmptyObject(inputDetails.serviceType)) {
                                        $scope.serviceTypeChecked = inputDetails.serviceType.id;
                                        $scope.getValueChecked = inputDetails.serviceType
                                    }
                                    else {
                                        $scope.serviceTypeChecked = 1;
                                        $scope.getValueChecked = descriptionTable[0];
                                    }
                                    $scope.descriptionTable = {
                                        "aaData": descriptionTable,
                                        "ordering": false,
                                        "search": false,
                                        "paging": false,
                                        "filter": false,
                                        "info": false,
                                        "columns": [
                                            {"data": "null"},
                                            {"data": "null"}
                                        ],
                                        "columnDefs": [
                                            {
                                                "targets": 0,
                                                "data": null,
                                                "width": "2%",
                                                "class": "text-center width2percent",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    element.append('<label class="ui-checkbox text-center"><input name="radio"  ng-model="$parent.serviceTypeChecked" ng-disabled="$parent.hideRadio"   data-ng-click="$parent.setCheck(aData)" ng-value="aData.id"  type="radio"><span></span></label>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 1,
                                                "data": null,
                                                "width": "98%",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    if (full.name == 'Other') {

                                                        element.append('<strong>' + full.name + '  :     ' + full.description + '</strong>');
                                                    } else {
                                                        element.append('<strong>' + full.description + '</strong>');
                                                    }
                                                    return element.html();
                                                }
                                            },
                                        ]
                                    };
                                } else {
                                    $scope.proposalRes.types.push(res.data.serviceOrder.serviceType.display);
                                    if (res.data.serviceOrder.budgetFrom != null) {
                                        $scope.proposalRes.budgetTo.push(res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '₫');
                                    }

                                }
                            } else {
                                $scope.proposalRes.types.push(res.data.serviceOrder.serviceType.display);
                                if (res.data.serviceOrder.budgetFrom != null) {
                                    $scope.proposalRes.budgetTo.push(res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '₫');
                                }

                            }


                            $scope.setCheck = function (aData) {
                                if (jQuery.isEmptyObject($scope.getValueChecked)) {
                                    aData.isChecked = true;
                                    $scope.getValueChecked = aData;
                                } else {
                                    aData.isChecked = true;
                                    $scope.getValueChecked = null;
                                    $scope.getValueChecked = aData;
                                }
                            }
                            $scope.proposalRes.budgetCurrency = res.data.serviceOrder.currency.name;
                            if (res.data.serviceOrder.expiredDate != null) {

                                var date = new Date(res.data.serviceOrder.expiredDate);
                                var day = "";
                                var month = "";
                                var time = new Date(date.setHours(date.getHours() - 7));
                                var hours = time.getHours();
                                var minutes = "";
                                if (String(date.getDate()).length < 2) {
                                    day = "0" + (date.getDate());
                                } else {
                                    day = date.getDate();
                                }
                                if (String(date.getMonth() + 1).length < 2) {
                                    month = "0" + (date.getMonth() + 1);
                                } else {
                                    month = (date.getMonth() + 1);
                                }
                                if (String(hours).length < 2) {
                                    hours = "0" + hours;
                                }
                                if (String(date.getMinutes()).length < 2) {
                                    minutes = "0" + (date.getMinutes());
                                } else {
                                    minutes = (date.getMinutes());
                                }

                                //$scope.proposalRes.expiredDate = hours.toString().concat(":",minutes.toString(),'  ',day.toString(),'/',month.toString(),'/',date.getFullYear().toString())
                                $scope.proposalRes.expiredDate = day.toString().concat('/', month.toString(), '/', date.getFullYear().toString())
                            }
                            $scope.proposalRes.content = res.data.serviceOrder.content;
                            $scope.proposalRes.skill = res.data.serviceOrder.skill;
                            if (res.data.serviceOrder.status === 2) {
                                if ($(".active-status-closed").hasClass("non-display")) {
                                    $(".active-status-closed").removeClass("non-display")
                                }
                                //$('.btn-payment').hide();
                                //$('.btn-delete').hide();
                                //$('.btn-bid').hide();
                                //$('.btn-accept').hide();
                                //$('.btn-reject').hide();
                                $scope.hideRadio = true;
                                disable = true;
                                $scope.showTitleProposal = true;
                                $scope.hideProposal = true;
                                $scope.hidePrice = true;
                                $scope.hideSelect2 = true;
                                $('#descriptionTable').addClass("non-display");
                            }
                            else {
                                if ($(".active-status-opening").hasClass("non-display")) {
                                    $(".active-status-opening").removeClass("non-display")
                                }
                                $('#bid').removeClass("non-display");
                            }
                            if (res.data.serviceOrder.jsonData != null) {
                                var dataMore = JSON.parse(res.data.serviceOrder.jsonData);
                                if (res.data.serviceOrder.budgetFrom != null) {
                                    $scope.postDeal = res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " VNĐ";
                                }
                                $scope.city = dataMore.city.name;
                                var isMore = false;
                                var infoHeadhunt = "";
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                if (isMore) {

                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            if (dataMore.information[i].id == 4) {
                                                $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].description + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' VNĐ' + '</span>');

                                            } else {
                                                $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].name + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' VNĐ' + '</span>');

                                            }

                                        }
                                    }
                                    if ($(".show-headhunt").hasClass("non-display")) {
                                        $(".show-headhunt").removeClass("non-display");
                                    }
                                    if (!$(".hide-headhunt").hasClass("non-display")) {
                                        $(".hide-headhunt").addClass("non-display");
                                    }

                                } else {
                                    if ($(".hide-headhunt").hasClass("non-display")) {
                                        $(".hide-headhunt").removeClass("non-display");
                                    }
                                    if (!$(".show-headhunt").hasClass("non-display")) {
                                        $(".show-headhunt").addClass("non-display");
                                    }
                                }


                                $scope.services = res.data.serviceOrder.serviceType.display;
                                $scope.currency = res.data.serviceOrder.currency.name;
                                if (res.data.serviceOrder.content != null) {
                                    $scope.description = res.data.serviceOrder.content.replace(/\r\n|\r|\n/g, "<br />")
                                }
                                else {
                                    $(".show-description").addClass("non-display");
                                }
                                var isMore = false;
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                var serviceType = "";
                                if (isMore) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            serviceType += dataMore.information[i].name + ", ";
                                        }
                                    }

                                } else {
                                    serviceType = res.data.serviceOrder.serviceType.display;
                                }
                                $scope.serviceType = serviceType;
                                if (res.data.serviceOrder.skill != null) {
                                    var skills = res.data.serviceOrder.skill.toString().split(",");
                                    var skill = "";
                                    for (var i = 0; i < skills.length; i++) {
                                        if (i != skills.length - 1) {
                                            skill += skills[i].toString() + ", ";
                                        } else {
                                            skill += skills[i].toString();
                                        }
                                    }
                                    $scope.skills = skill;
                                    if ($(".hide-is-not-content").hasClass("non-display")) {
                                        $(".hide-is-not-content").removeClass("non-display");
                                    }

                                } else {
                                    if (!$(".hide-is-not-content").hasClass("non-display")) {
                                        $(".hide-is-not-content").addClass("non-display");
                                    }
                                }

                                var proposalsMatched = _.filter(res.data.serviceOrder.proposals, function (proposal) {
                                    return proposal.status == 1 && proposal.expectedCost != null && proposal.proposal != null;
                                });
                                if (res.data.serviceOrder.status == 1 && proposalsMatched.length > 0) {
                                    $("#btnPayment").removeClass("non-display");
                                } else {
                                    if (!$("#btnPayment").hasClass("non-display")) {
                                        $("#btnPayment").addClass("non-display");
                                    }
                                }


                                if (res.data.serviceOrder.status != 2) {
                                    $(".is-so-close").removeClass("non-display");
                                }
                                if (res.data.serviceOrder.expiredDate != null) {
                                    var date = new Date(res.data.serviceOrder.expiredDate);
                                    var day = "";
                                    var month = "";

                                    if (String(date.getDate()).length < 2) {
                                        day = "0" + (date.getDate());
                                    } else {
                                        day = date.getDate();
                                    }
                                    if (String(date.getMonth() + 1).length < 2) {
                                        month = "0" + (date.getMonth() + 1);
                                    } else {
                                        month = (date.getMonth() + 1);
                                    }
                                    $scope.expiredDate = day + '/' + month + '/' + date.getFullYear();
                                }

                                if (res.data.serviceOrder.serviceType.id == 3) {
                                    if ($(".show-hide-helper").hasClass("non-display")) {
                                        $(".show-hide-helper").removeClass("non-display");
                                    }
                                    if (dataMore.workType == null) {
                                        $(".helper-work-type").addClass("non-display");
                                    }
                                    $scope.workType = dataMore.workType;
                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".helper-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDateHelper = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDateHelper = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }

                                    if (dataMore.salary != null) {

                                        $scope.postCurrencyHelper = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " VNĐ";
                                    } else {
                                        $(".helper-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".helper-location").addClass("non-display");
                                    }
                                    $scope.location = dataMore.position;
                                } else {
                                    if (!$(".show-hide-helper").hasClass("non-display")) {
                                        $(".show-hide-helper").addClass("non-display");
                                    }
                                }
                                if (res.data.serviceOrder.serviceType.id == 1) {
                                    if ($(".show-hide-quick").hasClass("non-display")) {
                                        $(".show-hide-quick").removeClass("non-display");
                                    }
                                    if (dataMore.workType == null) {
                                        $(".quick-work-type").addClass("non-display");
                                    }
                                    $scope.servicesMore = dataMore.workType;
                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".quick-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDateQuick = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDateQuick = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }
                                    if (dataMore.salary != null) {
                                        $scope.priceService = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " VNĐ";
                                    } else {
                                        $(".quick-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".quick-location").addClass("non-display");
                                    }
                                    $scope.location = dataMore.position;
                                } else {
                                    if (!$(".show-hide-quick").hasClass("non-display")) {
                                        $(".show-hide-quick").addClass("non-display");
                                    }
                                }

                                if (res.data.serviceOrder.serviceType.id == 5) {
                                    if ($(".show-hide-tutoring").hasClass("non-display")) {
                                        $(".show-hide-tutoring").removeClass("non-display");
                                    }

                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours()));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours()));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".tutoring-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDate = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDate = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }
                                    if (dataMore.sex == "" || dataMore.sex == null) {
                                        $(".tutoring-gender").addClass("non-display");
                                    }
                                    if (dataMore.career == "" || dataMore.career == null) {
                                        $(".tutoring-career").addClass("non-display");
                                    }
                                    $scope.gender = dataMore.sex;
                                    $scope.career = dataMore.career;
                                    if (dataMore.salary != null) {
                                        $scope.salaryTutoring = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " VNĐ";
                                    } else {
                                        $(".tutoring-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".tutoring-location").addClass("non-display");
                                    }
                                    else {
                                        $scope.location = dataMore.position;
                                    }
                                } else {
                                    if (!$(".show-hide-tutoring").hasClass("non-display")) {
                                        $(".show-hide-tutoring").addClass("non-display");
                                    }
                                }
                            }
                        }

                        else {
                            logger.logError(res.data.error.description);
                        }


                    });

                }

                var loadCurrency = function () {
                    api.get('currency/v1').then(function (res) {
                        if (res.data.result) {
                            if (res && res.data && res.data.currencies.length > 0) {
                                $scope.currency = res.data.currencies;


                            }
                            if (currency !== null) {

                                $scope.parentCurrency = $scope.currency[currency.id]


                            }
                            else {
                                $scope.parentCurrency = $scope.currency[100];
                            }


                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };

                var getSelectedCV = function (listCVs) {
                    $scope.selectCV = {
                        "aaData": listCVs,
                        "paging": true,
                        "oLanguage": {
                            "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                            "sLengthMenu": "_MENU_",
                            "sProcessing": "Đang xử lý...",
                            //"sLengthMenu":   "Xem _MENU_ mục",
                            "sZeroRecords": "Không có CV nào được chọn",
                            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                            "sInfoPostFix": "",
                            //"sSearch":       "Tìm:",
                            "sUrl": "",
                            "oPaginate": {
                                "sFirst": "Đầu",
                                "sPrevious": "Trước",
                                "sNext": "Tiếp",
                                "sLast": "Cuối"
                            }

                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null", width: '20%'},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "class": "text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hidden-xs hidden-sm'></div>");
                                    var content = angular.element("<div></div>");
                                    var element = angular.element("<div></div>");
                                    if (full.image != undefined && full.image != null) {
                                        desktop.append('<span><img src="' + full.image + '" class="img64_64 img-thumbnail"></span>');
                                    }
                                    else {
                                        desktop.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    }
                                    desktop.append(content);
                                    element.append(desktop);

                                    var mobile = angular.element("<div class='hidden-lg hidden-md no-padding col-xs-12'></div>");

                                    var bodyLeft = angular.element("<div class='col-xs-12 no-padding'></div>");
                                    var bodyRight = angular.element("<div class='bodyRight pull-right col-xs-3 no-padding text-center'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="col-xs-9 "></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var pullLeft = angular.element('<div class="pull-left footer-pull-left-mobile"></div>');
                                    var pullRight = angular.element('<div class="col-xs-3 position-checkbox no-padding pull-left"></div>');
                                    var img = angular.element('<div class="col-xs-3 no-padding"></div>');
                                    if (full.image != undefined && full.image != null) {
                                        img.append('<span><img src="' + full.image + '" class="img64_64 img-thumbnail"></span>');
                                    }
                                    else {
                                        img.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    }

                                    if (full.fullname != null) {
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="full name"></span>:<span>' + full.fullname + '</span></div>');
                                    }
                                    if (full.gender != null) {
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="gender"></span>: <span data-i18n="' + full.gender + '"></span></div>');
                                    }
                                    if (full.dob != null) {
                                        var date = new Date(full.dob);
                                        var day = "";
                                        var month = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="yob"></span>: <span>' + day + '/' + month + '/' + date.getFullYear() + '</span></div>');
                                    }
                                    if (full.yearofexperience != undefined) {
                                        if (full.yearofexperience != null) {
                                            header.append('<div class="col-xs-12 no-padding"><span class="col-md-12 col-sm-12 col-xs-12 no-padding"><span>Kinh nghiệm: </span>' + full.yearofexperience + ' <span>năm</span></span></div>');
                                        }
                                    }
                                    if (full.skill != undefined) {
                                        if (full.skill != null) {
                                            header.append('<div class="col-xs-12 no-padding"><span class="col-md-12 col-sm-12 col-xs-12 no-padding"><span>Kĩ năng: </span>' + full.skill + '</span></div>');
                                        }
                                    }
                                    body.append(header);
                                    mobile.append(img);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,

                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.fullname != null) {

                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding">' + full.fullname + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.sex != null) {
                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding" data-i18n="' + full.sex + '"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "class": "text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.dob != null) {
                                        element.append('<span>' + full.dob + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.skill != undefined) {
                                        element.append(full.skill);
                                    }
                                    return element.html();
                                }
                            },

                            {
                                "targets": 5,
                                "data": null,
                                "class": "text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.yearofexperience != undefined) {
                                        if (full.yearofexperience != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + full.yearofexperience + '</span>');
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.goal != undefined) {
                                        if (full.goal != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + full.goal + '</span>');
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    element.append('<a data-ng-click="$parent.editCV(aData.id)" class= "btn-gap no-border"><span class="glyphicon glyphicon-pencil"></span></a>');
                                    element.append('<a data-ng-click="$parent.removeCV(aData)" id="remove" class= "btn-gap no-border"><span class="glyphicon glyphicon-remove"></span></a>');
                                    return element.html();
                                }
                            }
                        ]
                    };


                };

                $scope.detailCV = function () {

                    $("#cvSelected").removeClass("non-display-mobile");
                    $("#tablecvSelected").removeClass("non-display-mobile");

                }
                $scope.removeCV = function (aData, index) {
                    var remove = true;
                    var idx = $scope.listCVs.indexOf(aData);
                    if (idx > -1) {
                        $scope.listCVs.splice(idx, 1);
                    }
                    var table = $('#selectCV').DataTable();
                    $('#selectCV tbody').on('click', "#remove", function () {
                        table
                            .row($(this).parents('tr'))
                            .remove()
                            .draw();
                    });
                }
                $scope.editCV = function (cvID) {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal
                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    Session.idParam = $stateParams.params;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    localStorage.setItem("urlCV", JSON.stringify(urlCV));
                    navigations.push({
                        title: "update cv",
                        path: "addnewcv",
                        params: cvID,
                        isParent: true,
                        selectedParentCV: $scope.listCVs
                    });
                }
                $scope.selectExistCV = function () {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal

                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    Session.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    navigations.current.isParent = true
                    if (!$scope.listCVs) {
                        $scope.listCVs = []
                    }
                    localStorage.setItem("selectedParentCV", JSON.stringify($scope.listCVs));
                    navigations.push({
                        title: "select cv",
                        path: "selectcv",
                        params: "",
                        isParent: true,
                        selectedParentCV: $scope.listCVs
                    });

                }
                $scope.addParentCV = function () {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal

                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    Session.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    navigations.current.isParent = true
                    navigations.push({
                        title: "add new cv",
                        path: "addnewcv",
                        params: "",
                        isParent: true,
                        selectedParentCV: $scope.listCVs
                    });
                    localStorage.setItem("urlCV", JSON.stringify(urlCV));
                }

                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };

                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };

                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };
                module.filter('withoutContainer', [function () {
                    return function () {
                        return _.without(config.panel_names, 'container');
                    };
                }]);

                function checkInputvalue(value1, value2, callback) {

                    var valueReplace1 = value1.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    var valueReplace2 = value2.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    db.get("validatePost.json").then(function (result) {
                        if (result.data.length > 0) {
                            var check = result.data;
                            var found = false;
                            var validateNumber1 = valueReplace1.replace(/\s+/g, '').match(/\d+/g);
                            var validateNumber2 = valueReplace2.replace(/\s+/g, '').match(/\d+/g);
                            if (!found) {
                                if (validateNumber1 != null) {
                                    for (var i = 0; i < validateNumber1.length; i++) {
                                        if (validateNumber1[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!found) {
                                if (validateNumber2 != null) {
                                    for (var i = 0; i < validateNumber2.length; i++) {
                                        if (validateNumber2[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!found) {
                                for (var j = 0; j < check.length; j++) {
                                    if (valueReplace1.search(check[j].name) != -1 || valueReplace2.search(check[j].name) != -1) {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found) {
                                callback(true)
                                return true;
                            }
                            else {
                                callback(false)
                                return false;
                            }
                        }
                    });
                }

                $scope.bid = function () {
                    if (!form.valid()) {
                        return;
                    }
                    var inputProposal = $scope.bids.proposal.toString().toLowerCase();
                    var inputTitle = $scope.titleProposal.toString().toLowerCase();
                    checkInputvalue(inputProposal, inputTitle, function (result) {
                        bids.status = 1;
                        if (result) {
                            bids.status = 6;
                        }

                        var getInformation = $('#infoMore').dataTable();

                        var infoToPush = _.filter(getInformation, function (proposal) {
                            return getInformation.isSelected == true;
                        });


                        var getAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < getAddonService.length; i++) {
                            if (getAddonService[i].isSelected) {
                                var objAddonService = {};
                                objAddonService = getAddonService[i];
                            }
                            //if (getAddonService[i].isSelected) {
                            //    getAddonService[i].isSelected = false;
                            //}
                        }
                        var dataJson = {};
                        dataJson.type = infoToPush
                        dataJson.addonService = objAddonService;
                        dataJson.title = $scope.titleProposal;
                        dataJson.cv = $scope.listCVs;
                        dataJson.serviceType = $scope.getValueChecked;
                        dataJson.budget = $scope.proposalRes.budgetTo;
                        dataJson.expiredDate = $scope.proposalRes.expiredDate;
                        dataJson.totalBids = $scope.proposalRes.bids;
                        dataJson.indexBroker = $scope.brokerList.indexOf($scope.chosenBroker);
                        var dataJsonPush = JSON.stringify(dataJson);
                        bids.username = Session.userId;
                        bids.expectedCost = $scope.price.net;
                        bids.jsonData = dataJsonPush;
                        bids.proposal = $scope.bids.proposal;
                        bids.currencyId = 101;
                        if (objAddonService) {
                            bids.addonServiceId = objAddonService.id;
                        }
                        if ($scope.chosenBroker) {
                            bids.brokerId = $scope.chosenBroker.id;
                        }
                        if ($scope.listCVs.length > 0) {
                            var cvIds = [];
                            for (var i = 0; i < $scope.listCVs.length; i++) {
                                cvIds.push($scope.listCVs[i].id);
                            }
                            bids.cvIds = cvIds;
                        }
                        bids.serviceOrderId = $stateParams.params;
                        api.post('proposal/v1', bids).then(function (res) {
                            if (res.data.result) {
                                store.set("inputDetailsOrder", "");
                                $location.path("/detailSupplier");
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });

                    });
                }
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);


    });

