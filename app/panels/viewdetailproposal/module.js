/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';
        var module = angular.module('bidproject.panels.viewdetailproposal', []);
        app.useModule(module);

        module.controller('viewdetailproposal', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', '$location', '$modal', 'store', 'USER_ROLES', 'navigations', 'notification', '$window',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, $location, $modal, store, USER_ROLES, navigations, notification, $window) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);

                $scope.proposals = localStorage.getItem("dataProposal") ? JSON.parse(localStorage.getItem("dataProposal")) : {};
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();
                    $scope.hideProposal = true;
                    $scope.isShow = false;
                    console.log(" $scope.proposal", $scope.proposals);
                    $scope.idSO = $scope.proposals.serviceOrderID;
                    $scope.titleSO = $scope.proposals.serviceOrderTitle;
                    $scope.idProposal = $scope.proposals.proposal.id;
                    $scope.titleProposal = $scope.proposals.proposal.jsonData.title;
                    $scope.contentProposal = $scope.proposals.proposal.proposal;
                    $scope.priceProposal = $scope.proposals.proposal.expectedCost;
                    $scope.serviceType = $scope.proposals.serviceType;
                    switch ($scope.proposals.proposal.status) {
                        case 1:
                            $(".active-status-bidding").removeClass("non-display");
                            $("#btnAward").removeClass("non-display");
                            break;
                        case 2:
                            $(".active-status-awarded").removeClass("non-display");
                            break;
                        case 3:
                            $(".active-status-complete").removeClass("non-display");
                            break;
                        case 4:
                            $(".active-status-transaction-failed").removeClass("non-display");
                            break;
                        case 5:
                            $(".active-status-rejected").removeClass("non-display");
                            break;
                    }
                    if($scope.proposals.proposal.jsonData.serviceType.id != undefined){
                        $scope.serviceProposal = $scope.proposals.proposal.jsonData.serviceType.description;
                        $scope.isShow = true;
                    }
                    getListCV();

                };


                var modalInstance;
                $scope.submitAwardOrder = function () {
                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/payment.html",
                        controller: 'PaymentProposalDialogCtrl',
                        resolve: {
                            data: function () {
                                return $scope.proposals.isAwardContinue;
                            }
                        }
                    });
                    modalInstance.result.then((function () {

                        var url = config.admin + "payment?paymentType=SO&username=" + Session.userId + "&serviceOrderId=" + $scope.proposals.serviceOrderID + "&proposalIds=" + $scope.proposals.proposal.id + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                        $window.location.href = url;


                    }), function () {});

                };

                $scope.cancel = function () {
                    $location.path("viewdetailpost/" +  $scope.proposals.serviceOrderID)

                };

                function getListCV() {
                    $scope.listCV = {
                        "aaData": $scope.proposals.proposal.jsonData.cv,
                        "paging": false,
                        "scrollCollapse": true,
                        lang:{
                            vi:{
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                "sEmptyTable": "Không có hồ sơ nào",
                                "sZeroRecords": "Không có hồ sơ nào",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ hồ sơ",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 hồ sơ",
                                "sInfoFiltered": "(được lọc từ _MAX_ hồ sơ)",
                                "sInfoPostFix": "",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en:{
                                "sEmptyTable":     "No CV available in table",
                                "sInfo":           "Showing _START_ to _END_ of _TOTAL_ CV",
                                "sInfoEmpty":      "Showing 0 to 0 of 0 CV",
                                "sInfoFiltered":   "(filtered from _MAX_ total CV)",
                                "sInfoPostFix":    "",
                                "sInfoThousands":  ",",
                                "sLengthMenu":     "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing":     "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords":    "No matching records found",
                                "oPaginate": {
                                    "sFirst":    "First",
                                    "sLast":     "Last",
                                    "sNext":     "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "width": "20%",
                                "class":"text-center",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hide-when-mobile'></div>");
                                    if($scope.serviceType.id == 2){
                                        desktop.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    } else{
                                        if(full.image != undefined){
                                            desktop.append('<span><img src="'+full.image+'" class="img64_64 img-thumbnail"></span>');
                                        } else{
                                            desktop.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                        }

                                    }

                                    var mobile = angular.element("<div class='hide-when-desktop text-left col-xs-12 no-padding'></div>");
                                    var img = angular.element('<div class="col-xs-3 no-padding"></div>');
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="col-xs-9 "></div>');
                                    //var content = angular.element('<div class="content-mobile"></div>');
                                    // var pullLeft = angular.element('<div class="col-xs-8 no-padding footer-pull-left-mobile"></div>');
                                    //var pullRight = angular.element('<div class="col-xs-4 no-padding footer-pull-right-mobile"></div>');
                                    //content.append('<span>' + full.content + '</span>');
                                    //body.append(pullLeft);
                                    //body.append(pullRight);
                                    if($scope.serviceType.id == 2){
                                        img.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                    } else{
                                        if(full.image != undefined){
                                            img.append('<span><img src="'+full.image+'" class="img64_64 img-thumbnail"></span>');
                                        } else{
                                            img.append('<span><img src="images/unknown.png" class="img64_64 img-thumbnail"></span>');
                                        }
                                    }

                                    header.append('<div class="col-sm-12 col-xs-12 no-padding"><span><span data-i18n="candidate"></span> {{iDataIndex + 1}}</span></div>');
                                    header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="gender"></span>: <span data-i18n="{{aData.sex}}"></span></div>');
                                    header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="yob"></span>: <span>{{aData.dob}}</span></div>');
                                    header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="skills"></span>: <span>{{aData.skill}}</span></div>');
                                   // header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="experience year"></span>: <span>{{aData.yearofexperience}}</span></div>');
                                    body.append(header);
                                    mobile.append(img);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": "20%",
                                "class":"text-center hide-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    element.append('<span><span data-i18n="candidate"></span> {{iDataIndex + 1}}</span>');
                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": "20%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if(full.sex != null && full.sex != undefined){
                                        element.append("<span data-i18n='{{aData.sex}}'></span>");
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "width": "20%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if(full.dob != null && full.dob != undefined){
                                        element.append("<span>{{aData.dob}}</span>");
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": "20%",
                                "class": "text-center hide-mobile",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if(full.skill != null && full.skill != undefined){
                                        element.append("<span>{{aData.skill}}</span>");
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 5,
                                "data": null,
                                "width": "20%",
                                "class": "text-center",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if(full.yearofexperience != null && full.yearofexperience != undefined){
                                        element.append("<span>{{aData.yearofexperience}}</span>");
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "width": "12%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if(full.startDate != null && full.startDate != undefined){
                                        element.append("<span data-i18n='{{aData.startDate}}'></span>");
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                "width": "12%",
                                "render": function (data, type, full, meta) {
                                    var element = angular.element("<div></div>");
                                    if (full.goal != null && full.goal != undefined) {
                                        element.append("<span>{{aData.goal}}</span>");
                                    }
                                    return element.html();
                                }
                            }

                        ]
                    };
                }

                $scope.$on("window.width", function (object, type) {
                    var rowDetail = $element.find(".row-detail");
                    rowDetail.remove();
                    var detailShow = $element.find(".detail-show");
                    detailShow.removeClass("detail-show");
                    detailShow.removeClass("active");
                });

                $scope.hideContent = function (index, value, element) {
                    if (windowsize.devicetype() !== "Desktop") {
                        var rowDetail = element.parent("tbody").find(".row-detail");
                        var classHideMobile = rowDetail.find(".hide-mobile");
                        var hiddenIsNotSkill = rowDetail.find(".hiddenIsNotSkill");
                        var columnValue = hiddenIsNotSkill.find(".columnValue");
                        if (columnValue.text() == "") {
                            hiddenIsNotSkill.addClass("non-display");
                        }
                        classHideMobile.addClass("non-display");

                    }

                }

                $scope.formatContent = function (index, value, element) {
                    var rowDetail = element.parent("tbody").find(".row-detail");
                    var td = rowDetail.find("td");
                    var ul = rowDetail.find("ul");
                    var li = rowDetail.find("li");
                    var body = angular.element("<div class='col-md-12 no-padding'></div>");
                    var contentLeft = angular.element("<div class='col-md-9 no-padding'></div>");
                    var contentRight = angular.element("<div class='col-md-3 no-padding'></div>");
                    if (li != undefined && li.length > 0) {
                        li.each(function () {
                            var content = angular.element("<div class='col-md-12 col-sm-12 no-padding'></div>");
                            var columnTitle = angular.element("<div class='col-md-4 col-sm-4 no-padding'></div>");
                            var columnValue = angular.element("<div class='col-md-8 col-sm-8 colValue'></div>");
                            if ($(this).hasClass("hide-mobile")) {
                                columnTitle.addClass("hide-mobile");
                                columnValue.addClass("hide-mobile");
                            }
                            var spanValue = $(this).find(".columnValue");
                            spanValue.removeClass("col-md-10");
                            spanValue.addClass("col-md-12");
                            var title = $(this).find("span").closest(".columnTitle");
                            title.removeClass("col-md-2");
                            title.addClass("col-md-12");
                            title.addClass("col-sm-12");
                            var value = $(this).find(".columnValue");
                            if (value.text() == "") {
                                title.addClass("non-display");
                                value.addClass("non-display");
                            }
                            value.removeClass("col-md-2");
                            value.addClass("col-md-12");
                            value.addClass("col-sm-12");
                            columnTitle.append(title);
                            columnValue.append(value);
                            content.append(columnTitle);
                            content.append(columnValue);
                            contentLeft.append(content);
                        });
                        body.append(contentLeft);
                        body.append(contentRight);
                        ul.addClass("non-display");
                        td.append(body);
                        if ($(window).width() == 768) {
                            var classHideMobile = rowDetail.find(".hide-mobile");
                            classHideMobile.addClass("non-display");
                            //rowDetail.remove();
                        }
                    }
                }

            }]);
        module.controller('PaymentProposalDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'data', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, data, Session) {

                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });
                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };

                $scope.init = function () {
                    loadPayment();
                };

                $scope.cancelAwardOrder = function () {
                    $modalInstance.dismiss("cancel");
                };

                function loadPayment() {
                    $scope.countSelection = 1;
                    if (data) {
                        $scope.vaberFee = "0";
                    } else {
                        $scope.vaberFee = "50,000";
                    }

                }

                $scope.confirmAwardOrder = function () {
                    $modalInstance.close();

                };

            }
        ]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);


    });

