/**
 * Created by Admin on 5/4/2015.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.first', []);
        app.useModule(module);

        module.controller('first', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', 'store', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', '$modal',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, store, $location, USER_ROLES, AuthService, AUTH_EVENTS, $modal) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults

                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.init = function () {
                    $scope.ready = false;
                    $scope.reset_panel();

                };

                $scope.nextPostOrder = function(){
                    var modalShowPostOrder;
                    modalShowPostOrder = $modal.open({
                        templateUrl: "app/partials/form/showPostOrder.html",
                        controller: 'ShowPostOrderDialogCtrl'
                    });
                    modalShowPostOrder.result.then((function (rate) {

                    }), function () {
                    });
                }

            }]);
        module.controller('ShowPostOrderDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'Session',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, Session) {

                $scope.isShowPostLate = false;
                $scope.timePost = new Date();
                $scope.isShow = false;
                $scope.isMeridian = true;
                $scope.isSpinners = false;
                var date = new Date();
                $scope.minDate = date;
                $scope.formats = ['dd/MM/yyyy'];
                $scope.format = $scope.formats[0];

                $scope.close = function () {
                    $modalInstance.dismiss("close");
                };

                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.isOpen = true;
                };

                $scope.showTime  = function(){
                    $scope.isShow = true;
                };

                $scope.setSelection = function(result){
                    if(result == 0){
                        if(!$("#post-now").hasClass("active")){
                            $("#post-now").addClass("active");
                            $("#post-late").removeClass("active");
                            $scope.isShowPostLate = !$scope.isShowPostLate;
                        }
                    } else if(result == 1){
                        if(!$("#post-late").hasClass("active")){
                            $("#post-now").removeClass("active");
                            $("#post-late").addClass("active");
                            $scope.isShowPostLate = !$scope.isShowPostLate;
                        }
                    }
                }




            }
        ]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);
    });