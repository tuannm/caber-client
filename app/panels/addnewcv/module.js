/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';
        var module = angular.module('bidproject.panels.welcome', []);
        app.useModule(module);

        module.controller('addnewcv', ['$scope', '$filter', '$rootScope', '$modal', 'db', '$location', '$stateParams', 'Session', 'api', 'logger', '$element', '$compile', 'store', '$timeout', 'windowsize', 'navigations', function ($scope, $filter, $rootScope, $modal, db, $location, $stateParams, Session, api, logger, $element, $compile, store, $timeout, windowsize, navigations) {
            $scope.panelMeta = {
                status: "Stable",
                description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode: "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content: "",
                style: {}
            };
            _.defaults($scope.panel, _d);

            $scope.init = function () {
                $scope.ready = false;
                loadDegree();
                getCV();
                getParentSkills()
                $scope.cv.gender = "man"
                $scope.times = ["Sau 1 tuần", "Sau 2 tuần", "Sau 3 tuần", "Sau 4 tuần", "Sau 1 tháng", "Sau 2 tháng", "Sau 3 tháng"]
            };
            var urlCV = JSON.parse(localStorage.getItem("urlCV")) || [];

            if (urlCV == null || urlCV == undefined || urlCV == "") {
                urlCV = "managecv"
            }
            $scope.selection = [];
            var inputDetails = store.get('inputDetailsOrder') ? store.get('inputDetailsOrder') : {};
            $scope.cancel = function () {
                if (!form.valid()) {
                    navigations.back({
                        isParent: false
                    });
                    $location.path(urlCV);
                }
                else {
                    navigations.back({
                        isParent: false
                    });
                    $location.path(urlCV);
                }
            }

            var form = $("#formAddnewcv");

            function getParentSkills() {
                db.get("work.json").then(function (result) {
                    if (result && result.data) {

                        $scope.parentskills = result.data;
                        $scope.categories = result.data;
                    }
                });
            }
            var parenCategoryValue = {};
            $scope.chooseParentCategory = function (value) {
                if (value != null) {
                    if (value.children != undefined) {

                        $scope.childrenCategory = '';
                        return;

                    }
                    else {
                        $timeout(function () {
                            var parentCategory = [];
                            angular.extend(parentCategory, $scope.selectedSkills)
                            for (var i = 0; i < value.skills.length; i++) {
                                parentCategory.push(value.skills[i]);
                            }
                            var uniqueCategory = unique(parentCategory)
                            $scope.customCategory = uniqueCategory;
                            $("#selectedSkill").select2();
                            $scope.$apply();
                            $("#selectedSkill").select2().trigger('change');
                        });

                    }
                }
            }
            var unique = function (origArr) {
                var newArr = [],
                    origLen = origArr.length,
                    found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if (origArr[x].name === newArr[y].name) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr;
            }
            $scope.chooseChildrenCategory = function (childrenValue) {

                if (childrenValue != undefined) {
                    if (childrenValue.children != undefined) {
                        if (childrenValue.children.length > 0) {
                            return;
                        }
                    }
                    else {
                        $timeout(function () {

                            var childrenCategory = [];
                            angular.extend(childrenCategory, $scope.selectedSkills)
                            for (var i = 0; i < childrenValue.skills.length; i++) {
                                childrenCategory.push(childrenValue.skills[i]);
                            }
                            var uniqueCategory = unique(childrenCategory)
                            $scope.customCategory = uniqueCategory;
                            $("#selectedSkill").select2();
                            $scope.$apply();
                            $("#selectedSkill").select2().trigger('change');
                        });

                    }
                }

            }
            //$scope.openedDateRange = function ($event) {
            //    $event.preventDefault();
            //    $event.stopPropagation();
            //    $scope.openeddateRange = true;
            //};
            //$scope.openDateOfBirth = function ($event) {
            //    $event.preventDefault();
            //    $event.stopPropagation();
            //    $scope.openedDateOfBirth = true;
            //};
            $scope.formats = ['dd/MM/yyyy'];
            $scope.format = $scope.formats[0];
            $scope.degrees = [];
            var modalInstance;
            var loadDegree = function () {
                db.get("degree.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.degrees = result.data;
                    }
                    $scope.degree = $scope.degrees[1];
                });
            };
            var cvID;
            $scope.addNewCV = {};
            $scope.cv = []
            $scope.cv.introductionVO = []
            $scope.cv.educations = [];
            $scope.cv.experiences = [];
            $scope.cropper = {};
            $scope.cropper.croppedImage = null;
            var fileUrl = "";
            var save = $('[save]', $element);
            var getCV = function () {
                $(".image-crop-final").attr("src", "images/unknown.png");
                fileUrl = 'images/unknown.png?time=' + new Date();
                if ($stateParams.params != null) {
                    //var ele = angular.element('<span data-i18n="update"></span>');
                    //var html = $compile(ele)($scope);
                    //save.html(html);
                    $("#Update").removeClass("non-display")
                    var url;
                    console.log("na",navigations)
                    if(navigations.current.isBroker){
                        url = 'cv/' + $stateParams.params + '/v1?userId=' + Session.id + '&userType=B';
                    } else{
                        url = 'cv/' + $stateParams.params + '/v1?userId=' + Session.id + '';
                    }
                    console.log("url",url);
                    api.get(url).then(function (res) {

                        if (res.data.result) {

                            res.data.cv.content = res.data.cv.content.replace(/&quot;/g, '"');

                            var content = JSON.parse(res.data.cv.content)
                            if (content.image != undefined) {
                                //$scope.imageCropStep = 3;
                                $(".image-crop-final").attr("src", content.image + '?time=' + new Date());
                                fileUrl = content.image + '?time=' + new Date();

                            }

                            if (content.dateOfIssue != null) {
                                var dateRange = new Date(content.dateOfIssue);
                                $scope.cv.dateOfIssue = dateRange
                            }
                            else {
                                $scope.cv.dateOfIssue = null;
                            }

                            if (content.dob != null) {
                                var dob = new Date(content.dob);
                                $scope.cv.dob = dob
                            }
                            else {
                                $scope.cv.dob = null;
                            }
                            var skills = content.introductionVO.skill
                            var skill = "";
                            if (skills != undefined) {
                                for (var j = 0; j < skills.length; j++) {
                                    if (j != skills.length - 1) {
                                        skill += skills[j].name.toString() + ", ";
                                    } else {
                                        skill += skills[j].name.toString();
                                    }
                                }
                                $timeout(function () {
                                    $scope.customCategory = skills
                                    $scope.selectedSkills = skills
                                    $("select").val(skills).trigger("change");
                                    if (content.introductionVO.workable != undefined) {
                                        if (content.introductionVO.workable != null) {
                                            $scope.cv.startDate = content.introductionVO.workable
                                        }
                                        else {
                                            $scope.cv.startDate = null;
                                        }
                                    }
                                });
                            }
                            $scope.cv.homephone =  content.homephone;
                            $scope.cv.yearofexperience = content.introductionVO.experienceYears
                            $scope.cv.gender = content.gender
                            $scope.cv.address = content.address
                            $scope.cv.placeOfIssue = content.placeOfIssue
                            $scope.cv.educations = content.educationVOList
                            $scope.cv.email = content.email
                            $scope.cv.experiences = content.experienceVOList
                            $scope.cv.fullname = content.fullname
                            $scope.cv.identification = content.identification
                            $scope.cv.cellphone = content.cellphone
                            $scope.cv.introductionVO.jobTarget = content.introductionVO.jobTarget
                            //$scope.cv.health = content.health
                            $scope.cv.introductionVO.hobby = content.introductionVO.hobby
                            $scope.cv.introductionVO.language = content.introductionVO.language
                            //$scope.cv.personal = content.personal
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });
                }
                else {
                    //var ele = angular.element('<span data-i18n="save"></span>');
                    //var html = $compile(ele)($scope);
                    //save.html(html);
                    $("#Save").removeClass("non-display")
                }
            }
            var formAddnewcv = $("#formAddnewcv");

            $scope.Save = function () {
                if (!$("#formAddnewcv").valid()) {
                    return;
                }
                var tomorrow = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
                var currentDate = Date.parse(tomorrow)
                $('#canvasImg').addClass('hide');
                $('#imgFinal').removeClass('hide');
                var content = "";
                if ($scope.cropper.croppedImage != null) {
                    content = $scope.cropper.croppedImage.split("base64,").pop()
                }
                var dataMore;
                api.post("attachment/v1", {
                    userId: Session.id,
                    fileName: "CV" + $scope.cv.fullname.replace(/ /g, '').replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i") + $scope.cv.cellphone.replace(/ /g, '') + currentDate,
                    fileType: "png",
                    content: content
                }).then(function (res) {
                    if (res.data.result) {
                        if ($scope.cropper.croppedImage != null) {
                            $scope.myImage = config.resources + res.data.attachment.fileUrl.split("cber-resource/resources/").pop();
                        }
                        else {
                            $scope.myImage = fileUrl;
                        }
                        dataMore = {
                            "image": $scope.myImage,
                            "gender": $scope.cv.gender,
                            "address": $scope.cv.address,
                            "addressIndentify": $scope.cv.addressIndentify,
                            "date_range": $scope.cv.date_range,
                            "dob": $scope.cv.dob,
                            "educationVOList": $scope.cv.educations,
                            "email": $scope.cv.email,
                            "experienceVOList": $scope.cv.experiences,
                            "fullname": $scope.cv.fullname,
                            "identification": $scope.cv.identification,
                            "cellphone": $scope.cv.cellphone,
                            "introductionVO": {
                                "jobTarget": $scope.cv.introductionVO.jobTarget,
                                "hobby": $scope.cv.introductionVO.hobby,
                                "language": $scope.cv.introductionVO.language,
                                "experienceYears": $scope.cv.yearofexperience,
                                "skill": $scope.selectedSkills,
                                "workable": $scope.cv.startDate
                            },
                            "dateOfIssue": $scope.cv.dateOfIssue,
                            "placeOfIssue": $scope.cv.placeOfIssue,
                            "homephone": $scope.cv.homephone
                            //"language": $scope.cv.language,
                            //"personal": $scope.cv.personal,

                        };
                        $scope.addNewCV.content = JSON.stringify(dataMore);
                        $scope.addNewCV.userId = Session.id;
                        var selection = {
                            fullname: "",
                            goal: "",
                            sex: "",
                            startDate: "",
                            skill: "",
                            yearofexperience: "",
                            dob: "",
                            image: ""
                        }
                        api.post('cv/v1', $scope.addNewCV).then(function (res) {
                            if (res.data.result) {
                                var content = JSON.parse(res.data.cv.content)
                                if (navigations.collections[navigations.collections.length - 1].isParent == true) {
                                    $scope.selection = navigations.current.selectedParentCV

                                    var newIndex = 0
                                    if ($scope.selection != undefined) {
                                        newIndex = $scope.selection.length
                                        if (newIndex > 0) {
                                            $scope.selection[newIndex] = _.clone($scope.selection[newIndex - 1])
                                        }
                                        else {
                                            $scope.selection = []
                                            $scope.selection[newIndex] = _.clone(selection)
                                        }
                                    }
                                    else {
                                        $scope.selection = []
                                        $scope.selection[newIndex] = _.clone(selection)
                                    }
                                    $scope.selection[newIndex].image = content.image
                                    $scope.selection[newIndex].fullname = $scope.cv.fullname;
                                    var date = new Date($scope.cv.dob);
                                    $scope.selection[newIndex].dob = date.getFullYear();
                                    $scope.selection[newIndex].sex = $scope.cv.gender;
                                    $scope.selection[newIndex].goal = $scope.cv.introductionVO.jobTarget;
                                    $scope.selection[newIndex].yearofexperience = $scope.cv.yearofexperience;

                                    var skill = "";
                                    for (var j = 0; j < $scope.selectedSkills.length; j++) {
                                        if (j != $scope.selectedSkills.length - 1) {
                                            skill += $scope.selectedSkills[j].name.toString() + ", ";
                                        } else {
                                            skill += $scope.selectedSkills[j].name.toString();
                                        }
                                    }
                                    $scope.selection[newIndex].skill = skill;
                                    $scope.selection[newIndex].id = res.data.cv.id;
                                    if (navigations.collections[navigations.collections.length - 2].params)
                                    {
                                    navigations.back({
                                        selectedParentCV: $scope.selection,
                                        isParent: false,
                                        path: navigations.collections[navigations.collections.length - 2].path,
                                        params: navigations.collections[navigations.collections.length - 2].params
                                    });
                                    }
                                    else
                                    {
                                        navigations.back({
                                            selectedParentCV: $scope.selection,
                                            isParent: false,
                                            path: navigations.collections[navigations.collections.length - 2].path
                                        });
                                    }
                                }
                                else {
                                    $location.path(urlCV);
                                }
                                $('#notifiDL').addClass('hide');
                                $('#notifiSDD').addClass('hide');
                                $('#notifiSuccess').addClass('hide');
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });
                    }
                });

//
            }
            $scope.Update = function () {
                if (!$("#formAddnewcv").valid()) {
                    return;
                }
                $('#canvasImg').addClass('hide');
                $('#imgFinal').removeClass('hide');
                var content = ""
                if ($scope.cropper.croppedImage != null) {
                    content = $scope.cropper.croppedImage.split("base64,").pop()
                }
                var dataMore;
                var tomorrow = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
                var currentDate = Date.parse(tomorrow)
                api.post("attachment/v1", {
                    userId: Session.id,
                    fileName: "CV" + $scope.cv.fullname.replace(/ /g, '').replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i") + $scope.cv.cellphone.replace(/ /g, '') + currentDate,
                    fileType: "png",
                    content: content
                }).then(function (res) {
                    if (res.data.result) {
                        if ($scope.cropper.croppedImage != null) {
                            $scope.myImage = config.resources + res.data.attachment.fileUrl.split("cber-resource/resources/").pop();
                        }
                        else {
                            $scope.myImage = fileUrl;
                        }
                        dataMore = {
                            "image": $scope.myImage,
                            "gender": $scope.cv.gender,
                            "address": $scope.cv.address,
                            "addressIndentify": $scope.cv.addressIndentify,
                            "date_range": $scope.cv.date_range,
                            "dob": $scope.cv.dob,
                            "educationVOList": $scope.cv.educations,
                            "email": $scope.cv.email,
                            "experienceVOList": $scope.cv.experiences,
                            "fullname": $scope.cv.fullname,
                            "identification": $scope.cv.identification,
                            "cellphone": $scope.cv.cellphone,
                            "introductionVO": {
                                "jobTarget": $scope.cv.introductionVO.jobTarget,
                                "hobby": $scope.cv.introductionVO.hobby,
                                "language": $scope.cv.introductionVO.language,
                                "experienceYears": $scope.cv.yearofexperience,
                                "skill": $scope.selectedSkills,
                                "workable": $scope.cv.startDate
                            },
                            "dateOfIssue": $scope.cv.dateOfIssue,
                            "placeOfIssue": $scope.cv.placeOfIssue,
                            "homephone": $scope.cv.homephone
                        };
                        $scope.addNewCV.content = JSON.stringify(dataMore);
                        $scope.addNewCV.userId = Session.id;
                        var selection = {
                            fullname: "",
                            goal: "",
                            sex: "",
                            startDate: "",
                            skill: "",
                            yearofexperience: "",
                            dob: "",
                            image: ""
                        }
                        api.put('cv/' + $stateParams.params + '/v1', $scope.addNewCV).then(function (res) {
                            if (res.data.result) {
                                var content = JSON.parse(res.data.cv.content)
                                if (navigations.collections[navigations.collections.length - 1].isParent == true) {
                                    $scope.selection = navigations.current.selectedParentCV
                                    if ($scope.selection != undefined) {
                                        for (var i = 0; i < $scope.selection.length; i++) {
                                            if ($scope.selection[i].id == $stateParams.params) {
                                                $scope.selection[i].image = content.image
                                                $scope.selection[i].fullname = $scope.cv.fullname;
                                                var date = new Date($scope.cv.dob);
                                                $scope.selection[i].dob = date.getFullYear();
                                                $scope.selection[i].sex = $scope.cv.gender;
                                                $scope.selection[i].goal = $scope.cv.introductionVO.jobTarget;
                                                $scope.selection[i].yearofexperience = $scope.cv.yearofexperience;
                                                var skill = "";
                                                for (var j = 0; j < $scope.selectedSkills.length; j++) {
                                                    if (j != $scope.selectedSkills.length - 1) {
                                                        skill += $scope.selectedSkills[j].name.toString() + ", ";
                                                    } else {
                                                        skill += $scope.selectedSkills[j].name.toString();
                                                    }
                                                }
                                                $scope.selection[i].skill = skill;
                                            }
                                        }
                                    }
                                    else {
                                        $scope.selection = [];
                                        $scope.selection[0] = _.clone(selection)

                                        $scope.selection[0].image = content.image
                                        $scope.selection[0].fullname = $scope.cv.fullname;
                                        var date = new Date($scope.cv.dob);
                                        $scope.selection[0].dob = date.getFullYear();
                                        $scope.selection[0].sex = $scope.cv.gender;
                                        $scope.selection[0].goal = $scope.cv.introductionVO.jobTarget;
                                        $scope.selection[0].yearofexperience = $scope.cv.yearofexperience;
                                        var skill = "";
                                        for (var j = 0; j < $scope.selectedSkills.length; j++) {
                                            if (j != $scope.selectedSkills.length - 1) {
                                                skill += $scope.selectedSkills[j].name.toString() + ", ";
                                            } else {
                                                skill += $scope.selectedSkills[j].name.toString();
                                            }
                                        }
                                        $scope.selection[0].skill = skill;
                                        $scope.selection[0].id = res.data.cv.id;
                                    }
                                    if (navigations.collections[navigations.collections.length - 2].params)
                                    {
                                        navigations.back({
                                            selectedParentCV: $scope.selection,
                                            isParent: false,
                                            path: navigations.collections[navigations.collections.length - 2].path,
                                            params: navigations.collections[navigations.collections.length - 2].params
                                        });
                                    }
                                    else
                                    {
                                        navigations.back({
                                            selectedParentCV: $scope.selection,
                                            isParent: false,
                                            path: navigations.collections[navigations.collections.length - 2].path
                                        });
                                    }
                                }
                                else {
                                    $location.path(urlCV);
                                }
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });
                    }
                });
            }
            function findElementInArray(array, array2) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i] === array2) {
                        return i;
                    }
                }
            }

            var indexEducation;
            $scope.editEducation = function (currenEducation) {

                modalInstance = $modal.open({
                    templateUrl: "app/partials/form/addEducation.html",
                    controller: 'addEducation',
                    resolve: {

                        degrees: function () {
                            return $scope.degrees;
                        },
                        education: function () {


                            return currenEducation;
                        }
                    }

                });
                var indexEducation = findElementInArray($scope.cv.educations, currenEducation)
                modalInstance.result.then((function (education) {
                    if (education === undefined) {
                        return;
                    }
                    $scope.cv.educations[indexEducation] = (education);
                }), function () {
                });
            }
            $scope.addEducation = function () {
                modalInstance = $modal.open({
                    templateUrl: "app/partials/form/addEducation.html",
                    controller: 'addEducation',
                    resolve: {

                        degrees: function () {
                            return $scope.degrees;
                        },
                        education: function () {

                            return null;
                        }
                    }

                });
                modalInstance.result.then((function (education) {

                    if (education === undefined) {
                        return;
                    }
                    $scope.cv.educations.push(education);

                }), function () {
                });
            }
            $scope.deleteEducation = function (currenEducation) {


                var indexEducation = $scope.cv.educations.indexOf(currenEducation);

                if (indexEducation > -1) {
                    $scope.cv.educations.splice(indexEducation, 1);
                }
            }
            module.controller('addEducation', [
                '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'degrees', 'education',
                function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, degrees, education) {
                    $scope.formats = ['dd/MM/yyyy'];
                    $scope.format = $scope.formats[0];
                    $scope.close = function () {
                        $modalInstance.close();
                    };
                    $scope.education = {};
                    $scope.cancel = function () {
                        $modalInstance.close();
                    }
                    $rootScope.$on('$locationChangeSuccess', function () {
                        $modalInstance.close();
                    });

                    $scope.degrees = degrees;
                    if (education != null) {
                        if (education.startDate != null) {
                            var star_date = new Date(education.startDate)
                            $scope.education.startDate = star_date;
                        }
                        else {
                            $scope.education.startDate = null;
                        }
                        if (education.endDate != null) {
                            var end_date = new Date(education.endDate)
                            $scope.education.endDate = end_date;
                        }
                        else {
                            $scope.education.endDate = null;
                        }
                        console.log("degrees",degrees)
                        $scope.education.degree = education.degree;
                        console.log("$scope.education.degree",$scope.education.degree)
                        $scope.education.schoolName = education.schoolName;
                        $scope.education.level = education.level;
                        $scope.education.ology = education.ology;


                        $scope.education.achievement = education.achievement
                    }
                    else
                    {
                        $scope.education.degree = $scope.degrees[0];
                    }
                    $scope.openStartDate = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.openedStartDate = true;
                    };
                    $scope.openedendDate = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.openedEndDate = true;
                    };


                    $scope.ok = function () {
                        if (!$('#formEducation').valid()) {
                            return
                        }
                        $modalInstance.close($scope.education);
                    };
                }
            ]);
            $scope.Sex = "Man"
            $scope.experiences = [];

            $scope.addExperience = function () {

                modalInstance = $modal.open({
                    templateUrl: "app/partials/form/addExperience.html",
                    controller: 'addExperience',
                    resolve: {

                        experience: function () {
                            return null;
                        }
                    }

                });
                modalInstance.result.then((function (experience) {
                    if (experience === undefined) {
                        return;
                    }

                    $scope.experiences.push(experience);
                    $scope.cv.experiences.push(experience);
                    var save = $('[workHere]', $element);
                    var ele = angular.element('<span>Hiện tại đang làm việc tại đây</span>');
                    var html = $compile(ele)($scope);
                    save.html(html);
                }), function () {
                });
            }
            $scope.deleteExperience = function (currenExperience) {

                var index = $scope.cv.experiences.indexOf(currenExperience);

                if (index > -1) {
                    $scope.cv.experiences.splice(index, 1);
                }
            }

            var indexExperiences;
            $scope.editExperience = function (currenExperience) {

                modalInstance = $modal.open({
                    templateUrl: "app/partials/form/addExperience.html",
                    controller: 'addExperience',
                    resolve: {

                        experience: function () {


                            return currenExperience;
                        }
                    }

                });
                var indexExperiences = findElementInArray($scope.cv.experiences, currenExperience)
                modalInstance.result.then((function (experience) {
                    if (experience === undefined) {
                        return;
                    }

                    $scope.cv.experiences[indexExperiences] = (experience);
                    if ($scope.cv.experiences[indexExperiences].status == true) {
                        var save = $('[workHere]', $element);
                        var ele = angular.element('<span>Hiện tại đang làm việc tại đây</span>');
                        var html = $compile(ele)($scope);
                        save.html(html);
                    }
                }), function () {
                });
            }
            module.controller('addExperience', [
                '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'experience',
                function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, experience) {
                    $rootScope.$on('$locationChangeSuccess', function () {
                        $modalInstance.close();
                    });
                    $scope.formats = ['dd/MM/yyyy'];
                    $scope.format = $scope.formats[0];
                    $scope.experience = {};
                    if (experience != null) {
                        if (experience.fromDate != null) {
                            var star_date = new Date(experience.fromDate)
                            $scope.experience.fromDate = star_date;
                        }
                        else {
                            $scope.experience.fromDate = null;
                        }
                        if (experience.toDate != null) {
                            var end_date = new Date(experience.toDate)
                            $scope.experience.toDate = end_date;
                        }
                        else {
                            $scope.experience.toDate = null;
                        }
                        $scope.experience.position = experience.position;
                        $scope.experience.companyName = experience.companyName;
                        $scope.experience.achievement = experience.achievement
                        $scope.experience.currentWork = experience.currentWork
                    }
                    $scope.close = function () {
                        $modalInstance.close();
                    };

                    $scope.cancel = function () {

                        $modalInstance.close();
                    }
                    $scope.openStartDate = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.openedStartDate = true;
                    };
                    $scope.openedendDate = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.openedEndDate = true;
                    };
                    $scope.ok = function () {
                        if (!$('#formExperience').valid()) {
                            return
                        }
                        var experience = $scope.experience;
                        if ($scope.experience.currentWork == true) {
                            experience.statusValue = "Đang làm việc tại đây"
                        }
                        $modalInstance.close(experience);
                    };
                }
            ]);
        }]);

    });
