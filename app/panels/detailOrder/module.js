/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'config',
        'css!./module.css'
    ],
    function (angular, app, _, require, config) {
        'use strict';
        var module = angular.module('bidproject.panels.detailOrder', []);
        app.useModule(module);

        module.controller('detailOrder', ['$scope', '$rootScope', '$compile', '$timeout', '$stateParams', 'db', 'windowsize', '$element', 'Session', 'api', 'logger', '$location', '$modal', 'store', 'USER_ROLES', 'navigations', 'notification',
            function ($scope, $rootScope, $compile, $timeout, $stateParams, db, windowsize, $element, Session, api, logger, $location, $modal, store, USER_ROLES, navigations, notification) {
                $scope.panelMeta = {
                    status: "Stable",
                    description: "A pseudo panel that lets you add other panels to be arranged in a column with" +
                    "defined heights."
                };
                // Set and populate defaults
                var _d = {
                    panels: []
                };
                _.defaults($scope.panel, _d);
                $scope.Currency = [];
                $scope.dataProposal = {};
                var statusServiceOrder2;
                var urlCV = "";
                $scope.init = function () {
                    if (Session.userRole == "supplier") {
                        $("#advancedServices").removeClass("non-display");
                        $("#selectExistCV").removeClass("non-display");
                        $("#customButton").addClass("non-display-important");
                        $("#customdownloadTemplateCVButton").addClass("non-display-important");
                        $("#downloadTemplateCV").addClass("non-display-important");

                    }
                    else {
                        $("#advancedServices").addClass("non-display");
                        $("#selectExistCV").addClass("non-display");
                        $("#customButton").removeClass("non-display-important");
                        $("#customdownloadTemplateCVButton").removeClass("non-display-important");
                        $("#downloadTemplateCV").removeClass("non-display-important");
                    }
                    $scope.ready = false;
                    $scope.reset_panel();
                    getDescription();
                    getProposal();
                    loadCurrency();
                    if (!angular.isUndefined(localStorage.getItem("selectedParentCV"))) {
                        selectedParentCV = [];
                    }
                    else {
                        selectedParentCV = localStorage.getItem("selectedParentCV")
                    }
                    if ($stateParams.params != null || $stateParams.params != null) {
                        urlCV = $stateParams.routeId.toString() + "/" + $stateParams.params
                    }
                    else {
                        urlCV = $stateParams.routeId
                    }
                    //$scope.showTitleProposal = false;
                    //$scope.hideProposal = false;
                    //$scope.hidePrice = false;
                    //$('.btn-payment').hide();
                    //$('.btn-delete').show();
                    //$('.btn-bid').show();
                    //$('.btn-accept').hide();
                    //$('.btn-reject').hide();
                };
                for (var i = 0; i < notification.messages.length; i++) {
                    if ($stateParams.params == notification.messages[i].data_id) {
                        if (!notification.messages[i].is_read) {
                            $rootScope.socket.emit('updateNotification', [notification.messages[i]._id], Session.id);
                        }
                    }
                }
                var selectedParentCV = [];
                $scope.getValueChecked = {};
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()

                });
                $scope.$on('render', function () {
                    tabResponsive(windowsize.divicetype())

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });
                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };
                $scope.isUpload = false;
                document.getElementById("customButton").addEventListener("click", function () {
                    document.getElementById("fileUploadDetailOrder").click();  // trigger the click of actual file upload button
                });
                document.getElementById("fileUploadDetailOrder").addEventListener("change", function () {
                    var fullPath = document.getElementById('fileUploadDetailOrder').value;
                    var fileName = fullPath.split(/(\\|\/)/g).pop();  // fetch the file name
                    document.getElementById("fileName").innerHTML = fileName;  // display the file name

                }, false);
                $timeout(function () {
                    jQuery("input#fileUploadDetailOrder").change(function () {
                        if (jQuery(this).val() != null) {
                            $("#isUploaded").removeClass("non-display")

                        }
                    });
                });
                var files;
                var fileReader = new FileReader();
                var contentCV = "";
                $element.on('change', function (e) {

                    files = document.querySelector('input[type=file]').files[0];
                    var reader = new FileReader();

                    reader.onloadend = function () {
                        contentCV = reader.result.split("base64,").pop()
                    }
                    if (files) {
                        reader.readAsDataURL(files);
                    }

                });
                $scope.uploadCv = function () {
                    var file = files.name.split(".");
                    var fileName = file[0];
                    var fileType = file[1];
                    if (!fileType.match("xlsx") || !fileType.match("xls")) {
                        logger.logWarning("Vui lòng chọn file excel");
                        return;
                    }
                    api.post("attachment/v1?type=cv", {
                        userId: Session.id,
                        fileName: fileName,
                        fileType: fileType,
                        content: contentCV
                    }).then(function (res) {
                        if (res.data.result) {
                            var dataCVExcel = []
                            for (var i = 0; i < res.data.cvs.length; i++) {
                                var dataMore = JSON.parse(res.data.cvs[i].content)
                                var dataToPush = []
                                dataToPush.fullname = dataMore.fullname;
                                if (dataMore.dob) {
                                    var date = new Date(dataMore.dob.replace(/(\d{2})[-/](\d{2})[-/](\d+)/, "$2/$1/$3"));
                                    dataToPush.dob = date.getFullYear();
                                }
                                dataToPush.sex = dataMore.gender;
                                dataToPush.goal = dataMore.introductionVO.jobTarget;
                                dataToPush.yearofexperience = dataMore.introductionVO.experienceYears;
                                var skill = "";
                                if (dataMore.introductionVO.skill) {
                                    for (var j = 0; j < dataMore.introductionVO.skill.length; j++) {
                                        if (j != dataMore.introductionVO.skill.length - 1) {
                                            skill += $scope.selectedSkills[j].name.toString() + ", ";
                                        } else {
                                            skill += $scope.selectedSkills[j].name.toString();
                                        }
                                    }
                                }
                                dataToPush.skill = skill;
                                dataToPush.id = dataMore.id;
                                dataCVExcel.push(dataToPush);
                            }
                            $scope.listCVs = $scope.listCVs.concat(dataCVExcel)
                            getSelectedCV($scope.listCVs)
                            $("#isUploaded").addClass("non-display")
                        }
                        else {
                            logger.logError(res.data.error.description);
                        }
                    });

                }
                function getDescription() {
                    api.get('user/v1?userType=B').then(function (res1) {
                        if (res1.data.result) {
                            $scope.brokerList = res1.data.brokers;
                            api.get('addonservice/v1').then(function (res) {
                                //var data;
                                //if ($scope.post.more.info != undefined) {
                                //    if($scope.post.isTemplate){
                                //        for (var i = 0; i < res.data.length; i++) {
                                //            for (var j = 0; j < $scope.post.more.info.length; j++) {
                                //                if(res.data[i].id == $scope.post.more.info[j].id){
                                //                    res.data[i].input = $scope.post.more.info[j].input;
                                //                    res.data[i].description = $scope.post.more.info[j].description;
                                //                    res.data[i].isSelected = true;
                                //                }
                                //            }
                                //        }
                                //        data = res.data;
                                //    } else{
                                //        data = $scope.post.more.info;
                                //    }
                                //} else {
                                //    data = res.data;
                                //}
                                for (var i = 0; i < res.data.addonServices.length; i++) {
                                    if (res.data.addonServices[i].id == 1) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 2) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                    if (res.data.addonServices[i].id == 3) {
                                        res.data.addonServices.splice(i, 1);
                                    }
                                }

                                $scope.data = res.data.addonServices;
                                $scope.serviceTable = {
                                    "aaData": res.data.addonServices,
                                    "paging": false,
                                    "columns": [
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"},
                                        {"data": "null"}
                                    ],
                                    "columnDefs": [
                                        {
                                            "targets": 0,
                                            "data": null,
                                            "width": "2%",
                                            "class": "id",
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                if($scope.dataProposal.jsonData != null && $scope.dataProposal.jsonData != undefined){
                                                    var dataJson = JSON.parse($scope.dataProposal.jsonData);
                                                    if(dataJson.addonService != undefined){
                                                        if(dataJson.addonService.type == full.type){
                                                            full.isSelected = true;
                                                            element.append('<label class="ui-checkbox"><input id="aData.id" ng-model="aData.isSelected" ng-disabled="true" ng-click="$parent.setCount(aData,$parent.data)" type="checkbox"><span></span></label>');
                                                        }else{
                                                            if (!full.isSelected) {
                                                                full.isSelected = false;
                                                            }
                                                            element.append('<label class="ui-checkbox"><input id="aData.id" ng-model="aData.isSelected" ng-disabled="true" ng-click="$parent.setCount(aData,$parent.data)" type="checkbox"><span></span></label>');
                                                        }
                                                    }else{
                                                        if (!full.isSelected) {
                                                            full.isSelected = false;
                                                        }
                                                        element.append('<label class="ui-checkbox"><input id="aData.id" ng-model="aData.isSelected" ng-click="$parent.setCount(aData,$parent.data)" type="checkbox"><span></span></label>');
                                                    }
                                                } else{
                                                    if (!full.isSelected) {
                                                        full.isSelected = false;
                                                    }
                                                    element.append('<label class="ui-checkbox"><input id="aData.id" ng-model="aData.isSelected" ng-click="$parent.setCount(aData,$parent.data)" type="checkbox"><span></span></label>');
                                                }



                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 1,
                                            "data": null,
                                            "width": "7%",
                                            "class": "name text-center",
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append('<span class="label label-warning">' + full.name + '</span>');
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 2,
                                            "data": null,
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append(full.description);
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 3,
                                            "data": null,
                                            "width": "20%",
                                            "class": "text-center",
                                            "render": function (data, type, full, meta) {
                                                var element = angular.element("<div></div>");
                                                element.append(full.serviceCost + "%");
                                                return element.html();
                                            }
                                        },
                                        {
                                            "targets": 4,
                                            "data": null,
                                            "width": "15%",
                                            "class": "quantity",
                                            "render": function (data, type, full, meta) {
                                                $scope.chosenBroker = $scope.brokerList[0];
                                                brokerselected = $scope.brokerList[0];
                                                var element = angular.element("<div></div>");
                                                if($scope.dataProposal.jsonData != null || $scope.dataProposal.jsonData != undefined){
                                                    var dataJson = JSON.parse($scope.dataProposal.jsonData);
                                                    if(dataJson.addonService != undefined){
                                                        if(dataJson.indexBroker != undefined){
                                                            $scope.chosenBroker = $scope.brokerList[dataJson.indexBroker];
                                                        }
                                                        element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><select class="form-control width100percent" style="color:black" ng-disabled="true" ng-model="$parent.chosenBroker" ng-options="broker.username for broker in $parent.brokerList" name="chosenBroker"  data-required data-message-required="validate choose city post" data-smart-validate-input></select></div>');
                                                    }else{
                                                        element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><select class="form-control width100percent" style="color:black" ng-model="$parent.chosenBroker" ng-options="broker.username for broker in $parent.brokerList" name="chosenBroker"  data-required data-message-required="validate choose city post" data-smart-validate-input></select></div>');
                                                    }
                                                } else{
                                                    element.append('<div class="form-group col-md-12 col-sm-12 no-padding"><select class="form-control width100percent" style="color:black" ng-model="$parent.chosenBroker" ng-options="broker.username for broker in $parent.brokerList" name="chosenBroker"  data-required data-message-required="validate choose city post" data-smart-validate-input></select></div>');
                                                }

                                                return element.html();
                                            }
                                        }
                                    ]
                                };
                            });
                        } else {
                            logger.logError(res1.data.error.description);
                        }
                    })
                }

                var brokerselected = {};
                $scope.setCount = function (aData, allData) {
                    for (var i = 0; i < allData.length; i++) {
                        if (allData[i].id != aData.id) {
                            allData[i].isSelected = false;
                            brokerselected = {};
                        }
                    }
                };
                $scope.selectExistCV = function () {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal

                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    Session.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    navigations.current.isParent = true
                    localStorage.setItem("selectedParentCV", JSON.stringify($scope.listCVs));
                    localStorage.setItem("urlCV", JSON.stringify(urlCV));
                    navigations.push({
                        title: "select cv",
                        path: "selectcv",
                        params: "",
                        isParent: true,
                        selectedParentCV: $scope.listCVs
                    });

                }
                $scope.addParentCV = function () {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal
                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    Session.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    navigations.current.isParent = true,
                        navigations.push({
                            title: "add new cv",
                            path: "addnewcv",
                            params: "",
                            isParent: true,
                            selectedParentCV: $scope.listCVs
                        });
                    localStorage.setItem("urlCV", JSON.stringify(urlCV));
                }
                $scope.selectedRow = function (value, element) {
                    var rowDetail = element.next('.row-detail');
                    var content = '<li class="block"><button class="btn btn-gap btn-success" ng-click="showProjects()">Bid Now</button><button class="btn btn-gap btn-primary" ng-click="showProjects()">Post a project like this</button></li>'
                    var compiledContents = $compile(content);
                    compiledContents($scope, function (clone) {
                        $('ul', rowDetail).append(clone);
                    });

                };
                $scope.vaberFee = "50,000";
                $scope.price = {}
                var inputDetails = store.get('inputDetailsOrder') ? store.get('inputDetailsOrder') : {};
                var bids = {};
                var idSer;
                var dataSer;
                var currency = {};
                var bid = $('[bid]', $element);
                var inputDetailsOrder = {};
                var modalInstance;
                var form = $("#formDetailOrder")
                $scope.disable = false;
                var found = false;

                $scope.hideRadio = false;
                $scope.showTable = true;
                $scope.bids = {};
                $scope.proposalRes = {};
                $scope.proposalRes.types = [];
                $scope.proposalRes.budgetTo = [];
                $scope.listCVs = [];
                $scope.parentCv = [];
                $scope.selectedCV = [];
                $scope.showInfo = function () {
                    var span = $("#showInfo").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-down")) {
                        $("#hideBrief").removeClass("non-display");
                        $("#viewBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-down");
                        $("#icon").addClass("glyphicon-chevron-up");
                        $("#detailInfo").slideDown();
                    } else {
                        $("#viewBrief").removeClass("non-display");
                        $("#hideBrief").addClass("non-display");
                        $("#icon").removeClass("glyphicon-chevron-up");
                        $("#icon").addClass("glyphicon-chevron-down");
                        $("#detailInfo").slideUp();
                    }
                };
                $scope.$watch('price.net', function (newVal, oldVal) {
                    if (newVal != null) {
                        if (newVal.toString().length > 12) {
                            $scope.price.net = oldVal;
                        }
                    }
                });
                var getSelectedCV = function (listCVs) {
                    $scope.selectCV = {
                        "aaData": listCVs,
                        "paging": false,
                        lang: {
                            vi: {
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sLengthMenu": "_MENU_",
                                "sProcessing": "Đang xử lý...",
                                //"sLengthMenu":   "Xem _MENU_ mục",
                                "sZeroRecords": "Không có CV nào được chọn",
                                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ CV",
                                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 CV",
                                "sInfoFiltered": "(được lọc từ _MAX_ CV)",
                                "sInfoPostFix": "",
                                //"sSearch":       "Tìm:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Đầu",
                                    "sPrevious": "Trước",
                                    "sNext": "Tiếp",
                                    "sLast": "Cuối"
                                }
                            },
                            en: {
                                "sEmptyTable": "No data available in table",
                                "sInfo": "Showing _START_ to _END_ of _TOTAL_ CV",
                                "sInfoEmpty": "Showing 0 to 0 of 0 CV",
                                "sInfoFiltered": "(filtered from _MAX_ total CV)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ",",
                                "sLengthMenu": "_MENU_",
                                "sLoadingRecords": "Loading...",
                                "sProcessing": "Processing...",
                                "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                "sZeroRecords": "No matching records found",
                                "oPaginate": {
                                    "sFirst": "First",
                                    "sLast": "Last",
                                    "sNext": "Next",
                                    "sPrevious": "Previous"
                                }
                            }
                        },
                        "columns": [
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"},
                            {"data": "null"}
                        ],
                        "columnDefs": [
                            {
                                "targets": 0,
                                "data": null,
                                "class": "text-center zoom-out",
                                "width": '12%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    var desktop = angular.element("<div class='hidden-xs hidden-sm'></div>");
                                    var content = angular.element("<div></div>");
                                    var element = angular.element("<div></div>");
                                    if (full.image != undefined && full.image != null) {
                                        desktop.append('<span><img src="' + full.image + '" class="img135x120 img-thumbnail"></span>');
                                    }
                                    else {
                                        desktop.append('<span><img src="images/unknown.png" class="img135x120 img-thumbnail" ></span>');
                                    }
                                    desktop.append(content);
                                    element.append(desktop);

                                    var mobile = angular.element("<div class='hidden-lg hidden-md no-padding col-xs-12'></div>");
                                    var bodyRight = angular.element("<div class='bodyRight pull-right col-xs-3 no-padding text-center'></div>");
                                    var header = angular.element('<div class="header-mobile"></div>');
                                    var body = angular.element('<div class="col-xs-8 "></div>');
                                    var content = angular.element('<div class="content-mobile"></div>');
                                    var img = angular.element('<div class="col-xs-4 no-padding"></div>');
                                    if (full.image != undefined && full.image != null) {
                                        img.append('<span><img src="' + full.image + '" class="img135x120 img-thumbnail"></span>');
                                    }
                                    else {
                                        img.append('<span><img src="images/unknown.png" class="img135x120 img-thumbnail"></span>');
                                    }

                                    if (full.fullname != null) {
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="full name"></span>:<span>' + full.fullname + '</span></div>');
                                    }
                                    if (full.gender != null) {
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="gender"></span>: <span data-i18n="' + full.gender + '"></span></div>');
                                    }
                                    if (full.dob != null) {
                                        var date = new Date(full.dob);
                                        var day = "";
                                        var month = "";
                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        header.append('<div class="col-sm-12 col-xs-12 no-padding"><span data-i18n="yob"></span>: <span>' + day + '/' + month + '/' + date.getFullYear() + '</span></div>');
                                    }
                                    if (full.yearofexperience != undefined) {
                                        if (full.yearofexperience != null) {
                                            header.append('<div class="col-xs-12 no-padding"><span class="col-md-12 col-sm-12 col-xs-12 no-padding"><span>Kinh nghiệm: </span>' + full.yearofexperience + ' <span>năm</span></span></div>');
                                        }
                                    }
                                    if (full.skill != undefined) {
                                        if (full.skill != null) {
                                            header.append('<div class="col-xs-12 no-padding"><span class="col-md-12 col-sm-12 col-xs-12 no-padding"><span>Kĩ năng: </span>' + full.skill + '</span></div>');
                                        }
                                    }
                                    body.append(header);
                                    mobile.append(img);
                                    mobile.append(body);
                                    element.append(desktop);
                                    element.append(mobile);
                                    return element.html();
                                }
                            },
                            {
                                "targets": 1,
                                "data": null,
                                "width": '10%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.fullname != null) {

                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding">' + full.fullname + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 2,
                                "data": null,
                                "width": '8%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.sex != null) {
                                        element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding" data-i18n="' + full.sex + '"></span>');
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 3,
                                "data": null,
                                "class": "text-center",
                                "width": '10%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.dob != null) {
                                        element.append('<span>' + full.dob + '</span>');
                                    }

                                    return element.html();
                                }
                            },
                            {
                                "targets": 4,
                                "data": null,
                                "width": '20%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if (full.skill != undefined) {
                                        element.append(full.skill);
                                    }
                                    return element.html();
                                }
                            },

                            {
                                "targets": 5,
                                "data": null,
                                "class": "text-center",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.yearofexperience != undefined) {
                                        if (full.yearofexperience != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + full.yearofexperience + '</span>');
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 6,
                                "data": null,
                                "width": '15%',
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");

                                    if (full.goal != undefined) {
                                        if (full.goal != null) {
                                            element.append('<span class="col-md-12 col-sm-12 col-xs-12 no-padding"> ' + full.goal + '</span>');
                                        }
                                    }
                                    return element.html();
                                }
                            },
                            {
                                "targets": 7,
                                "data": null,
                                class: "hide-children text-center verticalMiddle",
                                "render": function (data, type, full) {
                                    var element = angular.element("<div></div>");
                                    if($scope.dataProposal.jsonData != null && $scope.dataProposal.jsonData != undefined){
                                        var dataJson = JSON.parse($scope.dataProposal.jsonData);
                                        if(dataJson.addonService != undefined){
                                            if(dataJson.addonService.type == "pp_broker_advance" && Session.userRole == "broker"){
                                                element.append('<a data-ng-click="$parent.editCV(aData.id)" class= "btn-gap no-border no-border"><span class="glyphicon glyphicon-pencil"></span></a>');
                                                element.append('<a data-ng-click="$parent.removeCV(aData)" id="remove" class= "btn-gap no-border"><span class="glyphicon glyphicon-remove"></span></a>');
                                            }else if(dataJson.addonService.type == "pp_broker_basic" && Session.userRole == "supplier"){
                                                element.append('<a data-ng-click="$parent.editCV(aData.id)" class= "btn-gap no-border no-border"><span class="glyphicon glyphicon-pencil"></span></a>');
                                                element.append('<a data-ng-click="$parent.removeCV(aData)" id="remove" class= "btn-gap no-border"><span class="glyphicon glyphicon-remove"></span></a>');
                                            }
                                        }else{
                                            element.append('<a data-ng-click="$parent.editCV(aData.id)" class= "btn-gap no-border no-border"><span class="glyphicon glyphicon-pencil"></span></a>');
                                            element.append('<a data-ng-click="$parent.removeCV(aData)" id="remove" class= "btn-gap no-border"><span class="glyphicon glyphicon-remove"></span></a>');
                                        }
                                    }else{
                                        element.append('<a data-ng-click="$parent.editCV(aData.id)" class= "btn-gap no-border no-border"><span class="glyphicon glyphicon-pencil"></span></a>');
                                        element.append('<a data-ng-click="$parent.removeCV(aData)" id="remove" class= "btn-gap no-border"><span class="glyphicon glyphicon-remove"></span></a>');
                                    }

                                    return element.html();
                                }
                            }
                        ]
                    };


                };
                var remove = false
                $scope.removeCV = function (aData, index) {
                    var remove = true;
                    var idx = $scope.listCVs.indexOf(aData);
                    if (idx > -1) {
                        $scope.listCVs.splice(idx, 1);
                    }
                    var table = $('#selectCV').DataTable();
                    $('#selectCV tbody').on('click', "#remove", function () {
                        table
                            .row($(this).parents('tr'))
                            .remove()
                            .draw();
                    });
                }
                //$scope.hideContent = function (index, value, element) {
                //    if (windowsize.devicetype() != "Desktop") {
                //       if (remove)
                //       {
                //           var table = $('#selectCV').DataTable();
                //           table
                //               .row( $(this).parents('tr'))
                //               .remove()
                //               .draw();
                //       }
                //    }
                //    }
                console.log("Session",Session)
                $scope.editCV = function (cvID) {
                    inputDetailsOrder.expectedCost = $scope.price.net;
                    inputDetailsOrder.title = $scope.titleProposal
                    inputDetailsOrder.cv = $scope.listCVs;
                    inputDetailsOrder.proposal = $scope.bids.proposal;
                    inputDetailsOrder.expectedCost = $scope.price.net
                    inputDetailsOrder.idParam = $stateParams.params;
                    inputDetailsOrder.serviceType = $scope.getValueChecked;
                    Session.idParam = $stateParams.params;
                    store.set("inputDetailsOrder", inputDetailsOrder);
                    localStorage.setItem("urlCV", JSON.stringify(urlCV));
                    var isBroker = false;
                    if(Session.userRole == "broker"){
                        isBroker = true;
                    }
                    navigations.push({
                        title: "update cv",
                        path: "addnewcv",
                        params: cvID,
                        isParent: true,
                        selectedParentCV: $scope.listCVs,
                        isBroker:isBroker
                    });
                }
                function getServiceDetail(idSerRes, serviceTypeSelectedShow) {

                    api.get('serviceorder/' + idSerRes + '/v1?username= ' + Session.userId + ' &role= ' + USER_ROLES[Session.userRole] + '').then(function (res) {
                        if (res.data.result) {
                            dataSer = res.data.serviceOrder;
                            var id = res.data.serviceOrder.id
                            $scope.titleHeader = id.toString().concat(' - ', res.data.serviceOrder.title);
                            if (res.data.serviceOrder.totalProposalSubmitted === null) {
                                res.data.serviceOrder.totalProposalSubmitted = 0
                            }
                            if (res.data.serviceOrder.totalProposalSubmitted == null) {
                                res.data.serviceOrder.totalProposalSubmitted = 0;
                            }
                            if (res.data.serviceOrder.totalProposalAccepted == null) {
                                res.data.serviceOrder.totalProposalAccepted = 0;
                            }
                            if (res.data.serviceOrder.totalProposalConfirmed == null) {
                                res.data.serviceOrder.totalProposalConfirmed = 0;
                            }
                            if (res.data.serviceOrder.totalProposalClosed == null) {
                                res.data.serviceOrder.totalProposalClosed = 0;
                            }
                            if (res.data.serviceOrder.totalProposalRejected == null) {
                                res.data.serviceOrder.totalProposalRejected = 0;
                            }
                            $scope.proposalRes.bids = parseInt(res.data.serviceOrder.totalProposalSubmitted + res.data.serviceOrder.totalProposalAccepted + res.data.serviceOrder.totalProposalConfirmed + res.data.serviceOrder.totalProposalRejected + res.data.serviceOrder.totalProposalClosed);
                            if (res.data.serviceOrder.jsonData != null) {
                                res.data.serviceOrder.jsonData.replace(/&quot;/g, '"')
                                dataMore = JSON.parse(res.data.serviceOrder.jsonData);

                                var isMore = false;
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {

                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                if (isMore) {
                                    var descriptionTable = [];
                                    $('#descriptionTableTitle').removeClass("non-display");
                                    $('#descriptionTable').removeClass("non-display");
                                    $scope.showTable = false;
                                    //$('#content-addCV').removeClass("non-display");
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            $scope.proposalRes.types.push(dataMore.information[i].name);
                                            dataMore.information[i].isChecked = false;
                                            descriptionTable.push(dataMore.information[i]);
                                            if (dataMore.information[i].input != null) {
                                                $scope.proposalRes.budgetTo.push(dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫");
                                            }
                                            else {
                                                $scope.proposalRes.budgetTo.push(0 + "₫");
                                            }
                                        }
                                    }
                                    if (inputDetails.serviceType != undefined && !jQuery.isEmptyObject(inputDetails.serviceType)) {
                                        $scope.serviceTypeChecked = inputDetails.serviceType.id;
                                        $scope.getValueChecked = inputDetails.serviceType
                                    }

                                    else {
                                        if (serviceTypeSelectedShow != undefined && !jQuery.isEmptyObject(serviceTypeSelectedShow)) {
                                            $scope.serviceTypeChecked = serviceTypeSelectedShow.id;
                                            $scope.getValueChecked = serviceTypeSelectedShow
                                        }
                                        else {
                                            $scope.serviceTypeChecked = 1;
                                            $scope.getValueChecked = descriptionTable[0];
                                        }
                                    }
                                    $scope.descriptionTable = {
                                        "aaData": descriptionTable,
                                        "ordering": false,
                                        "search": false,
                                        "paging": false,
                                        "filter": false,
                                        "info": false,
                                        "columns": [
                                            {"data": "null"},
                                            {"data": "null"}
                                        ],
                                        "columnDefs": [
                                            {
                                                "targets": 0,
                                                "data": null,
                                                "width": "2%",
                                                "class": "text-center width2percent",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");

                                                    element.append('<label class="ui-checkbox text-center"><input name="radio"  ng-model="$parent.serviceTypeChecked" ng-disabled="$parent.hideRadio"   data-ng-click="$parent.setCheck(aData)" ng-value="aData.id"  type="radio"><span></span></label>');
                                                    return element.html();
                                                }
                                            },
                                            {
                                                "targets": 1,
                                                "data": null,
                                                "width": "98%",
                                                "render": function (data, type, full, meta) {
                                                    var element = angular.element("<div></div>");
                                                    if (full.name == 'Other') {

                                                        element.append('<strong>' + full.name + '  :     ' + full.description + '</strong>');
                                                    } else {
                                                        element.append('<strong>' + full.description + '</strong>');
                                                    }
                                                    return element.html();
                                                }
                                            },
                                        ]
                                    };
                                } else {
                                    $scope.proposalRes.types.push(res.data.serviceOrder.serviceType.display);
                                    if (res.data.serviceOrder.budgetFrom != null) {
                                        $scope.proposalRes.budgetTo.push(res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫");
                                    }

                                }
                            } else {
                                $scope.proposalRes.types.push(res.data.serviceOrder.serviceType.display);
                                if (res.data.serviceOrder.budgetFrom != null) {
                                    $scope.proposalRes.budgetTo.push(res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "₫");
                                }

                            }


                            $scope.setCheck = function (aData) {
                                if (jQuery.isEmptyObject($scope.getValueChecked)) {
                                    aData.isChecked = true;
                                    $scope.getValueChecked = aData;
                                } else {
                                    aData.isChecked = true;
                                    $scope.getValueChecked = null;
                                    $scope.getValueChecked = aData;
                                }
                            }
                            $scope.proposalRes.budgetCurrency = res.data.serviceOrder.currency.name;
                            if (res.data.serviceOrder.expiredDate != null) {

                                var date = new Date(res.data.serviceOrder.expiredDate);
                                var day = "";
                                var month = "";
                                var time = new Date(date.setHours(date.getHours() - 7));
                                var hours = time.getHours();
                                var minutes = "";
                                if (String(date.getDate()).length < 2) {
                                    day = "0" + (date.getDate());
                                } else {
                                    day = date.getDate();
                                }
                                if (String(date.getMonth() + 1).length < 2) {
                                    month = "0" + (date.getMonth() + 1);
                                } else {
                                    month = (date.getMonth() + 1);
                                }
                                if (String(hours).length < 2) {
                                    hours = "0" + hours;
                                }
                                if (String(date.getMinutes()).length < 2) {
                                    minutes = "0" + (date.getMinutes());
                                } else {
                                    minutes = (date.getMinutes());
                                }

                                //$scope.proposalRes.expiredDate = hours.toString().concat(":",minutes.toString(),'  ',day.toString(),'/',month.toString(),'/',date.getFullYear().toString())
                                $scope.proposalRes.expiredDate = day.toString().concat('/', month.toString(), '/', date.getFullYear().toString())
                            }
                            $scope.proposalRes.content = res.data.serviceOrder.content;
                            $scope.proposalRes.skill = res.data.serviceOrder.skill;
                            if (res.data.serviceOrder.status === 2) {
                                if ($(".active-status-closed").hasClass("non-display")) {
                                    $(".active-status-closed").removeClass("non-display")
                                }
                                //$('.btn-payment').hide();
                                //$('.btn-delete').hide();
                                //$('.btn-bid').hide();
                                //$('.btn-accept').hide();
                                //$('.btn-reject').hide();
                                $scope.hideRadio = true;
                                $scope.disable = true;
                                $scope.showTitleProposal = true;
                                $scope.hideProposal = true;
                                $scope.hidePrice = true;
                                $scope.hideSelect2 = true;
                                //$('#descriptionTable').addClass("non-display");
                            }
                            else {
                                statusServiceOrder2 = false;
                            }
                            if (res.data.serviceOrder.jsonData != null) {
                                res.data.serviceOrder.jsonData = res.data.serviceOrder.jsonData.replace(/&quot;/g, '"')
                                var dataMore = JSON.parse(res.data.serviceOrder.jsonData);
                                if (res.data.serviceOrder.budgetFrom != null) {
                                    $scope.postDeal = res.data.serviceOrder.budgetFrom.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                }
                                $scope.city = dataMore.city.name;
                                var isMore = false;
                                var infoHeadhunt = "";
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                if (isMore) {

                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            if (dataMore.information[i].id == 4) {
                                                $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].description + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫' + '</span>');

                                            } else {
                                                $(".info-headhunt").append('<span class="col-md-12 col-xs-12 no-padding">' + dataMore.information[i].name + " - " + dataMore.information[i].input.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ₫' + '</span>');

                                            }

                                        }
                                    }
                                    if ($(".show-headhunt").hasClass("non-display")) {
                                        $(".show-headhunt").removeClass("non-display");
                                    }
                                    if (!$(".hide-headhunt").hasClass("non-display")) {
                                        $(".hide-headhunt").addClass("non-display");
                                    }

                                } else {
                                    if ($(".hide-headhunt").hasClass("non-display")) {
                                        $(".hide-headhunt").removeClass("non-display");
                                    }
                                    if (!$(".show-headhunt").hasClass("non-display")) {
                                        $(".show-headhunt").addClass("non-display");
                                    }
                                }


                                $scope.services = res.data.serviceOrder.serviceType.display;
                                $scope.currency = res.data.serviceOrder.currency.name;
                                if (res.data.serviceOrder.content != null) {
                                    $scope.description = res.data.serviceOrder.content.replace(/\r\n|\r|\n/g, "<br />")
                                }
                                else {
                                    $(".show-description").addClass("non-display");
                                }
                                var isMore = false;
                                if (dataMore.information != undefined && dataMore.information.length > 0) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            isMore = true;
                                            break;
                                        }
                                    }
                                }
                                var serviceType = "";
                                if (isMore) {
                                    var i;
                                    for (i = 0; i < dataMore.information.length; i++) {
                                        if (dataMore.information[i].isSelected) {
                                            serviceType += dataMore.information[i].name + ", ";
                                        }
                                    }

                                } else {
                                    serviceType = res.data.serviceOrder.serviceType.display;
                                }
                                $scope.serviceType = serviceType;
                                if (res.data.serviceOrder.skill != null) {
                                    var skills = res.data.serviceOrder.skill.toString().split(",");
                                    var skill = "";
                                    for (var i = 0; i < skills.length; i++) {
                                        if (i != skills.length - 1) {
                                            skill += skills[i].toString() + ", ";
                                        } else {
                                            skill += skills[i].toString();
                                        }
                                    }
                                    $scope.skills = skill;
                                    if ($(".hide-is-not-content").hasClass("non-display")) {
                                        $(".hide-is-not-content").removeClass("non-display");
                                    }

                                } else {
                                    if (!$(".hide-is-not-content").hasClass("non-display")) {
                                        $(".hide-is-not-content").addClass("non-display");
                                    }
                                }

                                var proposalsMatched = _.filter(res.data.serviceOrder.proposals, function (proposal) {
                                    return proposal.status == 1 && proposal.expectedCost != null && proposal.proposal != null;
                                });
                                if (res.data.serviceOrder.status == 1 && proposalsMatched.length > 0) {
                                    $("#btnPayment").removeClass("non-display");
                                } else {
                                    if (!$("#btnPayment").hasClass("non-display")) {
                                        $("#btnPayment").addClass("non-display");
                                    }
                                }


                                if (res.data.serviceOrder.status != 2) {
                                    $(".is-so-close").removeClass("non-display");
                                }
                                if (res.data.serviceOrder.expiredDate != null) {
                                    var date = new Date(res.data.serviceOrder.expiredDate);
                                    var day = "";
                                    var month = "";

                                    if (String(date.getDate()).length < 2) {
                                        day = "0" + (date.getDate());
                                    } else {
                                        day = date.getDate();
                                    }
                                    if (String(date.getMonth() + 1).length < 2) {
                                        month = "0" + (date.getMonth() + 1);
                                    } else {
                                        month = (date.getMonth() + 1);
                                    }
                                    $scope.expiredDate = day + '/' + month + '/' + date.getFullYear();
                                }

                                if (res.data.serviceOrder.serviceType.id == 3) {
                                    if ($(".show-hide-helper").hasClass("non-display")) {
                                        $(".show-hide-helper").removeClass("non-display");
                                    }
                                    if (dataMore.workType == null) {
                                        $(".helper-work-type").addClass("non-display");
                                    }
                                    $scope.workType = dataMore.workType;
                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".helper-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDateHelper = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDateHelper = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }

                                    if (dataMore.salary != null) {

                                        $scope.postCurrencyHelper = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                    } else {
                                        $(".helper-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".helper-location").addClass("non-display");
                                    }
                                    $scope.location = dataMore.position;
                                } else {
                                    if (!$(".show-hide-helper").hasClass("non-display")) {
                                        $(".show-hide-helper").addClass("non-display");
                                    }
                                }

                                if (res.data.serviceOrder.serviceType.id == 1) {
                                    if ($(".show-hide-quick").hasClass("non-display")) {
                                        $(".show-hide-quick").removeClass("non-display");
                                    }
                                    if (dataMore.workType == null) {
                                        $(".quick-work-type").addClass("non-display");
                                    }
                                    $scope.servicesMore = dataMore.workType;
                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours() - 5));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".quick-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDateQuick = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDateQuick = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }
                                    if (dataMore.salary != null) {
                                        $scope.priceService = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                    } else {
                                        $(".quick-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".quick-location").addClass("non-display");
                                    }
                                    $scope.location = dataMore.position;
                                } else {
                                    if (!$(".show-hide-quick").hasClass("non-display")) {
                                        $(".show-hide-quick").addClass("non-display");
                                    }
                                }

                                if (res.data.serviceOrder.serviceType.id == 5) {
                                    if ($(".show-hide-tutoring").hasClass("non-display")) {
                                        $(".show-hide-tutoring").removeClass("non-display");
                                    }

                                    var dateHelper = "";
                                    if (dataMore.dateStart != null) {
                                        var date = new Date(dataMore.dateStart);
                                        var day = "";
                                        var month = "";

                                        if (String(date.getDate()).length < 2) {
                                            day = "0" + (date.getDate());
                                        } else {
                                            day = date.getDate();
                                        }
                                        if (String(date.getMonth() + 1).length < 2) {
                                            month = "0" + (date.getMonth() + 1);
                                        } else {
                                            month = (date.getMonth() + 1);
                                        }
                                        dateHelper = day + "/" + month + "/" + date.getFullYear();
                                    }
                                    var timeStartHelper = "";
                                    if (dataMore.timeStart != null) {
                                        var date = new Date(dataMore.timeStart);
                                        var time = new Date(date.setHours(date.getHours()));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeStartHelper = hours + ":" + minutes;
                                    }
                                    var timeFinishHelper = "";
                                    if (dataMore.timeFinish != null) {
                                        var date = new Date(dataMore.timeFinish);
                                        var time = new Date(date.setHours(date.getHours()));
                                        var hours = time.getHours();
                                        var minutes = "";

                                        if (String(hours).length < 2) {
                                            hours = "0" + hours;
                                        }
                                        if (String(date.getMinutes()).length < 2) {
                                            minutes = "0" + (date.getMinutes());
                                        } else {
                                            minutes = (date.getMinutes());
                                        }
                                        timeFinishHelper = hours + ":" + minutes;
                                    }
                                    if (dateHelper == "" && timeStartHelper == "" && timeFinishHelper == "") {
                                        $(".tutoring-start-date-time").addClass("non-display");
                                    }
                                    if (timeStartHelper == "" || timeFinishHelper == "") {
                                        $scope.startDate = dateHelper + " " + timeStartHelper + timeFinishHelper;
                                    } else {
                                        $scope.startDate = dateHelper + " " + timeStartHelper + " - " + timeFinishHelper;
                                    }
                                    if (dataMore.sex == "" || dataMore.sex == null) {
                                        $(".tutoring-gender").addClass("non-display");
                                    }
                                    if (dataMore.career == "" || dataMore.career == null) {
                                        $(".tutoring-career").addClass("non-display");
                                    }
                                    $scope.gender = dataMore.sex;
                                    $scope.career = dataMore.career;
                                    if (dataMore.salary != null) {
                                        $scope.salaryTutoring = dataMore.salary.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " ₫";
                                    } else {
                                        $(".tutoring-salary").addClass("non-display");
                                    }
                                    if (dataMore.position == null) {
                                        $(".tutoring-location").addClass("non-display");
                                    }
                                    else {
                                        $scope.location = dataMore.position;
                                    }
                                } else {
                                    if (!$(".show-hide-tutoring").hasClass("non-display")) {
                                        $(".show-hide-tutoring").addClass("non-display");
                                    }
                                }
                            }
                        }

                        else {
                            logger.logError(res.data.error.description);
                        }


                    });

                }

                function getProposal() {
                    $scope.isClosed = true;
                    api.get('proposal/' + $stateParams.params + '/v1?username=' + Session.userId + '&role=' + USER_ROLES[Session.userRole] + '').then(function (res) {
                        if (res.data.result) {
                            $scope.dataProposal = res.data.proposal;
                            var jsondata;
                            if (res.data.proposal.jsonData) {
                                jsondata = JSON.parse(res.data.proposal.jsonData.replace(/&quot;/g, '"'));
                            }
                            else {
                                jsondata = JSON.parse(res.data.proposal.jsonData);
                            }
                            if (jsondata) {
                                var serviceTypeSelected = jsondata.serviceType
                            }
                            idSer = res.data.proposal.serviceOrderId
                            getServiceDetail(idSer, serviceTypeSelected)
                            if (res.data.result) {
                                if (inputDetails.expectedCost == null) {
                                    $scope.price.net = res.data.proposal.expectedCost;
                                }
                                else {
                                    $scope.price.net = inputDetails.expectedCost;
                                    inputDetailsOrder.expectedCost = null;

                                }
                                if (inputDetails.proposal == null) {
                                    $scope.bids.proposal = res.data.proposal.proposal;
                                }
                                else {
                                    $scope.bids.proposal = inputDetails.proposal;
                                    inputDetailsOrder.proposal = null;
                                }
                                currency = res.data.proposal.currency;

                                if (inputDetails.title == null) {
                                    if (res.data.proposal.jsonData != null) {
                                        $scope.titleProposal = jsondata.title
                                    }
                                }
                                else {

                                    $scope.titleProposal = inputDetails.title
                                    inputDetailsOrder.title = null;

                                }
                                if (navigations.current.selectedParentCV == null) {
                                    if (res.data.proposal.jsonData != null) {
                                        if (jsondata.cv != undefined) {
                                            for (var i = 0; i < jsondata.cv.length; i++) {
                                                $scope.listCVs.push(jsondata.cv[i])
                                            }
                                        }
                                    }
                                    getSelectedCV($scope.listCVs)
                                }
                                else {
                                    $scope.listCVs = navigations.current.selectedParentCV
                                    getSelectedCV($scope.listCVs)
                                    inputDetailsOrder.cv = null;
                                }

                                if (res.data.proposal.status == 0) {
                                    if ($(".active-status-opening").hasClass("non-display")) {
                                        $(".active-status-opening").removeClass("non-display")
                                    }
                                    $scope.showTitleProposal = false;
                                    $scope.hideProposal = false;
                                    $scope.hidePrice = false;
                                    $scope.hideSelect2 = false;
                                    //$('.btn-payment').hide();
                                    //$('.btn-delete').hide();
                                    //$('.btn-bid').show();
                                    //$('.btn-accept').hide();
                                    //$('.btn-reject').hide();
                                    $("#bid").removeClass("non-display")
                                    var ele = angular.element('<span data-i18n="bid"></span>');
                                    var html = $compile(ele)($scope);
                                    bid.html(html);
                                }

                                else if (res.data.proposal.status == 1) {

                                    var dataJsonProposal = JSON.parse($scope.dataProposal.jsonData);

                                    if ($(".active-status-bidding").hasClass("non-display")) {
                                        $(".active-status-bidding").removeClass("non-display")
                                    }
                                    //$('.btn-payment').hide();
                                    //$('.btn-delete').show();
                                    //$('.btn-bid').show();
                                    //$('.btn-accept').hide();
                                    //$('.btn-reject').hide();
                                    //$("#bid").removeClass("non-display")
                                    if (dataJsonProposal.addonService != undefined) {
                                        if (dataJsonProposal.addonService.type == "pp_broker_advance" && Session.userRole == "broker") {
                                            $("#delete").removeClass("non-display");
                                            $("#update").removeClass("non-display");
                                        } else if (dataJsonProposal.addonService.type == "pp_broker_basic" && Session.userRole == "supplier") {
                                            $("#delete").removeClass("non-display");
                                            $("#update").removeClass("non-display");
                                        } else{
                                            $scope.showTitleProposal = true;
                                            $scope.hideProposal = true;
                                            $scope.hidePrice = true;
                                            $scope.hideSelect2 = true;
                                            $("#wrapperButtonAction").addClass("non-display");
                                        }
                                        if (dataJsonProposal.addonService.type == "pp_broker_advance" && Session.userRole == "supplier") {
                                            $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                                        } else if (dataJsonProposal.addonService.type == "pp_broker_basic" && Session.userRole == "broker") {
                                            $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                                        }

                                    } else {
                                        $scope.showTitleProposal = false;
                                        $scope.hideProposal = false;
                                        $scope.hidePrice = false;
                                        $scope.hideSelect2 = false;
                                        $("#delete").removeClass("non-display");
                                        $("#update").removeClass("non-display");
                                    }

                                    //var ele = angular.element('<span data-i18n="update"></span>');
                                    //var html = $compile(ele)($scope);
                                    //bid.html(html);
                                }
                                else if (res.data.proposal.status == 7) {
                                    var dataJsonProposal = JSON.parse($scope.dataProposal.jsonData);
                                    if ($(".active-status-broker").hasClass("non-display")) {
                                        $(".active-status-broker").removeClass("non-display")
                                    }
                                    if (dataJsonProposal.addonService != undefined) {
                                        if (dataJsonProposal.addonService.type == "pp_broker_advance" && Session.userRole == "broker") {
                                            $("#update").removeClass("non-display");
                                        } else{
                                            $scope.showTitleProposal = true;
                                            $scope.hideProposal = true;
                                            $scope.hidePrice = true;
                                            $scope.hideSelect2 = true;
                                            $("#wrapperButtonAction").addClass("non-display");
                                        }

                                        if (dataJsonProposal.addonService.type == "pp_broker_advance" && Session.userRole == "supplier") {
                                            $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                                        }
                                    } else {
                                        $scope.showTitleProposal = false;
                                        $scope.hideProposal = false;
                                        $scope.hidePrice = false;
                                        $scope.hideSelect2 = false;
                                        $("#update").removeClass("non-display");
                                    }

                                    //$('.btn-payment').hide();
                                    //$('.btn-delete').show();
                                    //$('.btn-bid').show();
                                    //$('.btn-accept').hide();
                                    //$('.btn-reject').hide();
                                    //$("#bid").removeClass("non-display")
                                    //$("#delete").removeClass("non-display")

                                    //var ele = angular.element('<span data-i18n="update"></span>');
                                    //var html = $compile(ele)($scope);
                                    //bid.html(html);
                                }
                                else if (res.data.proposal.status === 2) {

                                    var dataJsonProposal = JSON.parse($scope.dataProposal.jsonData);
                                    if ($(".active-status-waitpayment").hasClass("non-display")) {
                                        $(".active-status-waitpayment").removeClass("non-display")
                                    }
                                    $scope.showTitleProposal = true;
                                    $scope.hideProposal = true;
                                    $scope.hidePrice = true;
                                    $scope.hideSelect2 = true;
                                    $scope.isClosed = false;

                                    $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                                    $('#descriptionTable').addClass("non-display");
                                    if (dataJsonProposal.addonService != undefined) {
                                        if ((dataJsonProposal.addonService.type == "pp_broker_advance" && Session.userRole == "broker") || (dataJsonProposal.addonService.type == "pp_broker_basic" && Session.userRole == "supplier")) {
                                            $("#accept").removeClass("non-display")
                                            $("#reject").removeClass("non-display")
                                        }
                                    } else {
                                        $("#accept").removeClass("non-display")
                                        $("#reject").removeClass("non-display")
                                    }

                                    //$('.btn-payment').hide();
                                    //$('.btn-delete').hide();
                                    //$('.btn-bid').hide();
                                    //$('.btn-accept').show();
                                    //$('.btn-reject').show();

                                    $scope.hideRadio = true;
                                }
                                else if (res.data.proposal.status === 3 || res.data.proposal.status === 4 || res.data.proposal.status === 5 || res.data.proposal.status === 6) {
                                    if (res.data.proposal.status === 3) {

                                        if ($(".active-status-paid").hasClass("non-display")) {
                                            $(".active-status-paid").removeClass("non-display")
                                        }
                                    }
                                    else if (res.data.proposal.status === 4) {
                                        if ($(".active-status-reject").hasClass("non-display")) {
                                            $(".active-status-reject").removeClass("non-display")
                                        }
                                    }
                                    else if (res.data.proposal.status === 5) {
                                        if ($(".active-status-closed").hasClass("non-display")) {
                                            $(".active-status-closed").removeClass("non-display")
                                        }
                                    }
                                    else {
                                        if ($(".active-status-verifying").hasClass("non-display")) {
                                            $(".active-status-verifying").removeClass("non-display")
                                        }
                                    }
                                    $scope.showTitleProposal = true;
                                    $scope.hideProposal = true;
                                    $scope.hidePrice = true;
                                    $('#descriptionTable').addClass("non-display");
                                    //$('.btn-payment').hide();
                                    //$('.btn-delete').hide();
                                    //$('.btn-bid').hide();
                                    //$('.btn-accept').hide();
                                    //$('.btn-reject').hide();
                                    $scope.hideRadio = true;
                                    $scope.disable = true;
                                    $scope.isClosed = false
                                    $(".hide-children").attr("data-hide", "phone,tablet,desktop")
                                }
                            }

                            store.set("inputDetailsOrder", inputDetailsOrder);

                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })

                }

                var loadCurrency = function () {
                    api.get('currency/v1').then(function (res) {
                        if (res.data.result) {
                            if (res && res.data && res.data.currencies.length > 0) {
                                $scope.currency = res.data.currencies;


                            }
                            if (currency !== null) {

                                $scope.parentCurrency = $scope.currency[currency.id]


                            }
                            else {
                                $scope.parentCurrency = $scope.currency[100];
                            }


                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                };


                $scope.$on('render', function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                });

                $scope.toggle_row = function (panel) {
                    panel.collapse = panel.collapse ? false : true;
                    if (!panel.collapse) {
                        $timeout(function () {
                            $scope.send_render();
                        });
                    }
                };

                $scope.send_render = function () {
                    $scope.$broadcast('render');
                };

                $scope.add_panel = function (panel, newPanel) {
                    panel.panels.push(newPanel);
                };

                $scope.reset_panel = function (type) {
                    $scope.new_panel = {
                        loading: false,
                        error: false,
                        sizeable: false,
                        draggable: false,
                        removable: false,
                        span: 12,
                        height: "150px",
                        editable: true,
                        type: type
                    };
                };
                module.filter('withoutContainer', [function () {
                    return function () {
                        return _.without(config.panel_names, 'container');
                    };
                }]);

                function checkInputvalue(value1, value2, callback) {

                    var valueReplace1 = value1.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    var valueReplace2 = value2.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, "o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i");
                    db.get("validatePost.json").then(function (result) {
                        if (result.data.length > 0) {
                            var check = result.data;
                            var found = false;
                            var validateNumber1 = valueReplace1.replace(/\s+/g, '').match(/\d+/g);
                            var validateNumber2 = valueReplace2.replace(/\s+/g, '').match(/\d+/g);
                            if (!found) {
                                if (validateNumber1 != null) {
                                    for (var i = 0; i < validateNumber1.length; i++) {
                                        if (validateNumber1[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!found) {
                                if (validateNumber2 != null) {
                                    for (var i = 0; i < validateNumber2.length; i++) {
                                        if (validateNumber2[i].length > 7) {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!found) {
                                for (var j = 0; j < check.length; j++) {
                                    if (valueReplace1.search(check[j].name) != -1 || valueReplace2.search(check[j].name) != -1) {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                            if (found) {
                                callback(true)
                                return true;
                            }
                            else {
                                callback(false)
                                return false;
                            }
                        }
                    });
                }

                $scope.bid = function () {
                    if (!form.valid()) {
                        return;
                    }
                    var inputProposal = $scope.bids.proposal.toString().toLowerCase();
                    var inputTitle = $scope.titleProposal.toString().toLowerCase();
                    checkInputvalue(inputProposal, inputTitle, function (result) {
                        bids.status = 1;
                        if (result) {
                            bids.status = 6;
                        }

                        var getInformation = $('#infoMore').dataTable();

                        var infoToPush = _.filter(getInformation, function (proposal) {
                            return getInformation.isSelected == true;
                        });


                        var getAddonService = $('#addonService').dataTable().fnGetData();
                        for (var i = 0; i < getAddonService.length; i++) {
                            if (getAddonService[i].isSelected) {
                                var objAddonService = {};
                                objAddonService = getAddonService[i];
                            }
                            //if (getAddonService[i].isSelected) {
                            //    getAddonService[i].isSelected = false;
                            //}
                        }
                        var dataJson = {};
                        dataJson.type = infoToPush
                        dataJson.addonService = objAddonService;
                        dataJson.title = $scope.titleProposal;
                        dataJson.cv = $scope.listCVs
                        dataJson.serviceType = $scope.getValueChecked;
                        dataJson.budget = $scope.proposalRes.budgetTo;
                        dataJson.expiredDate = $scope.proposalRes.expiredDate;
                        dataJson.totalBids = $scope.proposalRes.bids;
                        dataJson.indexBroker = $scope.brokerList.indexOf($scope.chosenBroker);
                        var dataJsonPush = JSON.stringify(dataJson);
                        bids.username = Session.userId;
                        bids.expectedCost = $scope.price.net;
                        bids.jsonData = dataJsonPush;
                        bids.proposal = $scope.bids.proposal;
                        bids.currencyId = 101;
                        if (objAddonService) {
                            bids.addonServiceId = objAddonService.id;
                        }
                        if ($scope.chosenBroker) {
                            bids.brokerId = $scope.chosenBroker.id;
                        }
                        if($scope.listCVs.length > 0){
                            var cvIds = [];
                            for(var i =0; i < $scope.listCVs.length; i++ ){
                                cvIds.push($scope.listCVs[i].id);
                            }
                            bids.cvIds = cvIds;
                        }

                        api.put('proposal/' + $stateParams.params + '/v1', bids).then(function (res) {
                            if (res.data.result) {
                                $location.path("/detailSupplier");
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });

                    });
                }
                $scope.updateProposal = function () {
                    if (!form.valid()) {
                        return;
                    }
                    var dataJsonProposal = JSON.parse($scope.dataProposal.jsonData);
                    //console.log("dataJsonProposal",dataJsonProposal)
                    if ($scope.dataProposal.status == 7) {
                        var dataJson = {};
                        var getInformation = $('#infoMore').dataTable();

                        var infoToPush = _.filter(getInformation, function (proposal) {
                            return getInformation.isSelected == true;
                        });
                        dataJson.type = infoToPush
                        dataJson.cv = $scope.listCVs;
                        dataJson.title = $scope.titleProposal;
                        dataJson.serviceType = $scope.getValueChecked;
                        dataJson.addonService = dataJsonProposal.addonService;
                        dataJson.budget = dataJsonProposal.budget;
                        dataJson.indexBroker = dataJsonProposal.indexBroker;
                        dataJson.expiredDate = dataJsonProposal.expiredDate;
                        var dataJsonPush = JSON.stringify(dataJson);
                        bids.status = 1;
                        bids.username = Session.userId;
                        bids.expectedCost = $scope.price.net;
                        bids.jsonData = dataJsonPush;
                        bids.proposal = $scope.bids.proposal;
                        bids.currencyId = 101;
                        api.put('proposal/' + $stateParams.params + '/v1', bids).then(function (res) {


                            if (res.data.result) {

                                $location.path("/detailSupplier");
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        });

                    } else {
                        var inputProposal = $scope.bids.proposal.toString().toLowerCase();
                        var inputTitle = $scope.titleProposal.toString().toLowerCase();
                        checkInputvalue(inputProposal, inputTitle, function (result) {
                            bids.status = "";
                            if (result) {
                                bids.status = 6;
                            }
                            var getInformation = $('#infoMore').dataTable();

                            var infoToPush = _.filter(getInformation, function (proposal) {
                                return getInformation.isSelected == true;
                            });
                            var getAddonService = $('#addonService').dataTable().fnGetData();
                            for (var i = 0; i < getAddonService.length; i++) {
                                if (getAddonService[i].isSelected) {
                                    var objAddonService = {};
                                    objAddonService = getAddonService[i];
                                }
                            }

                            var dataJson = {};

                            dataJson.type = infoToPush
                            dataJson.cv = $scope.listCVs;
                            dataJson.title = $scope.titleProposal;
                            dataJson.serviceType = $scope.getValueChecked;
                            dataJson.addonService = dataJsonProposal.addonService;
                            dataJson.budget = dataJsonProposal.budget;
                            dataJson.indexBroker = dataJsonProposal.indexBroker;
                            dataJson.expiredDate = dataJsonProposal.expiredDate;
                            var dataJsonPush = JSON.stringify(dataJson);
                            bids.username = Session.userId;
                            bids.expectedCost = $scope.price.net;
                            bids.jsonData = dataJsonPush;
                            bids.proposal = $scope.bids.proposal;
                            bids.currencyId = 101;
                            if (objAddonService) {
                                bids.addonServiceId = objAddonService.id;
                            }
                            if ($scope.chosenBroker) {
                                bids.brokerId = $scope.chosenBroker.id;
                            }
                            api.put('proposal/' + $stateParams.params + '/v1', bids).then(function (res) {


                                if (res.data.result) {

                                    $location.path("/detailSupplier");
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            });

                        });
                    }


                }
                $scope.paymentProposal = function () {

                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/confirmProposal.html",
                        controller: 'confirmProposal',
                        resolve: {
                            dataSer: function () {
                                return dataSer;
                            },
                            idProposal: function () {
                                return $stateParams.params;
                            }
                        }

                    });
                }
                $scope.deleteProposal = function () {


                    var deletePopup = $modal.open({
                        templateUrl: "app/partials/form/deleteProposal.html",
                        controller: 'DeleteDialogCtrl',
                        resolve: {
                            idPost: function () {
                                return $stateParams.params;
                            }
                        }
                    });

                };
                $scope.rejectProposal = function () {
                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/rejectProposal.html",
                        controller: 'rejectProposal',
                        resolve: {

                            idPost: function () {
                                return $stateParams.params;
                            }
                        }
                    });
                };
                module.controller('rejectProposal', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idPost',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idPost) {

                        $scope.cancel = function () {
                            $modalInstance.close();
                        }
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        $scope.reject = function () {
                            $rootScope.$on('$locationChangeSuccess', function () {
                                $modalInstance.close();
                            });
                            var rejectOrder = {};
                            rejectOrder.username = Session.userId;
                            rejectOrder.status = 4;
                            api.put('proposal/' + idPost + '/v1', rejectOrder).then(function (res) {
                                if (res.data.result) {
                                    $location.path("/detailSupplier");
                                    return logger.logWarning("Bạn vừa từ chối 1 Order");
                                }
                                else {
                                    logger.logError(res.data.error.description);
                                }
                            });
                            $modalInstance.close();
                        };
                    }
                ]);
                $scope.showEducation = function () {
                    var span = $("#showEducation").children(".glyphicon");
                    if (span.hasClass("glyphicon-chevron-right")) {
                        $("#iconShowEducation").removeClass("glyphicon-chevron-right");
                        $("#iconShowEducation").addClass("glyphicon-chevron-left");
                        $("#addEducation").removeClass("no-display");
                        $("#addEducation").slideDown();


                    } else {
                        $("#iconShowEducation").removeClass("glyphicon-chevron-left");
                        $("#iconShowEducation").addClass("glyphicon-chevron-right");
                        $("#addEducation").addClass("no-display");
                        $("#addEducation").slideUp();
                    }
                };

                $scope.acceptProposal = function () {
                    //var acceptOrder = {};
                    //acceptOrder.username = Session.userId;
                    //acceptOrder.status = 3;
                    //
                    //api.put('serviceorder/' + dataSer.id + '/v1', acceptOrder).then(function (res) {
                    //
                    //    if (res.data.result) {
                    //
                    //    }
                    //    else {
                    //        logger.logError(res.data.error.description);
                    //    }
                    //});


                    modalInstance = $modal.open({
                        templateUrl: "app/partials/form/confirmProposal.html",
                        controller: 'confirmProposal',
                        resolve: {
                            dataSer: function () {
                                return dataSer;
                            },
                            idProposal: function () {
                                return $stateParams.params;
                            }
                        }

                    });
                };
                module.controller('confirmProposal', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'dataSer', 'idProposal',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, dataSer, idProposal) {

                        $scope.title = dataSer.title;
                        $scope.customer = dataSer.aliasName;
                        $scope.cancel = function () {

                            $modalInstance.close();
                        }
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        $scope.accept = function () {
                            $modalInstance.close();
                            var url = config.admin + "payment?paymentType=PP&username=" + Session.userId + "&serviceOrderId=" + dataSer.id + "&proposalIds=" + idProposal + "&redirect_url=" + encodeURIComponent(config.host + "#/payment");
                            window.location.href = url;

                        };

                    }
                ]);
                module.controller('DeleteDialogCtrl', [
                    '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'idPost',
                    function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, idPost) {
                        $scope.confirmDelete = function () {
                            api.delete('proposal/' + idPost + '/v1').then(function (res) {
                                if (res.data.result) {
                                    $location.path("/detailSupplier");

                                }
                                else {
                                    logger.logError(res.data.error.description);
                                }

                            });
                            $modalInstance.close();
                        };
                        $scope.close = function () {
                            $modalInstance.dismiss("cancel");
                        };
                        $rootScope.$on('$locationChangeSuccess', function () {
                            $modalInstance.close();
                        });
                        $scope.cancelDelete = function () {
                            $modalInstance.close();
                        };
                    }
                ]);
            }]);
        module.filter('withoutContainer', [function () {
            return function () {
                return _.without(config.panel_names, 'container');
            };
        }]);


    });

