/** @scratch /panels/5
 * include::panels/text.asciidoc[]
 */

/** @scratch /panels/text/0
 * == text
 * Status: *Stable*
 *
 * The text panel is used for displaying static text formated as markdown, sanitized html or as plain
 * text.
 *
 */
define([
        'angular',
        'app',
        'lodash',
        'require',
        'css!./module.css'
    ],
    function (angular, app, _, require) {
        'use strict';

        var module = angular.module('bidproject.panels.switchlogin', []);
        app.useModule(module);

        module.controller('switchlogin', ['$scope', '$rootScope', '$location', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'api', 'logger', function ($scope, $rootScope, $location, USER_ROLES, AuthService, AUTH_EVENTS, api, logger) {
            $scope.panelMeta = {
                status: "Stable",
                description: "A static text panel that can use plain text, markdown, or (sanitized) HTML"
            };

            // Set and populate defaults
            var _d = {
                /** @scratch /panels/text/5
                 * === Parameters
                 *
                 * mode:: `html', `markdown' or `text'
                 */
                mode: "markdown", // 'html','markdown','text'
                /** @scratch /panels/text/5
                 * content:: The content of your panel, written in the mark up specified in +mode+
                 */
                content: "",
                style: {}
            };
            _.defaults($scope.panel, _d);

            $scope.init = function () {
                $scope.ready = true;
                $scope.userRoles = USER_ROLES;
                $scope.isAuthorized = AuthService.isAuthorized;
            };

            $scope.showProjects = function () {
                var credentials = {};
                $scope.account.role = USER_ROLES.customer,
                    api.post('user/authen/v1', $scope.account).then(function (res) {
                        if (res.data.result) {
                            _.filter(USER_ROLES, function (value, key) {
                                if (res.data.role === value) {
                                    credentials.user_role = key;
                                }
                            });
                            credentials.user_id = res.data.username;
                            credentials.session_id = res.data.user.id;
                            credentials.roles = res.data.user.roles;
                            credentials.accessToken = res.data.accessToken;
                            if (res.data.user.jsonData) {
                                credentials.data = JSON.parse(res.data.user.jsonData);
                            }
                            AuthService.authenticate(credentials);
                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                            $location.path("/post");
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });

            };
            $scope.newAccount = {};
            $scope.signupClick = function () {

                var credentials = {};
                $scope.user.role = [];

                if ($scope.role.customer) {
                    $scope.user.role = USER_ROLES.customer;
                }
                if ($scope.role.supplier) {
                    $scope.user.role = USER_ROLES.supplier;
                }
                if ($scope.user.password === $scope.user.password1) {
                    delete $scope.user.password1;
                } else {
                    return false;
                }
                $scope.newAccount.role = USER_ROLES.customer,
                    console.log('newAccount', $scope.newAccount);
                api.post('customer/register/v1', $scope.newAccount).then(function (res) {

                    if (res.data.result) {
                        credentials.user_id = res.data.username;
                        credentials.session_id = res.data.user.id;
                        credentials.accessToken = res.data.accessToken;
                        if (res.data.user.jsonData) {
                            credentials.data = JSON.parse(res.data.user.jsonData);
                        }
                        credentials.roles = res.data.user.roles;
                        _.filter(USER_ROLES, function (value, key) {
                            if (res.data.role === value) {
                                credentials.user_role = key;
                            }
                        });
                        AuthService.authenticate(credentials);
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        $location.path("/post");
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $modalInstance.close();
                    } else {
                        logger.logError(res.data.error.description);
                    }
                });
            };
            $scope.authenticate = function (provider) {
                $auth.authenticate(provider);
            };
            $scope.account = {};
            $scope.mode = {};
            $scope.mode.selection = "return";
            $scope.setSelection = function (value, element) {
                console.log(value);
                $scope.mode.selection = value;
                if (value === "return") {
                    $("#hasAccount").removeClass("active");
                    $("#hasAccount").addClass("active");
                    $("#notHasAccount").removeClass("active");
                }

                if (value === "new") {
                    $("#notHasAccount").removeClass("active");
                    $("#notHasAccount").addClass("active");
                    $("#hasAccount").removeClass("active");
                }
            };
        }]);
    });
