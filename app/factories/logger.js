define([
  'angular',
  'lodash',
  'toastr'
],
function (angular, _, toastr) {
  'use strict';

  var module = angular.module('bidproject.factories');
  module.factory('logger', [
    function() {
        var logIt;
        toastr.options = {
          "closeButton": true,
          "positionClass": "toast-bottom-right",
          "timeOut": "3000"
        };
        logIt = function(message, type) {
            //console.log("logger", message, type);
          return toastr[type](message);
        };
        return {
          log: function(message) {
            logIt(message, 'info');
          },
          logWarning: function(message) {
            logIt(message, 'warning');
          },
          logSuccess: function(message) {
            logIt(message, 'success');
          },
          logError: function(message) {
            logIt(message, 'error');
          }
        };
    }
]);

});