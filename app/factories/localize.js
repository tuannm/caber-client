define([
  'angular',
  'lodash'
],
  function (angular, _) {
    'use strict';

    var module = angular.module('bidproject.factories');
    module.factory('localize', ['$http', '$rootScope', '$window', function ($http, $rootScope, $window) {
        var localize;
        localize = {
          language: '',
          url: void 0,
          resourceFileLoaded: false,
          successCallback: function (data) {
            localize.dictionary = data;
            localize.resourceFileLoaded = true;
                return $rootScope.$broadcast('localizeResourcesUpdated');
          },
          setLanguage: function (value) {
            console.log("value 1",value)
            localize.language = value.toLowerCase().split("-")[0];
            localStorage.setItem("lang", JSON.stringify(value));
            return localize.initLocalizedResources();
          },
          setUrl: function (value) {
              console.log("value 2",value)
            localize.url = value;
            return localize.initLocalizedResources();
          },
          buildUrl: function () {
            if (!localize.language) {
              localize.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase();
              localize.language = localize.language.split("-")[0];
            }
            return 'i18n/resources-locale_' + localize.language + '.js';
          },
          initLocalizedResources: function () {
            var url;
            url = localize.url || localize.buildUrl();
            return $http({
              method: "GET",
              url: url,
              cache: false
            }).success(localize.successCallback).error(function () {
              return $rootScope.$broadcast('localizeResourcesUpdated');
            });
          },
          urlLocalizedResources: function (file) {
            var url;
            url = 'i18n/resources/' + file + '_' + localize.language + '.js' || localize.buildUrl();
            return $http({
              method: "GET",
              url: url,
              cache: true
            }).success(function(data){
              return $rootScope.$broadcast('urlLocalizeResourcesLoaded', data);
            }).error(function () {
              return $rootScope.$broadcast('localizeResourcesUpdated');
            });
          },
          getLocalizedString: function (value) {
            var result, valueLowerCase;
            result = void 0;
            if (localize.dictionary && value) {
              valueLowerCase = value.toLowerCase();
              if (localize.dictionary[valueLowerCase] === '') {
                result = value;
              } else {
                result = localize.dictionary[valueLowerCase];
              }
            } else {
              result = value;
            }
            return result;
          }
        };
        return localize;
        }]);
  });