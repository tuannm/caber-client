/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define([
    'angular',
    'config',
    'lodash',
    'css!./dash.css'
], function (angular, config, _) {
    "use strict";

    var module = angular.module('bidproject.controllers');


    module.controller('HeaderCtrl', ['$scope', '$rootScope', 'alertSrv', '$location', '$modal', 'AUTH_EVENTS', 'Session', '$element', 'AuthService', 'logger', 'api', 'notification', '$state', 'store',
        function ($scope, $rootScope, alertSrv, $location, $modal, AUTH_EVENTS, Session, $element, AuthService, logger, api, notification, $state, store) {
            loadAvatar();
            $scope.init = function () {
                $scope.ready = false;
                $scope.username = Session.userId;
                switchRole();
                //loadAvatar();
            };

            $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                if (!Session.data) {
                    Session.data = {};
                }
                //$scope.myImage = Session.data.avatar;
                if (Session.data.avatar != undefined) {
                    $scope.myImage = Session.data.avatar;
                } else {
                    $scope.myImage = "images/unknown.png";
                }
                $('#avatar').attr('src', $scope.myImage + '?time=' + new Date());
            });

            $scope.$on("updateAvatar", function () {
                if (!Session.data) {
                    Session.data = {};
                }
                //$scope.myImage = Session.data.avatar;
                if (Session.data.avatar != undefined) {
                    $scope.myImage = Session.data.avatar;
                } else {
                    $scope.myImage = "images/unknown.png";
                }
                $('#avatar').attr('src', $scope.myImage + '?time=' + new Date());

            });

            $scope.notifications = notification;
            $scope.$on("notificationAdded", function () {
                $scope.notifications = notification;
                if (notification.current) {
                    logger.logSuccess(notification.current.title);
                }
            });
            $scope.displayNotification = function (message) {
                if (message.type === "notify-customer" && Session.userRole == 'customer') {
                    if ($location.$$path === "/viewdetailpost/" + message.data_id) {
                        $state.reload();

                    } else {
                        $location.path("/viewdetailpost/" + message.data_id);
                    }
                } else if (message.type === "notify-supplier" && (Session.userRole == 'supplier' || Session.userRole == 'broker')) {
                    if ($location.$$path === "/detailOrder/" + message.data_id) {
                        $state.reload();
                    } else {
                        $location.path("/detailOrder/" + message.data_id);
                    }
                }
                if (!message.is_read) {
                    $rootScope.socket.emit('updateNotification', [message._id], Session.id);
                }
            }

            $scope.$on("navigationDisplay", function (object, navigation) {
                $('.logo', $element).addClass("hidden");
                $('.menu-button', $element).addClass("hidden");
            })

            $scope.$on("navigationHidden", function (object, navigation) {
                $('.menu-button', $element).removeClass("hidden");
                $('.logo', $element).removeClass("hidden");
            })
            $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                switchRole();
            })

            function switchRole() {
                if (Session.multi !== 1 || Session.multi === undefined) {

                    $("#btn-switchRole").addClass("non-display");

                }
                else {

                    $("#btn-switchRole").removeClass("non-display");
                }
            }

            function loadAvatar() {
                if (!Session.data) {
                    Session.data = {};
                }
                if (Session.data.avatar != undefined) {
                    $scope.myImage = Session.data.avatar;
                } else {
                    $scope.myImage = "images/unknown.png";
                }
            }

            console.log("notifications", notification)
            $scope.switchRole = function () {
                var credentials = {};

                if (Session.multi == 1) {
                    Session.changeRole();
                    $scope.notifications = [];
                    notification.messages = [];
                    if (Session.userRole === "supplier") {
                        credentials.user_id = Session.userId;
                        credentials.session_id = Session.id;
                        credentials.user_role = Session.userRole;
                        credentials.multi = Session.multi;
                        credentials.accessToken = Session.accessToken;
                        credentials.data = Session.data;
                        //if(res.data.user.jsonData){
                        //    credentials.data = JSON.parse(res.data.user.jsonData);
                        //}
                        api.get('user/' + Session.id + '/v1').then(function (res) {

                            if (res.data.result) {
                                if (res.data.result) {
                                    if (res.data.user.skill === null) {
                                        AuthService.authenticate(credentials);
                                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                        $location.path("/updateskills");
                                    }
                                    else {

                                        AuthService.authenticate(credentials);
                                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                        $location.path("/detailSupplier");
                                    }

                                }
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        })
                    }
                    else {
                        credentials.user_id = Session.userId;
                        credentials.session_id = Session.id;
                        credentials.user_role = Session.userRole;
                        credentials.multi = Session.multi;
                        credentials.accessToken = Session.accessToken;
                        credentials.data = Session.data;
                        //if(res.data.user.jsonData){
                        //    credentials.data = JSON.parse(res.data.user.jsonData);
                        //}
                        AuthService.authenticate(credentials);

                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

                        $location.path("/post");
                    }
                }
            };

            $scope.loginClick = function () {
                $location.path("/login");
            };
            $scope.signupClick = function () {
                $location.path("/signup");
            };
            $scope.edituserClick = function () {
                $location.path("/edituser");
            };
            $scope.signupPopupClick = function () {
                $location.path("/mockup/file/updateskills");
            };
            $scope.loginPopup = function () {
                var modalInstance;

                modalInstance = $modal.open({
                    templateUrl: "loginPopup.html"
                });

            };
            $scope.signupPopup = function () {
                var modalInstance;

                modalInstance = $modal.open({
                    templateUrl: "signupPopup.html"


                });
            };
            $scope.makeOrderClick = function () {
                $location.path("/post");
            };
            $scope.switchLogin = function () {
                $location.path("/switchLogin");
            };
            $scope.logoutClick = function () {
                $scope.logout();
                $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
                store.data = {}
                $location.path("/default");

            };

        }]);
    module.controller('SignupCtrl', [
        '$scope', '$modalInstance', 'items', function ($scope, $modalInstance, items) {
        }
    ]);
});
