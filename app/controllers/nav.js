/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define(['angular',
    'app',
    'lodash',
    'config',
    'css!./nav.css'
],

    function (angular, app, _, config) {
        "use strict";

        var module = angular.module('bidproject.controllers');
        module.controller('nav', ['$scope', '$rootScope', 'alertSrv', '$location', 'db', 'AUTH_EVENTS', 'Session', 'localize', '$stateParams', 'api', 'logger', 'AuthService', 'notification', 'navigations', 'store', 'socketServices', '$state',
            function ($scope, $rootScope, alertSrv, $location, db, AUTH_EVENTS, Session, localize, $stateParams, api, logger, AuthService, notification, navigations, store, socketServices, $state) {
                //$scope.lang = 'Vietnam';
                $scope.menu = {};
                loadAvatar();
                $scope.init = function () {
                    var lang = JSON.parse(localStorage.getItem("lang")) || [];
                    console.log("lang", lang)
                    if (lang.length > 0) {
                        if (lang == "VI-VN") {
                            $scope.lang = 'Vietnam';
                        }
                        else {
                            $scope.lang = 'English';
                        }
                    }
                    else {
                        $scope.lang = 'Vietnam';
                    }
                    $scope.ready = false;
                    if (Session.id != undefined) {
                        $(".chat-design").removeClass("mar-left-20");
                        $(".flag-design").removeClass("mar-left-80");
                    }
                    getNav();
                    $scope.username = Session.userId;
                    $scope.setLang($scope.lang);
                    $scope.chatBox = {isHidden:false, isShow:false, message:''};
                    switchRole();
                };

                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    if (!Session.data) {
                        Session.data = {};
                    }
                    //$scope.myImage = Session.data.avatar;
                    if (Session.data.avatar != undefined) {
                        $scope.myImage = Session.data.avatar;
                    } else {
                        $scope.myImage = "images/unknown.png";
                    }
                    $('#avatarNav').attr('src', $scope.myImage + '?time=' + new Date());

                    $(".chat-design").removeClass("mar-left-20");
                    $(".flag-design").removeClass("mar-left-80");
                });

                $scope.$on(AUTH_EVENTS.logoutSuccess, function () {
                    $(".chat-design").addClass("mar-left-20");
                    $(".flag-design").addClass("mar-left-80");
                });

                $scope.$on("updateAvatar", function () {
                    if (!Session.data) {
                        Session.data = {};
                    }
                    //$scope.myImage = Session.data.avatar;
                    if (Session.data.avatar != undefined) {
                        $scope.myImage = Session.data.avatar;
                    } else {
                        $scope.myImage = "images/unknown.png";
                    }
                    $('#avatarNav').attr('src', $scope.myImage + '?time=' + new Date());
                });

                function loadAvatar() {
                    if (!Session.data) {
                        Session.data = {};
                    }
                    if (Session.data.avatar != undefined) {
                        $scope.myImage = Session.data.avatar;
                    } else {
                        $scope.myImage = "images/unknown.png";
                    }
                }

                $scope.setLang = function (lang) {
                    switch (lang) {
                        case 'Vietnam':
                            localize.setLanguage('VI-VN');
                            break;
                        case 'English':
                            localize.setLanguage('EN-US');
                            break;
                        case 'Español':
                            localize.setLanguage('ES-ES');
                            break;
                        case '日本語':
                            localize.setLanguage('JA-JP');
                            break;
                        case '中文':
                            localize.setLanguage('ZH-TW');
                            break;
                        case 'Deutsch':
                            localize.setLanguage('DE-DE');
                            break;
                        case 'français':
                            localize.setLanguage('FR-FR');
                            break;
                        case 'Italiano':
                            localize.setLanguage('IT-IT');
                            break;
                        case 'Portugal':
                            localize.setLanguage('PT-BR');
                            break;
                        case 'Русский язык':
                            localize.setLanguage('RU-RU');
                            break;
                        case '한국어':
                            localize.setLanguage('KO-KR');
                    }
                    return $scope.lang = lang;
                };
                $scope.getFlag = function () {
                    var lang;
                    lang = $scope.lang;
                    switch (lang) {
                        case 'Vietnam':
                            return 'flags-vietnam';
                        case 'English':
                            return 'flags-american';
                        case 'Español':
                            return 'flags-spain';
                        case '日本語':
                            return 'flags-japan';
                        case '中文':
                            return 'flags-china';
                        case 'Deutsch':
                            return 'flags-germany';
                        case 'français':
                            return 'flags-france';
                        case 'Italiano':
                            return 'flags-italy';
                        case 'Portugal':
                            return 'flags-portugal';
                        case 'Русский язык':
                            return 'flags-russia';
                        case '한국어':
                            return 'flags-korea';
                    }
                };

                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    switchRole();


                })
                function switchRole() {
                    if (Session.multi !== 1 || Session.multi === undefined) {
                        $("#btn-switchRoleMobile").removeClass("dropdown-menu");
                        $("#btn-switchRoleMobile").addClass("non-display");
                        $("#iconDown").addClass("non-display");
                    }
                    else {
                        $("#iconDown").removeClass("non-display");
                        $("#btn-switchRoleMobile").addClass("dropdown-menu");
                        $("#btn-switchRoleMobile").removeClass("non-display");
                    }
                }

                $scope.switchRole = function () {

                    var credentials = {};
                    if (Session.multi == 1) {
                        $scope.notifications = [];
                        notification.messages = [];
                        Session.changeRole();
                        if (Session.userRole === "supplier") {
                            credentials.user_id = Session.userId;
                            credentials.session_id = Session.id;
                            credentials.user_role = Session.userRole;
                            credentials.multi = Session.multi;
                            credentials.accessToken = Session.accessToken;
                            credentials.data = Session.data;
                            api.get('user/' + Session.id + '/v1').then(function (res) {

                                if (res.data.result) {
                                    if (res.data.result) {
                                        if (res.data.user.skill === null) {
                                            AuthService.authenticate(credentials);
                                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                            $location.path("/updateskills");
                                        }
                                        else {

                                            AuthService.authenticate(credentials);
                                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                            $location.path("/detailSupplier");
                                        }

                                    }
                                } else {
                                    logger.logError(res.data.error.description);
                                }
                            })
                        }
                        else {

                            credentials.user_id = Session.userId;
                            credentials.session_id = Session.id;
                            credentials.user_role = Session.userRole;
                            credentials.multi = Session.multi;
                            credentials.accessToken = Session.accessToken;
                            credentials.data = Session.data;
                            AuthService.authenticate(credentials);
                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                            $location.path("/post");
                        }
                    }
                };

                $scope.logoutClicks = function () {
                    $scope.logout();
                    $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
                    store.data = {}
                    $location.path("/default");
                };

                $scope.$on(AUTH_EVENTS.loginSuccess, function () {
                    $scope.username = Session.userId;
                    getNav();
                });

                $scope.redirect = function (path) {
                    //if (path == 'inviteFriend') {
                    //var customLocale = {};
                    //customLocale.cancelButtonLabel = "Không";
                    //customLocale.laterButtonLabel = "Để Sau";
                    //customLocale.rateButtonLabel = "Đánh Giá Ngay";
                    //AppRate.preferences.customLocale = customLocale;

                    //AppRate.preferences.openStoreInApp = true;
                    //AppRate.preferences.storeAppURL.ios = 'vaber.itsol.vn';
                    //AppRate.preferences.storeAppURL.android = 'market://details?id=vaber.itsol.vn';
                    //AppRate.preferences.promptAgainForEachNewVersion = true;
                    //AppRate.promptForRating(true);
                    //}
                    if (path !== "#") {
                        $location.path(path);
                        store.set('post')
                        store.set('searchFeed')
                    }
                };

                function getNav() {
                    if ($scope.menu.data) {
                        var urlCurrent = $stateParams.routeId;
                        var i;
                        for (i = 0; i < $scope.menu.data.length; i++) {
                            delete $scope.menu.data[i].active;
                            if (urlCurrent == "quick") {
                                $scope.menu.data[1].active = "active";
                                return;
                            }
                            if (urlCurrent == "helper") {
                                $scope.menu.data[1].active = "active";
                                return;
                            }
                            if (urlCurrent == "tutoring") {
                                $scope.menu.data[1].active = "active";
                                return;
                            }
                            if (urlCurrent == "headhunt") {
                                $scope.menu.data[1].active = "active";
                                return;
                            }
                            if (urlCurrent == "viewdetailpost") {
                                for (i = 0; i < $scope.menu.data.length; i++) {
                                    delete $scope.menu.data[i].active;
                                }
                                $scope.menu.data[4].active = "active";
                                return;
                            }
                            if (urlCurrent == "viewdetailproposal") {
                                $scope.menu.data[4].active = "active";
                                return;
                            }
                            if (urlCurrent == "payment") {
                                $scope.menu.data[4].active = "active";

                                return;
                            }
                            if (urlCurrent == "detailOrder") {
                                for (i = 0; i < $scope.menu.data.length; i++) {
                                    delete $scope.menu.data[i].active;
                                }
                                $scope.menu.data[5].active = "active";
                                return;
                            }
                            if (urlCurrent == $scope.menu.data[i].url) {
                                $scope.menu.data[i].active = "active";
                                $scope.menu.data = angular.copy($scope.menu.data);
                            }
                        }

                    } else {
                        db.get("nav.json").then(function (result) {
                            if (result && result.data) {
                                $scope.menu = {
                                    "data":result.data
                                };
                            }
                            var urlCurrent = $stateParams.routeId;
                            var i;
                            for (i = 0; i < result.data.length; i++) {
                                if (urlCurrent == "viewdetailpost") {
                                    result.data[4].active = "active";
                                    return;
                                }
                                if (urlCurrent == "viewdetailproposal") {
                                    $scope.menu.data[4].active = "active";

                                    return;
                                }
                                if (urlCurrent == "payment") {
                                    $scope.menu.data[4].active = "active";

                                    return;
                                }
                                if (urlCurrent == "detailOrder") {
                                    result.data[5].active = "active";
                                    return;
                                }
                                if (urlCurrent == "managecv") {
                                    result.data[6].active = "active";
                                    return;
                                }
                                if (urlCurrent == result.data[i].url) {
                                    result.data[i].active = "active";
                                }
                            }
                        });
                    }
                }

                $scope.$on("$stateChangeSuccess", function (event, args) {
                    getNav();
                });

                $scope.openChat = function () {
                    $rootScope.chatBox.isShow = true;
                    $rootScope.chatBox.isHidden = false;
                    $rootScope.chat_notifications = [];
                };

                $scope.hideChat = function () {
                    $rootScope.chatBox.isHidden = !$rootScope.chatBox.isHidden;
                    $rootScope.chat_notifications = [];
                };

                $scope.closeChat = function () {
                    $rootScope.chatBox.isShow = false;
                    $rootScope.chatBox.isHidden = false;
                };

                $scope.findSupporter = function () {
                    socketServices.findSupporter();
                };

                //Notification
                $scope.notifications = notification;
                $scope.$on("notificationAdded", function () {
                    $scope.notifications = notification;
                });
                $scope.displayNotification = function (message) {
                    if (message.type === "notify-customer" && Session.userRole == 'customer') {
                        if ($location.$$path === "/viewdetailpost/" + message.data_id) {
                            $state.reload();
                        } else {
                            $location.path("/viewdetailpost/" + message.data_id);
                        }
                    } else if (message.type === "notify-supplier" && Session.userRole == 'supplier') {
                        if ($location.$$path === "/detailOrder/" + message.data_id) {
                            $state.reload();
                        } else {
                            $location.path("/detailOrder/" + message.data_id);
                        }
                    }
                    if (!message.is_read) {
                        $rootScope.socket.emit('updateNotification', [message._id], Session.id);
                    }
                }
            }]);
    });
