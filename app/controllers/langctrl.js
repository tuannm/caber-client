/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define([
    'angular',
    'config',
    'lodash'
], function (angular, config, _) {
    "use strict";

    var module = angular.module('bidproject.controllers');
    module.controller('LangCtrl', ['$scope', 'localize',
        function ($scope, localize) {
            //$scope.lang = 'Vietnam';
            $scope.init = function () {
                var lang = JSON.parse(localStorage.getItem("lang")) || [];
                if (lang.length > 0) {
                    if (lang == "VI-VN") {
                        $scope.lang = 'Vietnam';
                    }
                    else {
                        $scope.lang = 'English';
                    }
                }
                else {
                    $scope.lang = 'Vietnam';
                }
                $scope.setLang($scope.lang);
            }

            $scope.setLang = function (lang) {
                switch (lang) {
                    case 'Vietnam':
                        localize.setLanguage('VI-VN');
                        break;
                    case 'English':
                        localize.setLanguage('EN-US');
                        break;
                    case 'Español':
                        localize.setLanguage('ES-ES');
                        break;
                    case '日本語':
                        localize.setLanguage('JA-JP');
                        break;
                    case '中文':
                        localize.setLanguage('ZH-TW');
                        break;
                    case 'Deutsch':
                        localize.setLanguage('DE-DE');
                        break;
                    case 'français':
                        localize.setLanguage('FR-FR');
                        break;
                    case 'Italiano':
                        localize.setLanguage('IT-IT');
                        break;
                    case 'Portugal':
                        localize.setLanguage('PT-BR');
                        break;
                    case 'Русский язык':
                        localize.setLanguage('RU-RU');
                        break;
                    case '한국어':
                        localize.setLanguage('KO-KR');
                }
                return $scope.lang = lang;
            };
            $scope.getFlag = function () {
                var lang;
                lang = $scope.lang;
                switch (lang) {
                    case 'Vietnam':
                        return 'flags-vietnam';
                    case 'English':
                        return 'flags-american';
                    case 'Español':
                        return 'flags-spain';
                    case '日本語':
                        return 'flags-japan';
                    case '中文':
                        return 'flags-china';
                    case 'Deutsch':
                        return 'flags-germany';
                    case 'français':
                        return 'flags-france';
                    case 'Italiano':
                        return 'flags-italy';
                    case 'Portugal':
                        return 'flags-portugal';
                    case 'Русский язык':
                        return 'flags-russia';
                    case '한국어':
                        return 'flags-korea';
                }
            };
        }]);
});
