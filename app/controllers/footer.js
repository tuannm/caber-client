/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define(['angular', 'config', 'lodash'], function(angular, config, _) {
    "use strict";

    var module = angular.module('bidproject.controllers');

    module.controller('FooterCtrl', ['$scope', '$rootScope', 'alertSrv', '$location', 'socketServices', 'USER_ROLES',
        function($scope, $rootScope, alertSrv, $location, socketServices, USER_ROLES) {
            $scope.init = function() {
                $rootScope.chatBox = {isHidden: false, isShow: false, message: ''};
            };

            $scope.$on("messages", function(obj, data){
                $scope.supports = angular.copy(data);
            })

            $scope.openChat = function() {
                if($rootScope.user.user_role != 'supporter'){
                    $rootScope.chatBox.isShow = true;
                    if ($rootScope.chat_id == undefined) {
                        $scope.startChat($scope.user.user_id, $scope.user.user_email);
                    }
                }else{
                    $location.path("/chat");
                }
            };

            $scope.startChat = function (user_id, email) {
                $rootScope.user = {user_id: user_id, user_email: email, user_role: 'guest', messages: []};
                socketServices.chatService();
            };

            $scope.sendMessage = function() {
                if ($rootScope.chatBox.message != '') {
                    socketServices.sendMessage($rootScope.chatBox.message);
                    $rootScope.chatBox.message = '';
                }
            };

            $scope.hideChat = function() {
                $rootScope.chatBox.isHidden = !$rootScope.chatBox.isHidden;
                $rootScope.chat_notifications = [];
            };

            $scope.closeChat = function() {
                $rootScope.chatBox.isShow = false;
                $rootScope.chatBox.isHidden = false;
            };

            $scope.findSupporter = function() {
                socketServices.findSupporter();
            };
        }
    ]);
});
