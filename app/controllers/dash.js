/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define([
        'angular',
        'config',
        'lodash',
        'services/all',
        'css!./dash.css'
    ],
    function (angular, config, _) {
        "use strict";

        var module = angular.module('bidproject.controllers');

        module.controller('DashCtrl', ['$rootScope', '$scope', '$location', 'dashboard', 'alertSrv', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', '$modal', 'windowsize', 'db', 'navigations', 'socketServices', 'localize', 'logger', 'store',
            function ($rootScope, $scope, $location, dashboard, alertSrv, USER_ROLES, AuthService, AUTH_EVENTS, $modal, windowsize, db, navigations, socketServices, localize, logger, store) {
                $scope.editor = {
                    index: 0
                };

                $rootScope.appBusy = null;
                $scope.init = function () {
                    $scope.config = config;
                    // Make stuff, including lodash available to views
                    $scope._ = _;
                    $scope.dashboard = dashboard;
                    $scope.dashAlerts = alertSrv;
                    // Clear existing alerts
                    $scope.userRoles = USER_ROLES;
                    $scope.isLogin = AuthService.isAuthenticated();
                    $scope.isAuthorized = AuthService.isAuthorized;
                    $scope.logout = AuthService.logout;
                    alertSrv.clearAll();
                    $scope.reset_row();
                    $rootScope.chat_notifications = [];
                    $rootScope.notifications = [];
                    socketServices.connect();
                    $rootScope.title = "";
                };


                //Menu display
                var isHidden = false;

                $scope.openChat = function () {
                    $rootScope.chatBox.isShow = true;
                    $rootScope.chatBox.isHidden = false;
                    $rootScope.chat_notifications = [];
                };

                $scope.notification = function () {
                    socketServices.notification();
                };

                $rootScope.online = navigator.onLine;
                $rootScope.$watch("online", function (lastConnect, isConnect) {
                    if (!lastConnect) {
                        alertSrv.set(localize.getLocalizedString('network disconnect'), localize.getLocalizedString('network warning'), 'danger');
                    } else {
                        alertSrv.clearAll();
                    }
                });

                $scope.$on(AUTH_EVENTS.serviceUnavailable, function () {
                    console.log("error");
                    logger.logError(localize.getLocalizedString('service unavailable'));
                });

                $scope.$on('$stateChangeSuccess', function (event, next) {
                    if (window.loading_screen) {
                        window.loading_screen.finish();
                    }
                    if (next && next.navigation === 'hidden' && $('#app').hasClass("on-canvas")) {
                        $('#app').toggleClass('on-canvas');
                        isHidden = true;
                    } else if (next && next.navigation !== 'hidden' && !$('#app').hasClass("on-canvas")) {
                        isHidden = false;
                        $('#app').toggleClass('on-canvas');
                    }
                    if ($('#app').hasClass('on-canvas') && windowsize.devicetype() !== 'Desktop') {
                        $('#app').toggleClass('on-canvas');
                    }
                });

                $scope.$on("window.size", function (object, type) {
                    if (isHidden && $('#app').hasClass("on-canvas")) {
                        $('#app').toggleClass('on-canvas');
                    } else if (!isHidden && !$('#app').hasClass("on-canvas")) {
                        $('#app').toggleClass('on-canvas');
                    }
                    if (type !== 'Desktop' && $('#app').hasClass("on-canvas")) {
                        $('#app').toggleClass('on-canvas');
                    }
                });
                if (navigations.current != undefined) {
                    $scope.navigation = navigations.current;
                }
                else {
                    $scope.navigation = navigations.collections[navigations.collections.length - 1]
                }
                $scope.$on("navigationChange", function () {
                    $scope.navigation = navigations.current;
                })

                document.addEventListener('deviceready', onDeviceReady, false);
                function onDeviceReady() {
                    if (window.cordova && window.cordova.InAppBrowser) {
                        window.open = window.cordova.InAppBrowser.open;
                    }
                }

                $rootScope.paymentUrl = function (url) {
                    console.log(url)
                    if (window.cordova && window.cordova.InAppBrowser) {
                        window.open(url, '_system');
                    } else {
                        window.location.href = url;
                    }
                }

                window.handleOpenURL = function (url) {
                    setTimeout(function () {
                        var convertUrl = url.replace(config.host, "").replace('#', '');
                        $scope.$apply(function () {
                            console.log("redirect", convertUrl);
                            $location.path(convertUrl);
                        })
                    }, 1000);
                }

                //Authorize
                $scope.account = {};
                $scope.newAccount = {};
                $scope.role = {};
                $scope.isShowlogin = false;


                var showLoginDialog = function () {
                    if (!$scope.isShowlogin) {
                        $scope.isShowlogin = true;
                        var modalInstance;
                        modalInstance = $modal.open({
                            templateUrl: "app/partials/form/login.html",
                            controller: 'LoginDialogCtrl'
                        });
                        modalInstance.result.then((function (result) {
                            $scope.isShowlogin = false;
                            store.set("categoryClick")
                        }), function (result) {
                            $scope.isShowlogin = false;
                            $location.path(config.default_route);
                            store.set("categoryClick")
                        });
                    }
                };
                $rootScope.$on(AUTH_EVENTS.notAuthenticated, showLoginDialog);
                $rootScope.$on(AUTH_EVENTS.sessionTimeout, showLoginDialog);

                $scope.isPanel = function (obj) {
                    if (!_.isNull(obj) && !_.isUndefined(obj) && !_.isUndefined(obj.type)) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.add_row = function (dash, row) {
                    dash.rows.push(row);
                };

                $scope.reset_row = function () {
                    $scope.row = {
                        title: '',
                        height: '150px',
                        editable: true
                    };
                };

                $scope.row_style = function (row) {
                    return {'min-height': row.collapse ? '5px' : row.height};
                };

                $scope.panel_path = function (type) {
                    if (type) {
                        return 'app/panels/' + type.replace(".", "/");
                    } else {
                        return false;
                    }
                };

                $scope.edit_path = function (type) {
                    var p = $scope.panel_path(type);
                    if (p) {
                        return p + '/editor.html?version=0.9.3';
                    } else {
                        return false;
                    }
                };

                $scope.pulldownTabStyle = function (i) {
                    var classes = ['bgPrimary', 'bgSuccess', 'bgWarning', 'bgDanger', 'bgInverse', 'bgInfo'];
                    i = i % classes.length;
                    return classes[i];
                };

                $scope.setEditorTabs = function (panelMeta) {
                    $scope.editorTabs = ['General', 'Panel'];
                    if (!_.isUndefined(panelMeta.editorTabs)) {
                        $scope.editorTabs = _.union($scope.editorTabs, _.pluck(panelMeta.editorTabs, 'title'));
                    }
                    return $scope.editorTabs;
                };

                // This is whoafully incomplete, but will do for now
                $scope.parse_error = function (data) {
                    var _error = data.match("nested: (.*?);");
                    return _.isNull(_error) ? data : _error[1];
                };

                $scope.init();
            }]);

        module.controller('LoginDialogCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', 'USER_ROLES', '$modal', 'ezfb', 'api', 'logger', 'socketServices', 'pushNotification', 'store',
            function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, USER_ROLES, $modal, ezfb, api, logger, socketServices, pushNotification, store) {

                $scope.account = {};
                $scope.mode = {};
                $scope.email_forgotPass = "";
                $scope.mode.selection = "return";
                $scope.setSelection = function (value, element) {
                    $scope.mode.selection = value;
                    if (value === "return") {
                        $("#hasAccount").removeClass("active");
                        $("#hasAccount").addClass("active");
                        $("#notHasAccount").removeClass("active");
                    }

                    if (value === "new") {
                        $("#notHasAccount").removeClass("active");
                        $("#notHasAccount").addClass("active");
                        $("#hasAccount").removeClass("active");
                    }
                };
                //$scope.getPasswordBtn = true;

                $scope.notify = function (type) {
                    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

                    if ($scope.account.email_forgotPass !== undefined) {
                        if (re.test($scope.account.email_forgotPass) == true) {
                            return logger.logSuccess("Email đã được gửi. Nếu địa chỉ email vừa nhập đã được đăng kí trên Vaber.com , bạn sẽ nhận được email hướng dẫn thiết lập mật khẩu mới.");

                        }
                    }
                };

                $scope.getPassword = function () {
                    var forgotPass = {};
                    forgotPass.email = $scope.account.email_forgotPass
                    api.post('password/v1', forgotPass).then(function (res) {
                        if (res.data.result) {
                            console.log("res.data", res.data)
                            logger.logSuccess("Password mới đã được gửi đến email của bạn")
                        }
                        else {
                            logger.logError(res.data.error.description);
                        }
                    });
                }
                function openChoserole(res) {
                    var _modalChoseRole;
                    _modalChoseRole = $modal.open({
                        templateUrl: "app/partials/form/choseRole.html",
                        controller: 'ChoseRoleCtrl',
                        resolve: {
                            res: function () {
                                return res;
                            }
                        }
                    });

                }

                var formPopupLogin = $("#formPopupLogin");


                $scope.loginPopup = function () {
                    if (!$("#formPopupLogin").valid()) {
                        return;

                    }
                    var credentials = {};

                    api.post('user/authen/v1', $scope.account).then(function (res) {
                        if (res.data.result) {
                            $rootScope.user = {
                                user_id: res.data.user.username,
                                user_email: res.data.user.email,
                                user_role: res.data.user.roles[0],
                                messages: []
                            };
                            credentials.user_id = res.data.user.username;
                            credentials.session_id = res.data.user.id;
                            credentials.accessToken = res.data.accessToken;
                            if (res.data.user.jsonData) {
                                credentials.data = JSON.parse(res.data.user.jsonData);
                            }
                            pushNotification.register(function (registerId, type) {
                                socketServices.notification({
                                    register_id: registerId,
                                    user: {session_id: res.data.user.id, user_role: res.data.user.roles[0]},
                                    type: type
                                });
                            });
                            if (res.data.user.roles.length > 1) {
                                openChoserole(res)
                                $modalInstance.close();
                            } else {
                                _.filter(USER_ROLES, function (value, key) {
                                    if (res.data.user.roles.indexOf(value) != -1) {
                                        if (res.data.user.userType == "B") {
                                            credentials.user_role = "broker";
                                        } else {
                                            credentials.user_role = key;
                                        }
                                    }
                                });
                                if (res.data.user.roles.indexOf(USER_ROLES.customer) != -1) {
                                    AuthService.authenticate(credentials);
                                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                    $scope.post = store.get("post") ? store.get("post") : {};
                                    if ($scope.post.title != undefined || $scope.post.indexCategory != undefined) {
                                        $location.path("/post");
                                    } else {
                                        $location.path("/dashboard");
                                    }
                                    $modalInstance.close();
                                }
                                if (res.data.user.roles.indexOf(USER_ROLES.supplier) != -1) {
                                    if (res.data.user.userType == "B") {
                                        AuthService.authenticate(credentials);
                                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                        $location.path("/detailSupplier");
                                        $modalInstance.close();
                                    } else {
                                        if (res.data.user.skill === null) {
                                            AuthService.authenticate(credentials);
                                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                            $location.path("/updateskills");
                                            $modalInstance.close();
                                        }
                                        else {
                                            $rootScope.user.user_skills = res.data.user.skill;
                                            AuthService.authenticate(credentials);
                                            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                            //$location.path("/detailSupplier");
                                            //$location.path("/feeds");
                                            $location.path("/dashboard");
                                            $modalInstance.close();
                                        }
                                    }


                                }
                                if (res.data.user.roles.indexOf(USER_ROLES.supporter) != -1) {
                                    AuthService.authenticate(credentials);
                                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                    $location.path("/chat");
                                    $modalInstance.close();
                                }

                            }
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    }, function (error) {

                    });
                };


                $scope.close = function () {
                    $modalInstance.dismiss("close");
                    store.set("categoryClick")
                };


                $scope.newAccount = {};
                $scope.role = {};
                //var formsignup = $("#signupPopup");
                if (store.get("categoryClick")) {
                    $scope.isClickCategory = store.get("categoryClick");
                    //$("#customer").prop("checked", true);
                    $scope.role.customer = true;
                }

                $scope.signupPopupClick = function () {
                    if (!$("#signupPopup").valid()) {
                        return;

                    }
                    $scope.newAccount.role = [];
                    var signupUser = {};

                    var credentials = {};
                    var _modalChoseRole;

                    if ($scope.role.customer) {
                        $scope.newAccount.role.push(USER_ROLES.customer);
                    }
                    if ($scope.role.supplier) {
                        $scope.newAccount.role.push(USER_ROLES.supplier);
                    }

                    signupUser.username = $scope.newAccount.username;
                    signupUser.password = $scope.newAccount.password;
                    signupUser.email = $scope.newAccount.email;
                    signupUser.roles = $scope.newAccount.role;
                    signupUser.phoneNumber = $scope.newAccount.phoneNumber;

                    api.post('user/register/v1', signupUser).then(function (res) {

                        if (res.data.result) {


                            credentials.user_id = res.data.user.username;
                            credentials.session_id = res.data.user.id;
                            credentials.accessToken = res.data.accessToken;
                            if (res.data.user.jsonData) {
                                credentials.data = JSON.parse(res.data.user.jsonData);
                            }
                            if (res.data.user.roles.length > 1) {
                                openChoserole(res)
                                $modalInstance.close();
                            }
                            else {
                                _.filter(USER_ROLES, function (value, key) {
                                    if (res.data.user.roles.indexOf(value) != -1) {
                                        credentials.user_role = key;
                                    }
                                });
                                if (res.data.user.roles.indexOf(USER_ROLES.customer) != -1) {
                                    AuthService.authenticate(credentials);
                                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                    $scope.post = store.get("post") ? store.get("post") : {};
                                    if ($scope.post.title != undefined || $scope.post.indexCategory != undefined) {
                                        $location.path("/post");
                                    } else {
                                        $location.path("/dashboard");
                                    }
                                    //$location.path("/post");
                                    //$location.path("/dashboard");
                                    $modalInstance.close();
                                }
                                if (res.data.user.roles.indexOf(USER_ROLES.supplier) != -1) {
                                    if (res.data.user.skill === null) {
                                        AuthService.authenticate(credentials);
                                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                        $location.path("/updateskills");
                                        $modalInstance.close();
                                    }
                                    else {
                                        AuthService.authenticate(credentials);

                                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                                        //$location.path("/feeds");
                                        $location.path("/dashboard");
                                        $modalInstance.close();
                                    }

                                }
                            }
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    });

                };
                $scope.forgotPass = true;
                $scope.toggle = function () {
                    $scope.forgotPass = !$scope.forgotPass;
                };
                document.addEventListener('deviceready', onDeviceReady, false);
                function onDeviceReady() {
                    console.log("window", window);
                }

            }
        ]);

        module.controller('AddRoleCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', '$modal', 'res', 'USER_ROLES', 'Session', 'store', 'api', 'logger', 'db', function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, $modal, res, USER_ROLES, Session, store, api, logger, db) {
                var credentials = {};
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });

                _.filter(USER_ROLES, function (value, key) {
                    credentials.user_role = key;

                });
                $scope.checkcustomer = false;
                $scope.checksupllier = false;
                $scope.role = {};
                    console.log("hehe");
                db.get("work.json").then(function (result) {
                    if (result && result.data && result.data.length > 0) {
                        $scope.categories = result.data;
                    }
                });
                    api.get('user/' + Session.id + '/v1').then(function (res) {
                        if (res.data.result) {
                            if (res.data.user.roles.indexOf(USER_ROLES.supplier) != -1) {
                                $("#supplier").prop("checked", true);
                                $scope.role.supplier = true;

                                $scope.checksupllier = true;
                            }
                            if (res.data.user.roles.indexOf(USER_ROLES.customer) != -1) {
                                $("#customer").prop("checked", true);
                                $scope.role.customer = true;

                                $scope.checkcustomer = true;
                            }
                        } else {
                            logger.logError(res.data.error.description);
                        }
                    })
                $scope.accountdetails = {};
                $scope.saveAddRoleClick = function () {
                    $scope.accountdetails.role = [];
                    var list = {};
                    if ($scope.role.customer) {
                        $scope.accountdetails.role.push(USER_ROLES.customer);
                    }
                    if ($scope.role.supplier) {
                        $scope.accountdetails.role.push(USER_ROLES.supplier);
                    }
                    list.roles = $scope.accountdetails.role;

                    if ($scope.accountdetails.role.length == 2) {
                        $("#btn-switchRole").removeClass("non-display");
                        credentials.user_id = Session.userId;
                        credentials.session_id = Session.id;
                        credentials.user_role = Session.userRole;
                        credentials.multi = 1;
                        credentials.accessToken = Session.accessToken;
                        credentials.data = Session.data;
                        AuthService.authenticate(credentials);
                    }
                    if ($scope.categories) {
                        store.set('post');
                        store.set('categoryClick');
                        var isClick = true;
                        store.set("categoryClick", isClick);
                        $scope.post = {
                            category: $scope.categories[res.categoryid],
                            indexCategory: res.categoryid
                        }
                        store.set("post", $scope.post);
                        $location.path('/post/');
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        credentials.user_id = Session.userId;
                        credentials.session_id = Session.id;
                        credentials.user_role = "customer";
                        credentials.multi = Session.multi;
                        credentials.accessToken = Session.accessToken;
                        credentials.data = Session.data;
                        AuthService.authenticate(credentials);
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    }
                        api.put('user/' + Session.id + '/v1', list).then(function (res) {
                            if (res.data.result) {
                                //logger.logSuccess("Bạn đã lưu thành công!");
                            } else {
                                logger.logError(res.data.error.description);
                            }
                        })
                }

                $scope.close = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);

        module.controller('ChoseRoleCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', '$modal', 'res', 'USER_ROLES', 'Session', 'store', function ($scope, $modalInstance, $rootScope, $location, AuthService, AUTH_EVENTS, $modal, res, USER_ROLES, Session, store) {
                var credentials = {};
                $rootScope.$on('$locationChangeSuccess', function () {
                    $modalInstance.close();
                });

                _.filter(USER_ROLES, function (value, key) {
                    credentials.user_role = key;

                });

                $scope.customer = function () {
                    credentials.user_id = res.data.user.username;
                    credentials.session_id = res.data.user.id;
                    credentials.accessToken = res.data.accessToken;
                    if (res.data.user.jsonData) {
                        credentials.data = JSON.parse(res.data.user.jsonData);
                    }
                    credentials.user_role = "customer";
                    credentials.multi = res.data.user.roles.length > 1 ? 1 : 0;
                    AuthService.authenticate(credentials);

                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $scope.post = store.get("post") ? store.get("post") : {};

                    if ($scope.post.title != undefined || $scope.post.category != undefined) {
                        $location.path("/post");
                    } else {
                        $location.path("/dashboard");
                    }
                    $modalInstance.close();

                };
                $scope.supplier = function () {

                    credentials.user_id = res.data.user.username;
                    credentials.session_id = res.data.user.id;
                    credentials.accessToken = res.data.accessToken;
                    if (res.data.user.jsonData) {
                        credentials.data = JSON.parse(res.data.user.jsonData);
                    }
                    credentials.user_role = "supplier";
                    credentials.multi = res.data.user.roles.length > 1 ? 1 : 0;
                    if (res.data.user.skill === null) {
                        AuthService.authenticate(credentials);
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        $location.path("/updateskills");
                        $modalInstance.close();

                    }
                    else {

                        AuthService.authenticate(credentials);
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        //$location.path("/detailSupplier");
                        //$location.path("/feeds");
                        $location.path("/dashboard");
                        $modalInstance.close();

                    }
                };
                $scope.close = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);
        module.controller('SignupFacebookCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', '$modal', 'account', function ($scope, $modalInstance, $rootScope, $location, $auth, AuthService, AUTH_EVENTS, $modal, account) {

                $scope.account = account;

                $scope.cancel = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);
        module.controller('GetPasswordCtrl', [
            '$scope', '$modalInstance', '$rootScope', '$location', 'AuthService', 'AUTH_EVENTS', '$modal', 'account', function ($scope, $modalInstance, $rootScope, $location, $auth, AuthService, AUTH_EVENTS, $modal, account) {

                $scope.account = account;

                $scope.OK = function () {
                    $modalInstance.dismiss("cancel");
                };
            }
        ]);
    });
