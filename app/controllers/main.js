/** @scratch /index/0
 * = Kibana
 *
 * // Why can't I have a preamble here?
 *
 * == Introduction
 *
 * Kibana is an open source (Apache Licensed), browser based analytics and search dashboard for
 * ElasticSearch. Kibana is a snap to setup and start using. Written entirely in HTML and Javascript
 * it requires only a plain webserver, Kibana requires no fancy server side components.
 * Kibana strives to be easy to get started with, while also being flexible and powerful, just like
 * Elasticsearch.
 *
 * include::configuration/config.js.asciidoc[]
 *
 * include::panels.asciidoc[]
 *
 */

define(['angular', 'config', 'lodash'], function(angular, config, _) {"use strict";

	var module = angular.module('bidproject.controllers');

	module.controller('MainController', ['$scope', 'alertSrv',
	function($scope, alertSrv) {
		$rootScope.$on("$routeChangeStart", function() {
			$rootScope.loading = true;
		});

		$rootScope.$on("$routeChangeSuccess", function() {
			$rootScope.loading = false;
		});

		var scrollItems = [];

		for (var i = 1; i <= 100; i++) {
			scrollItems.push("Item " + i);
		}

		$scope.scrollItems = scrollItems;
		$scope.invoice = {
			payed : true
		};
		$scope.userAgent = navigator.userAgent;

	}]);
});
