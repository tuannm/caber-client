define([
        'angular'
    ],
    function (angular, $) {
        'use strict';
        angular
            .module('bidproject.directives')
            .directive('ngBack', ['$parse', '$rootScope','navigations','windowsize', function($parse, $rootScope,navigations,windowsize) {
                return {
                    restrict: 'EA',
                    replace: true,
                    template: '<button class="btn btn-primary submitSettingsBtn form-group btn-reject margin-btn-submit btn-gap pull-right hidden-xs" ng-click="cancel()">'+
                    '<span data-i18n="back"></span>'+
                    '</button>',
                    link: function(scope, elt, attrs, navigationBarCtrl) {
                     scope.cancel = function()
                     {
                         //if ( windowsize.devicetype() == 'Desktop')
                         //{
                         //    var newIndex = navigations.collections.indexOf(navigations.current) - 2;
                         //    if (newIndex > -1) {
                         //        navigations.collections.splice(newIndex,1);
                         //    }
                         //}
                         //window.history.back();
                         navigations.current.onBack()
                     }
                    }
                };
            }]);
    });