define([
  'angular',
  'jquery',
  'dropzone'
],
function (angular, $) {
  'use strict';

  angular
    .module('bidproject.directives')
    .directive('smartDropzone', ['$parse','windowsize', 'api', 'Session', function($parse, windowsize, api, Session) {
      return {
            restrict: 'A',
            compile: function (element, attributes) {
                $(element).removeAttr('smart-dropzone data-smart-dropzone');

                $(element).dropzone({
                    url: 'upload',
                    addRemoveLinks : true,
                    dictDefaultMessage: '<a class="file-input-wrapper"><i class="glyphicon glyphicon-paperclip"></i> <span data-i18n="attachment"></span></a> <span class="hidden-xs" data-i18n="attach file required"></span>',
                    dictResponseError: 'Error uploading file!',
                    paramName: 'file',
                    clickable: true,
                    maxFilesize: 1,
                    uploadMultiple: false,
                    autoProcessQueue: false,
                    accept: function(file, done){
                        console.log("file", file);
                        var reader = new FileReader();
                        reader.onload = handleReaderLoad;
                        reader.readAsDataURL(file);
                        function handleReaderLoad(evt) {
                            console.log("handleReaderLoad", file);
                            api.post("attachment/v1",{
                                userId: Session.id,
                                fileName: file.name,
                                fileType: file.type.split("/").pop(),
                                content: evt.target.result.split("base64,").pop()
                            }, false).then(function(res){
                                console.log(res);
                            });
                        }
                        //document.getElementById("id_base64_name")
                        //    .setAttribute('value', file.name);
                        //document.getElementById("id_base64_content_type")
                        //    .setAttribute('value', file.type);
                        //form = $('#file-form');
                        //
                        //console.log("form.serialize()", form.serialize());
                        //$.pjax( {
                        //    method: "POST",
                        //    container: "#pjax-container",
                        //    timeout: 2000,
                        //    url: "/upload/",
                        //    data: form.serialize()
                        //});
                        done();
                    }
                });
            }
        };
    }]);

});