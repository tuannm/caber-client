define([
    'angular',
    'jquery'
],
        function (angular, $) {
            'use strict';

            angular
                    .module('bidproject.directives')
                    .directive('highlightActive', ['$compile', function ($compile) {
                            return {
                                restrict: "A",
                                controller: ['$scope', '$element', '$attrs', '$location', function ($scope, $element, $attrs, $location) {
                                        var highlightActive, links, path;
                                        highlightActive = function () {
                                            $('#nav-wrapper-vertical').on('click', 'li[role=parent]', function () {
                                                $(this).addClass('active').siblings('li[role=parent]').removeClass('active');
                                            });
                                             $('#nav-wrapper-vertical').on('click', 'li[role=children]', function () {
                                                $(this).addClass('active').siblings('li[role=children]').removeClass('active');
                                            });
                                        };
                                        return highlightActive();
//                links = $element.find('a');
//                path = function() {
//                  return $location.path();
//                };
//                highlightActive = function(links, path) {
//                  path = '#' + path;
//                  return angular.forEach(links, function(link) {
//                    var $li, $link, href;
//                    $link = angular.element(link);
//                    $li = $link.parent('li');
//                    href = $link.attr('href');
//                    if ($li.hasClass('active')) {
//                      $li.removeClass('active');
//                    }
//                    if (path.indexOf(href) === 0) {
//                      return $li.addClass('active');
//                    }
//                  });
//                };
//                highlightActive(links, $location.path());
//                return $scope.$watch(path, function(newVal, oldVal) {             
//                  if (newVal === oldVal) {
//                    return;
//                  }
//                  links = $element.find('a');
//                  return highlightActive(links, $location.path());
//                });
                                    }
                                ]
                            };
                        }]);
        });