define([
  'angular',
  'jquery'
],
function (angular, $) {
  'use strict';

  angular
    .module('bidproject.directives')
    .directive('i18n', ['$compile', '$rootScope', 'localize', function ($compile, $rootScope, localize) {
        var i18nDirective;
        i18nDirective = {
          restrict: "EA",
          updateText: function (ele, input, placeholder) {
            var result;
            result = void 0;
            if (input === 'i18n-placeholder') {
              result = localize.getLocalizedString(placeholder);
              return ele.attr('placeholder', result || placeholder);
            }else if (input === 'i18n-url') {
              return localize.urlLocalizedResources(ele[0].attributes["url"].value);
            } else if (input.length >= 1) {
              result = localize.getLocalizedString(input);
              var value = result || input;
              if(ele[0].attributes["i18n-prefix"]){
                value = ele[0].attributes["i18n-prefix"].value + " - " + value;
              }
              return ele.text(value);
            }
          },
          link: function (scope, ele, attrs) {

            scope.$on('urlLocalizeResourcesLoaded', function (obj, text) {
              //console.log("urlLocalizeResourcesLoaded", value);
              if(attrs.i18n === 'i18n-url'){
                ele.html(text);
              }
            });
            scope.$on('localizeResourcesUpdated', function () {
              return i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder);
            });
            return attrs.$observe('i18n', function (value) {
              return i18nDirective.updateText(ele, value, attrs.placeholder);
            });
          }
        };
        return i18nDirective;
      }]);

});