/**
 * Created by ngocnh on 5/4/15.
 */

define([
        'angular',
        'jquery',
    ],
    function (angular) {
        'use strict';

        angular.module('bidproject.directives')
            .directive('optionsClass', ['$timeout', '$parse', '$compile', function($timeout, $parse, $compile) {
                return {
                    require: 'select',
                    link: function(scope, elem, attrs, ngSelect) {

                        var optionsSourceStr = attrs.ngOptions.split(' ').pop(),

                            getOptionsClass = $parse(attrs.optionsClass);


                        $timeout(function () {
                            scope.$watch(optionsSourceStr, function(items) {
                                $timeout(function () {
                                var option = elem.find('option');
                                var j = 0;
                                for (var i = 1; i < option.length; i++) {
                                    $(option[i]).attr("data-i18n",items[j].name);
                                    j++;
                                    $(option[i]).attr("label","");
                                    var ele = option[i];
                                    (function(ele){
                                        $compile(ele)(scope);
                                    })(ele);
                                }
                                }, 0);
                            });
                        }, 0);




                    }
                };
            }]);
    });