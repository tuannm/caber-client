define([
    'angular',
    'jquery'
],
function (angular, $) {
    'use strict';

    angular
    .module('bidproject.directives')
    .directive('smartLocation', ['$compile', 'windowsize', 'navigations', '$stateParams', 'NavigatorGeolocation', 'GeoCoder', function ($compile, windowsize, navigations, $stateParams, NavigatorGeolocation, GeoCoder) {
        return {
            restrict: 'EA',
            transclude: true,
            require: 'ngModel',
            replace: true,
            template: '<div map-lazy-load="http://maps.google.com/maps/api/js">' +
                            '<div class="input-group  non-display">' +
                                '<input name="location"  places-auto-complete ng-model="address" value="Ha Noi" class="form-control col-lg-12" ng-model="address" xxx-component-restrictions="{country:\'us\'}" types="{{types}}" on-place-changed="placeChanged(place)" data-i18n="i18n-placeholder" placeholder="location" />' +
                                '<div class="input-group-btn">'+
                                '<span class="btn btn-default" data-ng-click="getCurrentPosition()"><i class="fa fa-dot-circle-o"></i></span>' +
                                '</div>' +
                            '</div>' +
                            '<map center="{{map.center.latitude}}, {{map.center.longitude}}" zoom="{{map.zoom}}">' +
                            '<marker position="{{marker.coords.latitude}}, {{marker.coords.longitude}}"  animation="DROP" draggable="true"></marker>' +
                            '</map>' +
                        '</div>',
            scope: {
                address: '='
            },
            link: function (scope, element, attrs) {
                console.log(scope);
                scope.toggleBounce = function() {
                    if (this.getAnimation() != null) {
                        this.setAnimation(null);
                    } else {
                        this.setAnimation(google.maps.Animation.BOUNCE);
                    }
                };
                NavigatorGeolocation.getCurrentPosition()
                    .then(function(position) {
                        scope.changePosition(position.coords.latitude, position.coords.longitude, 18);
                        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        var address = GeoCoder.geocode({ 'latLng': latlng });
                        address.then(function (data) {
                            scope.address = data[0].formatted_address;
                        });
                    });
                scope.getCurrentPosition = function(){
                    //NavigatorGeolocation.getCurrentPosition()
                    //.then(function(position) {
                    //        scope.changePosition(position.coords.latitude, position.coords.longitude, 18);
                    //        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    //        var address = GeoCoder.geocode({ 'latLng': latlng });
                    //        address.then(function (data) {
                    //            scope.address = data[0].formatted_address;
                    //        });
                    // });

                };

                scope.types = "['address']";
                scope.placeChanged = function() {
                    var place = this.getPlace();
                    scope.changePosition(place.geometry.location.lat(), place.geometry.location.lng(), 18);
                }

                scope.changePosition = function(latitude, longitude, zoom, title){
                    scope.map = {
                        "center": {
                            "latitude": latitude,
                            "longitude": longitude
                        },
                        "zoom": zoom
                    };
                    scope.marker = {
                        id: 0,
                        coords: {
                            latitude: latitude,
                            longitude: longitude
                        },
                        title: title || ""
                    };
                }
                if(!angular.isUndefined(attrs["currentPosition"]) && (angular.isUndefined(scope.address) || scope.address == '')){
                    scope.getCurrentPosition();
                }else{
                    scope.changePosition(21.0277644, 105.83415979999995, 8);
                    var latlng = new google.maps.LatLng(21.0277644, 105.83415979999995);
                    var address = GeoCoder.geocode({ 'latLng': latlng });
                }

                //scope.marker = {
                //    id: 0,
                //    coords: {
                //        latitude: 40.1451,
                //        longitude: -99.6680
                //    },
                //    options: { draggable: true },
                //    events: {
                //        dragend: function (marker, eventName, args) {
                //            $log.log('marker dragend');
                //            var lat = marker.getPosition().lat();
                //            var lon = marker.getPosition().lng();
                //            $log.log(lat);
                //            $log.log(lon);
                //
                //            scope.marker.options = {
                //                draggable: true,
                //                labelContent: "lat: " + scope.marker.coords.latitude + ' ' + 'lon: ' + scope.marker.coords.longitude,
                //                labelAnchor: "100 0",
                //                labelClass: "marker-labels"
                //            };
                //        }
                //    }
                //};
            }
        };
    }]);
});