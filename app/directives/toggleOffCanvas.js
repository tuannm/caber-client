define([
  'angular',
  'jquery'
],
function (angular, $) {
  'use strict';

  angular
    .module('bidproject.directives')
    .directive('toggleOffCanvas', ['$parse', '$rootScope', function($parse, $rootScope) {
      return function(scope, element, attr) {
        element.on('click', function () {
            $('#app').toggleClass('on-canvas');
          }
        );
      };
    }]);

});