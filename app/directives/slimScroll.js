define([
    'angular',
    'jquery',
    'jquery.slimscroll'
],
function (angular, $) {
    'use strict';

    angular
    .module('bidproject.directives')
    .directive('slimScroll', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                realtimeScroll: '='
            },
            link: function (scope, ele, attrs) {
                scope.$watch('realtimeScroll', function(data, newdata){
                    var scrollTo_int = $(ele).prop('scrollHeight') + 'px';
                    $(ele).slimScroll({scrollTo : scrollTo_int });
                })
                $(ele).slimScroll({
                    width: attrs.scrollWidth || 'auto',
                    height: attrs.scrollHeight || '100%',
                    alwaysVisible: attrs.alwaysVisible || false,
                    scrollTo: attrs.scrollStart || 'top'
                });
            }
        };
    }]);
});