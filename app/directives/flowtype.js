/**
 * Created by Admin on 6/8/2015.
 */
define([
        'angular',
        'jquery',
    ],
    function (angular) {
        'use strict';
        angular.module('bidproject.directives').
            directive('flowtype', ['$compile', 'windowsize', function ($compile, windowsize) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        $(window).resize(function () {
                            if (windowsize.devicetype() != 'Desktop') {
                                var options = {};
                                options.maximum = attrs.maximum || 768;
                                options.minimum = attrs.minimum || 1;
                                options.minFont = attrs.minFont || 13;
                                options.maxFont = attrs.maxFont || 14;
                                options.fontRatio = attrs.fontRatio || 30;
                                options.lineRatio = attrs.lineRatio || 1.45;
                                element.flowtype(options);
                            }
                        });
                    }
                };
            }])
    });