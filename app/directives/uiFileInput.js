define([
  'angular',
  'jquery',
  'file-input'
],
function (angular, $) {
  'use strict';

  angular
    .module('bidproject.directives')
    .directive('uiFileInput', ['$parse','windowsize', 'api', 'Session', function($parse, windowsize, api, Session) {
      return {
            restrict: 'A',
          link: function (scope,element, attributes) {
              $(element).bootstrapFileInput();
          }
        };
    }]);

});