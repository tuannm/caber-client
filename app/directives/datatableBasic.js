define([
        'angular',
        'jquery',
        'datatables',
        'datatables-responsive',
        'datatables-colvis',
        'datatables-tools',
        'datatables-bootstrap',
        'datatables-scroller'
    ],
    function (angular, $) {
        'use strict';

        angular
            .module('bidproject.directives')
            .directive('datatableBasic', ['$compile', '$timeout', 'windowsize', 'api', 'logger', 'localize', function ($compile, $timeout, windowsize, api, logger, localize) {
                return {
                    restrict: 'A',
                    scope: {
                        tableOptions: '=',
                        onSelection: '&',
                        onMouseover: '&',
                        dtTemplate: '=',
                        rowsSelected: '=',
                        rowIndex: '=',
                        rowDataChangeCallback: '&'
                    },
                    link: function (scope, element, attributes) {
                        var calcDataTableHeight = function () {
                            if (attributes["offsetHeight"]) {
                                return $(window).height() - attributes["offsetHeight"]
                            } else {
                                return $(window).height() - $("#header").height();
                            }

                        };
                        var config = {
                            type: attributes["type"] || 'local',
                            displayfooter: attributes["displayfooter"] || 'show',
                            height: calcDataTableHeight(),
                            method: "GET"
                        }

                        $(element).css("visibility", "hidden");
                        /* Time between each scrolling frame */
                        $.fn.dataTableExt.oPagination.iTweenTime = 100;

                        $.fn.dataTableExt.oPagination.infinite = {
                            "fnInit": function (oSettings, nPaging, fnCallbackDraw) {
                                var cacheScrollTop;
                                var oLang = oSettings.oLanguage.oPaginate;
                                var oClasses = oSettings.oClasses;

                                $(oSettings.nScrollBody).on("scroll", function (e) {
                                    var
                                        offsetHeight = e.target.offsetHeight,
                                        iScrollTop = e.target.scrollTop,
                                        scrollHeight = e.target.scrollHeight;
                                    if ((cacheScrollTop != iScrollTop && (iScrollTop + offsetHeight) >= scrollHeight) && !oSettings.infiniteLoad) {
                                        cacheScrollTop = iScrollTop;

                                        if (typeof oSettings.iPagingLoopStart != 'undefined' && oSettings.iPagingLoopStart != -1) {
                                            return;
                                        }

                                        oSettings.iPagingLoopStart = oSettings._iDisplayStart;
                                        if (oSettings._iDisplayStart + oSettings._iDisplayLength < oSettings.fnRecordsDisplay()) {
                                            oSettings.iPagingEnd = oSettings._iDisplayStart + oSettings._iDisplayLength;
                                        }

                                        /* Make sure we are not over running the display array */


                                        var iTween = $.fn.dataTableExt.oPagination.iTweenTime;
                                        var innerLoop = function () {
                                            var tableHeight = $(oSettings.nTable).height();
                                            var scrollHeight = $(oSettings.nScrollBody).height();
                                            if((tableHeight > 0 && tableHeight < scrollHeight && !oSettings.infiniteLoad)){
                                                if (oSettings._iDisplayStart + oSettings._iDisplayLength < oSettings.fnRecordsDisplay()) {
                                                    oSettings.iPagingEnd = oSettings._iDisplayStart + oSettings._iDisplayLength;
                                                }
                                            }
                                            if (oSettings.iPagingLoopStart < oSettings.iPagingEnd) {
                                                oSettings.iPagingLoopStart += oSettings._iDisplayLength;
                                                oSettings._iDisplayStart = oSettings.iPagingLoopStart;
                                                fnCallbackDraw(oSettings);
                                                setTimeout( function() { innerLoop(); }, iTween );
                                            } else {
                                                oSettings.iPagingLoopStart = -1;
                                                oSettings.infiniteLoad = false;
                                            }
                                        };
                                        innerLoop();
                                    }

                                })
                            },

                            "fnUpdate": function (oSettings, fnCallbackDraw) {
                            }
                        };
                        var pipeline = function (opts) {
                            // Configuration options
                            var conf = $.extend({
                                pages: 2,     // number of pages to cache
                                url: '',      // script url
                                data: null,   // function or object with parameters to send to the server
                                              // matching how `ajax.data` works in DataTables
                                method: 'GET', // Ajax HTTP method
                                remote_data: "data",
                                scroll: false,
                                cache: false
                            }, opts);

                            // Private variables for storing the cache
                            var cacheLower = -1;
                            var cacheUpper = null;
                            var cacheLastRequest = null;
                            var cacheLastJson = null;

                            return function (request, drawCallback, settings) {
                                var ajax = false;
                                var requestStart = request.start;
                                var drawStart = request.start;
                                var requestLength = request.length;
                                var requestEnd = requestStart + requestLength;
                                if (settings.sPaginationType == 'infinite') {
                                    conf.scroll = true;
                                }else{
                                    conf.scroll = false;
                                }
                                if (settings.clearCache) {
                                    // API requested that the cache be cleared
                                    ajax = true;
                                    settings.clearCache = false;
                                }
                                else if (cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper) {
                                    // outside cached data - need to make a request
                                    ajax = true;
                                }
                                else if (JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest.order) ||
                                    JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest.columns) ||
                                    JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)
                                ) {
                                    // properties changed (ordering, columns, searching)
                                    ajax = true;
                                }

                                // Store the request for checking next time around
                                cacheLastRequest = $.extend(true, {}, request);
                                if (ajax) {
                                    // Need data from the server
                                    if(conf.scroll){
                                        if (requestStart < cacheLower) {
                                            requestStart = requestStart - (requestLength * (conf.pages - 1));

                                            if (requestStart < 0) {
                                                requestStart = 0;
                                            }
                                        } else if (requestEnd > cacheUpper) {
                                            requestStart = cacheUpper;
                                        }
                                        cacheLower = drawStart;
                                    }else{
                                        cacheLower = Math.floor(drawStart/(requestLength * conf.pages))*(requestLength * conf.pages);
                                    }
                                    cacheUpper = cacheLower + (requestLength * conf.pages);
                                    request.start = requestStart;
                                    request.length = requestLength * conf.pages;


                                    // Provide the same `data` options as DataTables.
                                    if ($.isFunction(conf.data)) {
                                        // As a function it is executed with the data object as an arg
                                        // for manipulation. If an object is returned, it is used as the
                                        // data object to submit
                                        var d = conf.data(request);
                                        if (d) {
                                            $.extend(request, d);
                                        }
                                    }
                                    else if ($.isPlainObject(conf.data)) {
                                        // As an object, the data given extends the default
                                        $.extend(request, conf.data);
                                    }

                                    if (conf.method == 'GET') {
                                        api.get(conf.url + "&page=" + Math.floor(request.start / request.length) + "&pagesize=" + request.length, true, conf.cache).then(function (res) {
                                            if (res.data.result) {
                                                if (res.data[conf.remote_data]) {
                                                    scope.$emit('listCV',res.data);
                                                    var json = {
                                                        //"draw": request.draw,
                                                        "recordsTotal": res.data.paging.totalElement,
                                                        "recordsFiltered": res.data.paging.totalElement,
                                                        "data": res.data[conf.remote_data]
                                                    }

                                                    if (conf.scroll) {
                                                        if (!cacheLastJson) {
                                                            cacheLastJson = $.extend(true, {}, json);
                                                        } else {
                                                            json.data = $.merge(cacheLastJson.data, json.data);
                                                            cacheLastJson = $.extend(true, {}, json);
                                                        }
                                                        json.data.splice(drawStart + requestLength, json.data.length - (drawStart + requestLength));
                                                    } else {
                                                        cacheLastJson = $.extend(true, {}, json);
                                                        json.data.splice(0, requestStart - cacheLower);
                                                        json.data.splice(requestLength, json.data.length);
                                                    }
                                                    drawCallback(json);
                                                    settings.infiniteLoad = false;
                                                    $(element).css("visibility", "visible");
                                                }
                                            } else {
                                                logger.logError(res.data.error.description);
                                            }
                                        })
                                    } else if (conf.method == 'POST') {
                                        if (conf.data) {
                                            conf.data.page = Math.floor(request.start / request.length);
                                            conf.data.pagesize = request.length;
                                        }
                                        api.post(conf.url, conf.data).then(function (res) {
                                            if (res.data.result) {
                                                if (res.data[conf.remote_data]) {
                                                    var json = {
                                                        //"draw": request.draw,
                                                        "recordsTotal": res.data.paging.totalElement,
                                                        "recordsFiltered": res.data.paging.totalElement,
                                                        "data": res.data[conf.remote_data]
                                                    }

                                                    if (conf.scroll) {
                                                        if (!cacheLastJson) {
                                                            cacheLastJson = $.extend(true, {}, json);
                                                        } else {
                                                            json.data = $.merge(cacheLastJson.data, json.data);
                                                            cacheLastJson = $.extend(true, {}, json);
                                                        }
                                                        json.data.splice(drawStart + requestLength, json.data.length - (drawStart + requestLength));
                                                    } else {
                                                        cacheLastJson = $.extend(true, {}, json);
                                                        json.data.splice(0, requestStart - cacheLower);
                                                        json.data.splice(requestLength, json.data.length);
                                                    }
                                                    drawCallback(json);
                                                    settings.infiniteLoad = false;
                                                    $(element).css("visibility", "visible");
                                                }
                                            } else {
                                                logger.logError(res.data.error.description);
                                            }

                                        });
                                    }

                                }
                                else {
                                    var json = $.extend(true, {}, cacheLastJson);
                                    if(!cacheLastJson){
                                        json = {
                                            //"draw": request.draw,
                                            "recordsTotal": 0,
                                            "recordsFiltered": 0,
                                            "data": []
                                        }
                                    }
                                    if (json.data && json.data.length > 0) {
                                        json.draw = request.draw; // Update the echo for each response
                                        if (conf.scroll) {
                                            json.data.splice(drawStart + requestLength, json.data.length - (drawStart + requestLength));
                                        } else {
                                            json.data.splice(0, requestStart - cacheLower);
                                            json.data.splice(requestLength, json.data.length);
                                        }

                                    }
                                    drawCallback(json);
                                    settings.infiniteLoad = false;

                                }
                            }
                        };
                        $.fn.dataTable.Api.register('clearPipeline()', function () {
                            return this.iterator('table', function (settings) {
                                settings.clearCache = true;
                            });
                        });

                        scope.$on('localizeResourcesUpdated', function () {
                            options.oLanguage = localize.language == "vi"? options.lang.vi:options.lang.en;
                            element.dataTable().fnDestroy();
                            element.smartResponsiveHelper = null;
                            element.dataTable(options);
                            element.dataTable().fnDraw();
                        });
                        var options = {};
                        var initConfig = function (){
                            options = {
                                "dom": "t" +
                                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                                destroy: true,
                                lang:{
                                    vi:{
                                        "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                        "sLengthMenu": "_MENU_",
                                        "sProcessing": "Đang xử lý...",
                                        "sEmptyTable": "Không tìm thấy bản ghi nào phù hợp",
                                        "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
                                        "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ bản ghi",
                                        "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                                        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                                        "sInfoPostFix": "",
                                        "sUrl": "",
                                        "oPaginate": {
                                            "sFirst": "Đầu",
                                            "sPrevious": "Trước",
                                            "sNext": "Tiếp",
                                            "sLast": "Cuối"
                                        }
                                    },
                                    en:{
                                        "sEmptyTable":     "No data available in table",
                                        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
                                        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
                                        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
                                        "sInfoPostFix":    "",
                                        "sInfoThousands":  ",",
                                        "sLengthMenu":     "_MENU_",
                                        "sLoadingRecords": "Loading...",
                                        "sProcessing":     "Processing...",
                                        "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                        "sZeroRecords":    "No matching records found",
                                        "oPaginate": {
                                            "sFirst":    "First",
                                            "sLast":     "Last",
                                            "sNext":     "Next",
                                            "sPrevious": "Previous"
                                        }
                                    }
                                },

                                "autoWidth": false,
                                "deferRender": true,
                                "smartResponsiveHelper": null,
                                "preDrawCallback": function (oSettings) {
                                    // Initialize the responsive datatables helper once.
                                    if (!checkAttributes(attributes.visibilityHidden)) {
                                        $(element).css("visibility", "visible");
                                    }
                                    if(oSettings.aoData.length <= 0){
                                        $(element).next('.dt-toolbar-footer').hide();
                                    }else{
                                        if (config.displayfooter == 'hide')
                                        {
                                            $(element).next('.dt-toolbar-footer').hide();
                                        }
                                        else
                                        {
                                            $(element).next('.dt-toolbar-footer').show();
                                        }
                                    }
                                    if (!this.smartResponsiveHelper) {

                                        this.smartResponsiveHelper = new ResponsiveDatatablesHelper(element, {
                                            desktop: 1000000,
                                            tablet: 768,
                                            phone: 480

                                        });
                                        this.smartResponsiveHelper.rowLiTemplate = '<li class="col-md-12 col-xs-12 no-padding wrap-numberonly"><span class="columnTitle col-md-2 col-xs-3 no-padding"><!--column title--></span> <span class="columnValue col-md-10 col-xs-9 no-padding"><!--column value--></span></li>';
                                        scope.smartResponsiveHelper = this.smartResponsiveHelper;

                                    }
                                },
                                "rowCallback": function (nRow, aData, iDataIndex) {
                                    this.smartResponsiveHelper.createExpandIcon(nRow);
                                },
                                "drawCallback": function (oSettings) {
                                    this.smartResponsiveHelper.respond();
                                    $(element).css('width', '100%');
                                    if (!checkAttributes(attributes.visibilityHidden)) {
                                        $(element).css("visibility", "visible");
                                    }
                                },
                                "initComplete": function (settings, json) {
                                    scope.$emit("validate");
                                },
                                fnCreatedRow: function (nRow, aData, iDataIndex) {
                                    if (attributes.rowsSelected !== undefined) {
                                        if (scope.rowsSelected != undefined && !jQuery.isEmptyObject(scope.rowsSelected) )
                                        {
                                            if (scope.rowsSelected.indexOf(aData.id) != -1) {
                                                $(nRow).addClass("active");
                                            }
                                        }
                                    }
                                    if (aData.children && aData.children.length > 0 && scope.tableOptions.children) {
                                        $(nRow).addClass("childControl");
                                    }
                                    createChildScope(nRow, aData, iDataIndex);
                                    $(nRow).click(function (event) {
                                        if ($(this).hasClass('childControl')) {
                                            if (checkAttributes(attributes.multiSelect)) {
                                                $(this).toggleClass('active');
                                            } else {
                                                $(this).addClass('active').siblings($(this)).removeClass('active');
                                            }
                                            var tr = $(this).closest('tr');
                                            var row = element.DataTable().row(tr);
                                            if (row.child.isShown()) {
                                                // This row is already open - close it
                                                row.child.hide();
                                                tr.removeClass('shown');
                                            }
                                            else {
                                                var tr = $(this).closest('tr');
                                                var tbody = tr.closest('tbody');
                                                var classShown = tbody.find(".shown");
                                                var rowShownActive = element.DataTable().row(classShown);
                                                if (classShown.length > 0) {
                                                    rowShownActive.child.hide();
                                                    classShown.removeClass("shown");
                                                }


                                                // Open this row
                                                var childScope = scope.$new();
                                                childScope.data = {
                                                    paging: false,
                                                    scrollCollapse: false,
                                                    onSelection: scope.onSelection(),
                                                    multiSelect: attributes.multiSelect
                                                }
                                                angular.extend(childScope.data, scope.tableOptions.children);
                                                childScope.data.aaData = element.dataTable().fnGetData(this)["children"];
                                                var childTemplate = $('<table on-selection="data.onSelection" multi-select="{{data.multiSelect}}" datatable-basic table-options="data" class="table table-striped table-bordered"><thead><tr><th>Name</th></tr></thead></table>');
                                                var html = $compile(childTemplate)(childScope);
                                                row.child(html).show();
                                                tr.addClass('shown');
                                            }


                                        } else {
                                            if (checkAttributes(attributes.clickApply) || angular.isUndefined(attributes.clickApply)) {
                                                if (attributes.onSelection !== undefined) {
                                                    if (!$(this).hasClass("detail-show")) {
                                                        $('tbody tr.row-detail', element).remove();
                                                        $('tbody tr', element).removeClass('detail-show');
                                                        if (checkAttributes(attributes.showHidden)) {
                                                            event.data = {responsiveDatatablesHelperInstance: scope.smartResponsiveHelper};
                                                            scope.smartResponsiveHelper.showRowDetailEventHandler.call(this, event);
                                                        }
                                                        if (angular.isUndefined(attributes.noHightlight)) {
                                                            if (checkAttributes(attributes.multiSelect)) {
                                                                $(this).toggleClass('active');
                                                            } else {
                                                                $(this).addClass('active').siblings($(this)).removeClass('active');
                                                                var tr = $(this).closest('tr');
                                                                var tbody = tr.closest('tbody');
                                                                var classShown = tbody.find(".shown");
                                                                var rowShownActive = element.DataTable().row(classShown);
                                                                if (classShown.length > 0) {
                                                                    rowShownActive.child.hide();
                                                                    classShown.removeClass("shown");
                                                                }
                                                            }
                                                        } else {
                                                            $('tbody tr.row-detail td', element).addClass("blankColor");
                                                            $('tbody tr td', element).addClass("blankColor");
                                                        }
                                                        createChildScope($(this).next('.row-detail'), element.dataTable().fnGetData(this), $(this).index());
                                                        if (scope.onSelection() && typeof scope.onSelection() === 'function') {
                                                            scope.onSelection()($(this).index(), element.dataTable().fnGetData(this), $(this));
                                                        }
                                                        scope.$emit("validate");
                                                    }
                                                }
                                            }
                                        }


                                    });
                                    $(nRow).mouseover(function (event) {
                                        if (checkAttributes(attributes.mouseoverApply)) {
                                            if (!$(this).hasClass("detail-show")) {
                                                $('tbody tr.row-detail', element).remove();
                                                $('tbody tr', element).removeClass('detail-show');
                                                event.data = {responsiveDatatablesHelperInstance: scope.smartResponsiveHelper};
                                                scope.smartResponsiveHelper.showRowDetailEventHandler.call(this, event);
                                                $(this).addClass('active').siblings($(this)).removeClass('active');
                                                createChildScope($(this).next('.row-detail'), element.dataTable().fnGetData(this), $(this).index());
                                                if (scope.onMouseover() && typeof scope.onMouseover() === 'function') {
                                                    scope.onMouseover()($(this).index(), element.dataTable().fnGetData(this), $(this));
                                                }
                                            }
                                        }
                                    });

                                }
                            };
                        }
                        initConfig();

                        var remote = function () {
                            if (config.type == 'remote' && scope.tableOptions && scope.tableOptions.url) {
                                var remote = {
                                    //"paging": false,
                                    "serverSide": true,
                                    "ajax": pipeline({
                                        url: scope.tableOptions.url,
                                        pages: 2,
                                        remote_data: scope.tableOptions.remote_data,
                                        method: scope.tableOptions.method || "GET",
                                        data: scope.tableOptions.postData || null
                                    })
                                }
                                options = angular.extend(options, remote);
                                if ((windowsize.devicetype() != "Desktop" || navigator.userAgent.match(/(iPad)/)) && angular.isUndefined(attributes.paging)) {
                                    options = angular.extend(options, {
                                        "sPaginationType": "infinite",
                                        "bInfiniteScroll": true,
                                        "bColumnCollapse": true
                                    });
                                    if (!scope.tableOptions.scrollY) {
                                        options.scrollY = config.height;
                                    }
                                }
                            }
                        }

                        remote();

                        var createChildScope = function (row, aData, iDataIndex) {
                            var rowScope = scope.$parent.$new(true);
                            rowScope.aData = aData;
                            rowScope.iDataIndex = iDataIndex;
                            $compile(row)(rowScope);
                        }

                        if (attributes.tableOptions) {
                            options = angular.extend(options, scope.tableOptions);
                        }

                        var _dataTable;
                        options.oLanguage= localize.language == "vi"? options.lang.vi:options.lang.en;
                        _dataTable = element.dataTable(options);

                        var isReload = false;

                        scope.$on("window.size", function (object, type) {
                            if (checkAttributes(attributes.visibilityHidden)) {
                                $(element).css("visibility", "hidden");
                            }
                            if (isReload) {
                                $timeout.cancel(isReload);
                            }
                            isReload = $timeout(function (type) {
                                initConfig();
                                options = angular.extend(options, scope.tableOptions);
                                options.oLanguage= localize.language == "vi"? options.lang.vi:options.lang.en;
                                remote();
                                element.dataTable().fnDestroy();
                                element.smartResponsiveHelper = null;
                                element.dataTable(options);
                                element.dataTable().fnDraw();
                            }, 2000, true, type);

                        });
                        scope.$watch('tableOptions.aaData', function (data, newdata) {
                            if (config.type == 'local' && data && attributes.tableOptions && data !== newdata) {
                                options = angular.extend(options, scope.tableOptions);
                                options.oLanguage= localize.language == "vi"? options.lang.vi:options.lang.en
                                element.dataTable().fnDestroy();
                                element.smartResponsiveHelper = null;
                                element.dataTable(options);
                                element.dataTable().fnDraw();
                            }
                        });

                        scope.$watch('tableOptions.url', function (data, newdata) {
                            if (config.type == 'remote' && data && attributes.tableOptions && data !== newdata) {
                                options = angular.extend(options, scope.tableOptions);
                                options.oLanguage= localize.language == "vi"? options.lang.vi:options.lang.en;
                                remote();
                                element.dataTable().fnDestroy();
                                element.smartResponsiveHelper = null;
                                element.dataTable(options);
                                element.dataTable().fnDraw();
                            }
                        });

                        scope.$watch('tableOptions.postData', function (data, newdata) {
                            if (config.type == 'remote' && scope.tableOptions && scope.tableOptions.method == 'POST' && data && attributes.tableOptions && data !== newdata) {
                                options = angular.extend(options, scope.tableOptions);
                                options.oLanguage= localize.language == "vi"? options.lang.vi:options.lang.en;
                                remote();
                                element.dataTable().fnDestroy();
                                element.smartResponsiveHelper = null;
                                element.dataTable(options);
                                element.dataTable().fnDraw();
                            }
                        });

                        scope.$watch('rowIndex', function (idChange, newdata) {
                            if(attributes.rowIndex){
                                var data = element.dataTable().fnSettings().aoData;
                                for(var i = 0; i < data.length; i++){
                                    var item = data[i];
                                    if(item._aData.id == idChange.id){
                                        //item._aData.title = "TEST";
                                        var rowScope = $(item.nTr).scope();
                                        if(scope.rowDataChangeCallback()){
                                            scope.rowDataChangeCallback()(rowScope);
                                        }
                                        element.dataTable().fnUpdate(rowScope.aData, rowScope.iDataIndex,undefined,false);
                                        createChildScope($(item.nTr), rowScope.aData, rowScope.iDataIndex);
                                        break;
                                    }
                                }
                            }
                        });

                        function setScroll(type) {
                            //element.dataTable().fnDestroy();
                            //element.smartResponsiveHelper = null;
                            //element.dataTable(options);
                            //element.dataTable().fnDraw();
                        }

                        function checkAttributes(attr) {
                            if ((attr !== undefined && attr === '') || (angular.isString(attr) && attr.toUpperCase() === windowsize.devicetype().toUpperCase())) {
                                return true;
                            }
                            return false;
                        }
                    }
                };
            }]);
    });