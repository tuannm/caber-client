define([
    'angular',
    'jquery'
],
function (angular, $) {
    'use strict';

    angular
    .module('bidproject.directives')
    .directive('smartDate', ['$compile', 'windowsize', 'navigations', '$stateParams', 'NavigatorGeolocation', 'GeoCoder', function ($compile, windowsize, navigations, $stateParams, NavigatorGeolocation, GeoCoder) {
        return {
            restrict: 'EA',
            transclude: true,
            require: 'ngModel',
            replace: true,
            template:
                function(element, attrs) {
                    var type = attrs.type || 'text';
                    var dataRequired = attrs.hasOwnProperty('required') ? "required='required'" : "";
                    var mindate = attrs.hasOwnProperty('mindate') ? "min-date='minDate'" : "";
                    var dateformat = attrs.hasOwnProperty('dateformat') ? "dateformat='validate date'" : "";
                    var mindatemobile = attrs.hasOwnProperty('mindatemobile') ? "mindatemobile='validate date mobile'" : "";
                    if (navigator.userAgent.match(/(iPhone|Android|BlackBerry|IEMobile)/)){
                        var htmlTextMobile =  '<div class="form-group no-padding left-text"id="isMobile">'+
                        '<input type="date" class="form-control"  ng-model="ngModel" data-smart-validate-input data-i18n="i18n-placeholder" placeholder="expired date" name="name" ' + dataRequired + mindatemobile+'/>'+
                        '</div>'
                        return htmlTextMobile;
                    }
                    else
                    {
                        var htmlTextDesktop = '<div class="form-group no-padding displayBlock" id="isNotMobile">'+
                        '<div class="input-group" >' +
                        '<input type="text" class="form-control" '+ mindate +'  ng-model="ngModel" datepicker-popup="{{format}}"  data-smart-validate-input is-open="isOpen"  name="name" placeholder="dd/mm/yyyy" ng-click="open($event)" '   + dataRequired +''+ dateformat +'/>' +
                        '<span class="input-group-btn">'+
                        '<button type="button" class="btn btn-default" data-ng-click="open($event)"><i '+
                        'class="glyphicon glyphicon-calendar"></i></button>' +
                        '</span>'+
                        '</div>'
                        return htmlTextDesktop;
                    }
                },
            scope: {
                ngModel: '=',
                isOpen: '=',
                minDate:'@',
                name:'=',
                dateformat:'@'
            },

            link: function (scope, element, attrs) {
                var date = new Date();
                scope.minDate = date;
                if (navigator.userAgent.match(/(iPhone|iPod|Android|BlackBerry|IEMobile)/)) {
                    scope.isMobile = true;
                } else {
                    scope.isMobile = false;
                }

                scope.formats = ['dd/MM/yyyy'];
                scope.format = scope.formats[0];
                scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    scope.isOpen = true;
                };

            }
        };
    }]);
});