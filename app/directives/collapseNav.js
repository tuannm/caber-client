define([
    'angular',
    'jquery',
    'jquery.slimscroll'
],
        function (angular, $) {
            'use strict';

            angular
                    .module('bidproject.directives')
                    .directive('collapseNav', ['$compile', function ($compile) {
                            return {
                                restrict: 'A',
                                controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
                                        var collapseNav;
                                        collapseNav = function () {
                                            $('#nav-wrapper-vertical').on('click', '#nav > li > a', function () {
                                                var $ul = $(this).parent('li').find('ul');
                                                var $lengthUl = $ul.children('li').length;
                                                if ($lengthUl > 0) {
                                                    if ($ul.css('display') === 'none') {
                                                        //$('.sub-menu').slideUp();
                                                        //$ul.slideDown();
                                                        $ul.css('display','block');
                                                    } else {
                                                        $ul.css('display','none');
                                                       // $ul.slideUp();
                                                    }
                                                }
                                                else{
                                                    $('.sub-menu').css('display','none');
                                                    //$('.sub-menu').slideUp();
                                                }
                                            });
                                        };
                                        return collapseNav();
                                    }]
                            };
                        }]);
        });