define([
		'angular',
		'jquery'
	],
	function (angular, $) {
		'use strict';

		angular
			.module('bidproject.directives')
			.directive('smartWizard', ['$parse', 'windowsize', '$compile', function ($parse, windowsize, $compile) {
				return {
					restrict: 'A',
					scope: {
						'smartWizardCallback': '&',
						'validate': '='
					},
					link: function (scope, element, attributes) {
						var stepsCount = $('[data-smart-wizard-tab]').length;
						var currentStep = 1;

						var validSteps = [];

						var $form = element.closest('form');

						var $prev = $('[data-smart-wizard-prev]', element);

						var $next = $('[data-smart-wizard-next]', element);

						var $finishLabel = attributes["finishLabel"];

						var checkButton = function () {
							$next.show();
							var ele;
							if (currentStep < stepsCount && windowsize.devicetype() != 'Desktop') {
								ele = angular.element('<a href="" data-i18n="next"></a>');
							} else {
								ele = angular.element('<a href="" data-i18n="finish"></a>');
								if ($finishLabel) {
									ele = angular.element('<a href="" data-i18n="' + $finishLabel + '"></a>');
								}
							}
							var html = $compile(ele)(scope);
							$next.html(html);
						}

						function setStep(step) {
							currentStep = step;
							$('[data-smart-wizard-pane=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-pane]').removeClass('active');
							$('[data-smart-wizard-tab=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-tab]').removeClass('active');
							checkButton();
							$prev.toggleClass('disabled', step == 1)
						}

						element.on('click', '[data-smart-wizard-tab]', function (e) {
							setStep(parseInt($(this).data('smartWizardTab')));
//                    e.preventDefault();
						});



						$next.on('click', function (e) {
							if ($form.data('validator') && windowsize.devicetype() != 'Desktop') {
								if (!$form.valid()) {
									validSteps = _.without(validSteps, currentStep);
									$form.data('validator').focusInvalid();
									return false;
								} else {
									validSteps = _.without(validSteps, currentStep);
									validSteps.push(currentStep);
									element.find('[data-smart-wizard-tab=' + currentStep + ']')
										.addClass('complete')
										.find('.step')
										.html('<i class="fa fa-check"></i>');
								}
							}
							if (currentStep < stepsCount && windowsize.devicetype() != 'Desktop') {
								setStep(currentStep + 1);
							} else {
								if (validSteps.length < stepsCount && windowsize.devicetype() != 'Desktop') {
									var steps = _.range(1, stepsCount + 1)

									_(steps).forEach(function (num) {
										if (validSteps.indexOf(num) == -1) {
											setStep(num);
											return false;
										}
									})
								} else {
									if (windowsize.devicetype() === 'Desktop') {
										if (!$form.valid()) {
											validSteps = _.without(validSteps, currentStep);
											$form.data('validator').focusInvalid();
											return false;
										}
									}
									var data = {};
									_.each($form.serializeArray(), function (field) {
										data[field.name] = field.value
									});
									if (typeof  scope.smartWizardCallback() === 'function') {
										scope.smartWizardCallback()(data)
									}
								}
							}
							e.preventDefault();
						});

						$prev.on('click', function (e) {
							if (!$prev.hasClass('disabled') && currentStep > 0) {
								setStep(currentStep - 1);
							}
							e.preventDefault();
						});
						displayContent(windowsize.devicetype());
						scope.$on("window.size", function (object, type) {
							displayContent(type);
						});

						function displayContent(type) {
							if (type === "Desktop") {
								for (var i = 1; i <= stepsCount; i++) {
									$('[data-smart-wizard-pane=' + i + ']', element).addClass('active');
									$('[data-smart-wizard-tab=' + i + ']', element).addClass('active');
									$('[data-smart-wizard-tab=' + i + ']', element).append($('[data-smart-wizard-pane=' + i + ']', element));
								}
								checkButton();
							} else {
								for (var i = 1; i <= stepsCount; i++) {
									$('.tab-content', element).append($('[data-smart-wizard-pane=' + i + ']', element));
								}
								setStep(currentStep);
							}
						}
					}
				}
			}]);

	});