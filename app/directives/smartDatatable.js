define([
    'angular',
    'jquery',
    'datatables',
    'datatables-responsive',
    'datatables-colvis',
    'datatables-tools',
    'datatables-bootstrap'
],
        function (angular, $) {
            'use strict';

            angular
                    .module('bidproject.directives')
                    .directive('smartDatatable', ['$compile', 'windowsize', function ($compile, windowsize) {
                            return {
                                restrict: 'A',
                                scope: {
                                    tableOptions: '=',
                                    onSelection: '&',
                                    onMouseover: '&',
                                    dtTemplate: '='
                                },
                                replace: true,
                                templateUrl:'<table datatable-basic on-selection="hideContent" show-hidden mouseover-apply="Desktop" click-apply="SmartDevice" on-mouseover="formatContent" table-options="newOrder" class="datatable table table-striped table-bordered table-hover">',
                                link: function (scope, element, attributes) {

                                    var options = {
                                        "sDom": "<'dt-toolbar'<''f><'col-sm-6 col-xs-12 hidden'l>r>" +
                                                "t" +
                                                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                                        oLanguage: {
                                            "sSearch": "<label class='icon-addon addon-lg'><label class='glyphicon glyphicon-search' title='search'></label>",
                                            "sLengthMenu": "_MENU_"
                                        },
                                        "autoWidth": false,
                                        "smartResponsiveHelper": null,
                                        "preDrawCallback": function () {
                                            // Initialize the responsive datatables helper once.
                                            if (!this.smartResponsiveHelper) {
                                                this.smartResponsiveHelper = new ResponsiveDatatablesHelper(element, {
                                                    desktop: 1000000,
                                                    tablet: 768,
                                                    phone: 480

                                                });
                                                this.smartResponsiveHelper.rowLiTemplate = '<li class="col-md-12 col-xs-12 no-padding"><span class="columnTitle col-md-2 col-xs-3 no-padding"><!--column title--></span> <span class="columnValue col-md-10 col-xs-9 no-padding"><!--column value--></span></li>';

                                            }
                                        },
                                        "rowCallback": function (nRow) {
                                            this.smartResponsiveHelper.createExpandIcon(nRow);
                                        },
                                        "drawCallback": function (oSettings) {

                                            this.smartResponsiveHelper.respond();
                                        },
                                        fnCreatedRow: function( nRow, aData, iDataIndex ) {
                                            if(aData.children && aData.children.length > 0 && scope.tableOptions.children){
                                                $(nRow).addClass("childControl");
                                            }
                                            createChildScope( nRow, aData, iDataIndex)
                                            $(nRow).click(function (event) {
                                                if($(this).hasClass('childControl')){
                                                    var tr = $(this).closest('tr');

                                                    var row = element.DataTable().row(tr);
                                                    if ( row.child.isShown() ) {
                                                        // This row is already open - close it
                                                        row.child.hide();
                                                        tr.removeClass('shown');
                                                    }
                                                    else {
                                                        // Open this row
                                                        var childScope = scope.$new();
                                                        childScope.data = {
                                                            paging: false,
                                                            scrollCollapse: false,
                                                            onSelection: scope.onSelection(),
                                                            multiSelect: attributes.multiSelect
                                                        }
                                                        angular.extend(childScope.data, scope.tableOptions.children);
                                                        childScope.data.aaData =  element.dataTable().fnGetData(this)["children"];
                                                        console.log("childScope.data", childScope.data);
                                                        var childTemplate = $('<table on-selection="data.onSelection" multi-select="{{data.multiSelect}}" datatable-basic table-options="data" class="table table-striped table-bordered"><thead><tr><th>Name</th></tr></thead></table>');
                                                        var html = $compile(childTemplate)(childScope);
                                                        row.child(html).show();
                                                        tr.addClass('shown');
                                                    }
                                                }else{
                                                    if (checkAttributes(attributes.clickApply) || angular.isUndefined(attributes.clickApply) ){
                                                        if (attributes.onSelection !== undefined){
                                                            $('tbody tr.row-detail', element).remove();
                                                            $('tbody tr', element).removeClass('detail-show');
                                                            if (checkAttributes(attributes.showHidden)){
                                                                event.data = {responsiveDatatablesHelperInstance: element.smartResponsiveHelper};
                                                                element.smartResponsiveHelper.showRowDetailEventHandler.call(this, event);
                                                            }
                                                            if (checkAttributes(attributes.multiSelect)) {
                                                                $(this).toggleClass('active');
                                                            }else{
                                                                $(this).addClass('active').siblings($(this)).removeClass('active');
                                                            }
                                                            createChildScope( $(this).next('.row-detail'), element.dataTable().fnGetData(this), $(this).index());
                                                            if (scope.onSelection() && typeof scope.onSelection() === 'function') {
                                                                scope.onSelection()($(this).index(), element.dataTable().fnGetData(this), $(this));
                                                            }
                                                        }
                                                    }
                                                }

                                            });
                                            $(nRow).mouseover(function (event) {
                                                if (checkAttributes(attributes.mouseoverApply)){
                                                    $('tbody tr.row-detail', element).remove();
                                                    $('tbody tr', element).removeClass('detail-show');
                                                    event.data = {responsiveDatatablesHelperInstance: element.smartResponsiveHelper};
                                                    element.smartResponsiveHelper.showRowDetailEventHandler.call(this, event);
                                                    $(this).addClass('active').siblings($(this)).removeClass('active');
                                                    createChildScope( $(this).next('.row-detail'), element.dataTable().fnGetData(this), $(this).index());
                                                    if (scope.onMouseover() && typeof scope.onMouseover() === 'function') {
                                                        scope.onMouseover()($(this).index(), element.dataTable().fnGetData(this), $(this));
                                                    }
                                                }
                                            })
                                        }
                                    };

                                    var createChildScope = function(row, aData, iDataIndex){
                                        var rowScope = scope.$parent.$new(true);
                                        rowScope.aData = aData;
                                        rowScope.iDataIndex = iDataIndex;
                                        $compile(row)(rowScope);
                                    }

                                    if (attributes.tableOptions) {
                                        options = angular.extend(options, scope.tableOptions);
                                    }

                                    var _dataTable;
                                    setScroll(windowsize.devicetype());

                                    _dataTable = element.DataTable(options);

                                    scope.$on("window.size", function(object,type){
                                        setScroll(type);
                                    });
                                    scope.$watch('tableOptions.aaData', function (data, newdata) {
                                        if (data && attributes.tableOptions && data !== newdata) {
                                            options = angular.extend(options, scope.tableOptions);
                                            element.dataTable().fnDestroy();
                                            element.smartResponsiveHelper = null;
                                            element.dataTable(options);
                                            element.dataTable().fnDraw();
                                        }
                                    });

                                    function setScroll(type){
                                        if(type !=='Desktop'){
                                            options.paging = false;
                                            options.scrollCollapse = true;
                                        }
                                    }

                                    function checkAttributes(attr){
                                        if((attr !== undefined && attr === '') || (angular.isString(attr) && attr.toUpperCase() === windowsize.devicetype().toUpperCase())){
                                            return true;
                                        }
                                        return false;
                                    }
                                }
                            };
                        }]);
        });