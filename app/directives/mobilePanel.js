define([
		'angular',
		'jquery'
	],
	function (angular, $) {
		'use strict';

		angular
			.module('bidproject.directives')
			.directive('mobilePanel', ['$compile', '$rootScope', function ($compile, $rootScope) {
				var container = '<div class="panel-container" ng-style="{\'min-height\':row.height}"></div>';
				var content = '<div class="panel-content"></div>';

				var panelHeader =
					'<div class="panel-header">' +
					'<div class="row-fluid">' +
					'<div class="span12 alert-error panel-error" ng-hide="!panel.error">' +
					'<a class="close" ng-click="panel.error=false">&times;</a>' +
					'</div>' +
					'</div>';

				return {
					restrict: 'E',
					link: function ($scope, elem, attr) {
						// once we have the template, scan it for controllers and
						// load the module.js if we have any
						var newScope = $scope.$new();
						$scope.kbnJqUiDraggableOptions = {
							revert: 'invalid',
							helper: function () {
								return $('<div style="width:200px;height:100px;background: rgba(100,100,100,0.50);"/>');
							},
							placeholder: 'keep'
						};

						// compile the module and uncloack. We're done
						function loadModule($module) {
							$module.appendTo(elem);
							elem.wrap(container);
							/* jshint indent:false */
							$compile(elem.contents())(newScope);
							elem.removeClass("ng-cloak");
						}

						newScope.$on('$destroy', function () {
							elem.unbind();
							elem.remove();
						});

						$scope.$watch(attr.type, function (name) {
							elem.addClass("ng-cloak");
							// load the panels module file, then render it in the dom.
							if (name) {
								var nameAsPath = name.replace(".", "/");
								$scope.require([
									'jquery',
									'text!panels/' + nameAsPath + '/module.html',
									'text!panels/' + nameAsPath + '/editor.html'
								], function ($, moduleTemplate) {
									var $module = $(moduleTemplate);
									// top level controllers
									var $controllers = $module.filter('ngcontroller, [ng-controller], .ng-controller');
									// add child controllers
									$controllers = $controllers.add($module.find('ngcontroller, [ng-controller], .ng-controller'));

									if ($controllers.length) {
										$controllers.first().prepend(panelHeader);
										$controllers.first().find('.panel-header').nextAll().wrapAll(content);

										$scope.require([
											'panels/' + nameAsPath + '/module'
										], function () {
											loadModule($module);
										});
									} else {
										loadModule($module);
									}
								});
							}
						});
					}
				};
			}]);
			//.directive('textarea', [
			//	function () {
			//		return {
			//			restrict: 'E',
			//			link: link
			//		};
			//	}])
			//.directive('input', [
			//	function () {
			//		return {
			//			restrict: 'E',
			//			link: link
			//		};
			//	}
			//]);
		//function link(scope, elem) {
		//	// bind the events iff this is an input/textarea within a modal
		//	if (elem.parents('.modal').length) {
		//		elem.on('touchstart', function (e) {
		//			elem.focus();
		//			e.preventDefault();
		//			e.stopPropagation();
		//		});
		//	}
		//}

	});