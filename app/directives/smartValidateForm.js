define([
        'angular',
        'jquery',
        'jquery-validation'

    ],
    function (angular, $) {
        'use strict';

        angular
            .module('bidproject.directives')
            .directive('smartValidateForm', ['$parse','localize', '$compile', function ($parse,localize, $compile) {
                return {
                    restrict: 'A',
                    link: function (scope, form, attributes) {
                        jQuery.validator.addMethod("passwordcheck", function (value, element) {
                            if (value.length > 0) {
                                var strength = 0;
                                ///if length is 8 characters or more, increase strength value
                                if (value.length > 7) {
                                    strength += 1;
                                }

                                //if password contains both lower and uppercase characters
                                if (value.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                                    strength += 1;
                                }

                                //if it has numbers and characters,
                                if (value.match(/([a-zA-Z])/) && value.match(/([0-9])/)) {
                                    strength += 1;
                                }

                                //if it has one special character
                                if (value.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                                    strength += 1;
                                }


                                if (strength > 2) {
                                    return true;
                                }
                                return false
                            }
                            return true;
                        });
                        jQuery.validator.addMethod("retypepassword", function (value, element) {
                            if (value.length > 0) {
                                return value == $('[name=password]').val();
                                return false
                            }
                            return true;
                        });


                        jQuery.validator.addMethod("comparecurrency", function (value, element) {
                            var budgetTo = value.split(",").join("");
                            var budgetFrom = $("#budgetFrom").val().split(",").join("");
                            return parseInt(budgetTo) >= parseInt(budgetFrom);
                        });

                        jQuery.validator.addMethod("comparetimeFrom", function (value, element) {
                            var timeFrom = $('.timeFrom').val();
                            if(timeFrom.length > 0 && value.length > 0){
                                return value >= timeFrom;
                            }
                            return true;
                        });
                        jQuery.validator.addMethod("comparetimeTo", function (value, element) {
                            var timeTo = $('.timeTo').val();
                            if(timeTo.length > 0 && value.length > 0){
                                return value <= timeTo;
                            }
                           return true;
                        });

                        jQuery.validator.addMethod('timeformat',function (value,element){
                            if (value.length > 0) {
                                var regs = value.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/);
                                if (regs) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                            return true;
                        });
                        jQuery.validator.addMethod('mindatemobile',function (value,element){

                                if (value.length > 0) {
                                    var valueDate = Date.parse(value);
                                    //var tomorrow = new Date.today().add(1).day();
                                    //Date.today().add(1).day()
                                    //Today.setDate(Today.getDate() + 1)
                                    var tomorrow = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
                                    var currentDate =  Date.parse(tomorrow)
                                    if (valueDate < currentDate) {
                                        return false;
                                    }
                                    else {
                                        return true
                                    }
                                }
                                return true;


                        });
                        jQuery.validator.addMethod('dateformat',function (value,element){
                            if (value.length > 0) {
                                var regs = value.match(/^(?:(?:31(\/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);
                                if (regs) {
                                    var minDateAttribute = element.attributes['min-date'];
                                    if (minDateAttribute != undefined) {
                                        var tomorrow = new Date(new Date().getTime() - 24 * 60 * 60 * 1000)
                                        var currentDate =  Date.parse(tomorrow)
                                        var minDate = element.attributes['min-date'].value;
                                        if (minDate != "") {
                                            var valueDate = value.split("/");
                                            var date = new Date(valueDate[2], valueDate[1] - 1, valueDate[0]);

                                            var min = new Date(minDate.replace(/"/g, ""));
                                            min.setHours(0, 0, 0, 0);
                                            if (date < currentDate) {
                                                return false;
                                            }
                                        }
                                    }
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return true;
                            }
                        });
                        jQuery.validator.addMethod("requireField", function (value, element) {
                            var val = value.split(",").join("");
                            return parseInt(val) >= 10000;
                        });
                        jQuery.validator.addMethod("requirePrice", function (value, element) {
                            if(value != ""){
                                var val = value.split(",").join("");
                                return parseInt(val) >= 10000;
                            } else{
                                return true;
                            }

                        });
                        var valid;
                        var render_validate = function(){
                            jQuery.extend(jQuery.validator.prototype, {
                                showLabel: function( element, message ) {
                                    var messageNew = localize.getLocalizedString(message)
                                    var place, group, errorID,
                                        error = this.errorsFor( element ),
                                        elementID = this.idOrName( element ),
                                        describedBy = $( element ).attr( "aria-describedby" );
                                    if ( error.length ) {
                                        // refresh error/success class
                                        error.removeClass( this.settings.validClass ).addClass( this.settings.errorClass );
                                        // replace message on existing label
                                        error.html( messageNew );
                                    } else {
                                        // create error element
                                        error = $( "<" + this.settings.errorElement + ">" )
                                            .attr( "id", elementID + "-error" )
                                            .addClass( this.settings.errorClass )
                                            .html( message || "" );

                                        // Maintain reference to the element to be placed into the DOM
                                        place = error;
                                        if ( this.settings.wrapper ) {
                                            // make sure the element is visible, even in IE
                                            // actually showing the wrapped element is handled elsewhere
                                            place = error.hide().show().wrap( "<" + this.settings.wrapper + "/>" ).parent();
                                        }
                                        if ( this.labelContainer.length ) {
                                            this.labelContainer.append( place );
                                        } else if ( this.settings.errorPlacement ) {
                                            this.settings.errorPlacement( place, $( element ) );
                                        } else {
                                            place.insertAfter( element );
                                        }

                                        // Link error back to the element
                                        if ( error.is( "label" ) ) {
                                            // If the error is a label, then associate using 'for'
                                            error.attr( "for", elementID );
                                        } else if ( error.parents( "label[for='" + elementID + "']" ).length === 0 ) {
                                            // If the element is not a child of an associated label, then it's necessary
                                            // to explicitly apply aria-describedby

                                            errorID = error.attr( "id" ).replace( /(:|\.|\[|\])/g, "\\$1");
                                            // Respect existing non-error aria-describedby
                                            if ( !describedBy ) {
                                                describedBy = errorID;
                                            } else if ( !describedBy.match( new RegExp( "\\b" + errorID + "\\b" ) ) ) {
                                                // Add to end of list if not already present
                                                describedBy += " " + errorID;
                                            }
                                            $( element ).attr( "aria-describedby", describedBy );

                                            // If this element is grouped, then assign to all elements in the same group
                                            group = this.groups[ element.name ];
                                            if ( group ) {
                                                $.each( this.groups, function( name, testgroup ) {
                                                    if ( testgroup === group ) {
                                                        $( "[name='" + name + "']", this.currentForm )
                                                            .attr( "aria-describedby", error.attr( "id" ) );
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    if ( !message && this.settings.success ) {
                                        error.text( "" );
                                        if ( typeof this.settings.success === "string" ) {
                                            error.addClass( this.settings.success );
                                        } else {
                                            this.settings.success( error, element );
                                        }
                                    }
                                    this.toShow = this.toShow.add( error );
                                }
                            });
                            var validateOptions = {
                                rules: {},
                                messages: {},

                                highlight: function (element) {
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                                },
                                unhighlight: function (element) {
                                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                                },
                                errorElement: 'span',
                                errorClass: 'help-block',
                                errorPlacement: function (error, element) {
                                    $(error).attr("i18n",error[0].innerHTML);
                                    $compile(error)(scope);
                                    if (element.hasClass('select2')) {
                                        error.insertAfter(element.parent().find('.selection'));
                                    } else {
                                        if (element.parent('.input-group').length) {

                                            error.insertAfter(element.parent());
                                        } else {
                                            error.insertAfter(element);
                                        }
                                    }

                                }
                            };
                            form.find('[data-smart-validate-input], [smart-validate-input]').each(function () {
                                var $input = $(this), fieldName = $input.attr('name');

                                validateOptions.rules[fieldName] = {};

                                if ($input.data('required') != undefined) {
                                    validateOptions.rules[fieldName].required = true;
                                }
                                if ($input.data('email') != undefined) {
                                    validateOptions.rules[fieldName].email = true;
                                }

                                if ($input.data('maxlength') != undefined) {
                                    validateOptions.rules[fieldName].maxlength = $input.data('maxlength');
                                }

                                if ($input.data('minlength') != undefined) {
                                    validateOptions.rules[fieldName].minlength = $input.data('minlength');
                                }
                                if ($input.attr('passwordcheck') != undefined) {

                                    validateOptions.rules[fieldName].passwordcheck = true;
                                    validateOptions.messages[fieldName] = $input.attr('passwordcheck');
                                }
                                if ($input.attr('retypepassword') != undefined) {

                                    validateOptions.rules[fieldName].retypepassword = true;
                                    validateOptions.messages[fieldName] = $input.attr('retypepassword');
                                }
                                if ($input.attr('comparecurrency') != undefined) {

                                    validateOptions.rules[fieldName].comparecurrency = true;
                                    validateOptions.messages[fieldName] = $input.attr('comparecurrency');
                                }
                                if ($input.attr('mindatemobile') != undefined) {

                                    validateOptions.rules[fieldName].mindatemobile = true;
                                    validateOptions.messages[fieldName] = $input.attr('mindatemobile');
                                }
                                if ($input.attr('comparetimeFrom') != undefined) {

                                    validateOptions.rules[fieldName].comparetimeFrom = true;
                                    validateOptions.messages[fieldName] = $input.attr('comparetimeFrom');
                                }
                                if ($input.attr('comparetimeTo') != undefined) {

                                    validateOptions.rules[fieldName].comparetimeTo = true;
                                    validateOptions.messages[fieldName] = $input.attr('comparetimeTo');
                                }
                                if ($input.attr('dateformat') != undefined) {
                                    validateOptions.rules[fieldName].dateformat = true;
                                    validateOptions.messages[fieldName] = $input.attr('dateformat');
                                }
                                if ($input.attr('timeformat') != undefined) {
                                    validateOptions.rules[fieldName].timeformat = true;
                                    validateOptions.messages[fieldName] = $input.attr('timeformat');
                                }
                                if ($input.attr('requireField') != undefined) {

                                    validateOptions.rules[fieldName].requireField = true;
                                    validateOptions.messages[fieldName] = $input.attr('requireField');
                                }
                                if ($input.attr('requirePrice') != undefined) {

                                    validateOptions.rules[fieldName].requirePrice = true;
                                    validateOptions.messages[fieldName] = $input.attr('requirePrice');
                                }
                                if ($input.data('message')) {
                                    validateOptions.messages[fieldName] = $input.data('message');
                                } else {
                                    angular.forEach($input.data(), function (value, key) {
                                        if (key.search(/message/) == 0) {
                                            if (!validateOptions.messages[fieldName])
                                                validateOptions.messages[fieldName] = {};

                                            var messageKey = key.toLowerCase().replace(/^message/, '')
                                            //var valueMessage = localize.getLocalizedString(value)
                                            validateOptions.messages[fieldName][messageKey] = value;
                                        }
                                    });
                                }
                            });
                            if(!valid){
                                valid = form.validate(validateOptions);

                            }else{
                                jQuery.extend(valid.settings, validateOptions);
                            }

                        }
                        //
                        render_validate();
                        scope.$on("validate", function(){
                            render_validate();
                        })

                    }
                }
            }]);
        angular.module('bidproject.directives')
            .directive('smartClick', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    require: '^?smartValidateForm',
                    scope: {
                        'smartClick': '&'
                    },
                    link: function (scope, element, attributes) {

                        element.on('click', function (e) {
                            var $form = element.closest('form');
                            if (!$form.valid()) {
                                //validSteps = _.without(validSteps, currentStep);
                                $form.data('validator').focusInvalid();
                                return false;
                            }
                            if (typeof scope.smartClick() === 'function') {
                                scope.$apply(function () {
                                    scope.smartClick()()
                                });
                            }
                        });
                    }
                }
            }]);

    });