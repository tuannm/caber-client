define([
  'angular',
  'jquery'
],
function (angular, $) {
  'use strict';
  var template = ' \
  <div class="navbar-screen"> \
    <div class="navbar-title"> \
      <h1><span data-i18n="{{status.title}}">{{status.title}}</span></h1> \
    </div> \
    <div class="navbar-back"> \
      <a role="button"  ng-show="showBack" ng-click="status.onBack()"><span class="fa fa-angle-left pull-left back-button"></span> \
        {{status.backLabel}} \
      </a> \
    </div> \
    <div class="navbar-action"> \
      <a role="button" ng-show="showAction" ng-click="status.actionClick()"> \
        <span ng-bind-html="status.actionLabel"></span> \
      </a> \
    </div> \
  </div>';
  angular
    .module('bidproject.directives')
    .directive('navigationBarScreen', ['$parse', '$rootScope', function($parse, $rootScope) {
        return {
          restrict: 'E',
          replace: true,
          template: template,
          scope: {
            status: '='
          },
          require: '^navigationBar',
          link: function(scope, elt, attrs, navigationBarCtrl) {
            scope.$watch('status', function(status, oldStatus) {
              scope.showBack = !!(status && status.onBack);
              scope.showAction = !!(status && status.actionClick);
            }, true);
          }
        };
    }]);
});