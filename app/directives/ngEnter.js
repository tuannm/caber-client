/**
 * Created by ngocnh on 5/4/15.
 */

define([
        'angular',
        'jquery',
    ],
    function (angular) {
        'use strict';

        angular.module('bidproject.directives')
            .directive('ngEnter', function () {
                return {
                    restrict: 'A',
                    link: function (scope, elem, attrs) {

                        elem.bind('keydown', function (event) {
                            var code = event.keyCode || event.which;

                            if (code === 13) {
                                if (!event.shiftKey) {
                                    event.preventDefault();
                                    scope.$apply(attrs.ngEnter);
                                }
                            }
                        });
                    }
                }
            }
        );
    });