define([
  'angular',
  'jquery',
  'select2'
],
function (angular, $) {
  'use strict';

  angular
    .module('bidproject.directives')
    .directive('smartSelect2', ['$timeout', '$parse', 'NavigatorGeolocation',function($timeout, $parse, NavigatorGeolocation) {
      return {
              restrict: 'A',
              priority: 100,
              scope: {
                  ngModel: "="
              },
              require: '?ngModel',
              link: function (scope,element, attributes) {
                  $(element).removeAttr('smart-select2 data-smart-select2');
                  var options = {
                      allowClear:true
                  };
                  if(attributes.placeholder){
                      $(element).attr('data-placeholder',attributes.placeholder);
                      options.placeholder = attributes.placeholder;
                  }
                  $(element).select2(options);
                  var offset = 0;
                  //$(element).on("select2:open", function (e) {
                  //    offset = $(element).parents().parents().offset();
                  //    var position = offset.top - $(window).scrollTop();
                  //    $("html, body").animate({scrollTop: position}, "slow");
                  //});
                  //$(element).on("select2:close", function(e){
                  //    $("html, body").animate({scrollTop: offset}, "slow");
                  //});
                  var modelAccessor = $parse(attributes.ngModel);
                  $timeout(function() {
                      $(element).select2();
                  });
                  scope.$watch(modelAccessor, function (val) {
                      if(val) {
                          $(element).select2("val",val);
                          scope.$apply();
                      }
                  });
              }
          }

    }]);

});