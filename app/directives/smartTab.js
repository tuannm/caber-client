define([
        'angular',
        'jquery',
        'jquery.slimscroll'
    ],
    function (angular, $) {
        'use strict';

        angular
            .module('bidproject.directives')
            .directive('smartTab', ['$compile', 'windowsize', 'navigations', '$stateParams','$timeout', function ($compile, windowsize, navigations, $stateParams,$timeout) {
                return {
                    restrict: 'E',
                    link: function (scope, element, attrs) {
                        var tabClass = "col-xs-12 btn btn-primary btn-gap";
                        var tabContent = $(element).find("ul.nav-tabs");
                        var tabs = tabContent.find("li");
                        var contents = $(element).find(".tab-pane");
                        var position = 0;
                        var resonsiveType;
                        tabResponsive(windowsize.devicetype());
                        if (navigations.current.isCallBack == true) {
                            $(tabs[navigations.current.index]).click()
                        }
                        scope.$on("window.size", function (object, type) {
                            tabResponsive(type);
                        });
                        function tabResponsive(type) {
                            if (type == resonsiveType) return;
                            if (type === 'Desktop') {
                                resonsiveType = type;
                                tabContent.addClass("nav-tabs");
                                angular.forEach(tabs, function (tab) {
                                    $(tab).unbind("click");
                                    $(tab).removeClass(tabClass);
                                });
                                $("li", tabContent).removeClass("hide");
                                $(tabs[position]).addClass("active");
                                $(contents[position]).addClass("active");
                            } else {
                                resonsiveType = type;
                                tabContent.removeClass("nav-tabs");
                                _.forEach(tabs, function (tab, index) {
                                    $(tab).on("click", function (event) {
                                        $timeout(function () {
                                        scope.$apply(function(){
                                        if (navigations.current.isCallBack == true) {
                                            navigations.pushAtIndex(2, {
                                                title: $('a span', tab).last().attr("data-i18n") || $('a span', tab).last().html(),
                                                path: $stateParams.routeId,
                                                isCallBack: true,
                                                index: index,
                                                backCallback: function () {
                                                    $("li", tabContent).removeClass("hide");
                                                    angular.forEach(contents, function (content) {
                                                        $(content).removeClass("active");
                                                    });
                                                },
                                                actionCallback: function () {
                                                    position = index;
                                                    $("li", tabContent).addClass("hide");
                                                    $(contents[position]).addClass("active");
                                                }
                                            });
                                        }
                                        else {
                                            navigations.push({
                                                title: $('a span', tab).last().attr("data-i18n") || $('a span', tab).last().html(),
                                                path: $stateParams.routeId,
                                                isCallBack: true,
                                                index: index,
                                                backCallback: function () {
                                                    $("li", tabContent).removeClass("hide");
                                                    angular.forEach(contents, function (content) {
                                                        $(content).removeClass("active");
                                                    });
                                                },
                                                actionCallback: function () {
                                                    position = index;
                                                    $("li", tabContent).addClass("hide");
                                                    $(contents[position]).addClass("active");
                                                }
                                            });
                                        }
                                        //});
                                        })
                                        });
                                    });
                                    $(tab).removeClass("active");
                                    $(tab).addClass(tabClass);
                                });
                                angular.forEach(contents, function (content) {
                                    $(content).removeClass("active");
                                });
                                $(element).focus();
                            }
                        }
                    }
                };
            }]);
    });