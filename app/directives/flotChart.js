define([
    'angular',
    'jquery',
    'flot',
    'flot-resize',
    'flot-pie',
    'flot-stack',
    'flot-tooltip',
    'flot-time'
],
function (angular, $) {
    'use strict';

    angular
    .module('bidproject.directives')
    .directive('flotChart', ['$compile', 'windowsize', 'navigations', '$stateParams', function ($compile, windowsize, navigations, $stateParams) {
        return {
            restrict: 'A',
            scope: {
                data: '=',
                options: '='
            },
            link: function(scope, ele, attrs) {
                var data, options, plot;
                data = scope.data;
                options = scope.options;
                scope.$watch("data", function (data) {
                    if(data && data.length > 0){
                        $.plot(ele[0], data, options);
                    }
                });
            }
        };
    }]);
});