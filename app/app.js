/**
 * main app level module
 */
define(['angular', 'jquery', 'lodash', 'require', 'fastclick', 'angular-touch', 'angular-animate', 'angular-dialog', 'angular-easyfb', 'auth', 'angular-bootstrap', 'ng-tags-input', 'angular-busy', 'angular-loading-bar', 'angular-carousel', 'angular-sanitize', 'angular-strap', 'angular-cookies', 'extend-jquery', 'jquery-validation', 'bindonce', 'angular-ui-router', 'ui-select', 'ng-map', 'template', 'angular-bootstrap-slider', 'angular-image-crop', 'flowtype', 'socialLinks', 'indexedDB', 'angular-img-cropper', 'template'], function (angular, $, _, appLevelRequire, FastClick) {
    "use strict";

    var app = angular.module('BidProject-App', ["ngTouch", "ngAnimate", "ngDialog", "angular-loading-bar", "ezfb", "angular-carousel", 'auth', 'ui.bootstrap', 'ngTagsInput', 'cgBusy', 'ui.router', 'ui.select', 'ngMap', 'ui.bootstrap-slider', 'ImageCropper', 'socialLinks', 'xc.indexedDB', 'angular-img-cropper', 'appTemplates']).config(function (cfpLoadingBarProvider, $indexedDBProvider) {
            cfpLoadingBarProvider.includeSpinner = true;
            $indexedDBProvider
                .connection('vaber_indexed_db_v1')
                .upgradeDatabase(1, function (event, db) {
                    var objStore = db.createObjectStore('vaberIndexed', {keyPath:'uri'});
                    objStore.createIndex('data_idx', 'data', {unique:false});
                });
        }),
    // we will keep a reference to each module defined before boot, so that we can
    // go back and allow it to define new features later. Once we boot, this will be false
        pre_boot_modules = [],
    // these are the functions that we need to call to register different
    // features if we define them after boot time
        register_fns = {};

    // This stores the app revision number, @REV@ is replaced by grunt.
    app.constant('appVersion', "@REV@");

    // Use this for cache busting partials
    app.constant('cacheBust', "cache-bust=" + Date.now());
    /**
     * Tells the application to watch the module, once bootstraping has completed
     * the modules controller, service, etc. functions will be overwritten to register directly
     * with this application.
     * @param  {[type]} module [description]
     * @return {[type]}        [description]
     */
    app.useModule = function (module) {
        if (pre_boot_modules) {
            pre_boot_modules.push(module);
        } else {
            _.extend(module, register_fns);
        }
        return module;
    };

    app.safeApply = function ($scope, fn) {
        switch ($scope.$$phase) {
            case '$apply':
                // $digest hasn't started, we should be good
                $scope.$eval(fn);
                break;
            case '$digest':
                // waiting to $apply the changes
                setTimeout(function () {
                    app.safeApply($scope, fn);
                }, 10);
                break;
            default:
                // clear to begin an $apply $$phase
                $scope.$apply(fn);
                break;
        }
    };
    app.run(function ($window, $rootScope) {
        FastClick.attach(document.body);
        $window.addEventListener("offline", function () {
            $rootScope.$apply(function () {
                $rootScope.online = false;
                console.log("off");
            });
        }, false);
        $window.addEventListener("online", function () {
            $rootScope.$apply(function () {
                $rootScope.online = true;
                console.log("on");
            });
        }, false);

    });
    app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $locationProvider) {

            $stateProvider.state('dashboard', {
                url:'/default',
                templateUrl:'app/partials/dashboard.html',
                authorizedRoles:"all",
                navigation:"hidden",
                "level":0
            }).state('about', {
                    url:'/about',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('help', {
                    url:'/help',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('sample', {
                url:'/help',
                templateUrl:'app/partials/dashboard.html',
                authorizedRoles:"all"
            }).state('signup', {
                    url:'/signup',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"guest",
                    navigation:"hidden"
                }).state('chat', {
                    url:'/chat',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"supporter",
                    navigation:"hidden"
                }).state('login', {
                    url:'/login',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"guest",
                    navigation:"hidden",
                    level:1
                }).state('post', {
                    url:'/post',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('headhunt', {
                    url:'/headhunt',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('quick', {
                    url:'/quick',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('helper', {
                    url:'/helper',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('tutoring', {
                    url:'/tutoring',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('other', {
                    url:'/other',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('postlikethis', {
                    url:'/postlikethis',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('postedit', {
                    url:'/postedit',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('detailpost', {
                    url:'/detailpost',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('viewdetailpost', {
                    url:'/viewdetailpost/:params',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"customer"
                }).state('detailSupplier', {
                    url:'/detailSupplier',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["broker", "supplier"]
                }).state('updateskills', {
                    url:'/updateskills',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"supplier"
                }).state('detailOrder', {
                    url:'/detailOrder/:params',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["broker", "supplier"]
                }).state('detailAdvanceSearch', {
                    url:'/detailAdvanceSearch/:params',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["broker", "supplier"]
                }).state('addnewcv', {
                    url:'/addnewcv',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["broker", "supplier"]
                }).state('feeds', {
                    url:'/feeds',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["broker", "supplier"]
                }).state('policy', {
                    url:'/policy',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('inviteFriend', {
                    url:'/inviteFriend',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('dashboards', {
                    url:'/dashboard',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('updateuser', {
                    url:'/updateuser',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:"all"
                }).state('tab', {
                    url:'/:routeId',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["customer", "supplier", "broker"]
                }).state('detail', {
                    url:'/:routeId/:params',
                    templateUrl:'app/partials/dashboard.html',
                    authorizedRoles:["customer", "supplier", "broker"]
                });
            //$locationProvider.html5Mode(true);
            $urlRouterProvider.otherwise('/default');
            // this is how the internet told me to dynamically add modules :/
            register_fns.controller = $controllerProvider.register;
            register_fns.directive = $compileProvider.directive;
            register_fns.factory = $provide.factory;
            register_fns.service = $provide.service;
            register_fns.filter = $filterProvider.register;

            /**
             * Basic setup facebook sdk
             */
        }]);
    app.config(function (ezfbProvider) {
        /**
         * Basic setup
         *
         * https://github.com/pc035860/angular-easyfb#configuration
         */
        ezfbProvider.setInitParams({
            appId:'569630849807101'
        });
    });

    var apps_deps = ['mgcrea.ngStrap', 'ngSanitize', 'ngCookies', 'pasvaz.bindonce', 'BidProject-App'];

    _.each('controllers directives factories services filters'.split(' '), function (type) {
        var module_name = 'bidproject.' + type;
        // create the module
        app.useModule(angular.module(module_name, []));
        // push it into the apps dependencies
        apps_deps.push(module_name);
    });

    app.panel_helpers = {
        partial:function (name) {
            return 'app/partials/' + name + '.html';
        }
    };

    // load the core components
    require(['controllers/all', 'directives/all', 'factories/all', 'filters/all'], function () {
        // bootstrap the app
        angular.element(document).ready(function () {
            $('html').attr('ng-controller', 'DashCtrl');
            angular.bootstrap(document, apps_deps).invoke(['$rootScope',
                function ($rootScope) {
                    _.each(pre_boot_modules, function (module) {
                        _.extend(module, register_fns);
                    });
                    pre_boot_modules = false;

                    $rootScope.requireContext = appLevelRequire;

                    $rootScope.require = function (deps, fn) {
                        var $scope = this;
                        $scope.requireContext(deps, function () {
                            var deps = _.toArray(arguments);
                            // Check that this is a valid scope.
                            if ($scope.$id) {
                                $scope.$apply(function () {
                                    fn.apply($scope, deps);
                                });
                            }
                        });
                    };
                }]);
        });
    });

    return app;
});