/**
 * Created by ngocnh on 4/22/15.
 */
define([
    'angular',
    'require',
    'socket.io.client',
    'config',
], function (angular, _, io, config) {
    'use strict';
    var module = angular.module('bidproject.services');
    module.service('socketServices', ['$rootScope', 'Session', 'pushNotification', 'store', 'notification', 'AUTH_EVENTS',
        function ($rootScope, Session, pushNotification, store, notification, AUTH_EVENTS) {
            $rootScope.userWaiting = {};
            $rootScope.ready = true;
            $rootScope.online = 'online';
            $rootScope.busy = 'busy';
            $rootScope.away = 'away';

            if (Session.userRole == 'guest') {
                $rootScope.user = {user_id: 'Guest', session_id: new Date().getTime(), user_role: 'guest'};
            }

            $rootScope.messages = [];

            this.connect = function () {
                if ($rootScope.socket == undefined) {
                    $rootScope.socket = io.connect(config.socket);
                    $rootScope.socket.on('connect', function () {
                        console.log('connected');
                        $rootScope.socket.emit('join', {user: $rootScope.user});
                        pushNotification.register(function (registerId, type) {
                            $rootScope.socket.emit('pushRegister', {
                                register_id: registerId,
                                user: $rootScope.user,
                                type: type
                            });
                        });
                    });
                    $rootScope.socket.on('disconnect', function (data) {
                        if ($rootScope.user.user_role == 'supporter') {
                            if ($rootScope.user.chat[data.chat_id].messages != undefined) {
                                $rootScope.user.chat[data.chat_id].messages.push({
                                    chat_id: data.chat_id,
                                    sender: 'System',
                                    message: 'User ' + data.user_id + ' disconnected!'
                                });
                            }
                        }
                    });

                    // On user joined to services
                    $rootScope.socket.on('join', function (chat) {
                        if ($rootScope.user.user_role === 'supporter') {
                            if ($rootScope.user.chat[chat.chat_id] == undefined) {
                                $rootScope.user.chat[chat.chat_id] = {
                                    unread: 0,
                                    messages: []
                                };
                                if ($rootScope.chat_id == undefined || $rootScope.chat_id == '') {
                                    $rootScope.chat_id = chat.chat_id;
                                    $rootScope.user.chat[chat.chat_id].messages = chat.messages;
                                }
                            }
                        } else {
                            $rootScope.chat_id = chat.chat_id;
                            $rootScope.$apply(function () {
                                $rootScope.messages = chat.messages;
                            });
                        }
                    });
                    // Begin Chat Services

                    // On User or Support send a message
                    $rootScope.socket.on('chat', function (response) {
                        response.message = response.message.replace(/\r\n|\r|\n/g, "<br />");
                        $rootScope.$apply(function () {
                            if ($rootScope.user.user_role == 'supporter') {
                                if ($rootScope.user.chat[response.chat_id].messages == undefined) {
                                    $rootScope.user.chat[response.chat_id].messages = [];
                                }
                                $rootScope.user.chat[response.chat_id].messages.push(response);

                                if ($rootScope.chat_id != undefined && $rootScope.chat_id != response.chat_id) {
                                    $rootScope.user.chat[response.chat_id].unread += 1;
                                }
                            } else {
                                $rootScope.messages.push(response);
                                $rootScope.$broadcast("messages", $rootScope.messages);
                                if (response.sender.username != $rootScope.user.user_id) {
                                    if ($rootScope.chatBox.isShow == false || $rootScope.chatBox.isHidden == true) {
                                        $rootScope.chat_notifications.push({
                                            title: response.sender.username + ' đã gửi một tin nhắn',
                                            time: response.time
                                        });
                                    }
                                }
                            }
                        });
                    });

                    // On users changed
                    $rootScope.socket.on('updateUsers', function (chat) {
                        if ($rootScope.user.user_role == 'supporter') {
                            $rootScope.$apply(function () {
                                if ($rootScope.userWaiting[chat.user.user_id] == undefined) {
                                    chat.user.chat_id = chat.chat_id;
                                    $rootScope.userWaiting[chat.user.user_id] = chat.user;
                                } else {
                                    $rootScope.userWaiting[chat.user.user_id].status = chat.user.status;
                                }
                            });
                        }
                    });

                    // On Supporter change status
                    $rootScope.socket.on('changeStatus', function (status) {
                        $rootScope.$apply(function () {
                            $rootScope.online = status;
                        });
                    });

                    // End Chat Service

                    // Begin Notification Service
                    $rootScope.socket.on('notification', function (message) {

                        $rootScope.$apply(function () {
                            notification.push(message);
                        });
                    });

                    $rootScope.socket.on('notifications', function (response) {

                        response.forEach(function (item) {
                            $rootScope.$apply(function () {
                                notification.push(item);
                            });
                        });
                    });

                    $rootScope.socket.on('updateNotification', function (ids) {
                        notification.updateStatus(ids);
                    });
                    // End Notification Server
                }
            };
            $rootScope.$on(AUTH_EVENTS.loginSuccess, function() {
                $rootScope.user = {user_id: Session.userId, session_id: Session.id, user_role: Session.userRole, chat: {}};
                $rootScope.socket.emit('join', {user: $rootScope.user});
                pushNotification.register(function (registerId, type) {
                    $rootScope.socket.emit('pushRegister', {
                        register_id: registerId,
                        user: $rootScope.user,
                        type: type
                    });
                });
                $rootScope.socket.emit('login', $rootScope.user);
            });
            $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
                $rootScope.user = {user_id: 'Guest', session_id: new Date().getTime(), user_role: 'guest'};
                $rootScope.user.chat = false;
                $rootScope.chat_id = false;
                $rootScope.chatBox.isShow = false;
                $rootScope.messages = [];
                $rootScope.socket.emit('join', {user: $rootScope.user});
            });

            this.updateNotification = function (notification) {
                $rootScope.socket.emit('updateNotification', ids);
            };

            this.chatService = function () {
                this.connect();
            };

            this.sendMessage = function (message) {
                if (message != '') {
                    $rootScope.socket.emit('send', {
                        chat_id: $rootScope.chat_id,
                        sender: $rootScope.user.session_id,
                        message: message
                    });
                }
            };

            this.changeStatus = function (status) {
                $rootScope.socket.emit('changeStatus', status);
            };

            this.disconnect = function () {
                if ($rootScope.socket && $rootScope.socket.connected) {
                    $rootScope.socket.disconnect();
                    $rootScope.chat_id = undefined;
                }
            };

            this.pushRegister = function (data) {
                $rootScope.socket.emit('pushRegister', data);
            };

            this.findSupporter = function() {
                console.log($rootScope.user);
                $rootScope.socket.emit('join', {user: $rootScope.user});
            };
        }]);
});