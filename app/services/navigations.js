define(['angular', 'jquery', 'lodash', 'config', 'bidproject'], function (angular, $, _, config, bidproject) {
    'use strict';

    var module = angular.module('bidproject.services');

    module.service('navigations', ['$stateParams', '$http', '$rootScope', 'windowsize', '$location', '$timeout', 'alertSrv',
        function ($stateParams, $http, $rootScope, windowsize, $location, $timeout, alertSrv) {
            var navigation = {
                title: "",
                style: "light",
                hidden: false,
                backCallback: function (data) {
                },
                actionCallback: function (data) {
                },
                backLabel: "",
                actionLabel: "",
                path: "",
                level: "0",
                params: "",
                isParent: false,
                saveOnly: false
            };
            this.current = {};


            // Store a reference to this
            var self = this;
            var isBackClick = false;
            var collectionsLocal = JSON.parse(localStorage.getItem("navigation")) || [];

            if (collectionsLocal.length > 0) {
                this.collections = collectionsLocal
                //this.current = collectionsLocal[collectionsLocal.length -1]
            }
            else {
                this.collections = [];
            }

            //this.collections = [];
            $rootScope.$on('$stateChangeSuccess', function () {
                checkNavigation();
            });

            $rootScope.$on("window.size", function (object, type) {
                checkNavigation();
            });


            var checkNavigation = function (type) {

                if (windowsize.devicetype() === "Desktop" || self.collections.length == 0 || self.current.hidden) {
                    $rootScope.$broadcast("navigationHidden");
                } else {
                    $rootScope.$broadcast("navigationDisplay");
                }
            }
            var baseStatus = {
                backLabel: "",
                onBack: function (data) {

                    if (self.current && self.current.backCallback) {
                        self.current.backCallback();
                    }
                    var newIndex = self.collections.indexOf(self.current) - 1;
                    if (newIndex > -1) {
                        self.current = self.collections[newIndex];
                        self.collections.splice(newIndex + 1, 1);
                        angular.extend(self.current, baseStatus, data)

                        if (data) {
                            angular.extend(self.current, _.clone(data));
                        }

                        if (self.current.path && self.current.path != '#') {
                            console.log("self",self.collections[newIndex]);
                            if (self.collections[newIndex].params != "" && self.collections[newIndex].params != "undefined" && self.collections[newIndex].params != undefined) {
                                $location.path(self.current.path + "/" + self.collections[newIndex].params);
                            } else {
                                $location.path(self.current.path);
                            }

                        }
                    }
                    $rootScope.$broadcast("navigationChange");
                    checkNavigation();
                },
                onAction: function () {
                    var newIndex = self.collections.indexOf(self.current);
                    if (newIndex < self.collections.length) {
                        self.current = self.collections[newIndex];
                    }
                    if (navigation.actionCallback) {
                        self.current.actionCallback();
                    }

                    if (self.current.path && self.current.path != '#') {
                        if (self.collections[newIndex].params == undefined || self.collections[newIndex].params == "") {
                            $location.path(self.current.path);

                        } else {
                            $location.path(self.current.path + "/" + self.collections[newIndex].params);
                        }

                    }
                    $rootScope.$broadcast("navigationChange");
                    checkNavigation();
                }
            };

            this.push = function (data, options) {
                self.current = _.clone(navigation);
                angular.extend(self.current, data);
                if (options == undefined) options = baseStatus;
                if (navigation.path && navigation.path == "") {
                    return;
                }
                angular.extend(self.current, options);
                //if (self.collections[self.collections.length]||$stateParams.params == null || $stateParams.params == "" || $stateParams.params == undefined)
                //{
                //    self.current.params = "";
                //}else
                //{self.current.params = $stateParams.params}
                self.collections.push(self.current);
                localStorage.setItem("navigation", JSON.stringify(self.collections));
                if (!self.current.saveOnly) {
                    self.current.onAction();
                }

            }

            this.back = function (data,options) {
                if (jQuery.isEmptyObject(self.current))
                {
                    self.current = _.clone(navigation);
                    angular.extend(self.current, data);
                    if (options == undefined) options = baseStatus;
                    angular.extend(self.current, options);
                    self.collections[self.collections.length -1] = self.current
                }
                self.current.onBack(data);
            }

            this.pushAtIndex = function (index, data, options) {
                self.current = _.clone(navigation);
                angular.extend(self.current, data);
                if (navigation.path && navigation.path == '#') {
                    return;
                }
                if (options == undefined) options = baseStatus;
                angular.extend(self.current, options);
                self.collections[index] = self.current;
                self.collections.splice(index + 1, self.collections.length - index - 1);
                localStorage.setItem("navigation", JSON.stringify(self.collections));
                if (!self.current.saveOnly) {
                    self.current.onAction();
                }
            }

            if (this.collections.length == 0) {
                self.push({
                    title: "home page",
                    path: "default",
                    hidden: true
                }, undefined, true);
                }

        }]);

});
