define([
  'angular',
  'lodash',
  'config',
  'uriparser',
  'require',
  'app'
],
function (angular, _, config, uri) {
  'use strict';

  var module = angular.module('bidproject.services');

  module.service('store', [ function () {
      return {
          data:{},
          get: function (key) {
              if (this.data[key]) {
                  return this.data[key];
              }
              return false;
          },

          set: function (key, val) {
              if (val === undefined) {
                  delete this.data[key];
              } else {
                  this.data[key] = val;
              }
              return this.data[key];
          }
      }
  }])
});