define(['angular', 'jquery', 'lodash', 'config', 'bidproject'], function (angular, $, _, config, bidproject) {
	'use strict';

	var module = angular.module('bidproject.services');

	module.service('notification', ['$rootScope', 'Session', 'AUTH_EVENTS',
		function ($rootScope, Session, AUTH_EVENTS) {
			var localData = {
				status: true
			};
			var self = this;

			this.current = {};

			this.messages = [];
			this.news = {true: 0};

			function countNotification() {
				self.news = _.countBy(self.messages, function (message) {
					return !message.is_read;
				});
			};

			countNotification();

			this.push = function (data, options) {
				var notificationExist = false;
				for (var i = 0; i < self.messages.length; i++) {
					if (data.action == self.messages.action && data.data_id == self.messages.data_id) {
						notificationExist = true
						break;
					}
				}
				if (notificationExist) {
					return;
				}
				else {
                    self.messages.reverse()
					self.messages.push(data);
                    self.messages.reverse();
                    self.messages = _.sortBy(self.messages,function (item) {
                        return!item.is_read;
                    });
					self.current = data;
					$rootScope.$broadcast("notificationAdded");
					countNotification();
				}
			};

			this.updateStatus = function (data) {
				if (data.action === "deleted") {
					self.messages.forEach(function (item) {
						if (data.ids.indexOf(item._id) != -1) {
							item.is_read = true;
						}
					})
				}
				self.messages = _.sortBy(self.messages, function (item) {
					return item.is_read;
				});
				countNotification();

			}

			$rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
				self.messages = [];
			});
		}]);

});
