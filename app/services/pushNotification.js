/**
 * Created by ngocnh on 6/9/15.
 */
define([
    'angular',
    'require'
], function (angular, _) {
    'use strict';
    var module = angular.module('bidproject.services');
    module.service('pushNotification', ['$rootScope', '$cookieStore', 'notification',
        function ($rootScope, $cookieStore, notification) {
            this.register = function (registedCallback) {
                document.addEventListener('deviceready', onDeviceReady, false);
                function onDeviceReady() {
                    var pushNotification = window.plugins.pushNotification;
                    if (device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos") {
                        pushNotification.register(successHandler, errorHandler,
                            {
                                "senderID": "15078461882",
                                "ecb": "onNotificationGCM"
                            }
                        );
                    } else {
                        pushNotification.register(
                            tokenHandler,
                            errorHandler,
                            {
                                "badge": "true",
                                "sound": "true",
                                "alert": "true",
                                "ecb": "onNotificationAPN"
                            }
                        );
                    }
                }

                function successHandler(result) {
                }

                function errorHandler(error) {
                }

                function tokenHandler (token) {
                    if (token.length > 0) {
                        registedCallback(token, 'ios');
                    }
                }

                // Android and Amazon Fire OS
                window.onNotificationGCM = function (e) {
                    switch (e.event) {
                        case 'registered':
                            if (e.regid.length > 0) {
                                registedCallback(e.regid, 'android');
                            }
                            break;

                        case 'message':
                            if (e.foreground) {
                                var soundfile = e.soundname || e.payload.sound;
                                var my_media = new Media("/android_asset/www/" + soundfile);
                                my_media.play();
                            }
                            notification.push(e.data);
                            break;

                        case 'error':
                            break;

                        default:
                            break;
                    }
                };
                // iOS
                window.onNotificationAPN = function (event) {
                    console.log("notification event", event);
                    if (event.alert) {
                        navigator.notification.alert(event.alert);
                        notification.push(event.data);
                    }

                    if (event.sound) {
                        var snd = new Media(event.sound);
                        snd.play();
                    }

                    if (event.badge) {
                        window.plugins.pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
                    }
                };
            };
        }
    ]);
});