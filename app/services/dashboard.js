define(['angular', 'jquery', 'lodash', 'config', 'modernizr', 'moment', 'bidproject', 'uriparser'], function(angular, $, _, config, Modernizr, moment, bidproject, uri) {'use strict';

    var module = angular.module('bidproject.services');

    module.service('dashboard', ['$stateParams', '$http', '$rootScope', '$injector', '$location', '$timeout', '$q', 'alertSrv', 'navigations','windowsize',
        function($stateParams, $http, $rootScope, $injector, $location, $timeout, $q, alertSrv, navigations,windowsize) {
            var _dash = {
                title : "",
                style : "light",
                editable : true,
                failover : false,
                panel_hints : true,
                rows : [],
                services : {},
                refresh : false
            };

            var gist_pattern = /(^\d{5,}$)|(^[a-z0-9]{10,}$)|(gist.github.com(\/*.*)\/[a-z0-9]{5,}\/*$)/;

            // Store a reference to this
            var self = this;
            var filterSrv, querySrv;

            this.current = _.clone(_dash);
            this.last = {};
            this.availablePanels = [];

            $rootScope.$on('$stateChangeSuccess', function() {
                // Clear the current dashboard to prevent reloading
                self.current = {};
                route();
            });

            var route = function() {
                // Is there a dashboard type and id in the URL?
                if ((_.isUndefined($stateParams.routeType)) && !(_.isUndefined($stateParams.routeId))) {
                    var _type = $stateParams.routeType;
                    var _name = $stateParams.routeId;
                    var _param = $stateParams.params;
                    self.file_load(_name);
                    // No dashboard in the URL
                } else if (!(_.isUndefined($stateParams.routeType)) && !(_.isUndefined($stateParams.routeId))) {
                    var _type = $stateParams.routeType;
                    var _name = $stateParams.routeId;
                    var _param = $stateParams.params;
                    switch(_type) {
                        case ('file'):
                            self.file_load(_name);
                            break;
                        default:
                            $location.path(config.default_route);
                    }
                    // No dashboard in the URL
                } else {
                    // Check if browser supports localstorage, and if there's an old dashboard. If there is,
                    // inform the user that they should save their dashboard to Elasticsearch and then set that
                    // as their default
                    var params = $location.$$url.split("/");
                    if(params[1]){
                        var _name = params[1];
                        $stateParams.routeId = params[1];
                        self.file_load(_name);
                        if(params[2]){
                            $stateParams.params = params[2];
                        }
                    }else if (Modernizr.localstorage) {
                        if (!(_.isUndefined(window.localStorage['bidproject'])) && window.localStorage['bidproject'] !== '') {
                            $location.path(config.default_route);
                            alertSrv.set('Saving to browser storage has been replaced', ' with saving to Elasticsearch.' + ' Click <a href="#/dashboard/local/deprecated">here</a> to load your old dashboard anyway.');
                        } else if (!(_.isUndefined(window.localStorage.kibanaDashboardDefault))) {
                            $location.path(window.localStorage.kibanaDashboardDefault);
                        } else {
                            $location.path(config.default_route);
                        }
                        // No? Ok, grab the default route, its all we have now
                    } else {
                        $location.path(config.default_route);
                    }
                }
            };

            var dash_defaults = function(dashboard) {
                _.defaults(dashboard, _dash);
                return dashboard;
            };

            this.dash_load = function(dashboard) {
                // Make sure the dashboard being loaded has everything required
                dashboard = dash_defaults(dashboard);

                // Set the current dashboard
                self.current = _.clone(dashboard);

                // Set the available panels for the "Add Panel" drop down
                self.availablePanels = _.difference(config.panel_names, _.pluck(_.union({}), 'type'));

                // Take out any that we're not allowed to add from the gui.
                self.availablePanels = _.difference(self.availablePanels, config.hidden_panels);

                return true;
            };

            var renderTemplate = function(json, params) {
                var _r;
                _.templateSettings = {
                    interpolate : /\{\{(.+?)\}\}/g
                };
                var template = _.template(json);
                var rendered = template({
                    ARGS : params
                });
                try {
                    _r = angular.fromJson(rendered);
                } catch(e) {
                    _r = false;
                }
                return _r;
            };

            this.file_load = function(file) {
                var deferred = $q.defer();
                $rootScope.appBusy = deferred.promise;
                return $http({
                    url : "app/dashboards/" + file + '?' + new Date().getTime(),
                    method : "GET",
                    transformResponse : function(response) {
                        return renderTemplate(response, $stateParams);
                    }
                }).then(function(result) {
                    deferred.resolve();
                    if (!result) {
                        return false;
                    }
                    if(navigations.current.path != $stateParams.routeId)
                    {
                        if(!$stateParams.params){
                            if($stateParams.routeId != config.default_route){
                                if (result.data.level != undefined && result.data.level > 0) {
                                    if(navigations.collections[result.data.level] != undefined && navigations.collections[navigations.collections.length -1].title != result.data.title && navigations.collections[navigations.collections.length -1].path.split("/")[0] != $stateParams.routeId)
                                    {
                                        if (result.data.level >= 1 && !result.data.isLogin && result.data.hasChildrenInMobile) {
                                            navigations.pushAtIndex(result.data.level + (navigations.collections.length - result.data.level), {
                                                title: result.data.title,
                                                path: $stateParams.routeId,
                                                params: $stateParams.params
                                            });
                                        }
                                        else
                                        {
                                            navigations.pushAtIndex(result.data.level, {
                                                title: result.data.title,
                                                path: $stateParams.routeId,
                                                params: $stateParams.params
                                            });
                                        }
                                    }
                                    else
                                    {
                                        navigations.pushAtIndex(result.data.level, {
                                            title: result.data.title,
                                            path: $stateParams.routeId
                                        });
                                    }
                                }
                                else{
                                    navigations.pushAtIndex(0, {
                                        title: result.data.title,
                                        path: $stateParams.routeId,
                                        hidden: true
                                    });
                                }
                            }
                            else{
                                navigations.pushAtIndex(0, {
                                    title: result.data.title,
                                    path: $stateParams.routeId,
                                    hidden: true
                                });
                            }
                        }else{
                            if (result.data.level != undefined && result.data.level > 0)
                            {
                                    if (navigations.collections[result.data.level - 1] == undefined)
                                    {
                                        navigations.pushAtIndex(result.data.level - 1,{
                                            title: result.data.previoustitle,
                                            path: result.data.previouspath,
                                            saveOnly:true
                                        });
                                    }
                                    if(navigations.collections[result.data.level] != undefined && navigations.collections[navigations.collections.length -1].title != result.data.title && navigations.collections[navigations.collections.length -1].path.split("/")[0] != $stateParams.routeId)
                                    {

                                        if (result.data.level >= 1 && !result.data.isLogin && result.data.hasChildrenInMobile) {
                                            navigations.pushAtIndex(result.data.level + (navigations.collections.length - result.data.level), {
                                                title: result.data.title,
                                                path: $stateParams.routeId,
                                                params: $stateParams.params
                                            });
                                        }
                                        else
                                        {
                                            navigations.pushAtIndex(result.data.level, {
                                                title: result.data.title,
                                                path: $stateParams.routeId,
                                                params: $stateParams.params
                                            });
                                        }
                                    }
                                    else
                                    {
                                        navigations.pushAtIndex(result.data.level,{
                                            title: result.data.title,
                                            path: $stateParams.routeId,
                                            params:$stateParams.params
                                        });
                                    }
                                }
                        else{
                        navigations.pushAtIndex(0, {
                            title: result.data.title,
                            path: $stateParams.routeId,
                            hidden: true
                        });
                    }
                        }
                    }
                    $rootScope.title = result.data.title;
                    self.dash_load(dash_defaults(result.data));
                    return true;
                }, function() {
                    deferred.resolve();
                    alertSrv.set('Error', "Could not load <i>dashboards/" + file + "</i>. Please make sure it exists", 'error');
                    return false;
                });
            };

        }]);

});
