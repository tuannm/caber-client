define([
  'angular',
  'lodash',
  'config',
  'uriparser',
  'require',
  'app'
],
function (angular, _, config, uri) {
  'use strict';

  var module = angular.module('bidproject.services');

  module.service('api', ['$http', '$rootScope', '$q', 'Session', '$indexedDB', 'logger', 'localize', function ($http, $rootScope, $q, Session, $indexedDB, logger, localize) {

        function checkNetwork(){
            if(!$rootScope.online){
                logger.logError(localize.getLocalizedString('network disconnect'));
                return false;
            }
            return true;
        }

        this.get = function (resource, isLoading, indexes) {
            if(!$rootScope.online){
                indexes = true;
            }
            var objStore = $indexedDB.objectStore('vaberIndexed');
            if (isLoading === undefined) {
                isLoading = true;
            }
            var deferred = $q.defer();
            if (isLoading) {
                $rootScope.appBusy = deferred.promise;
            }
            var url = uri.get(config.api, resource);
            if(Session.accessToken){
                url = uri.add("accessToken", Session.accessToken, url);
            }

            var req = {
                method: 'GET',
                url: url,
                headers: {
                    "Authorization": 'Basic ' + config.secret,
                    "Content-Type": 'application/json;charset=UTF-8',
                    "Accept": 'application/json;charset=UTF-8'
                },
                dataType: "json",
                crossDomain : true

            };

            var httpRequest = function() {
                return $http(req).success(function (response) {
                    deferred.resolve();
                    objStore.insert({"uri": resource, "data": response}).then(function (e) {
                    });
                    return response;
                }).error(function (res) {
                    deferred.reject();
                    if(res != null && res != undefined){
                        logger.logError(res.error.description);
                    }
                    return res;
                });
            }
            if (indexes == undefined || !indexes) {
                return httpRequest();
            } else {
                return objStore.find(resource).then(function(row) {
                    deferred.resolve();
                    if (row) {
                        return row;
                    } else {
                        return httpRequest();
                    }
                });
            }
        };

        this.post = function (resource, data, isLoading) {

            if (isLoading === undefined) {
                isLoading = true;
            }
            var deferred = $q.defer();
            if (isLoading) {
                $rootScope.appBusy = deferred.promise;
            }
            if(!checkNetwork()){
                return deferred.resolve({data:{result: false}})
            }
            var url = uri.get(config.api, resource);
            if(Session.accessToken){
                url = uri.add("accessToken", Session.accessToken, url);
            }
            var req = {
                method: 'POST',
                url: url,
                headers: {
                    "Authorization": 'Basic ' + config.secret,
                    "Content-Type": 'application/json;charset=UTF-8',
                    "Accept": 'application/json;charset=UTF-8'
                },
                dataType: "json",
                crossDomain : true,
                data: data
            };
            return $http(req).success(function (response) {
                deferred.resolve();
                return response;
            }).error(function(res){
                deferred.reject();
                if(res != null && res != undefined){
                    logger.logError(res.error.description);
                }
                return res;
            });
        };
      this.put = function (resource, data, isLoading) {
          if (isLoading === undefined) {
              isLoading = true;
          }
          var deferred = $q.defer();
          if (isLoading) {
              $rootScope.appBusy = deferred.promise;
          }
          if(!checkNetwork()){
              return deferred.resolve({data:{result: false}})
          }
          var url = uri.get(config.api, resource);
          if(Session.accessToken){
              url = uri.add("accessToken", Session.accessToken, url);
          }
          var req = {
              method: 'PUT',
              url: url,
              headers: {
                  "Authorization": 'Basic ' + config.secret,
                  "Content-Type": 'application/json;charset=UTF-8',
                  "Accept": 'application/json;charset=UTF-8'
              },
              dataType: "json",
              crossDomain : true,
              data: data
          };
          return $http(req).success(function (response) {
              deferred.resolve();
              return response;
          }).error(function(res){
              deferred.reject();
              if(res != null && res != undefined){
                  logger.logError(res.error.description);
              }
              return res;
          });
      };
      this.delete = function (resource, isLoading) {
          console.log("Session", Session);
          if (isLoading === undefined) {
              isLoading = true;
          }
          var deferred = $q.defer();
          if (isLoading) {
              $rootScope.appBusy = deferred.promise;
          }
          if(!checkNetwork()){
              return deferred.resolve({data:{result: false}})
          }
          var url = uri.get(config.api, resource);
          if(Session.accessToken){
              url = uri.add("accessToken", Session.accessToken, url);
          }
          var req = {
              method: 'DELETE',
              url: url,
              headers: {
                  "Authorization": 'Basic ' + config.secret,
                  "Content-Type": 'application/json;charset=UTF-8',
                  "Accept": 'application/json;charset=UTF-8'
              },
              dataType: "json",
              crossDomain : true
          };
          return $http(req).success(function (response) {
              deferred.resolve();
              return response;
          }).error(function(res){
              deferred.reject();
              if(res != null && res != undefined){
                  logger.logError(res.error.description);
              }
              return res;
          });
      };
    }]);
});