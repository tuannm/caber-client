define([
  './alertSrv',
  './windowsize',
  './navigations',
  './notification',
  './dashboard',
  './db',
  './api',
  './base64',
  './store',
  './socketServices',
  './pushNotification'
],
function () {});