define([
  'angular',
  'lodash',
  'config',
  'uriparser',
  'require',
  'app'
],
function (angular, _, config, uri) {
  'use strict';

  var module = angular.module('bidproject.services');

  module.service('db', ['$http', '$rootScope', '$q', function ($http, $rootScope, $q) {
      this.get = function (resource, query, limit, skip, total, isLoading) {
        if(isLoading === undefined){
            isLoading = true;
        }
        var deferred = $q.defer();
        if(isLoading){
            $rootScope.appBusy = deferred.promise; 
        }
        return $http.get(uri.get(config.dbpath, resource)).success(function (response) {
            deferred.resolve();
            return response;
          
        });
      };
    }]);
});