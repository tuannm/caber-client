/**
 * @preserve AngularJS PDF viewer directive using pdf.js.
 *
 * https://github.com/akrennmair/ng-pdfviewer 
 *
 * MIT license
 */
define([
    'angular',
    'app'
],
function(angular, app) {
    'use strict';

    var module = angular.module('bidproject.services');

    module.service('windowsize', ['$rootScope', '$window', function($rootScope,$window) {
            var w = angular.element($window);
            var currentType;
            this.devicetype = function(){
                if(this.getWindowDimensions().w < 768){
                    currentType = "SmartDevice";
                }else{
                    currentType = "Desktop";
                };
                return currentType;
            };
            this.getWindowDimensions = function() {
                return {
                    'h': w.height(),
                    'w': w.width()
                };
            };
            $rootScope.$watch(this.getWindowDimensions, function(newValue, oldValue) {
                checkDevice(newValue);
            }, true);

            w.bind('resize', function() {
                $rootScope.$apply();
            });
            
            function checkDevice(size){
                var type;
                if (size.w < 768) {
                    type = "SmartDevice";
                } else {
                    type = "Desktop";
                }
                if(currentType != type){
                    currentType = type;
                    $rootScope.$broadcast('window.size', type);
                }
                $rootScope.$broadcast('window.width', size.w);
            }
        }]);
});